'use strict';
module.exports = (sequelize, DataTypes) => {
  const ChangeLog = sequelize.define('ChangeLog', {
    module: DataTypes.STRING,
    relatedId: DataTypes.INTEGER,
    type: DataTypes.STRING,
    description: DataTypes.STRING,
    previousData: DataTypes.TEXT,
    nextData: DataTypes.TEXT
  }, {});
  ChangeLog.associate = function(models) {
    // associations can be defined here
  };
  return ChangeLog;
};