'use strict';
module.exports = (sequelize, DataTypes) => {
  const Restaurant = sequelize.define('Restaurant', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    schedule: DataTypes.STRING,
  }, {});
  Restaurant.associate = function (models) {
    // associations can be defined here
  };
  return Restaurant;
};