cd /home/ubuntu/cek-toko-back-end
git stash
git pull origin develop
export NVM_DIR="$HOME/.nvm"
. ${NVM_DIR}/nvm.sh
nvm use 12
cp .env.example .env
npm install --only=prod
# npm run build
pm2 start ecosystem.config.js