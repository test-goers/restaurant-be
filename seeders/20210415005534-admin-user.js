'use strict';
require('dotenv').config();
const bcrypt = require('bcrypt');
module.exports = {
  up: async (queryInterface, _) => {
    return queryInterface.bulkInsert('Users', [
      {
        username: 'admin',
        fullname: 'Admin',
        password: await bcrypt.hash('admin123', 10),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },
  down: (queryInterface, _) => {
    return queryInterface.bulkDelete('Users', null, { truncate: true });
  },
};

