### Panduan Menggunakan RBAC

1. Pada Group Admin ada beberapa menu, Menu(Isinya CRUD MENU), User, Role (CRUD), dan Permission (Isinya CRUD Permission)
2. Saat membuat menu akan secara default membuat beberapa permission dengan MenuId, dan name permission Create, Update, Delete, Index
3. Setiap user dapat memimiliki beberapa role
4. Tiap Role memiliki permission (bisa ditambahkan di menu role->permission)
5. User dapat melihat list menu di sidebar harus memiliki role yang memiliki permission index pada sebuah menu
6. untuk memunculkan dan menghilangkan tombol create update delete saat  membuat view kita perlu membuat kondisi(contoh: if(can.create) isi object can adalah permission2 yang ada pada sebuah menu), di semua halaman seharusnya sudah dapat menggunakan variabel can. Untuk contoh penggunaan bisa dilihat di semua menu admin