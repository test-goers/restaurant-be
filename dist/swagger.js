// import pegawai from './modules/master/pegawai/pegawai.doc.json';
// import peraturan from './modules/master/peraturan/peraturan.doc.json';
// import riwayatMutasi from './modules/master/riwayat/riwayat.doc.json';
// import notifikasi from './modules/notifikasi/notifikasi.doc.json';
// import riwayatPayroll from './modules/payroll/riwayat/riwayat.doc.json';
// import perubahanDataPegawai from './modules/pengajuan/perubahanDataPegawai/perubahanDataPegawai.doc';
// import pendidikan from './modules/referensi/Institusi/institusi.doc.json';
// import SilDoc from './modules/referensi/suratIjinLayanan/suratIjinLayanan.doc';
// import diklat from './modules/rumahDiklat/diklat/diklat.doc.json';
// const apiDocs = {
//   openapi: '3.0.0',
//   info: {
//     version: '1.0.0',
//     title: 'HRIS API DOCUMENTATION',
//     description: '### Aplikasi manajemen HR',
//   },
//   servers: [
//     {
//       url: '',
//       description: 'This Host',
//     },
//     {
//       url: 'https://hris.dev.azuralabs.id/api',
//       description: 'Development',
//     },
//   ],
//   tags: [
//     {
//       name: 'Autentikasi',
//       description: 'Auth',
//     },
//     {
//       name: 'Pegawai',
//       description: 'data Pegawai',
//     },
//     {
//       name: 'Universitas',
//       description: 'Data Universitas',
//     },
//     {
//       name: 'Wilayah',
//       description: 'Data Wilayah',
//     },
//     {
//       name: 'Referensi',
//       description: 'Data Referensi',
//     },
//     {
//       name: 'Pengajuan',
//       description: 'Pengajuan',
//     },
//     {
//       name: 'Payroll',
//       description: 'payroll',
//     },
//     {
//       name: 'Diklat',
//       description: 'diklat',
//     },
//     {
//       name: 'Riwayat Mutasi',
//       description: 'riwayat_mutasi',
//     },
//     {
//       name: 'Notifikasi',
//       description: 'notifikasi',
//     },
//   ],
//   paths: {
//     '/auth/login/': {
//       post: {
//         tags: ['Autentikasi'],
//         description: 'LOGIN',
//         requestBody: {
//           required: true,
//           content: {
//             'application/json': {
//               schema: {
//                 type: 'object',
//                 properties: {
//                   nip: {
//                     type: 'string',
//                   },
//                   password: {
//                     type: 'string',
//                   },
//                 },
//               },
//             },
//           },
//         },
//         responses: {
//           '200': {
//             description: 'tes',
//           },
//         },
//       },
//     },
//     '/auth/forgot_password/': {
//       post: {
//         tags: ['Autentikasi'],
//         description: 'LOGIN',
//         requestBody: {
//           required: true,
//           content: {
//             'application/json': {
//               schema: {
//                 type: 'object',
//                 properties: {
//                   nip: {
//                     type: 'string',
//                   },
//                 },
//               },
//             },
//           },
//         },
//         responses: {
//           '200': {
//             description: 'tes',
//           },
//         },
//       },
//     },
//     '/auth/reset_password/': {
//       post: {
//         tags: ['Autentikasi'],
//         description: 'LOGIN',
//         parameters: [
//           {
//             'name': 'auth',
//             'in': 'header',
//             'required': true,
//           },
//         ],
//         requestBody: {
//           required: true,
//           content: {
//             'application/json': {
//               schema: {
//                 type: 'object',
//                 properties: {
//                   password: {
//                     type: 'string',
//                   },
//                 },
//               },
//             },
//           },
//         },
//         responses: {
//           '200': {
//             description: 'tes',
//           },
//         },
//       },
//     },
//     ...pegawai,
//     ...pendidikan,
//     '/referensi/provinsi': {
//       get: {
//         tags: ['Wilayah'],
//         description: 'Ambil data Provinsi',
//         parameters: [
//           {
//             name: 'name',
//             in: 'query',
//           },
//         ],
//         responses: {
//           '200': {
//             description: 'tes',
//           },
//         },
//       },
//     },
//     '/referensi/kota': {
//       get: {
//         tags: ['Wilayah'],
//         description: 'Ambil data kota',
//         parameters: [
//           {
//             name: 'provinsiId',
//             in: 'query',
//           },
//           {
//             name: 'name',
//             in: 'query',
//           },
//         ],
//         responses: {
//           '200': {
//             description: 'tes',
//           },
//         },
//       },
//     },
//     '/referensi/kecamatan': {
//       get: {
//         tags: ['Wilayah'],
//         description: 'Ambil data kecamatan',
//         parameters: [
//           {
//             name: 'kotaId',
//             in: 'query',
//           },
//           {
//             name: 'name',
//             in: 'query',
//           },
//         ],
//         responses: {
//           '200': {
//             description: 'tes',
//           },
//         },
//       },
//     },
//     '/referensi/kelurahan': {
//       get: {
//         tags: ['Wilayah'],
//         description: 'Ambil data kelurahan',
//         parameters: [
//           {
//             name: 'kecamatanId',
//             in: 'query',
//           },
//           {
//             name: 'name',
//             in: 'query',
//           },
//         ],
//         responses: {
//           '200': {
//             description: 'tes',
//           },
//         },
//       },
//     },
//     '/referensi/statusPerkawinan': {
//       get: {
//         tags: ['Referensi'],
//         description: 'Ambil data status perkawinan',
//         responses: {
//           '200': {
//             description: 'tes',
//           },
//         },
//       },
//     },
//     '/referensi/bank': {
//       get: {
//         tags: ['Referensi'],
//         description: 'Ambil data status bank',
//         responses: {
//           '200': {
//             description: 'tes',
//           },
//         },
//       },
//     },
//     ...perubahanDataPegawai,
//     ...riwayatPayroll,
//     ...diklat,
//     ...peraturan,
//     ...SilDoc,
//     ...riwayatMutasi,
//     ...notifikasi,
//   },
// };
// export default apiDocs;
//# sourceMappingURL=swagger.js.map