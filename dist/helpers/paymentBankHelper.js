"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generatePaymentBank = void 0;
const axios_1 = __importDefault(require("axios"));
const paymentHelper_1 = require("./paymentHelper");
const generatePaymentBank = (externalId = '', amount = 0, description = '', merchant = '') => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const payload = {
            'external_id': externalId,
            amount,
            payer_email: 'customer@cektoko.com',
            description,
        };
        const data = yield axios_1.default.post(process.env.XENDIT_BANK_INVOICE_ENDPOINT, payload, paymentHelper_1.authPayment)
            .then((response) => response.data)
            .catch((err) => err.message);
        let va = {};
        if (data.available_banks.length > 0) {
            [va] = data.available_banks.filter((v) => v.bank_code === merchant);
            va.id = data.id;
            va.status = data.status;
        }
        console.log(va);
        return Object.assign({ expiry_date: data.expiry_date }, va);
    }
    catch (error) {
        return error;
    }
});
exports.generatePaymentBank = generatePaymentBank;
//# sourceMappingURL=paymentBankHelper.js.map