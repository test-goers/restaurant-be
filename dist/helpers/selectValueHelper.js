"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.categoriesFilter = exports.typeDiscountValue = exports.categoriesValue = void 0;
const categoriesValue = [
    { 'value': 'SEMUA', 'text': 'Semua Layanan' },
    { 'value': 'CEKTOKO', 'text': 'Cektoko' },
    { 'value': 'CEKPAY', 'text': 'Cekpay' },
];
exports.categoriesValue = categoriesValue;
const typeDiscountValue = [
    { 'value': 'AMOUNT', 'text': 'Amount' },
    { 'value': 'PERCENT', 'text': 'Percent' },
];
exports.typeDiscountValue = typeDiscountValue;
const categoriesFilter = [
    { 'value': 'ALL', 'text': 'All' },
    { 'value': 'SEMUA', 'text': 'Semua Layanan' },
    { 'value': 'CEKTOKO', 'text': 'Cektoko' },
    { 'value': 'CEKPAY', 'text': 'Cekpay' },
];
exports.categoriesFilter = categoriesFilter;
//# sourceMappingURL=selectValueHelper.js.map