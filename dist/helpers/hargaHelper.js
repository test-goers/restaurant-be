"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateHargaAwal = exports.pembulatanHargaAdminPayment = exports.pembulatanHargaByKodeProduk = exports.pembulatanHarga = void 0;
const lodash_1 = require("lodash");
const pembulatanHarga = (harga) => (harga >= 10000) ? lodash_1.ceil(harga / 10000) * 10000 : lodash_1.ceil(harga / 1000) * 1000;
exports.pembulatanHarga = pembulatanHarga;
const pembulatanHargaByKodeProduk = (kodeProduk) => parseInt(kodeProduk.match(/\d+/)[0] + '000', 0);
exports.pembulatanHargaByKodeProduk = pembulatanHargaByKodeProduk;
const pembulatanHargaAdminPayment = (costType, cost, harga) => (costType === 'fixed') ? cost : Math.ceil((cost / 100) * harga);
exports.pembulatanHargaAdminPayment = pembulatanHargaAdminPayment;
const generateHargaAwal = (harga = 0, admin = 0, margin = 0, point = 0) => harga + admin + margin - point;
exports.generateHargaAwal = generateHargaAwal;
//# sourceMappingURL=hargaHelper.js.map