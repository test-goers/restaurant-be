"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateCodeVoucher = void 0;
const voucher_repository_1 = require("../modules/master/voucher/voucher.repository");
const generateCodeVoucher = (maxLength) => __awaiter(void 0, void 0, void 0, function* () {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const charactersLength = characters.length;
    for (let index = 0; index < maxLength; index++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    const check = yield voucher_repository_1.VoucherRepository.get({ code: result });
    if (check) {
        result = (yield generateCodeVoucher(6)).toString();
    }
    return result;
});
exports.generateCodeVoucher = generateCodeVoucher;
//# sourceMappingURL=codeVoucherHelper.js.map