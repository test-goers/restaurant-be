"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.kodeProdukPascaBayarSeluler = void 0;
const kodeProdukPascaBayarSeluler = (provider) => {
    const kodeProduk = {
        'TELKOMSEL': 'HPTSELH',
        'AXIS / XL': 'HPXL',
        'KARTU3': 'HPTHREE',
        'ISAT': 'HPMTRIX',
        'SMART': 'HPSMART',
        'FREN': 'HPFREN',
    };
    return (kodeProduk[provider]) ? kodeProduk[provider] : false;
};
exports.kodeProdukPascaBayarSeluler = kodeProdukPascaBayarSeluler;
//# sourceMappingURL=pascaBayarHelper.js.map