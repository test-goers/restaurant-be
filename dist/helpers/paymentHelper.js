"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateCallback = exports.closeInvoice = exports.checkPaymentStatus = exports.generateInvoice = exports.generatePayment = exports.authPayment = void 0;
const axios_1 = __importDefault(require("axios"));
const paymentBankHelper_1 = require("./paymentBankHelper");
const paymentEwallettHelper_1 = require("./paymentEwallettHelper");
const authPayment = {
    auth: {
        username: process.env.PAYMENT_TOKEN,
        password: '',
    },
};
exports.authPayment = authPayment;
const generatePayment = (callback, nomorTransaksi, harga, channelCode) => __awaiter(void 0, void 0, void 0, function* () {
    const eWallet = yield paymentEwallettHelper_1.generatePaymentEwallet(callback, nomorTransaksi, harga, channelCode);
    return {
        id: eWallet.id,
        actions: eWallet.actions,
        status: eWallet.status,
    };
});
exports.generatePayment = generatePayment;
const generateInvoice = (externalId, nominal, description, merchant) => __awaiter(void 0, void 0, void 0, function* () {
    const bank = yield paymentBankHelper_1.generatePaymentBank(externalId, nominal, description, merchant);
    return bank;
});
exports.generateInvoice = generateInvoice;
const checkPaymentStatus = (paymentId, metodePembayaran) => {
    if (metodePembayaran === 'EWALLET') {
        const paymentCheck = axios_1.default.get(`${process.env.EWALLET_PAYMENT_ENDPOINT}/${paymentId}`, authPayment)
            .then((item) => item.data.status)
            .catch((err) => err.message);
        return paymentCheck;
    }
    else {
        const paymentCheck = axios_1.default.get(`${process.env.XENDIT_BANK_INVOICE_ENDPOINT}/${paymentId}`, authPayment)
            .then((item) => item.data.status)
            .catch((err) => err.message);
        return paymentCheck;
    }
};
exports.checkPaymentStatus = checkPaymentStatus;
const closeInvoice = (paymentId) => {
    const paymentCheck = axios_1.default.post(`https://api.xendit.co/invoices/${paymentId}/expire!`, {}, authPayment)
        .then((item) => item.data)
        .catch((err) => `${err.message}`);
    return paymentCheck;
};
exports.closeInvoice = closeInvoice;
const generateCallback = (req) => `http://${req.headers.host}${req.baseUrl}\/proses`;
exports.generateCallback = generateCallback;
//# sourceMappingURL=paymentHelper.js.map