"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.kodeProdukSelulerPascabayar = exports.cekInputTelkom = exports.cekPayProduk = exports.pulsaResponseParse = exports.cekPayMethod = void 0;
const cekPayMethod = {
    saldo: {
        balance: 'rajabiller.balance',
    },
    pulsa: {
        checkGroupPrices: 'rajabiller.cekharga_gp',
        transactionBuy: 'rajabiller.pulsa',
    },
    pulsaPostPaid: {
        inqueryTransaction: 'rajabiller.inq',
        payTransaction: 'rajabiller.pay',
    },
    telkom: {
        inqueryTransaction: 'rajabiller.inq',
        payTransaction: 'rajabiller.pay',
    },
    pln: {
        inquiryTransaction: 'rajabiller.inq',
        payTransaction: 'rajabiller.paydetail',
    },
};
exports.cekPayMethod = cekPayMethod;
const pulsaResponseParse = (provider, response) => response.split('.')[0] === `Operator pulsa ${provider} tidak ditemukan` ? 'data tidak ditemukan' : response;
exports.pulsaResponseParse = pulsaResponseParse;
const cekPayProduk = {
    telkom: {
        telepon: 'TELEPON',
        speedy: 'SPEEDY',
    },
};
exports.cekPayProduk = cekPayProduk;
const kodeProdukSelulerPascabayar = (provider) => {
    switch (provider) {
        case 'TELKOMSEL': {
            return 'HPTSELH';
        }
        case 'AXIS / XL': {
            return 'HPXL';
        }
        case 'KARTU3': {
            return 'HPTHREE';
        }
        case 'ISAT': {
            return 'HPMTRIX';
        }
        case 'FREN': {
            return 'HPFREN';
        }
        case 'SMART': {
            return 'HPSMART';
        }
        default: {
            return false;
        }
    }
};
exports.kodeProdukSelulerPascabayar = kodeProdukSelulerPascabayar;
const cekInputTelkom = (pelangganId) => (pelangganId.charAt(0) === '0') ? 'TELEPON' : 'SPEEDY';
exports.cekInputTelkom = cekInputTelkom;
//# sourceMappingURL=cekPayMethod.js.map