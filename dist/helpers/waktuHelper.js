"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reformatWaktu = void 0;
const moment_1 = __importDefault(require("moment"));
const reformatWaktu = (waktu) => moment_1.default(waktu, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss');
exports.reformatWaktu = reformatWaktu;
//# sourceMappingURL=waktuHelper.js.map