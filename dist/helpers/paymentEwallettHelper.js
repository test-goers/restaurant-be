"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generatePaymentEwallet = void 0;
const axios_1 = __importDefault(require("axios"));
const paymentHelper_1 = require("./paymentHelper");
const generatePaymentEwallet = (callback, nomorTransaksi, harga, channelCode) => __awaiter(void 0, void 0, void 0, function* () {
    const paymentData = {
        'reference_id': nomorTransaksi,
        'currency': 'IDR',
        'amount': harga,
        'checkout_method': 'ONE_TIME_PAYMENT',
        'channel_code': channelCode,
        'channel_properties': {
            'success_redirect_url': `${callback}?invoiceNumber=${nomorTransaksi}`,
        },
        'metadata': {
            'branch_area': 'PLUIT',
            'branch_city': 'JAKARTA',
        },
    };
    const paymentProcess = yield axios_1.default.post(process.env.EWALLET_PAYMENT_ENDPOINT, paymentData, paymentHelper_1.authPayment)
        .then((item) => item.data)
        .catch((err) => err.message);
    return paymentProcess;
});
exports.generatePaymentEwallet = generatePaymentEwallet;
//# sourceMappingURL=paymentEwallettHelper.js.map