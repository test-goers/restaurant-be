"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkRedeemtion = exports.checkVoucher = void 0;
const moment_1 = __importDefault(require("moment"));
const redemption_repository_1 = require("../modules/master/redemption/redemption.repository");
const checkVoucher = (voucher, layanan) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (!voucher) {
            return {
                status: false,
                message: 'Tidak ada voucher untuk saat ini',
            };
        }
        if (voucher.qty <= 0) {
            return {
                status: false,
                message: 'Promo voucher sudah tidak tersedia',
            };
        }
        if (voucher.expDate !== null && moment_1.default(voucher.expDate).format('YYYY-MM-DD') < moment_1.default().format('YYYY-MM-DD')) {
            return {
                status: false,
                message: 'Promo voucher sudah tidak berlaku',
            };
        }
        if (voucher.startDate !== null && moment_1.default(voucher.startDate).format('YYYY-MM-DD') > moment_1.default().format('YYYY-MM-DD')) {
            return {
                status: false,
                message: 'Promo voucher belum berlaku',
            };
        }
        if (voucher.category !== 'SEMUA' && layanan !== 'SEMUA' && voucher.category !== layanan) {
            return {
                status: false,
                message: 'Promo voucher tidak dapat digunakan di layanan ini',
            };
        }
        if (voucher.isActive === '0') {
            return {
                status: false,
                message: 'Promo voucher sudah tidak aktif',
            };
        }
        return {
            status: true,
            message: '',
        };
    }
    catch (error) {
        return { status: false, message: error };
    }
});
exports.checkVoucher = checkVoucher;
const checkRedeemtion = (userId, code, campaignId, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (campaignId === 0) {
            const checkPenggunaan = yield redemption_repository_1.RedemptionRepository.checkRedeem(String(userId), String(code));
            if (checkPenggunaan) {
                return {
                    status: false,
                    message: 'Voucher telah digunakan',
                };
            }
        }
        else {
            const checkPenggunaan = yield redemption_repository_1.RedemptionRepository.checkRedeemWithCampaign(String(userId), Number(campaignId));
            if (checkPenggunaan) {
                return {
                    status: false,
                    message: 'Voucher Campaign telah digunakan',
                };
            }
        }
        return { status: true, message: '' };
    }
    catch (error) {
        return { status: false, message: error };
    }
});
exports.checkRedeemtion = checkRedeemtion;
//# sourceMappingURL=checkVoucherHelper.js.map