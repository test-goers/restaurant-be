"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const compression_1 = __importDefault(require("compression"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const cors_1 = __importDefault(require("cors"));
const express_1 = __importDefault(require("express"));
const express_flash_1 = __importDefault(require("express-flash"));
const express_session_1 = __importDefault(require("express-session"));
const express_status_monitor_1 = __importDefault(require("express-status-monitor"));
const lusca_1 = __importDefault(require("lusca"));
const moment_1 = __importDefault(require("moment"));
require("moment/locale/id");
const morgan_1 = __importDefault(require("morgan"));
const numeral_1 = __importDefault(require("numeral"));
const passport_1 = __importDefault(require("passport"));
const path_1 = __importDefault(require("path"));
require("reflect-metadata");
const response_time_1 = __importDefault(require("response-time"));
const logger_1 = require("./helpers/logger");
const breadcrumb_middleware_1 = require("./modules/core/middlewares/breadcrumb.middleware");
const path_middleware_1 = require("./modules/core/middlewares/path.middleware");
moment_1.default().locale('id');
const middlewareLoader = (app) => {
    const MySQLStore = require('connect-mysql')(express_session_1.default);
    const options = {
        config: {
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME,
        },
    };
    app.use(express_status_monitor_1.default());
    app.use(compression_1.default());
    app.use(express_1.default.json());
    app.use(express_1.default.urlencoded({ extended: true }));
    app.use(cors_1.default());
    app.options('*', cors_1.default());
    const tx2 = require('tx2');
    const meter = tx2.meter({
        name: 'req/sec',
        samples: 1,
        timeframe: 60,
    });
    app.use(response_time_1.default((req, res, time) => {
        meter.mark();
    }));
    app.use(morgan_1.default('tiny', {
        stream: {
            write: message => {
                logger_1.logger.info(message);
            },
        },
    }));
    app.use(cookie_parser_1.default());
    app.use(express_session_1.default({
        cookie: {
            maxAge: 1000 * 60 * 60 * 24 * 3,
        },
        secret: process.env.SESSION_SECRET,
        resave: true,
        saveUninitialized: true,
        store: new MySQLStore(options),
    }));
    app.use(passport_1.default.initialize());
    app.use(passport_1.default.session());
    app.use(express_flash_1.default());
    app.use((req, res, next) => {
        // if (req.is('multipart/form-data')) {
        //   next();
        // }
        if (req.path.split('/')[1] === 'api' || req.is('multipart/form-data')) {
            next();
        }
        else {
            lusca_1.default.csrf()(req, res, next);
        }
    });
    app.use(lusca_1.default.xframe('SAMEORIGIN'));
    app.use(lusca_1.default.xssProtection(true));
    app.disable('x-powered-by');
    app.use((req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        res.locals.user = req.user;
        moment_1.default.locale('id');
        res.locals.moment = moment_1.default;
        res.locals.numeral = numeral_1.default();
        next();
    }));
    // Authentication
    app.post('/auth/login', passport_1.default.authenticate('local', {
        failureFlash: true,
        failureRedirect: '/auth/login',
        successRedirect: '/',
    }));
    app.use('/file', express_1.default.static(path_1.default.join(__dirname, '../file')));
    app.use(path_middleware_1.pathMiddleware);
    app.use(breadcrumb_middleware_1.breadcrumbMiddleware);
};
module.exports = middlewareLoader;
//# sourceMappingURL=middlewareLoader.js.map