"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiController = void 0;
const express_1 = require("express");
const api_middleware_1 = require("./modules/core/middlewares/api.middleware");
const routers_1 = __importDefault(require("./routers"));
const apiControllerList = express_1.Router();
apiControllerList.use(routers_1.default);
const apiController = express_1.Router();
exports.apiController = apiController;
apiController.use('/api', api_middleware_1.apiMiddleware, apiControllerList);
//# sourceMappingURL=apiControllerLoader.js.map