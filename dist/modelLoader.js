"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.modelLoader = void 0;
require("reflect-metadata");
const restaurant_model_1 = require("./modules/master/restaurant/restaurant.model");
const user_model_1 = require("./modules/master/user/user.model");
const modelLoader = (sequelize) => {
    sequelize.addModels([
        user_model_1.User,
        restaurant_model_1.Restaurant,
    ]);
};
exports.modelLoader = modelLoader;
//# sourceMappingURL=modelLoader.js.map