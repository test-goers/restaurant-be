"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = require("../modules/core/config/passport");
const auth_api_controller_1 = __importDefault(require("../modules/core/controllers/auth.api.controller"));
const file_middleware_1 = require("../modules/core/middlewares/file.middleware");
const health_service_1 = __importDefault(require("../modules/health.service"));
const restaurant_api_controller_1 = __importDefault(require("../modules/master/restaurant/restaurant.api.controller"));
const user_controller_1 = __importDefault(require("../modules/master/user/user.controller"));
function group(cb) {
    const route = express_1.Router();
    cb(route);
    return route;
}
const appRouter = express_1.Router();
appRouter.use('/auth', group((router, next) => {
    router.post('/login', auth_api_controller_1.default.login);
    router.post('/reset-password', auth_api_controller_1.default.resetPassword);
    router.get('/me', passport_1.apiAuthentication, auth_api_controller_1.default.me);
}));
appRouter.use('/health', group((router, next) => {
    router.get('/', health_service_1.default.index);
}));
appRouter.use('/user', passport_1.apiAuthentication, group((router, next) => {
    router.get('/', user_controller_1.default.index);
    router.get('/profile', user_controller_1.default.profile);
    router.patch('/profile', file_middleware_1.productImageMiddleware({ fields: [{ name: 'photo', maxCount: 1 }] }), user_controller_1.default.updateProfile);
}));
appRouter.use('/restaurant', group((router, next) => {
    router.get('/', restaurant_api_controller_1.default.getAll);
    router.get('/:id', restaurant_api_controller_1.default.getDetail);
}));
exports.default = appRouter;
//# sourceMappingURL=index.js.map