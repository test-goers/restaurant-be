"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseHandler_1 = require("./core/helpers/responseHandler");
class HealthService {
}
HealthService.index = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return responseHandler_1.ResponseHandler.jsonSuccess(res, 'OK');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res, error);
    }
});
exports.default = HealthService;
//# sourceMappingURL=health.service.js.map