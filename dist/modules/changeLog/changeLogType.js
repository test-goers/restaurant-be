"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChangeLogType = void 0;
class ChangeLogType {
}
exports.ChangeLogType = ChangeLogType;
ChangeLogType.CREATE = 'create';
ChangeLogType.UPDATE = 'update';
ChangeLogType.DELETE = 'delete';
//# sourceMappingURL=changeLogType.js.map