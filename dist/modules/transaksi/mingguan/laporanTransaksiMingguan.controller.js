"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const moment_1 = __importDefault(require("moment"));
const passport_1 = require("../../core/config/passport");
const asyncHandler_1 = require("../../core/helpers/asyncHandler");
const laporanTransaksiMingguan_repository_1 = require("./laporanTransaksiMingguan.repository");
const transaksiMingguan = express_1.Router();
transaksiMingguan.get('/mingguan', passport_1.isAuthenticated, asyncHandler_1.asyncHandler((req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.render('pages/master/transaksi/mingguan.pug');
})));
transaksiMingguan.get('/mingguan/json', passport_1.isAuthenticated, asyncHandler_1.asyncHandler((req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const search = (req.query.search) ? req.query.search : '';
    const limit = (req.query.limit) ? Number(req.query.limit) : 10;
    const offset = (req.query.offset) ? Number(req.query.offset) : 0;
    const order = (req.query.order) ? req.query.order : 'asc';
    const data = yield laporanTransaksiMingguan_repository_1.LaporanTransaksiMingguanRepository.dataTable(search, order, offset, limit);
    res.json({
        rows: data,
        total: data.length,
    });
})));
transaksiMingguan.get('/mingguan/export', passport_1.isAuthenticated, asyncHandler_1.asyncHandler((req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const search = (req.query.search) ? req.query.search : '';
    const limit = (req.query.limit) ? Number(req.query.limit) : 10;
    const offset = (req.query.offset) ? Number(req.query.offset) : 0;
    const order = (req.query.order) ? req.query.order : 'desc';
    const data = yield laporanTransaksiMingguan_repository_1.LaporanTransaksiMingguanRepository.dataTable(search, order, offset, limit);
    const result = data.map((value, index) => ({
        no: index + 1,
        week: value.get('week'),
        monthName: value.get('monthName'),
        yearDate: value.get('yearDate'),
        category: value.category,
        providerName: value.providerName,
        paymentStatus: value.paymentStatus,
        transactionStatus: value.transactionStatus,
        invoiceNumberTotal: Number(value.get('invoiceNumberTotal')),
        billAmountTotal: Number(value.get('billAmountTotal')),
        commisionTotal: Number(value.get('commisionTotal')),
    }));
    const workbook = yield laporanTransaksiMingguan_repository_1.LaporanTransaksiMingguanRepository.generateExcel(result);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', `attachment; filename=Data_Rekapitulasi_Mingguan.xlsx`);
    workbook.xlsx.write(res).then(() => {
        res.status(200).end();
    });
})));
transaksiMingguan.get('/mingguan/print', passport_1.isAuthenticated, asyncHandler_1.asyncHandler((req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const search = (req.query.search) ? req.query.search : '';
    const limit = (req.query.limit) ? Number(req.query.limit) : 10;
    const offset = (req.query.offset) ? Number(req.query.offset) : 0;
    const order = (req.query.order) ? req.query.order : 'desc';
    const data = yield laporanTransaksiMingguan_repository_1.LaporanTransaksiMingguanRepository.dataTable(search, order, offset, limit);
    const pdf = yield laporanTransaksiMingguan_repository_1.LaporanTransaksiMingguanRepository.generatePdf(data);
    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', `attachment; filename=Transaksi Mingguan per ${moment_1.default().format('DD-MMMM-YYYY')}.pdf`);
    res.send(pdf);
})));
exports.default = transaksiMingguan;
//# sourceMappingURL=laporanTransaksiMingguan.controller.js.map