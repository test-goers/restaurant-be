"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LaporanTransaksiMingguanRepository = void 0;
const exceljs_1 = require("exceljs");
const moment_1 = __importDefault(require("moment"));
const pug_1 = __importDefault(require("pug"));
const puppeteer_1 = __importDefault(require("puppeteer"));
const sequelize_1 = require("sequelize");
const cekPay_model_1 = __importDefault(require("../../master/cekpay/cekPay.model"));
class LaporanTransaksiMingguanRepository {
    static dataTable(search = '', order = 'asc', offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield cekPay_model_1.default.findAll({
                attributes: [
                    [sequelize_1.Sequelize.fn('WEEK', sequelize_1.Sequelize.col('date')), 'week'],
                    [sequelize_1.Sequelize.fn('MONTHNAME', sequelize_1.Sequelize.col('date')), 'monthName'],
                    [sequelize_1.Sequelize.fn('YEAR', sequelize_1.Sequelize.col('date')), 'yearDate'],
                    [sequelize_1.Sequelize.fn('COUNT', sequelize_1.Sequelize.col('invoiceNumber')), 'invoiceNumberTotal'],
                    [sequelize_1.Sequelize.fn('SUM', sequelize_1.Sequelize.col('billAmount')), 'billAmountTotal'],
                    [sequelize_1.Sequelize.fn('SUM', sequelize_1.Sequelize.col('commision')), 'commisionTotal'],
                    'category',
                    'providerName',
                    'paymentStatus',
                    'transactionStatus',
                ],
                where: {
                    [sequelize_1.Op.or]: [
                        sequelize_1.Sequelize.where(sequelize_1.Sequelize.fn('MONTHNAME', sequelize_1.Sequelize.col('date')), { [sequelize_1.Op.like]: `%${search}%` }),
                        sequelize_1.Sequelize.where(sequelize_1.Sequelize.fn('YEAR', sequelize_1.Sequelize.col('date')), { [sequelize_1.Op.like]: `%${search}%` }),
                        sequelize_1.Sequelize.where(sequelize_1.Sequelize.col('providerName'), { [sequelize_1.Op.like]: `%${search}%` }),
                        sequelize_1.Sequelize.where(sequelize_1.Sequelize.col('category'), { [sequelize_1.Op.like]: `%${search}%` }),
                        sequelize_1.Sequelize.where(sequelize_1.Sequelize.col('paymentStatus'), { [sequelize_1.Op.like]: `%${search}%` }),
                        sequelize_1.Sequelize.where(sequelize_1.Sequelize.col('transactionStatus'), { [sequelize_1.Op.like]: `%${search}%` }),
                    ],
                },
                order: [
                    ['date', order],
                ],
                group: [
                    sequelize_1.Sequelize.fn('YEAR', sequelize_1.Sequelize.col('date')),
                    sequelize_1.Sequelize.fn('MONTH', sequelize_1.Sequelize.col('date')),
                    sequelize_1.Sequelize.fn('MONTHNAME', sequelize_1.Sequelize.col('date')),
                    sequelize_1.Sequelize.fn('WEEK', sequelize_1.Sequelize.col('date')),
                    'category',
                    'providerName',
                    'paymentStatus',
                    'transactionStatus',
                ],
                offset,
                limit,
            });
            return data;
        });
    }
    static generateExcel(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const workbook = new exceljs_1.Workbook();
            const sheet = workbook.addWorksheet(`Laporan Transaksi Mingguan`);
            sheet.mergeCells('A1:J1');
            sheet.getCell('A1:J1').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('A1:J1').value = { richText: [{ font: { bold: true }, text: 'LAPORAN TRANSAKSI MINGGUAN' }] };
            sheet.getCell('A3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('B3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('C3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('D3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('E3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('F3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('G3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('H3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('I3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('J3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('K3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('A3').value = { richText: [{ font: { bold: true }, text: 'No' }] };
            sheet.getCell('B3').value = { richText: [{ font: { bold: true }, text: 'Minggu' }] };
            sheet.getCell('C3').value = { richText: [{ font: { bold: true }, text: 'Bulan' }] };
            sheet.getCell('D3').value = { richText: [{ font: { bold: true }, text: 'Tahun' }] };
            sheet.getCell('E3').value = { richText: [{ font: { bold: true }, text: 'Kategori' }] };
            sheet.getCell('F3').value = { richText: [{ font: { bold: true }, text: 'Provider' }] };
            sheet.getCell('G3').value = { richText: [{ font: { bold: true }, text: 'Status Pembayaran' }] };
            sheet.getCell('H3').value = { richText: [{ font: { bold: true }, text: 'Status Transaksi' }] };
            sheet.getCell('I3').value = { richText: [{ font: { bold: true }, text: 'Total Transaksi' }] };
            sheet.getCell('J3').value = { richText: [{ font: { bold: true }, text: 'Total Pembayaran' }] };
            sheet.getCell('K3').value = { richText: [{ font: { bold: true }, text: 'Total Komisi' }] };
            sheet.columns = [
                { key: 'no', width: 5 },
                { key: 'week', width: 10 },
                { key: 'monthName', width: 10 },
                { key: 'yearDate', width: 10 },
                { key: 'category', width: 15 },
                { key: 'providerName', width: 15 },
                { key: 'paymentStatus', width: 20 },
                { key: 'transactionStatus', width: 20 },
                { key: 'invoiceNumberTotal', width: 15 },
                { key: 'billAmountTotal', width: 20 },
                { key: 'commisionTotal', width: 20 },
            ];
            sheet.addRows(data);
            const lastRow = 3 + data.length + 1;
            let sumTransaksi = 0;
            let sumPembayaran = 0;
            let sumKomisi = 0;
            const totalPembayaranTransaksi = data.map((value, index) => ([
                sumTransaksi += Number(value.invoiceNumberTotal),
                sumPembayaran += Number(value.billAmountTotal),
                sumKomisi += Number(value.commisionTotal),
            ]));
            sheet.mergeCells(`A${lastRow}:H${lastRow}`);
            sheet.getCell(`A${lastRow}:H${lastRow}`).alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell(`I${lastRow}`).alignment = { vertical: 'middle', 'horizontal': 'right' };
            sheet.getCell(`J${lastRow}`).alignment = { vertical: 'middle', 'horizontal': 'right' };
            sheet.getCell(`K${lastRow}`).alignment = { vertical: 'middle', 'horizontal': 'right' };
            sheet.getCell(`A${lastRow}:H${lastRow}`).value = { richText: [{ font: { bold: true }, text: 'Total Pembayaran' }] };
            sheet.getCell(`I${lastRow}`).value = { richText: [{ font: { bold: true }, text: totalPembayaranTransaksi[data.length - 1][0] }] };
            sheet.getCell(`J${lastRow}`).value = { richText: [{ font: { bold: true }, text: totalPembayaranTransaksi[data.length - 1][1] }] };
            sheet.getCell(`K${lastRow}`).value = { richText: [{ font: { bold: true }, text: totalPembayaranTransaksi[data.length - 1][2] }] };
            return workbook;
        });
    }
    static generatePdf(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const periode = moment_1.default().format('DD-MM-YYYY');
            const template = pug_1.default.compileFile(`${__dirname}/../../../../views/pages/master/transaksi/printTransaksiMingguan.pug`);
            const content = template({
                data,
                periode,
            });
            const browser = yield puppeteer_1.default.launch({ headless: true });
            const page = yield browser.newPage();
            yield page.setContent(content);
            const pdf = yield page.pdf({ format: 'a4' });
            yield browser.close();
            return pdf;
        });
    }
}
exports.LaporanTransaksiMingguanRepository = LaporanTransaksiMingguanRepository;
//# sourceMappingURL=laporanTransaksiMingguan.repository.js.map