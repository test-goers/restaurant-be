"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LaporanTransaksiHarianRepository = void 0;
const exceljs_1 = require("exceljs");
const moment_1 = __importDefault(require("moment"));
const pug_1 = __importDefault(require("pug"));
const puppeteer_1 = __importDefault(require("puppeteer"));
const sequelize_1 = require("sequelize");
const cekPay_model_1 = __importDefault(require("../../master/cekpay/cekPay.model"));
class LaporanTransaksiHarianRepository {
    static dataTable(search = '', category = '', order = 'asc', offset = 0, limit = 10, start = '', end = '') {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield cekPay_model_1.default.findAndCountAll({
                attributes: ['id', 'invoiceNumber', 'date', 'billAmount', 'paymentMethod', 'category', 'providerName', 'commision', 'paymentStatus', 'transactionStatus'],
                where: {
                    [sequelize_1.Op.and]: {
                        [sequelize_1.Op.or]: {
                            invoiceNumber: { [sequelize_1.Op.like]: `%${search}%` },
                            date: { [sequelize_1.Op.like]: `%${search}%` },
                            billAmount: { [sequelize_1.Op.like]: `%${search}%` },
                            paymentMethod: { [sequelize_1.Op.like]: `%${search}%` },
                            category: { [sequelize_1.Op.like]: `%${search}%` },
                            providerName: { [sequelize_1.Op.like]: `%${search}%` },
                            paymentStatus: { [sequelize_1.Op.like]: `%${search}%` },
                            transactionStatus: { [sequelize_1.Op.like]: `%${search}%` },
                        },
                        date: ((start !== '' && start !== null) || (end !== '' && end !== null)) ? { [sequelize_1.Op.between]: [start, end] } : { [sequelize_1.Op.not]: null },
                        category: (category !== '' && category !== null) ? category : { [sequelize_1.Op.not]: null },
                    },
                },
                order: [
                    ['createdAt', order],
                ],
                offset: (offset !== '' && offset != null) ? offset : null,
                limit,
            });
            return data;
        });
    }
    static generateExcel(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const workbook = new exceljs_1.Workbook();
            const sheet = workbook.addWorksheet(`Laporan Transaksi Harian`);
            sheet.mergeCells('A1:J1');
            sheet.getCell('A1:J1').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('A1:J1').value = { richText: [{ font: { bold: true }, text: 'LAPORAN TRANSAKSI HARIAN' }] };
            sheet.getCell('A3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('B3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('C3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('D3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('E3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('F3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('G3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('H3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('I3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('J3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('A3').value = { richText: [{ font: { bold: true }, text: 'No' }] };
            sheet.getCell('B3').value = { richText: [{ font: { bold: true }, text: 'Tanggal Pembayaran' }] };
            sheet.getCell('C3').value = { richText: [{ font: { bold: true }, text: 'Nomor Invoice' }] };
            sheet.getCell('D3').value = { richText: [{ font: { bold: true }, text: 'Komisi' }] };
            sheet.getCell('E3').value = { richText: [{ font: { bold: true }, text: 'Kategori' }] };
            sheet.getCell('F3').value = { richText: [{ font: { bold: true }, text: 'Provider' }] };
            sheet.getCell('G3').value = { richText: [{ font: { bold: true }, text: 'Metode Pembayaran' }] };
            sheet.getCell('H3').value = { richText: [{ font: { bold: true }, text: 'Status Pembayaran' }] };
            sheet.getCell('I3').value = { richText: [{ font: { bold: true }, text: 'Status Transaksi' }] };
            sheet.getCell('J3').value = { richText: [{ font: { bold: true }, text: 'Total' }] };
            sheet.columns = [
                { key: 'no', width: 5 },
                { key: 'date', width: 20 },
                { key: 'invoiceNumber', width: 30 },
                { key: 'commision', width: 10 },
                { key: 'category', width: 20 },
                { key: 'providerName', width: 20 },
                { key: 'paymentMethod', width: 20 },
                { key: 'paymentStatus', width: 20 },
                { key: 'transactionStatus', width: 20 },
                { key: 'billAmount', width: 15 },
            ];
            sheet.addRows(data);
            const lastRow = 3 + data.length + 1;
            let sum = 0;
            const totalPembayaranTransaksi = data.map((value, index) => (sum += Number(value.billAmount)));
            sheet.mergeCells(`A${lastRow}:I${lastRow}`);
            sheet.getCell(`A${lastRow}:I${lastRow}`).alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell(`A${lastRow}:I${lastRow}`).value = { richText: [{ font: { bold: true }, text: 'Total Pembayaran' }] };
            sheet.getCell(`J${lastRow}`).value = { richText: [{ font: { bold: true }, text: totalPembayaranTransaksi[data.length - 1] }] };
            return workbook;
        });
    }
    static generatePdf(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const periode = moment_1.default().format('DD-MM-YYYY');
            const template = pug_1.default.compileFile(`${__dirname}/../../../../views/pages/master/transaksi/printTransaksiHarian.pug`);
            const content = template({
                data,
                periode,
            });
            const browser = yield puppeteer_1.default.launch({ headless: true });
            const page = yield browser.newPage();
            yield page.setContent(content);
            const pdf = yield page.pdf({ format: 'a4' });
            yield browser.close();
            return pdf;
        });
    }
}
exports.LaporanTransaksiHarianRepository = LaporanTransaksiHarianRepository;
//# sourceMappingURL=laporanTransaksiHarian.repository.js.map