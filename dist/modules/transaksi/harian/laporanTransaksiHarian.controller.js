"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const moment_1 = __importDefault(require("moment"));
const passport_1 = require("../../core/config/passport");
const asyncHandler_1 = require("../../core/helpers/asyncHandler");
const laporanTransaksiHarian_repository_1 = require("./laporanTransaksiHarian.repository");
const transaksiHarian = express_1.Router();
transaksiHarian.get('/harian', passport_1.isAuthenticated, asyncHandler_1.asyncHandler((req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.render('pages/master/transaksi/harian.pug');
})));
transaksiHarian.get('/harian/json', passport_1.isAuthenticated, asyncHandler_1.asyncHandler((req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const search = (req.query.search) ? req.query.search : '';
    const limit = (req.query.limit) ? Number(req.query.limit) : null;
    const offset = (req.query.offset) ? Number(req.query.offset) : 0;
    const order = (req.query.order) ? req.query.order : 'asc';
    const start = (req.query.start) ? req.query.start : '';
    const end = (req.query.end) ? req.query.end : '';
    const category = (req.query.category) ? req.query.category : '';
    const data = yield laporanTransaksiHarian_repository_1.LaporanTransaksiHarianRepository.dataTable(search, category, order, offset, limit, start, end);
    res.json({
        rows: data.rows,
        total: data.count,
    });
})));
transaksiHarian.get('/harian/export', passport_1.isAuthenticated, asyncHandler_1.asyncHandler((req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const search = (req.query.search) ? req.query.search : '';
    const limit = (req.query.limit) ? Number(req.query.limit) : null;
    const offset = (req.query.offset) ? Number(req.query.offset) : 0;
    const order = (req.query.order) ? req.query.order : 'desc';
    const start = (req.query.start) ? req.query.start : '';
    const end = (req.query.end) ? req.query.end : '';
    const category = (req.query.category) ? req.query.category : '';
    const data = yield laporanTransaksiHarian_repository_1.LaporanTransaksiHarianRepository.dataTable(search, category, order, offset, limit, start, end);
    const result = data.rows.map((value, index) => ({
        no: index + 1,
        date: value.date,
        invoiceNumber: value.invoiceNumber,
        billAmount: Number(value.billAmount),
        commision: Number(value.commision),
        category: value.category,
        providerName: value.providerName,
        paymentMethod: value.paymentMethod,
        paymentStatus: value.paymentStatus,
        transactionStatus: value.transactionStatus,
    }));
    const workbook = yield laporanTransaksiHarian_repository_1.LaporanTransaksiHarianRepository.generateExcel(result);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', `attachment; filename=Data_Rekapitulasi_Harian.xlsx`);
    workbook.xlsx.write(res).then(() => {
        res.status(200).end();
    });
})));
transaksiHarian.get('/harian/print', passport_1.isAuthenticated, asyncHandler_1.asyncHandler((req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const search = (req.query.search) ? req.query.search : '';
    const limit = (req.query.limit) ? Number(req.query.limit) : null;
    const offset = (req.query.offset) ? Number(req.query.offset) : 0;
    const order = (req.query.order) ? req.query.order : 'desc';
    const start = (req.query.start) ? req.query.start : '';
    const end = (req.query.end) ? req.query.end : '';
    const category = (req.query.category) ? req.query.category : null;
    const data = yield laporanTransaksiHarian_repository_1.LaporanTransaksiHarianRepository.dataTable(search, category, order, offset, limit, start, end);
    const pdf = yield laporanTransaksiHarian_repository_1.LaporanTransaksiHarianRepository.generatePdf(data.rows);
    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', `attachment; filename=Transaksi Harian per ${moment_1.default().format('DD-MMMM-YYYY')}.pdf`);
    res.send(pdf);
})));
exports.default = transaksiHarian;
//# sourceMappingURL=laporanTransaksiHarian.controller.js.map