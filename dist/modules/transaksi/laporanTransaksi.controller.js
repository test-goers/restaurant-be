"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const laporanTransaksiBulanan_controller_1 = __importDefault(require("./bulanan/laporanTransaksiBulanan.controller"));
const laporanTransaksiHarian_controller_1 = __importDefault(require("./harian/laporanTransaksiHarian.controller"));
const laporanTransaksiMingguan_controller_1 = __importDefault(require("./mingguan/laporanTransaksiMingguan.controller"));
const laporanTransaksiController = express_1.Router();
laporanTransaksiController.use('/transaksi', laporanTransaksiHarian_controller_1.default);
laporanTransaksiController.use('/transaksi', laporanTransaksiMingguan_controller_1.default);
laporanTransaksiController.use('/transaksi', laporanTransaksiBulanan_controller_1.default);
exports.default = laporanTransaksiController;
//# sourceMappingURL=laporanTransaksi.controller.js.map