"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const dashboard_controller_1 = __importDefault(require("../../master/restaurant/dashboard/dashboard.controller"));
const restaurant_controller_1 = __importDefault(require("../../master/restaurant/restaurant.controller"));
const passport_1 = require("../config/passport");
const dashboardRouter = express_1.Router();
dashboardRouter.get('/', passport_1.isAuthenticated, dashboard_controller_1.default.index);
dashboardRouter.get('/dashboard', passport_1.isAuthenticated, dashboard_controller_1.default.index);
dashboardRouter.get('/restaurant', passport_1.isAuthenticated, restaurant_controller_1.default.index);
dashboardRouter.get('/restaurant/json', passport_1.isAuthenticated, restaurant_controller_1.default.json);
dashboardRouter.get('/restaurant/create', passport_1.isAuthenticated, restaurant_controller_1.default.create);
dashboardRouter.post('/restaurant/store', passport_1.isAuthenticated, restaurant_controller_1.default.store);
dashboardRouter.get('/restaurant/:id/edit', passport_1.isAuthenticated, restaurant_controller_1.default.edit);
dashboardRouter.get('/restaurant/:id/show', passport_1.isAuthenticated, restaurant_controller_1.default.show);
dashboardRouter.post('/restaurant/delete', passport_1.isAuthenticated, restaurant_controller_1.default.delete);
dashboardRouter.post('/restaurant/update', passport_1.isAuthenticated, restaurant_controller_1.default.update);
const dashboardController = express_1.Router();
dashboardController.use('/', passport_1.isAuthenticated, dashboardRouter);
exports.default = dashboardController;
//# sourceMappingURL=dashboard.controller.js.map