"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authController = void 0;
const express_1 = require("express");
const passport_1 = require("../config/passport");
const asyncHandler_1 = require("./../helpers/asyncHandler");
const authRouter = express_1.Router();
authRouter.get('/login', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.render('login.pug', {
        errors: req.flash('error'),
    });
}));
authRouter.get('/logout', passport_1.isAuthenticated, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    req.session.destroy((error) => {
        if (error) {
            throw Error(error.message);
        }
        res.redirect('/auth/login');
    });
}));
authRouter.get('/reset_password', asyncHandler_1.asyncHandler((req, res) => __awaiter(void 0, void 0, void 0, function* () {
    return res.render('reset_password_mobile.pug');
})));
const authController = express_1.Router();
exports.authController = authController;
authController.use('/auth', authRouter);
//# sourceMappingURL=auth.controller.js.map