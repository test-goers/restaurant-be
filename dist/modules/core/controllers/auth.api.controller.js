"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const user_model_1 = require("../../master/user/user.model");
const user_repository_1 = require("../../master/user/user.repository");
const responseHandler_1 = require("../helpers/responseHandler");
class AuthApiController {
}
exports.default = AuthApiController;
AuthApiController.login = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { username } = req.body;
        const user = yield user_model_1.User.findOne({
            where: { username },
            raw: true,
        });
        if (!user) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'User tidak ditemukan');
        }
        if (user.deletedAt === null) {
            delete user.deletedAt;
        }
        const passwordIsValid = bcrypt_1.default.compareSync(req.body.password, user.password);
        if (!passwordIsValid) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'password tidak sesuai');
        }
        const { password } = user, result = __rest(user, ["password"]);
        const token = jsonwebtoken_1.default.sign(result, process.env.SESSION_SECRET);
        const data = {
            token,
            result,
        };
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data, 'Login Berhasil');
    }
    catch (error) {
        console.log(error);
        return responseHandler_1.ResponseHandler.serverError(res, 'Gagal Login');
    }
});
AuthApiController.me = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield user_repository_1.UserRepository.getDetail(req.user.id);
    const data = {
        user,
    };
    return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
});
AuthApiController.resetPassword = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = yield user_repository_1.UserRepository.getUsername(req.body.username);
        if (!user) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'User Tidak ditemukan');
        }
        yield user_model_1.User.update({
            password: yield bcrypt_1.default.hash(req.body.password, 10),
        }, {
            where: { id: user.id },
        });
        return responseHandler_1.ResponseHandler.jsonSuccess(res, { user }, 'update password berhasil');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res);
    }
});
//# sourceMappingURL=auth.api.controller.js.map