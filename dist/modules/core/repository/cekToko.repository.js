"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CekTokoRepository = void 0;
const cekTokoAuth_model_1 = require("../models/cekTokoAuth.model");
const cekTokoUser_model_1 = require("../models/cekTokoUser.model");
class CekTokoRepository {
    static getToken(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            // const token = await CekTokoUser.findOne({
            //   where: {
            //     uid: 'as9msadxpASlkdsaaB7',
            //   }
            // });
            // return token;
            const token = yield cekTokoUser_model_1.CekTokoUser.findOne({
                // attributes: [['$users_authentication.token$', 'token']],
                where: {
                    id: userId,
                },
                include: [
                    {
                        model: cekTokoAuth_model_1.CekTokoAuth,
                        where: {
                        // expired_at: {
                        //   [Op.gt]: Sequelize.fn('NOW'),
                        // }
                        },
                    },
                ],
            });
            return token;
        });
    }
}
exports.CekTokoRepository = CekTokoRepository;
//# sourceMappingURL=cekToko.repository.js.map