"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.merchantMiddleware = void 0;
const merchantAuth_repository_1 = require("../../master/merchantAuth/merchantAuth.repository");
const responseHandler_1 = require("../helpers/responseHandler");
const merchantMiddleware = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    if (req.headers.merchant_token === 'undefined') {
        return responseHandler_1.ResponseHandler.jsonError(res, 'Forbidden, must include the token');
    }
    const result = yield merchantAuth_repository_1.MerchantAuthAPIRepository.auth(req.headers.merchant_token);
    if (!result) {
        yield merchantAuth_repository_1.MerchantAuthAPIRepository.destroySession(req.headers.merchant_token);
        return responseHandler_1.ResponseHandler.jsonError(res, 'Forbidden or expired token');
    }
    next();
});
exports.merchantMiddleware = merchantMiddleware;
//# sourceMappingURL=merchant.middleware.js.map