"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authMiddleware = void 0;
const sequelize_1 = require("sequelize");
const database_1 = require("../config/database");
const responseHandler_1 = require("../helpers/responseHandler");
const authMiddleware = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    // const str = `SELECT a.id, b.token, b.expired_at FROM tb_user a
    // LEFT JOIN users_authentication b ON a.uid = b.user_id WHERE b.expired_at > NOW()`
    // const [cekToken, cekTokenMeta] = await cektoko.query(str, {type:QueryTypes.SELECT});
    const [result, _] = yield database_1.cektoko.query(`SELECT b.id, b.uid, a.token, a.expired_at FROM users_authentication a
  LEFT JOIN tb_user b ON a.user_id = b.uid
  WHERE a.token = '${req.headers.token}' AND a.expired_at > NOW()`, { type: sequelize_1.QueryTypes.SELECT });
    if (!result) {
        return responseHandler_1.ResponseHandler.jsonError(res, 'User not found or token expired');
    }
    req.headers.id = result.id;
    req.headers.user_id = result.uid;
    console.log('UserID:', result.id, 'User UID:', result.uid);
    next();
});
exports.authMiddleware = authMiddleware;
//# sourceMappingURL=auth.middleware.js.map