"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.categoryImageMiddleware = exports.merchantImageMiddleware = exports.productImageMiddleware = void 0;
const aws_sdk_1 = __importDefault(require("aws-sdk"));
const dotenv_1 = __importDefault(require("dotenv"));
const express_1 = require("express");
const multer_1 = __importDefault(require("multer"));
const multer_s3_1 = __importDefault(require("multer-s3"));
const slugify_1 = __importDefault(require("slugify"));
const fileUpload_model_1 = require("../../master/files/file/fileUpload.model");
dotenv_1.default.config();
const s3 = new aws_sdk_1.default.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
    region: 'ap-southeast-1',
});
const storageBucketProduct = multer_1.default({
    storage: multer_s3_1.default({
        s3,
        bucket: process.env.AWS_BUCKET_NAME,
        acl: 'public-read',
        metadata(req, file, cb) {
            cb(null, { fieldname: file.fieldname });
        },
        key(req, file, cb) {
            cb(null, `products/${file.fieldname}_${Date.now()}_${slugify_1.default(file.originalname)}`);
        },
    }),
});
const storageBucketMerchant = multer_1.default({
    storage: multer_s3_1.default({
        s3,
        bucket: process.env.AWS_BUCKET_NAME,
        acl: 'public-read',
        metadata(req, file, cb) {
            cb(null, { fieldname: file.fieldname });
        },
        key(req, file, cb) {
            cb(null, `merchants/${file.fieldname}_${Date.now()}_${slugify_1.default(file.originalname)}`);
        },
    }),
});
const storageBucketCategory = multer_1.default({
    storage: multer_s3_1.default({
        s3,
        bucket: process.env.AWS_BUCKET_NAME,
        acl: 'public-read',
        metadata(req, file, cb) {
            cb(null, { fieldname: file.fieldname });
        },
        key(req, file, cb) {
            cb(null, `categories/${file.fieldname}_${Date.now()}_${slugify_1.default(file.originalname)}`);
        },
    }),
});
const productImageMiddleware = ({ fields, name = null, tag = 'file', access = 'public', }) => {
    const router = express_1.Router();
    router.use(storageBucketProduct.fields(fields), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        yield Promise.all(fields.map((element) => __awaiter(void 0, void 0, void 0, function* () {
            if (req.files[element.name] !== undefined) {
                const file = req.files[element.name][0];
                const saveFile = {
                    key: file.key,
                    location: file.location,
                    mimetype: file.mimetype,
                };
                const dataFromUpload = yield fileUpload_model_1.FileUpload.create(saveFile);
                req[element.name] = dataFromUpload;
            }
            else {
                req[element.name] = {};
            }
        })));
        next();
    }));
    return router;
};
exports.productImageMiddleware = productImageMiddleware;
const merchantImageMiddleware = ({ fields, name = null, tag = 'file', access = 'public', }) => {
    const router = express_1.Router();
    router.use(storageBucketMerchant.fields(fields), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        yield Promise.all(fields.map((element) => __awaiter(void 0, void 0, void 0, function* () {
            if (req.files[element.name] !== undefined) {
                const file = req.files[element.name][0];
                const saveFile = {
                    key: file.key,
                    location: file.location,
                    mimetype: file.mimetype,
                };
                const dataFromUpload = yield fileUpload_model_1.FileUpload.create(saveFile);
                req[element.name] = dataFromUpload;
            }
            else {
                req[element.name] = {};
            }
        })));
        next();
    }));
    return router;
};
exports.merchantImageMiddleware = merchantImageMiddleware;
const categoryImageMiddleware = ({ fields, name = null, tag = 'file', access = 'public', }) => {
    const router = express_1.Router();
    router.use(storageBucketCategory.fields(fields), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        yield Promise.all(fields.map((element) => __awaiter(void 0, void 0, void 0, function* () {
            if (req.files[element.name] !== undefined) {
                const file = req.files[element.name][0];
                const saveFile = {
                    key: file.key,
                    location: file.location,
                    mimetype: file.mimetype,
                };
                const dataFromUpload = yield fileUpload_model_1.FileUpload.create(saveFile);
                req[element.name] = dataFromUpload;
            }
            else {
                req[element.name] = {};
            }
        })));
        next();
    }));
    return router;
};
exports.categoryImageMiddleware = categoryImageMiddleware;
//# sourceMappingURL=file.middleware.js.map