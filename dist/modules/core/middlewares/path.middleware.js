"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pathMiddleware = void 0;
const pathMiddleware = (req, res, next) => {
    res.locals.path = req.path;
    next();
};
exports.pathMiddleware = pathMiddleware;
//# sourceMappingURL=path.middleware.js.map