"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.breadcrumbMiddleware = void 0;
const path_to_regexp_1 = require("path-to-regexp");
const url = __importStar(require("url"));
const breadcrumb_config_1 = require("../config/breadcrumb.config");
const breadcrumbMiddleware = (req, res, next) => {
    const urlParts = url.parse(req.url).pathname.split('/');
    const breadcrumbs = [];
    for (let i = 0; i < urlParts.length; i++) {
        const constructedUrl = urlParts.slice(0, i + 1).join('/');
        const breadcrumb = breadcrumb_config_1.breadcrumbConfig.find(breadcrumbItem => path_to_regexp_1.pathToRegexp(breadcrumbItem.url).test(constructedUrl));
        if (breadcrumb) {
            breadcrumbs.push({
                name: breadcrumb.name,
                url: constructedUrl,
                last: i === urlParts.length - 1,
            });
        }
    }
    res.locals.breadcrumbs = breadcrumbs;
    next();
};
exports.breadcrumbMiddleware = breadcrumbMiddleware;
//# sourceMappingURL=breadcrumb.middleware.js.map