"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiMiddleware = void 0;
const apiMiddleware = (req, res, next) => {
    res.locals.isApi = true;
    res.locals.api = true;
    next();
};
exports.apiMiddleware = apiMiddleware;
//# sourceMappingURL=api.middleware.js.map