"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.breadcrumbConfig = void 0;
exports.breadcrumbConfig = [
    { url: '', name: 'Home' },
    // Master Payment
    { url: '/payment', name: 'Payment' },
    { url: '/payment/create', name: 'Insert' },
    { url: '/payment/edit', name: 'Edit' },
    // Master Partner Product
    { url: '/partner-product', name: 'Partner Product' },
    { url: '/partner-product/create', name: 'Insert' },
    { url: '/partner-product/edit', name: 'Edit' },
    // Master Commision
    { url: '/commision', name: 'Commision' },
    { url: '/commision/create', name: 'Insert' },
    { url: '/commision/edit', name: 'Edit' },
];
//# sourceMappingURL=breadcrumb.config.js.map