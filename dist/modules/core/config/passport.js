"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiAuthentication = exports.passport = exports.isAuthenticated = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const passport_1 = __importDefault(require("passport"));
exports.passport = passport_1.default;
// import { Strategy as LocalStrategy } from 'passport-local';
const passport_local_1 = require("passport-local");
const logger_1 = require("../../../helpers/logger");
const user_model_1 = require("../../master/user/user.model");
const errorThrower_1 = require("../helpers/errorThrower");
const responseType_1 = require("../helpers/responseType");
passport_1.default.serializeUser((user, done) => {
    done(null, user.id);
});
passport_1.default.deserializeUser((id, done) => {
    user_model_1.User.findByPk(id)
        .then(user => done(null, user))
        .catch(error => done(error));
});
/**
 * Sign in using username and password
 */
passport_1.default.use(new passport_local_1.Strategy({
    passReqToCallback: true,
    passwordField: 'password',
    session: true,
    usernameField: 'username',
}, (req, username, password, done) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.logger.debug('Passport Local Strategy', username);
        console.log('local', username, password);
        const user = yield user_model_1.User.findOne({ where: { username } });
        if (!user) {
            throw Error('Username tidak ditemukan');
        }
        logger_1.logger.debug('Passport Local Strategy', user.fullname);
        const result = bcrypt_1.default.compareSync(password, user.password);
        if (!result) {
            throw Error('Username atau password salah');
        }
        return done(null, user);
    }
    catch (error) {
        logger_1.logger.debug('Passport Local Strategy', error);
        return done(null, false, { message: error.message });
    }
})));
/**
 * Login Required middleware.
 */
const apiAuthentication = (req, res, next) => {
    if (!req.headers.authorization && !req.headers.auth) {
        throw new errorThrower_1.ErrorThrower({ message: 'NO_TOKEN', responseType: responseType_1.ResponseType.UNAUTHORIZED });
    }
    let token = req.headers.authorization || req.headers.auth.toString();
    token = token.slice(7, token.length);
    jsonwebtoken_1.default.verify(token, process.env.SESSION_SECRET, (error, decoded) => {
        if (error || decoded === null) {
            throw new errorThrower_1.ErrorThrower({ message: 'Token Not Valid', responseType: responseType_1.ResponseType.FORBIDDEN });
        }
        else {
            req.user = decoded;
            return next();
        }
    });
};
exports.apiAuthentication = apiAuthentication;
const isAuthenticated = (req, res, next) => {
    if (process.env.AUTH_DISABLED === 'yes') {
        return next();
    }
    if (res.locals.api === true) {
        return apiAuthentication(req, res, next);
    }
    if (req.isAuthenticated()) {
        return next();
    }
    // req.session.afterLoginUrl = req.path;
    res.redirect('/auth/login');
};
exports.isAuthenticated = isAuthenticated;
//# sourceMappingURL=passport.js.map