"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.rajaBillerCredential = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const rajaBillerCredential = {
    uid: process.env.RAJABILLER_UID,
    pin: process.env.RAJABILLER_PIN,
};
exports.rajaBillerCredential = rajaBillerCredential;
//# sourceMappingURL=credential.js.map