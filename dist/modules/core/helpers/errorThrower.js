"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorThrower = void 0;
const responseType_1 = require("./responseType");
class ErrorThrower extends Error {
    constructor({ message = 'Internal Server Error', responseType = responseType_1.ResponseType.SERVER_ERROR, data = null }) {
        super();
        Error.captureStackTrace(this, this.constructor);
        this.name = this.constructor.name;
        this.message = message ||
            'Internal Server Error';
        this.responseType = responseType;
    }
}
exports.ErrorThrower = ErrorThrower;
//# sourceMappingURL=errorThrower.js.map