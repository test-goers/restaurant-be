"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OauthClientHandler = void 0;
const axios_1 = __importDefault(require("axios"));
const google_auth_library_1 = require("google-auth-library");
const qs_1 = __importDefault(require("qs"));
class OauthClientHandler {
    static getGoogleProfile(idToken) {
        return __awaiter(this, void 0, void 0, function* () {
            const client = new google_auth_library_1.OAuth2Client(process.env.GOOGLE_CLIENT_ID);
            const verifyToken = yield client.verifyIdToken({
                idToken,
                audience: process.env.GOOGLE_CLIENT_ID,
            });
            const profile = verifyToken.getPayload();
            return profile;
        });
    }
    static getLinkedinProfile(code) {
        return __awaiter(this, void 0, void 0, function* () {
            const document = {
                'grant_type': 'authorization_code',
                'code': code,
                'redirect_uri': process.env.LINKEDIN_REDIRECT_URI,
                'client_id': process.env.LINKEDIN_CLIENT_ID,
                'client_secret': process.env.LINKEDIN_CLIENT_SECRET,
            };
            const { data } = yield axios_1.default.post('https://www.linkedin.com/oauth/v2/accessToken', qs_1.default.stringify(document), { headers: { 'content-type': 'application/x-www-form-urlencoded' },
            });
            axios_1.default.defaults.headers.common.Authorization = `Bearer ${data.access_token}`;
            const linkedinData = yield axios_1.default.all([
                axios_1.default.get('https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))'),
                axios_1.default.get('https://api.linkedin.com/v2/me?projection=(id,localizedFirstName,localizedLastName)'),
            ]);
            const email = linkedinData[0].data.elements[0]['handle~'].emailAddress;
            const profile = linkedinData[1].data;
            const result = {
                email,
                name: `${profile.localizedFirstName} ${profile.localizedLastName}`,
                linkedin: profile.id,
                verified: true,
            };
            return result;
        });
    }
}
exports.OauthClientHandler = OauthClientHandler;
//# sourceMappingURL=oauthClientHandler.js.map