"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResponseHandler = void 0;
class ResponseHandler {
    static jsonSuccess(res, data, message) {
        res.status(200)
            .json({
            message: message || 'Success',
            error: false,
            data,
        });
    }
    static jsonError(res, message) {
        res.status(200)
            .json({
            message: message || 'Success with warning',
            error: true,
        });
    }
    static unauthorized(res, message) {
        res.status(401)
            .json({
            message: message || 'Unathorized',
            error: true,
        });
    }
    static serverError(res, message) {
        res.status(500)
            .json({
            message: message || 'Error Server',
            error: true,
        });
    }
    static badRequest(res, message) {
        res.status(400)
            .json({
            message: message || 'Bad Request',
            error: true,
        });
    }
}
exports.ResponseHandler = ResponseHandler;
//# sourceMappingURL=responseHandler.js.map