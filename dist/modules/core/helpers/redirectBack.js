"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.redirectBackIfEmpty = exports.redirectBackIfError = void 0;
const logger_1 = require("../../../helpers/logger");
const responseType_1 = require("./responseType");
const redirectBackIfError = (error, req, res, val = null) => {
    if (error) {
        logger_1.logger.error(error.message);
        if (res.locals.api) {
            responseType_1.responseJsonBuilder(res, {
                responseType: responseType_1.ResponseType.VALIDATION_ERROR,
                message: error.message,
            });
            return true;
        }
        req.flash('val', val);
        req.flash('errors', error.message);
        res.redirect('back');
        return true;
    }
    return false;
};
exports.redirectBackIfError = redirectBackIfError;
const redirectBackIfEmpty = (object, errorMessage, req, res, val = null) => {
    if (!object) {
        logger_1.logger.error(errorMessage);
        if (res.locals.api) {
            responseType_1.responseJsonBuilder(res, {
                responseType: responseType_1.ResponseType.VALIDATION_ERROR,
                message: errorMessage,
            });
            return true;
        }
        req.flash('val', val);
        req.flash('errors', errorMessage);
        res.redirect('back');
        return true;
    }
    return false;
};
exports.redirectBackIfEmpty = redirectBackIfEmpty;
//# sourceMappingURL=redirectBack.js.map