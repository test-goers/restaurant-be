"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResponseType = exports.responseJsonBuilder = void 0;
const responseJsonBuilder = (res, response) => {
    var _a;
    res.status(response.responseType.status);
    res.json({
        error: response.responseType.error,
        message: (_a = response.message) !== null && _a !== void 0 ? _a : response.responseType.message,
        data: response.data,
    });
};
exports.responseJsonBuilder = responseJsonBuilder;
class ResponseType {
    constructor(status, error, message) {
        this.status = status;
        this.error = error;
        this.message = message;
    }
}
exports.ResponseType = ResponseType;
ResponseType.SUCCESS = new ResponseType(200, false, 'Success');
ResponseType.NOT_FOUND = new ResponseType(404, true, 'Not found');
ResponseType.FORBIDDEN = new ResponseType(400, true, 'Forbidden');
ResponseType.UNAUTHORIZED = new ResponseType(401, true, 'Unauthorized');
ResponseType.SERVER_ERROR = new ResponseType(500, true, 'Server Error');
ResponseType.VALIDATION_ERROR = new ResponseType(422, true, 'Validation Error');
//# sourceMappingURL=responseType.js.map