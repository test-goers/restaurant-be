"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.numberFormat = void 0;
const numberFormat = (numbers) => {
    return numbers.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.');
    // return new Intl.NumberFormat('IDR-ID', {
    //   style: 'currency',
    //   currency: 'IDR',
    // }).format(number);
};
exports.numberFormat = numberFormat;
//# sourceMappingURL=formatting.js.map