"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CekTokoAuth = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const cekTokoUser_model_1 = require("./cekTokoUser.model");
let CekTokoAuth = class CekTokoAuth extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.ForeignKey(() => cekTokoUser_model_1.CekTokoUser),
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekTokoAuth.prototype, "userId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => cekTokoUser_model_1.CekTokoUser),
    __metadata("design:type", cekTokoUser_model_1.CekTokoUser)
], CekTokoAuth.prototype, "cekTokoUser", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekTokoAuth.prototype, "token", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], CekTokoAuth.prototype, "expiredAt", void 0);
CekTokoAuth = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'users_authentication',
        timestamps: false,
    })
], CekTokoAuth);
exports.CekTokoAuth = CekTokoAuth;
//# sourceMappingURL=cekTokoAuth.model.js.map