"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseHandler_1 = require("../../core/helpers/responseHandler");
const restaurant_repository_1 = require("./restaurant.repository");
class RestaurantApiController {
}
exports.default = RestaurantApiController;
RestaurantApiController.getAll = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield restaurant_repository_1.RestaurantRepository.getAll(req);
        return data
            ? responseHandler_1.ResponseHandler.jsonSuccess(res, data)
            : responseHandler_1.ResponseHandler.jsonError(res, 'Tidak ada restoran untuk saat ini');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
RestaurantApiController.getDetail = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield restaurant_repository_1.RestaurantRepository.get(Number(req.params.id));
        return data
            ? responseHandler_1.ResponseHandler.jsonSuccess(res, data)
            : responseHandler_1.ResponseHandler.jsonError(res, 'Tidak ada restoran untuk saat ini');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
//# sourceMappingURL=restaurant.api.controller.js.map