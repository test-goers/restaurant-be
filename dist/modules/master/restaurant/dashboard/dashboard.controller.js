"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const restaurant_repository_1 = require("../restaurant.repository");
class DashboardRestaurantController {
}
exports.default = DashboardRestaurantController;
DashboardRestaurantController.index = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const lastUpdate = moment_1.default().format('DD-MM-YYYY HH:MM:SS');
    const data = yield restaurant_repository_1.RestaurantRepository.getCount();
    return res.render('home.pug', { lastUpdate, data });
});
//# sourceMappingURL=dashboard.controller.js.map