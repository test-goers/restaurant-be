"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseHandler_1 = require("../../core/helpers/responseHandler");
const restaurant_repository_1 = require("./restaurant.repository");
class RestaurantController {
}
exports.default = RestaurantController;
RestaurantController.index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    return res.render('pages/master/restaurant/index.pug');
});
RestaurantController.create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const old = req.flash('val')[0];
    return res.render('pages/master/restaurant/create.pug', { old });
});
RestaurantController.json = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const search = req.query.search || '';
        const limit = req.query.limit || null;
        const order = req.query.order || 'asc';
        const offset = req.query.offset || 0;
        const data = yield restaurant_repository_1.RestaurantRepository.dataTable(String(search), String(order), Number(offset), Number(limit));
        res.json({
            rows: data.rows,
            total: data.count,
        });
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
RestaurantController.store = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const create = yield restaurant_repository_1.RestaurantRepository.create(req.body);
        if (create) {
            req.flash(`success`, `Restaurant berhasil ditambahkan`);
        }
        else {
            req.flash('errors', 'Gagal menambahkan Restaurant');
        }
        res.redirect(`/restaurant`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
RestaurantController.delete = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield restaurant_repository_1.RestaurantRepository.delete(Number(req.body.id));
        req.flash(`success`, `Restaurant berhasil dihapus`);
        return res.redirect('/restaurant');
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
RestaurantController.show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield restaurant_repository_1.RestaurantRepository.get(Number(req.params.id));
    return res.render('pages/master/restaurant/show.pug', { data });
});
RestaurantController.edit = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield restaurant_repository_1.RestaurantRepository.get(Number(req.params.id));
    const old = req.flash('val')[0];
    return res.render('pages/master/restaurant/edit.pug', { data, old });
});
RestaurantController.update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const id = req.body.id || 0;
        const update = yield restaurant_repository_1.RestaurantRepository.update(req.body, Number(id));
        if (update) {
            req.flash(`success`, `Restaurant berhasil diperbaharui`);
        }
        else {
            req.flash('errors', 'Gagal memperbaharui restaurant');
        }
        res.redirect(`/restaurant`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
//# sourceMappingURL=restaurant.controller.js.map