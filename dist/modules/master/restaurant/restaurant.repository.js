"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantRepository = void 0;
const sequelize_1 = require("sequelize");
const restaurant_model_1 = require("./restaurant.model");
class RestaurantRepository {
    static dataTable(search = '', order = 'asc', offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield restaurant_model_1.Restaurant.findAndCountAll({
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'deletedAt'],
                },
                where: {
                    [sequelize_1.Op.or]: {
                        name: { [sequelize_1.Op.substring]: `%${search}%` },
                        schedule: { [sequelize_1.Op.substring]: `%${search}%` },
                    },
                },
                order: [
                    ['id', order],
                ],
                offset,
                limit,
                logging: console.log,
            });
            return data;
        });
    }
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const create = yield restaurant_model_1.Restaurant.create(data);
            return create;
        });
    }
    static delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield restaurant_model_1.Restaurant.destroy({ where: { id } });
        });
    }
    static get(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield restaurant_model_1.Restaurant.findByPk(id);
            return data;
        });
    }
    static update(data, id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield restaurant_model_1.Restaurant.update(data, {
                where: { id },
            });
            const updatedData = yield restaurant_model_1.Restaurant.findByPk(id);
            return updatedData;
        });
    }
    static getCount() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield restaurant_model_1.Restaurant.count();
            return data;
        });
    }
    static getAll(req) {
        return __awaiter(this, void 0, void 0, function* () {
            const limit = req.query.limit || 10;
            const offset = req.query.offset || 0;
            const search = req.query.search || '';
            const data = yield restaurant_model_1.Restaurant.findAndCountAll({
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'deletedAt'],
                },
                where: {
                    [sequelize_1.Op.or]: {
                        name: { [sequelize_1.Op.substring]: `%${search}%` },
                        schedule: { [sequelize_1.Op.substring]: `%${search}%` },
                    },
                },
                offset,
                limit,
                logging: console.log,
            });
            return {
                rows: data.rows,
                count: data.count,
            };
        });
    }
}
exports.RestaurantRepository = RestaurantRepository;
//# sourceMappingURL=restaurant.repository.js.map