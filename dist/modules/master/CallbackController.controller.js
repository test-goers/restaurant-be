"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const moment_1 = __importDefault(require("moment"));
const cekPayMethod_1 = require("../../helpers/cekPayMethod");
const credential_1 = require("../core/config/credential");
const responseHandler_1 = require("../core/helpers/responseHandler");
const cekPay_repository_1 = __importDefault(require("./cekpay/cekPay.repository"));
class CallbackController {
}
exports.default = CallbackController;
CallbackController.index = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { external_id: invoiceNumber, payment_method: paymentMethod2, payment_channel: paymentChannel, amount, paid_amount: paidAmount, fees_paid_amount: feesPaidAmount, adjusted_received_amount: adjustedReceivedAmount, paid_at: paidAt, status: paymentStatus, } = req.body;
        const transaction = yield cekPay_repository_1.default.getInvoiceNumber(invoiceNumber);
        let updateToDatabase = {};
        if (transaction) {
            if (transaction.paymentStatus === 'PAID' || transaction.paymentStatus === 'SETTLED' || transaction.paymentStatus === 'SUCCEEDED') {
                return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah selesai pada tanggal ' + moment_1.default(transaction.paidAt).format('YYYY-MM-DD HH:mm:ss'));
            }
            if (transaction.paymentStatus === 'EXPIRED') {
                return responseHandler_1.ResponseHandler.jsonError(res, 'Telah melampaui batas pembayaran');
            }
            if (moment_1.default(transaction.transactionExpiredAt).isBefore(moment_1.default())) {
                return responseHandler_1.ResponseHandler.jsonError(res, 'Telah melampaui batas pembayaran');
            }
            if (paymentStatus === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, 'Silahkan selesaikan pembayaran terlebih dahulu');
            }
            const transactionNumber = yield cekPay_repository_1.default.invoiceNumber('transaction');
            let data = {};
            if (transaction.product === 'PULSA') {
                data = Object.assign(Object.assign({}, credential_1.rajaBillerCredential), { 'method': cekPayMethod_1.cekPayMethod.pulsa.transactionBuy, 'no_hp': transaction.accNumber1, 'kode_produk': transaction.productCode, 'ref1': transactionNumber });
            }
            const response = yield axios_1.default.post(process.env.RAJABILLER_ENDPOINT, data)
                .then((item) => item.data)
                .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, 'Network error ' + err));
            updateToDatabase = {
                transactionNumber,
                deductedBalance: response.SALDO_TERPOTONG,
                endingBalance: response.SISA_SALDO,
                paymentMethod2,
                paymentChannel,
                amount,
                paidAmount,
                feesPaidAmount,
                adjustedReceivedAmount,
                paidAt,
                paymentStatus,
                transactionStatus: response.STATUS_TRX,
            };
            yield cekPay_repository_1.default.update(updateToDatabase, invoiceNumber);
        }
        else {
            if (paymentMethod2 === 'EWALLET') {
                axios_1.default.post('https://cektoko.com/panel/api/payment/payment_ewallet_callback', req.body);
            }
            else if (paymentMethod2 === 'BANK_TRANSFER') {
                axios_1.default.post('https://cektoko.com/panel/api/payment/payment_transfer_bank_callback', req.body);
            }
            else {
                axios_1.default.post('https://cektoko.com/panel/api/payment/expired_payment_transfer_bank', req.body);
            }
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor invoice tidak ditemukan');
        }
        return responseHandler_1.ResponseHandler.jsonSuccess(res, updateToDatabase);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=CallbackController.controller.js.map