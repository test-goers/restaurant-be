"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseHandler_1 = require("../../core/helpers/responseHandler");
const village_repository_1 = require("./village.repository");
class VillageController {
}
exports.default = VillageController;
VillageController.getAll = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const districtId = (req.query.districtId) ? req.query.districtId : '';
        const search = (req.query.search) ? req.query.search : '';
        const data = yield village_repository_1.VillageRepository.getAll(Number(districtId), search);
        const result = data.map((value, index) => ({
            id: value.id,
            text: value.name,
        })).filter((query) => query.text.includes(String(search).toUpperCase()));
        return res.status(200).json({ results: result });
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=village.controller.js.map