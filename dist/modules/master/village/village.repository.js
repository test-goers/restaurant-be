"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VillageRepository = void 0;
const sequelize_1 = require("sequelize");
const district_model_1 = require("../district/district.model");
const village_model_1 = require("./village.model");
class VillageRepository {
    static getAll(id, search = '') {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield village_model_1.Village.findAll({
                include: [
                    {
                        model: district_model_1.District,
                    },
                ],
                where: {
                    districtId: id,
                    name: {
                        [sequelize_1.Op.like]: `%${search}%`,
                    },
                },
            });
            return data;
        });
    }
}
exports.VillageRepository = VillageRepository;
//# sourceMappingURL=village.repository.js.map