"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DistrictRepository = void 0;
const sequelize_1 = require("sequelize");
const city_model_1 = require("../city/city.model");
const district_model_1 = require("./district.model");
class DistrictRepository {
    static getAll(id, search = '') {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield district_model_1.District.findAll({
                include: [
                    {
                        model: city_model_1.City,
                    },
                ],
                where: {
                    cityId: id,
                    name: {
                        [sequelize_1.Op.like]: `%${search}%`,
                    },
                },
            });
            return data;
        });
    }
}
exports.DistrictRepository = DistrictRepository;
//# sourceMappingURL=district.repository.js.map