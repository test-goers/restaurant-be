"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategorySchemaUpdate = exports.CategorySchema = void 0;
const typesafe_joi_1 = __importDefault(require("typesafe-joi"));
exports.CategorySchema = typesafe_joi_1.default.object({
    name: typesafe_joi_1.default.string().required(),
    description: typesafe_joi_1.default.string(),
    logoId: typesafe_joi_1.default.number().required(),
});
exports.CategorySchemaUpdate = typesafe_joi_1.default.object({
    categoryId: typesafe_joi_1.default.string().required(),
    name: typesafe_joi_1.default.string().required(),
    description: typesafe_joi_1.default.string(),
    logoId: typesafe_joi_1.default.number(),
});
//# sourceMappingURL=kategoriProduk.shema.js.map