"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const redirectBack_1 = require("../../../core/helpers/redirectBack");
const responseHandler_1 = require("../../../core/helpers/responseHandler");
const fileUpload_service_1 = require("../file/fileUpload.service");
const kategoriProduk_repository_1 = require("./kategoriProduk.repository");
const kategoriProduk_shema_1 = require("./kategoriProduk.shema");
class KategoriProdukController {
}
exports.default = KategoriProdukController;
KategoriProdukController.index = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    return res.render('pages/master/kategori/index.pug');
});
KategoriProdukController.create = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const old = req.flash('val')[0];
    return res.render('pages/master/kategori/create.pug', { old });
});
KategoriProdukController.edit = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield kategoriProduk_repository_1.KategoriProdukRepository.get(req.params.categoryId);
    const old = req.flash('val')[0];
    return res.render('pages/master/kategori/edit.pug', { old, data });
});
KategoriProdukController.json = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const search = (req.query.search) ? req.query.search : '';
    const limit = (req.query.limit) ? Number(req.query.limit) : null;
    const offset = (req.query.offset) ? Number(req.query.offset) : 0;
    const order = (req.query.order) ? req.query.order : 'asc';
    const data = yield kategoriProduk_repository_1.KategoriProdukRepository.dataTable(search, order, offset, limit);
    res.json({
        rows: data.rows,
        total: data.count,
    });
});
KategoriProdukController.store = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const checkCategory = yield kategoriProduk_repository_1.KategoriProdukRepository.checkCategory(req.body.name, null);
        if (checkCategory) {
            throw new Error('Category is already exist');
        }
        req.body.logoId = (req.logo) ? req.logo.id : '';
        const { error, value } = kategoriProduk_shema_1.CategorySchema.validate(req.body);
        if (redirectBack_1.redirectBackIfError(error, req, res, value)) {
            return;
        }
        yield kategoriProduk_repository_1.KategoriProdukRepository.create(value);
        req.flash(`success`, `Category added successfully`);
        res.redirect(`/produk/kategori`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
KategoriProdukController.update = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const checkCategory = yield kategoriProduk_repository_1.KategoriProdukRepository.checkCategory(req.body.name, req.body.categoryId);
        if (checkCategory) {
            throw new Error('Category is already exist');
        }
        const dataIcon = yield kategoriProduk_repository_1.KategoriProdukRepository.get(req.body.categoryId);
        req.body.logoId = (yield (req.logo)) ? req.logo.id : dataIcon.logoId;
        if (Object.keys(req.logo).length > 0) {
            yield fileUpload_service_1.FileUploadService.removeFile(dataIcon.logoId);
        }
        const { error, value } = kategoriProduk_shema_1.CategorySchemaUpdate.validate(req.body);
        if (redirectBack_1.redirectBackIfError(error, req, res, value)) {
            return;
        }
        yield kategoriProduk_repository_1.KategoriProdukRepository.update(value, req.body.categoryId);
        req.flash(`success`, `Category added successfully`);
        res.redirect(`/produk/kategori`);
    }
    catch (error) {
        console.log(error);
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
KategoriProdukController.delete = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield kategoriProduk_repository_1.KategoriProdukRepository.destroy(req.query.id);
        req.flash(`success`, `Category deleted successfully`);
        return res.redirect('/produk/kategori');
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
KategoriProdukController.getAll = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield kategoriProduk_repository_1.KategoriProdukRepository.getAll();
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Data kategori belum tersedia');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=kategoriProduk.controller.js.map