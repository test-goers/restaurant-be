"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KategoriProduk = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const fileUpload_model_1 = require("../file/fileUpload.model");
let KategoriProduk = class KategoriProduk extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], KategoriProduk.prototype, "name", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], KategoriProduk.prototype, "description", void 0);
__decorate([
    sequelize_typescript_1.ForeignKey(() => fileUpload_model_1.FileUpload),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], KategoriProduk.prototype, "logoId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => fileUpload_model_1.FileUpload),
    __metadata("design:type", Array)
], KategoriProduk.prototype, "image", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], KategoriProduk.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], KategoriProduk.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], KategoriProduk.prototype, "deletedAt", void 0);
KategoriProduk = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'Category',
        timestamps: true,
        paranoid: true,
    })
], KategoriProduk);
exports.KategoriProduk = KategoriProduk;
//# sourceMappingURL=kategoriProduk.model.js.map