"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KategoriProdukRepository = void 0;
const sequelize_1 = require("sequelize");
const fileUpload_model_1 = require("../file/fileUpload.model");
const kategoriProduk_model_1 = require("./kategoriProduk.model");
class KategoriProdukRepository {
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield kategoriProduk_model_1.KategoriProduk.findAll({
                attributes: ['id', 'name', 'description', 'logoId'],
                order: [
                    ['name', 'ASC'],
                ],
            });
            return data;
        });
    }
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const create = yield kategoriProduk_model_1.KategoriProduk.create(data);
            return create;
        });
    }
    static update(data, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const update = yield kategoriProduk_model_1.KategoriProduk.update(data, { where: { id } });
            console.log(update);
            const kategori = yield kategoriProduk_model_1.KategoriProduk.findByPk(id);
            return kategori;
        });
    }
    static get(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const kategori = yield kategoriProduk_model_1.KategoriProduk.findByPk(id, {
                include: [
                    { model: fileUpload_model_1.FileUpload },
                ],
            });
            return kategori;
        });
    }
    static checkCategory(name, id = null) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield kategoriProduk_model_1.KategoriProduk.findOne({
                where: {
                    name,
                    [sequelize_1.Op.and]: {
                        id: (id !== null) ? { [sequelize_1.Op.notIn]: [id] } : { [sequelize_1.Op.notIn]: [] },
                    },
                },
            });
            return data;
        });
    }
    static dataTable(search = '', order = 'asc', offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield kategoriProduk_model_1.KategoriProduk.findAndCountAll({
                where: {
                    [sequelize_1.Op.and]: {
                        [sequelize_1.Op.or]: {
                            name: { [sequelize_1.Op.like]: `%${search}%` },
                        },
                    },
                },
                order: [
                    ['createdAt', order],
                ],
                offset: (offset !== '' && offset != null) ? offset : null,
                limit,
            });
            return data;
        });
    }
    static destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield kategoriProduk_model_1.KategoriProduk.destroy({
                where: { id },
            });
        });
    }
}
exports.KategoriProdukRepository = KategoriProdukRepository;
//# sourceMappingURL=kategoriProduk.repository.js.map