"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatusRepository = void 0;
const status_model_1 = require("./status.model");
class StatusRepository {
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield status_model_1.StatusTransaksi.findAll({
                attributes: ['id', 'status', 'description', 'code'],
            });
            return data;
        });
    }
    static getByCode(code) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield status_model_1.StatusTransaksi.findOne({
                where: { code },
            });
            return data;
        });
    }
}
exports.StatusRepository = StatusRepository;
//# sourceMappingURL=status.repository.js.map