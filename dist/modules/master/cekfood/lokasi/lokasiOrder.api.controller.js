"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseHandler_1 = require("../../../core/helpers/responseHandler");
const lokasiOrder_repository_1 = require("./lokasiOrder.repository");
class LokasiOrderAPIController {
}
exports.default = LokasiOrderAPIController;
LokasiOrderAPIController.getAll = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield lokasiOrder_repository_1.LokasiOrderRepository.getAll(Number(req.headers.id));
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Data alamat belum tersedia');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
LokasiOrderAPIController.get = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const lokasiId = (req.query.id) ? req.query.id : 0;
        const data = yield lokasiOrder_repository_1.LokasiOrderRepository.get(Number(req.headers.id), Number(lokasiId));
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Data alamat belum tersedia');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
LokasiOrderAPIController.create = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const dataToDatabase = {
            userId: req.headers.id,
            label: req.body.label,
            alamat: req.body.alamat,
            latitude: req.body.latitude,
            longitude: req.body.longitude,
            isMain: req.body.isMain,
            status: req.body.status,
        };
        const create = yield lokasiOrder_repository_1.LokasiOrderRepository.create(dataToDatabase);
        return (create) ? responseHandler_1.ResponseHandler.jsonSuccess(res, create) : responseHandler_1.ResponseHandler.jsonError(res, 'Data alamat gagal ditambahkan');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
LokasiOrderAPIController.update = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const dataToDatabase = {
            label: req.body.label,
            alamat: req.body.alamat,
            latitude: req.body.latitude,
            longitude: req.body.longitude,
            isMain: req.body.isMain,
            status: req.body.status,
        };
        const update = yield lokasiOrder_repository_1.LokasiOrderRepository.update(dataToDatabase, Number(req.query.id), Number(req.headers.id));
        return (update) ? responseHandler_1.ResponseHandler.jsonSuccess(res, update) : responseHandler_1.ResponseHandler.jsonError(res, 'Data alamat gagal disunting');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
LokasiOrderAPIController.delete = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const deleteData = yield lokasiOrder_repository_1.LokasiOrderRepository.delete(Number(req.query.id), Number(req.headers.id));
        return (deleteData) ? responseHandler_1.ResponseHandler.jsonSuccess(res, 'Data alamat berhasil dihapus') : responseHandler_1.ResponseHandler.jsonError(res, 'Data alamat gagal dihapus');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=lokasiOrder.api.controller.js.map