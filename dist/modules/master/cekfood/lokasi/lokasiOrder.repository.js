"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LokasiOrderRepository = void 0;
const sequelize_1 = require("sequelize");
const lokasiOrder_model_1 = require("./lokasiOrder.model");
class LokasiOrderRepository {
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const lokasi = yield lokasiOrder_model_1.LokasiOrder.create(data);
            return lokasi;
        });
    }
    static update(data, id, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield lokasiOrder_model_1.LokasiOrder.update(data, {
                where: {
                    [sequelize_1.Op.and]: {
                        id,
                        userId,
                    },
                },
            });
            const lokasi = yield lokasiOrder_model_1.LokasiOrder.findByPk(id);
            return lokasi;
        });
    }
    static delete(id, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deleteData = yield lokasiOrder_model_1.LokasiOrder.destroy({
                where: {
                    [sequelize_1.Op.and]: {
                        id,
                        userId,
                    },
                },
            });
            return deleteData;
        });
    }
    static getAll(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield lokasiOrder_model_1.LokasiOrder.findAndCountAll({
                where: { userId },
                order: [['isMain', 'DESC']],
            });
            return data;
        });
    }
    static get(userId, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield lokasiOrder_model_1.LokasiOrder.findOne({
                where: {
                    [sequelize_1.Op.and]: {
                        id,
                        userId,
                    },
                },
            });
            return data;
        });
    }
}
exports.LokasiOrderRepository = LokasiOrderRepository;
//# sourceMappingURL=lokasiOrder.repository.js.map