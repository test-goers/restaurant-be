"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LokasiOrder = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
let LokasiOrder = class LokasiOrder extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], LokasiOrder.prototype, "userId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], LokasiOrder.prototype, "label", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], LokasiOrder.prototype, "alamat", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], LokasiOrder.prototype, "latitude", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], LokasiOrder.prototype, "longitude", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], LokasiOrder.prototype, "isMain", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], LokasiOrder.prototype, "status", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], LokasiOrder.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], LokasiOrder.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], LokasiOrder.prototype, "deletedAt", void 0);
LokasiOrder = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'OrderAddress',
        timestamps: true,
        paranoid: true,
    })
], LokasiOrder);
exports.LokasiOrder = LokasiOrder;
//# sourceMappingURL=lokasiOrder.model.js.map