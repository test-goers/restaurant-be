"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogStock = void 0;
const moment_1 = __importDefault(require("moment"));
const sequelize_typescript_1 = require("sequelize-typescript");
const merchant_model_1 = require("../merchant/merchant.model");
const produk_model_1 = require("../produk/produk.model");
let LogStock = class LogStock extends sequelize_typescript_1.Model {
    get date() {
        const theDate = this.getDataValue('date');
        return moment_1.default(theDate).format('YYYY-MM-DD HH:mm:ss');
    }
};
__decorate([
    sequelize_typescript_1.ForeignKey(() => merchant_model_1.Merchant),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], LogStock.prototype, "merchantId", void 0);
__decorate([
    sequelize_typescript_1.ForeignKey(() => produk_model_1.Produk),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], LogStock.prototype, "productId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => merchant_model_1.Merchant),
    __metadata("design:type", Array)
], LogStock.prototype, "merchant", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => produk_model_1.Produk),
    __metadata("design:type", Array)
], LogStock.prototype, "produk", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], LogStock.prototype, "lastStock", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], LogStock.prototype, "inputStock", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], LogStock.prototype, "finalStock", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], LogStock.prototype, "type", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], LogStock.prototype, "inputStockString", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], LogStock.prototype, "keterangan", void 0);
__decorate([
    sequelize_typescript_1.Column(sequelize_typescript_1.DataType.DATE),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [])
], LogStock.prototype, "date", null);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], LogStock.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], LogStock.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], LogStock.prototype, "deletedAt", void 0);
LogStock = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'LogStock',
        timestamps: true,
        paranoid: true,
    })
], LogStock);
exports.LogStock = LogStock;
//# sourceMappingURL=stok.model.js.map