"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const responseHandler_1 = require("../../../core/helpers/responseHandler");
const produk_repository_1 = require("../produk/produk.repository");
const stok_repository_1 = require("./stok.repository");
class StockAPIController {
}
exports.default = StockAPIController;
StockAPIController.create = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const lastStock = yield stok_repository_1.LogStockRepository.getLastStock(req.body.productId);
        const finalStock = (req.body.type === 'IN') ? Number(lastStock.stock) + Number(req.body.inputStock) : Number(lastStock.stock) - Number(req.body.inputStock);
        const keterangan = (req.body.type === 'IN') ? 'Penambahan Stok' : 'Pengurangan Stok';
        const inputStockString = (req.body.type === 'IN') ? `+${req.body.inputStock}` : `-${req.body.inputStock}`;
        const date = moment_1.default().format('YYYY-MM-DD HH:mm:ss');
        const dataToDatabase = {
            merchantId: req.body.merchantId,
            productId: req.body.productId,
            lastStock: lastStock.stock,
            inputStock: req.body.inputStock,
            finalStock,
            type: req.body.type,
            keterangan,
            inputStockString,
            date,
        };
        const create = yield stok_repository_1.LogStockRepository.create(dataToDatabase);
        if (create) {
            const updatedStock = yield stok_repository_1.LogStockRepository.updateStock(req.body.productId, finalStock);
            return (Number(finalStock) === Number(updatedStock.stock)) ? responseHandler_1.ResponseHandler.jsonSuccess(res, create) : responseHandler_1.ResponseHandler.jsonError(res, 'Terjadi miskalkulasi pada perhitungan stok');
        }
        else {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Data stok gagal ditambahkan');
        }
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
StockAPIController.update = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const lastStock = yield stok_repository_1.LogStockRepository.getLastStock(req.body.productId);
        const type = (Number(req.body.stock) < Number(lastStock.stock)) ? 'OUT' : 'IN';
        const inputStock = (Number(req.body.stock) < Number(lastStock.stock)) ? Number(lastStock.stock) - Number(req.body.stock) : Number(req.body.stock) - Number(lastStock.stock);
        const finalStock = Number(req.body.stock);
        const date = moment_1.default().format('YYYY-MM-DD HH:mm:ss');
        const keterangan = (type === 'IN') ? 'Pertambahan Stok' : 'Pengurangan Stok';
        const inputStockString = (type === 'IN') ? `+${inputStock}` : `-${inputStock}`;
        const dataToDatabase = {
            merchantId: req.body.merchantId,
            productId: req.body.productId,
            lastStock: lastStock.stock,
            inputStock,
            finalStock,
            type,
            keterangan,
            inputStockString,
            date,
        };
        const create = yield stok_repository_1.LogStockRepository.create(dataToDatabase);
        if (create) {
            const updatedStock = yield stok_repository_1.LogStockRepository.updateStock(req.body.productId, finalStock);
            return (Number(finalStock) === Number(updatedStock.stock)) ? responseHandler_1.ResponseHandler.jsonSuccess(res, create) : responseHandler_1.ResponseHandler.jsonError(res, 'Terjadi miskalkulasi pada perhitungan stok');
        }
        else {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Data stok gagal ditambahkan');
        }
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
StockAPIController.riwayat = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield stok_repository_1.LogStockRepository.getByProductId(req.query.productId);
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Produk tidak ditemukan atau belum memiliki riwayat stok');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
StockAPIController.updateMinimumStock = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const updatedStock = yield stok_repository_1.LogStockRepository.updateMinimumStock(req.body.productId, req.body.minimumStock);
        return (updatedStock) ? responseHandler_1.ResponseHandler.jsonSuccess(res, updatedStock) : responseHandler_1.ResponseHandler.jsonError(res, 'Gagal mengubah minimum stok produk');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
StockAPIController.detailStok = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const productId = (req.query.productId) ? req.query.productId : 0;
        const data = yield stok_repository_1.LogStockRepository.detailStok(Number(productId));
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Detail produk stok tidak tersedia');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
StockAPIController.updateDetail = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const lastStock = yield stok_repository_1.LogStockRepository.getLastStock(req.body.productId);
        const type = (Number(req.body.stock) < Number(lastStock.stock)) ? 'OUT' : 'IN';
        const inputStock = (Number(req.body.stock) < Number(lastStock.stock)) ? Number(lastStock.stock) - Number(req.body.stock) : Number(req.body.stock) - Number(lastStock.stock);
        const finalStock = Number(req.body.stock);
        const date = moment_1.default().format('YYYY-MM-DD HH:mm:ss');
        const keterangan = (type === 'IN') ? 'Pertambahan Stok' : 'Pengurangan Stok';
        const inputStockString = (type === 'IN') ? `+${inputStock}` : `-${inputStock}`;
        const dataToDatabase = {
            merchantId: req.body.merchantId,
            productId: req.body.productId,
            lastStock: lastStock.stock,
            inputStock,
            finalStock,
            type,
            keterangan,
            inputStockString,
            date,
        };
        const create = yield stok_repository_1.LogStockRepository.create(dataToDatabase);
        if (create) {
            const updatedStock = yield stok_repository_1.LogStockRepository.updateStock(req.body.productId, finalStock);
            yield stok_repository_1.LogStockRepository.updateMinimumStock(req.body.productId, req.body.minimumStock);
            yield produk_repository_1.ProdukRepository.updateTypeProduct(Number(req.body.productId), Number(req.body.preorder));
            const data = yield stok_repository_1.LogStockRepository.detailStok(Number(req.body.productId));
            return (Number(finalStock) === Number(updatedStock.stock)) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Terjadi miskalkulasi pada perhitungan stok');
        }
        else {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Data stok gagal diubah');
        }
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=stok.api.controller.js.map