"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogStockRepository = void 0;
const sequelize_1 = require("sequelize");
const produk_model_1 = require("../produk/produk.model");
const stok_model_1 = require("./stok.model");
class LogStockRepository {
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const create = yield stok_model_1.LogStock.create(data);
            return create;
        });
    }
    static calculate(productId, type) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield stok_model_1.LogStock.findAll({
                attributes: [
                    [sequelize_1.Sequelize.fn('SUM', sequelize_1.Sequelize.col('inputStock')), 'total'],
                ],
                where: {
                    [sequelize_1.Op.and]: {
                        productId,
                        type,
                    },
                },
                group: [
                    productId,
                ],
            });
            return data;
        });
    }
    static getLastStock(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield produk_model_1.Produk.findOne({ where: { id } });
            return data;
        });
    }
    static updateStock(id, stock) {
        return __awaiter(this, void 0, void 0, function* () {
            yield produk_model_1.Produk.update({ stock }, { where: { id } });
            const updated = yield produk_model_1.Produk.findByPk(id);
            return updated;
        });
    }
    static updateMinimumStock(id, minimumStock) {
        return __awaiter(this, void 0, void 0, function* () {
            yield produk_model_1.Produk.update({ minimumStock }, { where: { id } });
            const updated = yield produk_model_1.Produk.findByPk(id);
            return updated;
        });
    }
    static getByProductId(productId) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield stok_model_1.LogStock.findAll({ where: { productId } });
            return data;
        });
    }
    static detailStok(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield produk_model_1.Produk.findOne({
                attributes: [
                    'id', 'merchantId', 'name', 'price', 'stock', 'minimumStock', 'type',
                    [sequelize_1.Sequelize.literal(`(CASE WHEN type = 'PREORDER' THEN true ELSE false END)`), 'isPreorder'],
                ],
                where: { id },
                logging: console.log,
            });
            return data;
        });
    }
}
exports.LogStockRepository = LogStockRepository;
//# sourceMappingURL=stok.repository.js.map