"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentMethod = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
let PaymentMethod = class PaymentMethod extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaymentMethod.prototype, "paymentType", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaymentMethod.prototype, "merchantCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaymentMethod.prototype, "channel", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaymentMethod.prototype, "label", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaymentMethod.prototype, "icon", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaymentMethod.prototype, "costType", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaymentMethod.prototype, "cost", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PaymentMethod.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PaymentMethod.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PaymentMethod.prototype, "deletedAt", void 0);
PaymentMethod = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'Payment',
        timestamps: true,
        paranoid: true,
    })
], PaymentMethod);
exports.PaymentMethod = PaymentMethod;
//# sourceMappingURL=paymentMethod.model.js.map