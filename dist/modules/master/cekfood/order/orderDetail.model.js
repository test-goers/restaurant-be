"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderDetail = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const produk_model_1 = require("../produk/produk.model");
const order_model_1 = require("./order.model");
let OrderDetail = class OrderDetail extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.ForeignKey(() => order_model_1.Order),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], OrderDetail.prototype, "orderId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => order_model_1.Order),
    __metadata("design:type", Array)
], OrderDetail.prototype, "order", void 0);
__decorate([
    sequelize_typescript_1.ForeignKey(() => produk_model_1.Produk),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], OrderDetail.prototype, "productId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => produk_model_1.Produk),
    __metadata("design:type", Array)
], OrderDetail.prototype, "produk", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], OrderDetail.prototype, "price", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], OrderDetail.prototype, "discount", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], OrderDetail.prototype, "finalPrice", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], OrderDetail.prototype, "amount", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], OrderDetail.prototype, "productType", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], OrderDetail.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], OrderDetail.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], OrderDetail.prototype, "deletedAt", void 0);
OrderDetail = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'OrderDetail',
        timestamps: true,
        paranoid: true,
    })
], OrderDetail);
exports.OrderDetail = OrderDetail;
//# sourceMappingURL=orderDetail.model.js.map