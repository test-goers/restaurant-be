"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Order = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const transaksi_model_1 = require("../transaksi/transaksi.model");
const orderDetail_model_1 = require("./orderDetail.model");
let Order = class Order extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.ForeignKey(() => transaksi_model_1.Transaksi),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Order.prototype, "transaksiId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => transaksi_model_1.Transaksi),
    __metadata("design:type", Array)
], Order.prototype, "transaksi", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Order.prototype, "buyerName", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Order.prototype, "buyerAlamat", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Order.prototype, "tanggalDiterima", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Order.prototype, "buyerNote", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Order.prototype, "merchantNote", void 0);
__decorate([
    sequelize_typescript_1.HasMany(() => orderDetail_model_1.OrderDetail),
    __metadata("design:type", Array)
], Order.prototype, "orderDetail", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Order.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Order.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Order.prototype, "deletedAt", void 0);
Order = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'Orders',
        timestamps: true,
        paranoid: true,
    })
], Order);
exports.Order = Order;
//# sourceMappingURL=order.model.js.map