"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const campaign_repository_1 = require("../../campaign/campaign.repository");
const redemption_repository_1 = require("../../redemption/redemption.repository");
const voucher_repository_1 = require("../../voucher/voucher.repository");
class DashboardCekfoodController {
}
exports.default = DashboardCekfoodController;
DashboardCekfoodController.index = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const totalVoucher = yield voucher_repository_1.VoucherRepository.getTotal();
    const totalRedemption = yield redemption_repository_1.RedemptionRepository.getTotal();
    const totalCampaign = yield campaign_repository_1.CampaignRepository.getTotal();
    const lastUpdate = moment_1.default().format('DD-MM-YYYY HH:MM:SS');
    return res.render('home.pug', { totalVoucher, totalRedemption, lastUpdate, totalCampaign });
});
//# sourceMappingURL=dashboard.controller.js.map