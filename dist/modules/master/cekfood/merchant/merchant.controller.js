"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const redirectBack_1 = require("../../../core/helpers/redirectBack");
const fileUpload_service_1 = require("../file/fileUpload.service");
const merchant_repository_1 = require("./merchant.repository");
const merchant_schema_1 = require("./merchant.schema");
class MerchantController {
}
exports.default = MerchantController;
MerchantController.index = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield merchant_repository_1.MerchantRepository.getList();
    return res.render('pages/master/merchant/index.pug', { data });
});
MerchantController.create = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const old = req.flash('val')[0];
    return res.render('pages/master/merchant/create.pug', { old });
});
MerchantController.show = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield merchant_repository_1.MerchantRepository.get(req.params.merchantId);
    return res.render('pages/master/merchant/show.pug', { data });
});
MerchantController.json = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const search = (req.query.search) ? req.query.search : '';
    const limit = (req.query.limit) ? Number(req.query.limit) : null;
    const offset = (req.query.offset) ? Number(req.query.offset) : 0;
    const order = (req.query.order) ? req.query.order : 'asc';
    const merchant = (req.query.merchant) ? req.query.merchant : '';
    const data = yield merchant_repository_1.MerchantRepository.dataTable(search, merchant, order, offset, limit);
    res.json({
        rows: data.rows,
        total: data.count,
    });
});
MerchantController.store = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const checkUsername = yield merchant_repository_1.MerchantRepository.checkUsername(req.body.username, null);
        if (checkUsername) {
            throw new Error('Username is already exist');
        }
        req.body.logoId = (req.logo) ? req.logo.id : '';
        req.body.status = 'BUKA';
        req.body.password = yield bcrypt_1.default.hash('1234', 10);
        const { error, value } = merchant_schema_1.MerchantSchema.validate(req.body);
        if (redirectBack_1.redirectBackIfError(error, req, res, value)) {
            return;
        }
        yield merchant_repository_1.MerchantRepository.create(value);
        req.flash(`success`, `Merchant added successfully`);
        res.redirect(`/merchant`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
MerchantController.edit = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield merchant_repository_1.MerchantRepository.get(req.params.merchantId);
    const old = req.flash('val')[0];
    return res.render('pages/master/merchant/edit.pug', { old, data });
});
MerchantController.update = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const checkUsername = yield merchant_repository_1.MerchantRepository.checkUsername(req.body.username, req.body.merchantId);
        if (checkUsername) {
            throw new Error('Username is already exist');
        }
        const dataLogo = yield merchant_repository_1.MerchantRepository.get(req.body.merchantId);
        req.body.logoId = (yield (req.logo)) ? req.logo.id : dataLogo.logoId;
        if (Object.keys(req.logo).length > 0) {
            yield fileUpload_service_1.FileUploadService.removeFile(dataLogo.logoId);
        }
        const { error, value } = merchant_schema_1.MerchantSchemaUpdate.validate(req.body);
        if (redirectBack_1.redirectBackIfError(error, req, res, value)) {
            return;
        }
        yield merchant_repository_1.MerchantRepository.update(value, req.body.merchantId);
        req.flash(`success`, `Merchant updated successfully`);
        res.redirect(`/merchant`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
MerchantController.delete = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield merchant_repository_1.MerchantRepository.destroy(req.query.id);
        req.flash(`success`, `Merchant deleted successfully`);
        return res.redirect('/merchant');
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
//# sourceMappingURL=merchant.controller.js.map