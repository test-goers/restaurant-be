"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantRepository = void 0;
const sequelize_1 = require("sequelize");
const sequelize_typescript_1 = require("sequelize-typescript");
const city_model_1 = require("../../city/city.model");
const district_model_1 = require("../../district/district.model");
const province_model_1 = require("../../province/province.model");
const village_model_1 = require("../../village/village.model");
const fileUpload_model_1 = require("../file/fileUpload.model");
const produk_model_1 = require("../produk/produk.model");
const merchant_model_1 = require("./merchant.model");
class MerchantRepository {
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield merchant_model_1.Merchant.findAll({
                attributes: { exclude: ['createdAt', 'updatedAt', 'deletedAt', 'password'] },
                include: [
                    { model: village_model_1.Village },
                    { model: district_model_1.District },
                    { model: city_model_1.City },
                    { model: province_model_1.Province },
                    { model: fileUpload_model_1.FileUpload },
                ],
                order: [
                    ['name', 'ASC'],
                ],
            });
            return data;
        });
    }
    static get(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield merchant_model_1.Merchant.findOne({
                include: [
                    { model: village_model_1.Village },
                    { model: district_model_1.District },
                    { model: city_model_1.City },
                    { model: province_model_1.Province },
                    { model: fileUpload_model_1.FileUpload },
                ],
                where: { id },
            });
            return data;
        });
    }
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const create = yield merchant_model_1.Merchant.create(data);
            return create;
        });
    }
    static update(data, id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield merchant_model_1.Merchant.update(data, { where: { id } });
            const updatedData = merchant_model_1.Merchant.findByPk(id);
            return updatedData;
        });
    }
    static destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield merchant_model_1.Merchant.destroy({
                where: { id },
            });
        });
    }
    static checkUsername(username, id = null) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield merchant_model_1.Merchant.findOne({
                where: {
                    username,
                    [sequelize_1.Op.and]: {
                        id: (id !== null) ? { [sequelize_1.Op.notIn]: [id] } : { [sequelize_1.Op.notIn]: [] },
                    },
                },
            });
            return data;
        });
    }
    static dataTable(search = '', merchant, order = 'asc', offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield merchant_model_1.Merchant.findAndCountAll({
                attributes: [
                    'id', 'name', 'nomorHp', 'namaPemilik', 'status', 'username',
                    [sequelize_typescript_1.Sequelize.fn('concat', sequelize_typescript_1.Sequelize.col('latitude'), ',', sequelize_typescript_1.Sequelize.col('longitude')), 'lokasi'],
                ],
                where: {
                    [sequelize_1.Op.and]: {
                        id: (merchant === '') ? { [sequelize_1.Op.ne]: null } : merchant,
                        [sequelize_1.Op.or]: {
                            name: { [sequelize_1.Op.substring]: `%${search}%` },
                            nomorHp: { [sequelize_1.Op.substring]: `%${search}%` },
                            kelurahan: { [sequelize_1.Op.substring]: `%${search}%` },
                            kota: { [sequelize_1.Op.substring]: `%${search}%` },
                            namaPemilik: { [sequelize_1.Op.substring]: `%${search}%` },
                            username: { [sequelize_1.Op.substring]: `%${search}%` },
                        },
                    },
                },
                order: [
                    ['createdAt', order],
                ],
                offset: (offset !== '' && offset != null) ? offset : null,
                limit,
            });
            return data;
        });
    }
    static detail(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield merchant_model_1.Merchant.findOne({
                attributes: { exclude: ['createdAt', 'updatedAt', 'deletedAt', 'password'] },
                include: [
                    { model: village_model_1.Village },
                    { model: district_model_1.District },
                    { model: city_model_1.City },
                    { model: province_model_1.Province },
                    { model: fileUpload_model_1.FileUpload },
                ],
                where: { id },
            });
            return data;
        });
    }
    static listProduk(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield merchant_model_1.Merchant.findOne({
                attributes: { include: ['id', 'name', 'alamat', 'jamBuka', 'jamTutup', 'status'] },
                include: [
                    { model: produk_model_1.Produk, include: [{ model: fileUpload_model_1.FileUpload }] },
                    { model: fileUpload_model_1.FileUpload },
                ],
                where: { id },
            });
            return data;
        });
    }
    static getTotal() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield merchant_model_1.Merchant.count();
            return data;
        });
    }
    static terdekat(lat, lng, limit, offset) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield merchant_model_1.Merchant.findAll({
                attributes: [
                    [sequelize_typescript_1.Sequelize.literal(`@km := ROUND((6371 * acos( cos( radians(latitude) ) 
               * cos( radians(${lat}) ) 
               * cos( radians(${lng}) - radians(longitude)) + sin(radians(latitude)) 
               * sin( radians(${lat})))
          ), 3)`), 'distance'],
                    [sequelize_typescript_1.Sequelize.literal('ROUND(((@km * 1000) * 1.4) / 60)'), 'time'],
                    'name',
                    'alamat',
                    'id',
                    'status',
                ],
                where: {
                    [sequelize_1.Op.and]: [
                        sequelize_typescript_1.Sequelize.literal(`ROUND((6371 * acos( cos( radians(latitude) ) 
               * cos( radians(${lat}) ) 
               * cos( radians(${lng}) - radians(longitude)) + sin(radians(latitude)) 
               * sin( radians(${lat})))
          ), 3) < 1.0`),
                    ],
                },
                include: [
                    { model: fileUpload_model_1.FileUpload },
                ],
                order: [
                    ['status', 'ASC'],
                ],
                limit,
                offset,
                logging: console.log,
            });
            return data;
        });
    }
    static getList() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield merchant_model_1.Merchant.findAll({
                order: [
                    ['name', 'ASC'],
                    ['status', 'ASC'],
                ],
            });
            return data;
        });
    }
}
exports.MerchantRepository = MerchantRepository;
//# sourceMappingURL=merchant.repository.js.map