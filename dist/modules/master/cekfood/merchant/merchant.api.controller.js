"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseHandler_1 = require("../../../core/helpers/responseHandler");
const merchant_repository_1 = require("./merchant.repository");
class MerchantAPIController {
}
exports.default = MerchantAPIController;
MerchantAPIController.getAll = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield merchant_repository_1.MerchantRepository.getAll();
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Data merchant belum tersedia');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
MerchantAPIController.detail = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield merchant_repository_1.MerchantRepository.detail(req.query.merchantId);
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Data merchant belum tersedia');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
MerchantAPIController.listProduk = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield merchant_repository_1.MerchantRepository.listProduk(req.query.merchantId);
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Data merchant belum tersedia');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
MerchantAPIController.terdekat = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const limit = (req.query.limit) ? req.query.limit : 10;
        const offset = (req.query.offset) ? req.query.offset : 0;
        const lat = (req.query.lat) ? req.query.lat : '';
        const lon = (req.query.lon) ? req.query.lon : '';
        const result = yield merchant_repository_1.MerchantRepository.terdekat(lat, lon, Number(limit), Number(offset));
        console.log(result);
        const data = {
            total: result.length,
            data: result,
            limit,
            offset,
        };
        return (result.length > 0) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Tidak ada merchant dalam radius 1KM');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=merchant.api.controller.js.map