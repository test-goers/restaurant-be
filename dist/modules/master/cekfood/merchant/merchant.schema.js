"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantSchemaUpdate = exports.MerchantSchema = void 0;
const typesafe_joi_1 = __importDefault(require("typesafe-joi"));
exports.MerchantSchema = typesafe_joi_1.default.object({
    namePIC: typesafe_joi_1.default.string().required(),
    nomorHpPIC: typesafe_joi_1.default.string().required(),
    name: typesafe_joi_1.default.string().required(),
    nomorHp: typesafe_joi_1.default.string().required(),
    logoId: typesafe_joi_1.default.number().required(),
    alamat: typesafe_joi_1.default.string().required(),
    kelurahan: typesafe_joi_1.default.string().required(),
    kecamatan: typesafe_joi_1.default.string().required(),
    kota: typesafe_joi_1.default.string().required(),
    provinsi: typesafe_joi_1.default.string().required(),
    latitude: typesafe_joi_1.default.string().required(),
    longitude: typesafe_joi_1.default.string().required(),
    jamBuka: typesafe_joi_1.default.string().required(),
    jamTutup: typesafe_joi_1.default.string().required(),
    username: typesafe_joi_1.default.string().required(),
    namaPemilik: typesafe_joi_1.default.string().required(),
    nomorHpPemilik: typesafe_joi_1.default.string().required(),
    alamatPemilik: typesafe_joi_1.default.string().required(),
    email: typesafe_joi_1.default.string().allow(''),
    facebook: typesafe_joi_1.default.string().allow(''),
    instagram: typesafe_joi_1.default.string().allow(''),
    password: typesafe_joi_1.default.string().required(),
    status: typesafe_joi_1.default.string().required(),
});
exports.MerchantSchemaUpdate = typesafe_joi_1.default.object({
    merchantId: typesafe_joi_1.default.string().required(),
    namePIC: typesafe_joi_1.default.string().required(),
    nomorHpPIC: typesafe_joi_1.default.string().required(),
    name: typesafe_joi_1.default.string().required(),
    nomorHp: typesafe_joi_1.default.string().required(),
    logoId: typesafe_joi_1.default.number(),
    alamat: typesafe_joi_1.default.string().required(),
    kelurahan: typesafe_joi_1.default.string().required(),
    kecamatan: typesafe_joi_1.default.string().required(),
    kota: typesafe_joi_1.default.string().required(),
    provinsi: typesafe_joi_1.default.string().required(),
    latitude: typesafe_joi_1.default.string().required(),
    longitude: typesafe_joi_1.default.string().required(),
    jamBuka: typesafe_joi_1.default.string().required(),
    jamTutup: typesafe_joi_1.default.string().required(),
    username: typesafe_joi_1.default.string().required(),
    namaPemilik: typesafe_joi_1.default.string().required(),
    nomorHpPemilik: typesafe_joi_1.default.string().required(),
    alamatPemilik: typesafe_joi_1.default.string().required(),
    email: typesafe_joi_1.default.string().allow(''),
    facebook: typesafe_joi_1.default.string().allow(''),
    instagram: typesafe_joi_1.default.string().allow(''),
});
//# sourceMappingURL=merchant.schema.js.map