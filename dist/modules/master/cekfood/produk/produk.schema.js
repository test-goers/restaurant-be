"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductSchemaUpdate = exports.ProductSchema = void 0;
const typesafe_joi_1 = __importDefault(require("typesafe-joi"));
exports.ProductSchema = typesafe_joi_1.default.object({
    merchantId: typesafe_joi_1.default.string().required(),
    name: typesafe_joi_1.default.string().required(),
    description: typesafe_joi_1.default.string(),
    categoryId: typesafe_joi_1.default.string().required(),
    pictureId: typesafe_joi_1.default.number().required(),
    price: typesafe_joi_1.default.string().required(),
    stock: typesafe_joi_1.default.string().required(),
    minimumStock: typesafe_joi_1.default.string().required(),
    type: typesafe_joi_1.default.string().required(),
});
exports.ProductSchemaUpdate = typesafe_joi_1.default.object({
    productId: typesafe_joi_1.default.string().required(),
    merchantId: typesafe_joi_1.default.string().required(),
    name: typesafe_joi_1.default.string().required(),
    description: typesafe_joi_1.default.string(),
    categoryId: typesafe_joi_1.default.string().required(),
    pictureId: typesafe_joi_1.default.number(),
    price: typesafe_joi_1.default.string().required(),
    stock: typesafe_joi_1.default.string().required(),
    minimumStock: typesafe_joi_1.default.string().required(),
    type: typesafe_joi_1.default.string().required(),
});
//# sourceMappingURL=produk.schema.js.map