"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseHandler_1 = require("../../../core/helpers/responseHandler");
const produk_repository_1 = require("./produk.repository");
class ProdukAPIController {
}
exports.default = ProdukAPIController;
ProdukAPIController.listProdukMerchant = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const merchantId = (req.query.merchantId) ? req.query.merchantId : '';
        const filter = (req.query.filter) ? req.query.filter : '';
        const order = (req.query.order) ? req.query.order : '';
        const menipis = (req.query.menipis) ? req.query.menipis : '';
        const data = yield produk_repository_1.ProdukRepository.getListProdukMerchant(merchantId, filter, order, menipis);
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Data produk tidak tersedia');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
ProdukAPIController.updateHarga = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield produk_repository_1.ProdukRepository.updateHarga(req.body.productId, req.body.harga);
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Data produk tidak tersedia');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
ProdukAPIController.search = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const lat = (req.query.lat) ? req.query.lat : '';
        const lon = (req.query.lon) ? req.query.lon : '';
        const search = (req.query.search) ? req.query.search : '';
        const limit = (req.query.limit) ? req.query.limit : 10;
        const offset = (req.query.offset) ? req.query.offset : 0;
        const result = yield produk_repository_1.ProdukRepository.search(search, lat, lon, Number(limit), Number(offset));
        const data = {
            total: result.length,
            data: result,
            limit,
            offset,
        };
        return (result.length > 0) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, `Pencarian ${search} tidak ditemukan dalam radius 1KM`);
    }
    catch (error) {
        console.log(error);
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=produk.api.controller.js.map