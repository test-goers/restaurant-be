"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Produk = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const fileUpload_model_1 = require("../file/fileUpload.model");
const kategoriProduk_model_1 = require("../kategori/kategoriProduk.model");
const merchant_model_1 = require("../merchant/merchant.model");
let Produk = class Produk extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.ForeignKey(() => merchant_model_1.Merchant),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Produk.prototype, "merchantId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => merchant_model_1.Merchant),
    __metadata("design:type", Array)
], Produk.prototype, "merchant", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Produk.prototype, "name", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Produk.prototype, "description", void 0);
__decorate([
    sequelize_typescript_1.ForeignKey(() => kategoriProduk_model_1.KategoriProduk),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Produk.prototype, "categoryId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => kategoriProduk_model_1.KategoriProduk),
    __metadata("design:type", Array)
], Produk.prototype, "category", void 0);
__decorate([
    sequelize_typescript_1.ForeignKey(() => fileUpload_model_1.FileUpload),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Produk.prototype, "pictureId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => fileUpload_model_1.FileUpload),
    __metadata("design:type", Array)
], Produk.prototype, "image", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Produk.prototype, "price", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Produk.prototype, "stock", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Produk.prototype, "minimumStock", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Produk.prototype, "type", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Produk.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Produk.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Produk.prototype, "deletedAt", void 0);
Produk = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'Product',
        timestamps: true,
        paranoid: true,
    })
], Produk);
exports.Produk = Produk;
//# sourceMappingURL=produk.model.js.map