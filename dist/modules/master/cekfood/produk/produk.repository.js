"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProdukRepository = void 0;
const sequelize_1 = require("sequelize");
const fileUpload_model_1 = require("../file/fileUpload.model");
const kategoriProduk_model_1 = require("../kategori/kategoriProduk.model");
const merchant_model_1 = require("../merchant/merchant.model");
const produk_model_1 = require("./produk.model");
class ProdukRepository {
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield produk_model_1.Produk.findAll({
                include: [
                    { model: fileUpload_model_1.FileUpload },
                ],
                order: [
                    ['createdAt', 'DESC'],
                ],
            });
            return data;
        });
    }
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const create = yield produk_model_1.Produk.create(data);
            return create;
        });
    }
    static update(data, id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield produk_model_1.Produk.update(data, { where: { id } });
            const produk = yield produk_model_1.Produk.findByPk(id);
            return produk;
        });
    }
    static get(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const produk = yield produk_model_1.Produk.findByPk(id, {
                include: [
                    { model: fileUpload_model_1.FileUpload },
                ],
            });
            return produk;
        });
    }
    static dataTable(search = '', merchant, category, order = 'asc', offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield produk_model_1.Produk.findAndCountAll({
                include: [
                    { model: kategoriProduk_model_1.KategoriProduk },
                ],
                where: {
                    [sequelize_1.Op.and]: {
                        merchantId: (merchant === '') ? { [sequelize_1.Op.ne]: null } : merchant,
                        categoryId: (category === '') ? { [sequelize_1.Op.ne]: null } : category,
                        [sequelize_1.Op.or]: {
                            name: { [sequelize_1.Op.substring]: `%${search}%` },
                            type: { [sequelize_1.Op.substring]: `%${search}%` },
                        },
                    },
                },
                order: [
                    ['createdAt', order],
                ],
                offset: (offset !== '' && offset != null) ? offset : null,
                limit,
            });
            return data;
        });
    }
    static destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield produk_model_1.Produk.destroy({
                where: { id },
            });
        });
    }
    static getListProdukMerchant(merchantId, filter = 'name', order = 'asc', menipis = '') {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield produk_model_1.Produk.findAndCountAll({
                where: {
                    [sequelize_1.Op.and]: {
                        merchantId,
                        stock: (menipis !== null && menipis !== '') ? { [sequelize_1.Op.lte]: sequelize_1.Sequelize.col('minimumStock') } : { [sequelize_1.Op.gte]: 0 },
                    },
                },
                order: [
                    [filter, order],
                ],
                include: [{ model: fileUpload_model_1.FileUpload }],
            });
            return data;
        });
    }
    static updateHarga(id, price) {
        return __awaiter(this, void 0, void 0, function* () {
            yield produk_model_1.Produk.update({ price }, { where: { id } });
            const updatedData = yield produk_model_1.Produk.findByPk(id);
            return updatedData;
        });
    }
    static getTotal() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield produk_model_1.Produk.count();
            return data;
        });
    }
    static search(search, lat, lng, limit, offset) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield merchant_model_1.Merchant.findAll({
                attributes: [
                    'id',
                    'name',
                    'alamat',
                    'status',
                    'isPreOrder',
                    'isExpress',
                    'status',
                    [sequelize_1.Sequelize.literal(`@km := ROUND((6371 * acos( cos( radians(latitude) ) 
               * cos( radians(${lat}) ) 
               * cos( radians(${lng}) - radians(longitude)) + sin(radians(latitude)) 
               * sin( radians(${lat})))
          ), 3)`), 'distance'],
                    [sequelize_1.Sequelize.literal('ROUND(((@km * 1000) * 1.4) / 60)'), 'time'],
                ],
                include: [
                    { model: fileUpload_model_1.FileUpload },
                    {
                        model: produk_model_1.Produk,
                        attributes: ['id', 'name'],
                        where: {
                            name: { [sequelize_1.Op.substring]: `${search}` },
                        },
                    },
                ],
                where: {
                    [sequelize_1.Op.and]: [
                        sequelize_1.Sequelize.literal(`ROUND((6371 * acos( cos( radians(latitude) ) 
               * cos( radians(${lat}) ) 
               * cos( radians(${lng}) - radians(longitude)) + sin(radians(latitude)) 
               * sin( radians(${lat})))
          ), 3) < 1.0`),
                    ],
                },
                order: [
                    ['status', 'ASC'],
                ],
                limit,
                offset,
                logging: console.log,
            });
            return data;
        });
    }
    static updateTypeProduct(id, isPreorder) {
        return __awaiter(this, void 0, void 0, function* () {
            const type = (isPreorder) ? 'PREORDER' : 'INSTOCK';
            yield produk_model_1.Produk.update({ type }, { where: { id } });
        });
    }
}
exports.ProdukRepository = ProdukRepository;
//# sourceMappingURL=produk.repository.js.map