"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const redirectBack_1 = require("../../../core/helpers/redirectBack");
const responseHandler_1 = require("../../../core/helpers/responseHandler");
const fileUpload_service_1 = require("../file/fileUpload.service");
const kategoriProduk_repository_1 = require("../kategori/kategoriProduk.repository");
const merchant_repository_1 = require("../merchant/merchant.repository");
const stok_repository_1 = require("../stok/stok.repository");
const produk_repository_1 = require("./produk.repository");
const produk_schema_1 = require("./produk.schema");
class ProdukController {
}
exports.default = ProdukController;
ProdukController.index = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const categories = yield kategoriProduk_repository_1.KategoriProdukRepository.getAll();
    const merchants = yield merchant_repository_1.MerchantRepository.getList();
    return res.render('pages/master/produk/index.pug', { categories, merchants });
});
ProdukController.create = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const merchants = (yield merchant_repository_1.MerchantRepository.getAll()).map((value, index) => ({ value: value.id, text: value.name }));
    const categories = (yield kategoriProduk_repository_1.KategoriProdukRepository.getAll()).map((value, index) => ({ value: value.id, text: value.name }));
    const old = req.flash('val')[0];
    return res.render('pages/master/produk/create.pug', { old, merchants, categories });
});
ProdukController.edit = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const merchants = (yield merchant_repository_1.MerchantRepository.getAll()).map((value, index) => ({ value: value.id, text: value.name }));
    const categories = (yield kategoriProduk_repository_1.KategoriProdukRepository.getAll()).map((value, index) => ({ value: value.id, text: value.name }));
    const data = yield produk_repository_1.ProdukRepository.get(req.params.productId);
    const old = req.flash('val')[0];
    return res.render('pages/master/produk/edit.pug', { merchants, categories, old, data });
});
ProdukController.json = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const search = (req.query.search) ? req.query.search : '';
    const limit = (req.query.limit) ? Number(req.query.limit) : null;
    const offset = (req.query.offset) ? Number(req.query.offset) : 0;
    const order = (req.query.order) ? req.query.order : 'asc';
    const merchant = (req.query.merchant) ? req.query.merchant : '';
    const category = (req.query.category) ? req.query.category : '';
    const data = yield produk_repository_1.ProdukRepository.dataTable(search, merchant, category, order, offset, limit);
    res.json({
        rows: data.rows,
        total: data.count,
    });
});
ProdukController.store = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        req.body.pictureId = (req.picture) ? req.picture.id : '';
        const { error, value } = produk_schema_1.ProductSchema.validate(req.body);
        if (redirectBack_1.redirectBackIfError(error, req, res, value)) {
            return;
        }
        const produk = yield produk_repository_1.ProdukRepository.create(value);
        const logStock = {
            merchantId: produk.merchantId,
            productId: produk.id,
            lastStock: 0,
            inputStock: produk.stock,
            finalStock: produk.stock,
            inputStockString: `+${produk.stock}`,
            type: 'IN',
            keterangan: 'Penambahan Stok',
            date: moment_1.default().format('YYYY-MM-DD HH:mm:ss'),
        };
        yield stok_repository_1.LogStockRepository.create(logStock);
        req.flash(`success`, `Product added successfully`);
        res.redirect(`/produk`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
ProdukController.update = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const dataProduct = yield produk_repository_1.ProdukRepository.get(req.body.productId);
        req.body.pictureId = (yield (req.picture)) ? req.picture.id : dataProduct.pictureId;
        if (Object.keys(req.picture).length > 0) {
            yield fileUpload_service_1.FileUploadService.removeFile(dataProduct.pictureId);
        }
        const { error, value } = produk_schema_1.ProductSchemaUpdate.validate(req.body);
        if (redirectBack_1.redirectBackIfError(error, req, res, value)) {
            return;
        }
        yield produk_repository_1.ProdukRepository.update(value, req.body.productId);
        req.flash(`success`, `Product udpated successfully`);
        res.redirect(`/produk`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
ProdukController.delete = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield produk_repository_1.ProdukRepository.destroy(req.query.id);
        req.flash(`success`, `Product deleted successfully`);
        return res.redirect('/produk');
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
ProdukController.getAll = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield produk_repository_1.ProdukRepository.getAll();
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Data Produk belum tersedia');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=produk.controller.js.map