"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Transaksi = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const merchant_model_1 = require("../merchant/merchant.model");
const order_model_1 = require("../order/order.model");
let Transaksi = class Transaksi extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.ForeignKey(() => merchant_model_1.Merchant),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Transaksi.prototype, "merchantId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => merchant_model_1.Merchant),
    __metadata("design:type", Array)
], Transaksi.prototype, "merchant", void 0);
__decorate([
    sequelize_typescript_1.HasOne(() => order_model_1.Order),
    __metadata("design:type", Array)
], Transaksi.prototype, "order", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Transaksi.prototype, "nomorTransaksi", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Transaksi.prototype, "totalAmount", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Transaksi.prototype, "totalBill", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Transaksi.prototype, "paymentMethodId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Transaksi.prototype, "jenisPengiriman", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Transaksi.prototype, "jenisProduk", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Transaksi.prototype, "batasPembayaran", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Transaksi.prototype, "batasPengiriman", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Transaksi.prototype, "statusCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Transaksi.prototype, "status", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Transaksi.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Transaksi.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Transaksi.prototype, "deletedAt", void 0);
Transaksi = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'Transactions',
        timestamps: true,
        paranoid: true,
    })
], Transaksi);
exports.Transaksi = Transaksi;
//# sourceMappingURL=transaksi.model.js.map