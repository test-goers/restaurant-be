"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const responseHandler_1 = require("../../../core/helpers/responseHandler");
const order_repository_1 = require("../order/order.repository");
const orderDetail_repository_1 = require("../order/orderDetail.repository");
const status_repository_1 = require("../status/status.repository");
const transaksi_repository_1 = require("./transaksi.repository");
class TransaksiAPIController {
}
exports.default = TransaksiAPIController;
TransaksiAPIController.getAll = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield transaksi_repository_1.TransaksiRepository.getAll();
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Data transaksi belum tersedia');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
TransaksiAPIController.getByMerchantId = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const statusCode = (req.query.statusCode) ? String(req.query.statusCode).toUpperCase() : '';
        const merchantId = (req.query.merchantId) ? req.query.merchantId : 0;
        const data = yield transaksi_repository_1.TransaksiRepository.getByMerchantId(merchantId, statusCode);
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Data transaksi tidak tersedia');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
TransaksiAPIController.create = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const nomorTransaksi = yield transaksi_repository_1.TransaksiRepository.generateNomorTransaksi(req.body.jenisProduk);
        const batasPembayaran = moment_1.default().add(30, 'minutes').format('YYYY-MM-DD');
        const batasPengiriman = (req.body.jenisProduk === 'PREORDER') ? moment_1.default(batasPembayaran).add(7, 'days').format('YYYY-MM-DD') : moment_1.default(batasPembayaran).endOf('day').format('YYYY-MM-DD');
        const dataToDatabase = {
            merchantId: req.body.merchantId,
            nomorTransaksi,
            totalAmount: req.body.totalAmount,
            totalBill: req.body.totalBill,
            jenisPengiriman: req.body.jenisPengiriman,
            jenisProduk: req.body.jenisProduk,
            statusCode: 'BARU',
            status: 'Pesanan Baru',
            paymentMethodId: req.body.paymentMethod,
            batasPembayaran,
            batasPengiriman,
        };
        const transaksi = yield transaksi_repository_1.TransaksiRepository.create(dataToDatabase);
        const dataOrder = {
            transaksiId: transaksi.id,
            buyerName: req.body.namaPembeli,
            buyerAlamat: req.body.alamatPembeli,
            buyerNote: req.body.catatanPembeli,
            merchantNote: '',
            tanggalDiterima: null,
        };
        const order = yield order_repository_1.OrderRepository.create(dataOrder);
        for (const iterasi of req.body.produk) {
            const orderDetail = {
                orderId: order.id,
                productId: req.body.produk[iterasi].produkId,
                price: req.body.produk[iterasi].harga,
                discount: req.body.produk[iterasi].discount,
                finalPrice: Number(req.body.produk[iterasi].harga) - (Number(req.body.produk[iterasi].harga) * Number(req.body.produk[iterasi].discount)),
                amount: req.body.produk[iterasi].amount,
                productType: 'BARU',
            };
            yield orderDetail_repository_1.OrderDetailRepository.create(orderDetail);
        }
        return (order) ? responseHandler_1.ResponseHandler.jsonSuccess(res, dataToDatabase) : responseHandler_1.ResponseHandler.jsonError(res, 'Gagal membuat pemesanan');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
TransaksiAPIController.updateStatus = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const checkTransaksi = transaksi_repository_1.TransaksiRepository.getDetailByNomorTransaksi(req.body.nomorTransaksi);
        if (!checkTransaksi) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi tidak ditemukan');
        }
        const dataStatus = yield status_repository_1.StatusRepository.getByCode(req.body.statusCode);
        if (!dataStatus) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Kode status tidak valid');
        }
        const dataUpdate = {
            status: dataStatus.status,
            statusCode: dataStatus.code,
        };
        const updateStatus = yield transaksi_repository_1.TransaksiRepository.updateStatus(dataUpdate, req.body.nomorTransaksi);
        return (updateStatus) ? responseHandler_1.ResponseHandler.jsonSuccess(res, updateStatus) : responseHandler_1.ResponseHandler.serverError(res, 'Update status transaksi gagal');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
TransaksiAPIController.detail = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const detailTransaksi = yield transaksi_repository_1.TransaksiRepository.getDetailByNomorTransaksi(req.query.nomorTransaksi);
        if (!detailTransaksi) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi tidak ditemukan');
        }
        return responseHandler_1.ResponseHandler.jsonSuccess(res, detailTransaksi);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=transaksi.api.controller.js.map