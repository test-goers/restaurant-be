"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransaksiRepository = void 0;
const moment_1 = __importDefault(require("moment"));
const sequelize_1 = require("sequelize");
const sequelize_2 = require("sequelize");
const merchant_model_1 = require("../merchant/merchant.model");
const order_model_1 = require("../order/order.model");
const transaksi_model_1 = require("./transaksi.model");
class TransaksiRepository {
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield transaksi_model_1.Transaksi.findAll({});
            return data;
        });
    }
    static getByMerchantId(merchantId, statusCode) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield transaksi_model_1.Transaksi.findAndCountAll({
                where: {
                    merchantId,
                    statusCode: (statusCode) ? { [sequelize_2.Op.eq]: statusCode } : { [sequelize_2.Op.not]: null },
                },
            });
            return data;
        });
    }
    static getDetailByNomorTransaksi(nomorTransaksi) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield transaksi_model_1.Transaksi.findOne({
                include: [
                    { model: order_model_1.Order },
                    { model: merchant_model_1.Merchant },
                ],
                where: { nomorTransaksi },
            });
            return data;
        });
    }
    static generateNomorTransaksi(tipeProduk) {
        return __awaiter(this, void 0, void 0, function* () {
            const year = moment_1.default().format('YYYY');
            const month = moment_1.default().format('MM');
            const day = moment_1.default().format('DD');
            const label = (tipeProduk === 'PREORDER') ? 'PO' : 'RS';
            let nomorTransaksi = `${label}${year}${month}${day}0000001`;
            const data = yield transaksi_model_1.Transaksi.findOne({
                attributes: [
                    [sequelize_1.Sequelize.fn('MAX', sequelize_1.Sequelize.col('nomorTransaksi')), 'maks'],
                ],
                where: {
                    nomorTransaksi: { [sequelize_2.Op.like]: `${label}%` },
                },
            });
            if (data.get('maks')) {
                const maks = String(data.get('maks'));
                const splitTransactionNumber = maks.substr(10);
                const next = Number(splitTransactionNumber) + 1;
                console.log(maks, splitTransactionNumber, next);
                const len = 7;
                const diff = len - String(next).length;
                const step = '0';
                let postfix = '';
                for (let i = 1; i <= diff; i++) {
                    postfix += step;
                }
                postfix += next;
                nomorTransaksi = `${label}${year}${month}${day}${postfix}`;
            }
            return nomorTransaksi;
        });
    }
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const create = yield transaksi_model_1.Transaksi.create(data);
            return create;
        });
    }
    static updateStatus(data, nomorTransaksi) {
        return __awaiter(this, void 0, void 0, function* () {
            yield transaksi_model_1.Transaksi.update(data, { where: { nomorTransaksi } });
            const transaksi = yield transaksi_model_1.Transaksi.findOne({ where: { nomorTransaksi } });
            return transaksi;
        });
    }
}
exports.TransaksiRepository = TransaksiRepository;
//# sourceMappingURL=transaksi.repository.js.map