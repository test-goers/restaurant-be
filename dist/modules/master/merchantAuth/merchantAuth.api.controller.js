"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const responseHandler_1 = require("../../core/helpers/responseHandler");
const merchantAuth_repository_1 = require("./merchantAuth.repository");
class MerchantAuthAPIController {
}
exports.default = MerchantAuthAPIController;
MerchantAuthAPIController.login = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const checkMerchant = yield merchantAuth_repository_1.MerchantAuthAPIRepository.checkMerchant(req.body.username);
        if (!checkMerchant) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Username atau password tidak sesuai');
        }
        const validatePassword = yield bcrypt_1.default.compare(req.body.password, checkMerchant.password);
        if (!validatePassword) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Username atau password tidak sesuai');
        }
        const checkMerchantLogged = yield merchantAuth_repository_1.MerchantAuthAPIRepository.checkMerchantLogged(checkMerchant.id);
        if (checkMerchantLogged) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Merchant telah login');
        }
        const cred = {
            id: checkMerchant.id,
            username: checkMerchant.username,
            name: checkMerchant.name,
            owner: checkMerchant.namaPemilik,
        };
        const data = jsonwebtoken_1.default.sign(cred, process.env.SESSION_SECRET, { expiresIn: '7d' });
        const detailData = jsonwebtoken_1.default.verify(data, process.env.SESSION_SECRET);
        const saveSession = yield merchantAuth_repository_1.MerchantAuthAPIRepository.createSession(data, detailData);
        return (saveSession) ? responseHandler_1.ResponseHandler.jsonSuccess(res, saveSession) : responseHandler_1.ResponseHandler.jsonError(res, 'Gagal melakukan login');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
MerchantAuthAPIController.logout = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield merchantAuth_repository_1.MerchantAuthAPIRepository.destroySession(req.headers.merchant_token);
        responseHandler_1.ResponseHandler.jsonSuccess(res, 'Berhasil logout dari aplikasi');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=merchantAuth.api.controller.js.map