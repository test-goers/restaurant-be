"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommisionRepository = void 0;
const sequelize_1 = require("sequelize");
const commision_model_1 = require("./commision.model");
class CommisionRepository {
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const commision = yield commision_model_1.Commision.create(data);
            return commision;
        });
    }
    static get(search = '', limit = 10, offset = 0) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield commision_model_1.Commision.findAndCountAll({
                where: search.length > 0 ?
                    {
                        [sequelize_1.Op.or]: [
                            {
                                service: {
                                    [sequelize_1.Op.like]: `%${String(search)}%`,
                                },
                            },
                            {
                                partner: {
                                    [sequelize_1.Op.like]: `%${String(search)}%`,
                                },
                            },
                            {
                                product: {
                                    [sequelize_1.Op.like]: `%${String(search)}%`,
                                },
                            },
                            {
                                productCode: {
                                    [sequelize_1.Op.like]: `%${String(search)}%`,
                                },
                            },
                            {
                                productName: {
                                    [sequelize_1.Op.like]: `%${String(search)}%`,
                                },
                            },
                        ],
                    } : {},
                offset: Number(offset),
                limit: Number(limit),
            });
            return data;
        });
    }
    static find(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const commision = yield commision_model_1.Commision.findByPk(id);
            return commision;
        });
    }
    static findOne(where) {
        return __awaiter(this, void 0, void 0, function* () {
            const commision = yield commision_model_1.Commision.findOne({ where });
            return commision;
        });
    }
    static update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            yield commision_model_1.Commision.update(data, { where: { id } });
            const commision = yield commision_model_1.Commision.findByPk(id);
            return commision;
        });
    }
    static delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield commision_model_1.Commision.destroy({
                where: {
                    id: Number(id),
                },
            });
        });
    }
    static getCommision(partner = '', product = '', productCode = '') {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield commision_model_1.Commision.findOne({
                where: {
                    partner,
                    product,
                    productCode,
                },
            });
            return data;
        });
    }
}
exports.CommisionRepository = CommisionRepository;
//# sourceMappingURL=commision.repository.js.map