"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommisionRequestSchema = void 0;
const typesafe_joi_1 = __importDefault(require("typesafe-joi"));
exports.CommisionRequestSchema = typesafe_joi_1.default.object({
    service: typesafe_joi_1.default.string().required(),
    partner: typesafe_joi_1.default.string().required(),
    product: typesafe_joi_1.default.string().min(3).required(),
    productCode: typesafe_joi_1.default.string().min(3).required(),
    productName: typesafe_joi_1.default.string().min(3).required().error(new Error('Product Description is not allowed to be empty')),
    commision: typesafe_joi_1.default.number().required(),
});
//# sourceMappingURL=commision.schema.js.map