"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const redirectBack_1 = require("../../core/helpers/redirectBack");
const partnerProduct_repository_1 = require("../partnerProduct/partnerProduct.repository");
const commision_repository_1 = require("./commision.repository");
const commision_schema_1 = require("./commision.schema");
class CommisionController {
}
exports.default = CommisionController;
CommisionController.index = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    return res.render('pages/master/commision/index.pug');
});
CommisionController.data = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield commision_repository_1.CommisionRepository.get(req.query.search, req.query.limit, req.query.offset);
    return res.json({ rows: data.rows, total: data.count });
});
CommisionController.create = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const old = req.flash('val')[0];
    const partnerProductData = yield partnerProduct_repository_1.PartnerProductRepository.getAll();
    const partnerProduct = partnerProductData.map((v) => ({
        name: v.name,
        value: v.name,
    }));
    return res.render('pages/master/commision/create.pug', { old, partnerProduct });
});
CommisionController.store = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const isExist = yield commision_repository_1.CommisionRepository.findOne({ productCode: req.body.productCode });
        if (isExist) {
            throw new Error('Product Code is already exist');
        }
        req.body.productCode = req.body.productCode.toUpperCase();
        req.body.product = req.body.product.toUpperCase();
        req.body.productName = req.body.productName.toUpperCase();
        req.body.commision = Number(req.body.commision);
        const { error, value } = commision_schema_1.CommisionRequestSchema.validate(req.body);
        if (redirectBack_1.redirectBackIfError(error, req, res, value)) {
            return;
        }
        const commision = yield commision_repository_1.CommisionRepository.create(value);
        req.flash(`success`, `Insert Commision successfully`);
        res.redirect(`./edit/${commision.id}`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
CommisionController.edit = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const commision = yield commision_repository_1.CommisionRepository.find(Number(req.params.commisionId));
        if (!commision) {
            throw new Error('Data not found');
        }
        const old = req.flash('val')[0];
        const partnerProductData = yield partnerProduct_repository_1.PartnerProductRepository.getAll();
        const partnerProduct = partnerProductData.map((v) => ({
            name: v.name,
            value: v.name,
        }));
        return res.render('pages/master/commision/edit.pug', { old, commision, partnerProduct });
    }
    catch (error) {
        return res.render('pages/errors/error.pug', { error: 404 });
    }
});
CommisionController.update = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const id = req.body.commisionId;
        delete req.body.commisionId;
        req.body.commision = Number(req.body.commision);
        req.body.productCode = req.body.productCode.toUpperCase();
        req.body.product = req.body.product.toUpperCase();
        req.body.productName = req.body.productName.toUpperCase();
        req.body.commision = Number(req.body.commision);
        const { error, value } = commision_schema_1.CommisionRequestSchema.validate(req.body);
        if (redirectBack_1.redirectBackIfError(error, req, res, value)) {
            return;
        }
        yield commision_repository_1.CommisionRepository.update(id, value);
        req.flash(`success`, `Update Commision successfully`);
        res.redirect(`back`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
CommisionController.delete = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    yield commision_repository_1.CommisionRepository.delete(req.query.id);
    return res.redirect('/commision');
});
//# sourceMappingURL=commision.controller.js.map