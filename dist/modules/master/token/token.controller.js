"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../core/config/database");
const responseHandler_1 = require("../../core/helpers/responseHandler");
class TokenController {
    static index(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const [token, meta] = yield database_1.cektoko.query(`SELECT a.id, b.user_id, b.token, b.expired_at FROM tb_user a
      LEFT JOIN users_authentication b ON a.uid = b.user_id WHERE b.expired_at > NOW()`, { type: sequelize_1.QueryTypes.RAW });
                console.log(meta);
                const data = token.length > 0 ? token : 'token not available';
                return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
            }
            catch (error) {
                return responseHandler_1.ResponseHandler.serverError(res, error);
            }
        });
    }
}
exports.default = TokenController;
//# sourceMappingURL=token.controller.js.map