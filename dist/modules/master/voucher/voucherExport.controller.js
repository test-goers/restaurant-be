"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseHandler_1 = require("../../core/helpers/responseHandler");
const redemption_repository_1 = require("../redemption/redemption.repository");
const voucherExport_repository_1 = require("./voucherExport.repository");
class VoucherExportController {
}
exports.default = VoucherExportController;
VoucherExportController.exportExcel = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const search = req.query.search || '';
        const limit = req.query.limit || (yield redemption_repository_1.RedemptionRepository.countAll());
        const offset = req.query.offset || 0;
        const order = req.query.order || 'asc';
        const voucherId = req.query.voucherId || null;
        const data = yield redemption_repository_1.RedemptionRepository.dataTable(String(search), Number(voucherId), String(order), Number(offset), Number(limit));
        const result = data.rows.map((value, index) => ({
            no: index + 1,
            date: value.date,
            code: value.code,
            initialPrice: Number(value.initialPrice),
            finalPrice: Number(value.finalPrice),
            merchantName: value.merchantName,
            userName: value.userName,
            discountType: value.discountType,
            discountOff: value.discountOff,
            status: value.status,
            service: value.service,
        }));
        const workbook = yield voucherExport_repository_1.VoucherExportRepository.generateExcel(result);
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', `attachment; filename=Data_Rekapitulasi_Riwayat_Redeem_Voucher.xlsx`);
        workbook.xlsx.write(res).then(() => {
            res.status(200).end();
        });
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=voucherExport.controller.js.map