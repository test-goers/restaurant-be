"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Voucher = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const campaign_model_1 = require("../campaign/campaign.model");
const redemption_model_1 = require("../redemption/redemption.model");
let Voucher = class Voucher extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.ForeignKey(() => campaign_model_1.Campaign),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Voucher.prototype, "campaignId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => campaign_model_1.Campaign),
    __metadata("design:type", Array)
], Voucher.prototype, "campaign", void 0);
__decorate([
    sequelize_typescript_1.HasMany(() => redemption_model_1.Redemption),
    __metadata("design:type", Array)
], Voucher.prototype, "redemptions", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Voucher.prototype, "code", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Voucher.prototype, "description", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Voucher.prototype, "category", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Voucher.prototype, "discountType", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Voucher.prototype, "discountOff", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Voucher.prototype, "qty", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Voucher.prototype, "total", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Voucher.prototype, "isActive", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Boolean)
], Voucher.prototype, "isNewUser", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Voucher.prototype, "startDate", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Voucher.prototype, "expDate", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Voucher.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Voucher.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Voucher.prototype, "deletedAt", void 0);
Voucher = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'Vouchers',
        timestamps: true,
        paranoid: true,
    })
], Voucher);
exports.Voucher = Voucher;
//# sourceMappingURL=voucher.model.js.map