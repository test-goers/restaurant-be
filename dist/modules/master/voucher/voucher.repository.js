"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VoucherRepository = void 0;
const moment_1 = __importDefault(require("moment"));
const sequelize_1 = require("sequelize");
const database_1 = require("../../core/config/database");
const campaign_model_1 = require("../campaign/campaign.model");
const customer_model_1 = require("../customer/customer.model");
const redemption_model_1 = require("../redemption/redemption.model");
const redemption_repository_1 = require("../redemption/redemption.repository");
const voucher_model_1 = require("./voucher.model");
class VoucherRepository {
    static getAll(req) {
        return __awaiter(this, void 0, void 0, function* () {
            const layanan = req.query.layanan || 'SEMUA';
            const status = req.query.status === 'active' ? '1' : '0';
            const limit = req.query.limit || 10;
            const offset = req.query.offset || 0;
            const userId = String(req.headers.user_id);
            let dataVoucherNewCustomer = null;
            const { isVoucherNewUserUsed } = yield this.getStatusCustomer(userId);
            if (!isVoucherNewUserUsed) {
                dataVoucherNewCustomer = yield VoucherRepository.getListVoucher({
                    category: layanan,
                    isActive: status,
                    limit,
                    offset,
                    isNewUser: true,
                });
            }
            const dataAllVoucherExceptNewUser = yield VoucherRepository.getListVoucher({
                category: layanan,
                isActive: status,
                limit,
                offset,
                isNewUser: false,
            });
            if (dataVoucherNewCustomer) {
                if ((dataVoucherNewCustomer === null || dataVoucherNewCustomer === void 0 ? void 0 : dataVoucherNewCustomer.count) !== 0 && dataAllVoucherExceptNewUser.count !== 0) {
                    return {
                        rows: [...dataVoucherNewCustomer.rows, ...dataAllVoucherExceptNewUser.rows],
                        count: dataVoucherNewCustomer.count + dataAllVoucherExceptNewUser.count,
                    };
                }
            }
            return {
                rows: dataAllVoucherExceptNewUser.rows,
                count: dataAllVoucherExceptNewUser.count,
            };
        });
    }
    static adminGetAllVoucher(req) {
        return __awaiter(this, void 0, void 0, function* () {
            const layanan = req.query.layanan || 'SEMUA';
            const status = req.query.status === 'active' ? '1' : '0';
            const perPage = req.query.limit || 10;
            const page = req.query.page || 1;
            const order = req.query.order || 'asc';
            let isNewUser = req.query.isNewUser || null;
            let showExpired = req.query.showExpired || null;
            const limit = perPage ? Number(perPage) : 10;
            const offset = page ? (Number(page) - 1) * Number(limit) : 0;
            if (typeof isNewUser === 'string') {
                isNewUser = (isNewUser === 'true') ? true : false;
            }
            if (typeof showExpired === 'string') {
                showExpired = (showExpired === 'true') ? true : false;
            }
            const dataAllVoucher = yield VoucherRepository.getListVoucher({
                isAdmin: true,
                category: layanan,
                isActive: status,
                limit,
                offset,
                isNewUser,
                order,
                showExpired,
            });
            return {
                vouchers: dataAllVoucher.rows,
                total: dataAllVoucher.count,
                page: page ? Number(page) : 1,
                limit: limit ? Number(limit) : 10,
            };
        });
    }
    static getAllRedeemsInVoucher(req) {
        return __awaiter(this, void 0, void 0, function* () {
            const perPage = req.query.limit || 10;
            const page = req.query.page || 1;
            const order = (req.query.order) ? req.query.order : 'asc';
            const limit = perPage ? Number(perPage) : 10;
            const offset = page ? (Number(page) - 1) * Number(limit) : 0;
            const voucherId = (req.params.voucherId) ? req.params.voucherId : null;
            const data = yield redemption_repository_1.RedemptionRepository.dataTable('', Number(voucherId), String(order), Number(offset), Number(limit));
            return {
                redeems: data.rows,
                total: data.count,
                page: page ? Number(page) : 1,
                limit: limit ? Number(limit) : 10,
            };
        });
    }
    static getStatusCustomer(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            let isNewUser = false;
            let isVoucherNewUserUsed = false;
            let currentCustomer = yield customer_model_1.Customer.findByPk(userId);
            if (!currentCustomer) {
                const [customerTransaction, _] = yield database_1.cekTokoHistoryTransaction.query(`SELECT userId FROM HistoryTransaction WHERE userId = '${userId}' LIMIT 1`, { type: sequelize_1.QueryTypes.RAW });
                if (customerTransaction.length === 0) {
                    isNewUser = true;
                }
                else {
                    isVoucherNewUserUsed = true;
                }
                currentCustomer = yield customer_model_1.Customer.create({
                    id: userId,
                    isNewUser,
                    isVoucherNewUserUsed,
                });
            }
            else {
                isNewUser = currentCustomer.isNewUser;
                isVoucherNewUserUsed = currentCustomer.isVoucherNewUserUsed;
            }
            return {
                isNewUser,
                isVoucherNewUserUsed,
                currentCustomer,
            };
        });
    }
    static getListVoucher(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { isAdmin = false, category, isActive, limit, offset, isNewUser = null, order = 'asc', showExpired = false } = data;
            const whereQuery = {
                category,
                isActive,
                isNewUser,
                expDate: {
                    [sequelize_1.Op.gte]: moment_1.default().format('YYYY-MM-DD HH:mm:ss'),
                },
            };
            const ordering = [];
            if (isAdmin) {
                if (showExpired) {
                    delete whereQuery.expDate;
                }
                ordering.push(['id', order]);
            }
            else {
                ordering.push(['expDate', order]);
            }
            if (isNewUser === null) {
                delete whereQuery.isNewUser;
            }
            const result = yield voucher_model_1.Voucher.findAndCountAll({
                attributes: {
                    include: [
                        [sequelize_1.Sequelize.literal(`( SELECT COUNT(id) FROM Redemptions WHERE Redemptions.voucherId = Voucher.id AND Redemptions.status = 'SUKSES' AND Redemptions.deletedAt IS null)`), 'redeem'],
                    ],
                    exclude: ['deletedAt'],
                },
                include: [{ model: campaign_model_1.Campaign, attributes: { exclude: ['deletedAt'] } }],
                where: whereQuery,
                order: ordering,
                limit,
                offset,
            });
            let listData = [];
            if (result.rows.length > 0) {
                listData = result.rows.map(VoucherRepository.mapToVoucherDTO);
            }
            return {
                rows: listData,
                count: result.count,
            };
        });
    }
    static get(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { code } = data;
            const result = yield voucher_model_1.Voucher.findOne({
                attributes: {
                    include: [
                        [sequelize_1.Sequelize.literal(`( SELECT COUNT(id) FROM Redemptions WHERE Redemptions.voucherId = Voucher.id AND Redemptions.status = 'SUKSES' AND Redemptions.deletedAt IS null)`), 'redeem'],
                    ],
                    exclude: ['deletedAt'],
                },
                include: [{ model: campaign_model_1.Campaign }],
                where: { code },
            });
            return result;
        });
    }
    static getById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield voucher_model_1.Voucher.findOne({
                attributes: {
                    include: [
                        [sequelize_1.Sequelize.literal(`( SELECT COUNT(id) FROM Redemptions WHERE Redemptions.voucherId = Voucher.id AND Redemptions.status = 'SUKSES' AND Redemptions.deletedAt IS null)`), 'redeem'],
                    ],
                    exclude: ['deletedAt'],
                },
                where: { id },
            });
            return data;
        });
    }
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const create = yield voucher_model_1.Voucher.create(data);
            return create;
        });
    }
    static update(data, id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield voucher_model_1.Voucher.update(data, { where: { id } });
            const updatedData = yield voucher_model_1.Voucher.findByPk(id);
            return updatedData;
        });
    }
    static delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield voucher_model_1.Voucher.destroy({ where: { id } });
        });
    }
    static dataTable(search = '', category, order = 'DESC', offset = 0, limit = 10, campaignId) {
        return __awaiter(this, void 0, void 0, function* () {
            const categories = category === 'ALL' ? null : category;
            const data = yield voucher_model_1.Voucher.findAndCountAll({
                attributes: {
                    include: [
                        '*',
                        [
                            sequelize_1.Sequelize.literal(`( SELECT COUNT(id) FROM Redemptions WHERE Redemptions.voucherId = Voucher.id)`),
                            'totalRedeem',
                        ],
                    ],
                    exclude: ['createdAt', 'deletedAt', 'updatedAt'],
                },
                include: [{ model: campaign_model_1.Campaign }],
                where: {
                    [sequelize_1.Op.and]: {
                        category: categories || { [sequelize_1.Op.ne]: null },
                        campaignId: campaignId || { [sequelize_1.Op.ne]: null },
                        [sequelize_1.Op.or]: {
                            code: { [sequelize_1.Op.substring]: `%${search}%` },
                            category: { [sequelize_1.Op.substring]: `%${search}%` },
                            startDate: { [sequelize_1.Op.substring]: `%${search}%` },
                            expDate: { [sequelize_1.Op.substring]: `%${search}%` },
                            qty: { [sequelize_1.Op.substring]: `%${search}%` },
                        },
                    },
                },
                order: [['id', order]],
                offset,
                limit,
            });
            return data;
        });
    }
    static findByCode(code, id = 0) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield voucher_model_1.Voucher.findOne({
                where: {
                    [sequelize_1.Op.and]: {
                        code,
                        id: id !== null ? { [sequelize_1.Op.notIn]: [id] } : { [sequelize_1.Op.notIn]: [] },
                    },
                },
            });
            return data;
        });
    }
    static getTotal() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield voucher_model_1.Voucher.count();
            return data;
        });
    }
    static redeem(id, code, avaliableQty) {
        return __awaiter(this, void 0, void 0, function* () {
            const qty = avaliableQty - 1;
            yield voucher_model_1.Voucher.update({ qty }, {
                where: {
                    [sequelize_1.Op.and]: {
                        id,
                        code,
                    },
                },
            });
            const data = yield voucher_model_1.Voucher.findByPk(id);
            return data;
        });
    }
    static updateStatusFromCampaign(data, campaignId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield voucher_model_1.Voucher.update(data, { where: { campaignId } });
        });
    }
    static getWhere(where) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield voucher_model_1.Voucher.findAndCountAll(where);
            return data;
        });
    }
    static updateFromCampaign(data, campaignId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield voucher_model_1.Voucher.update(data, {
                where: { campaignId },
            });
        });
    }
    static countAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const count = yield voucher_model_1.Voucher.count();
            return count;
        });
    }
    static deleteByCampaignId(campaignId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield voucher_model_1.Voucher.destroy({ where: { campaignId } });
        });
    }
    static getVoucherAndCheckRedeemption(code, category, req, type = 'check') {
        return __awaiter(this, void 0, void 0, function* () {
            const withCategoryFilter = type === 'check'
                ? `AND (Vouchers.category = '${category}' OR Vouchers.category = 'SEMUA')`
                : '';
            // const [results, _] = await sequelize.query(
            //   `SELECT Vouchers.*, Redemptions.id as redemptionId FROM Vouchers
            //    INNER JOIN Redemptions ON Redemptions.campaignId = Vouchers.campaignId
            //    AND Redemptions.userId = '${req.headers.user_id}'
            //    AND Redemptions.status = 'SUKSES' WHERE Vouchers.code = '${code}'
            //    ${withCategoryFilter}
            //    AND Vouchers.isActive = '1'
            //    AND Vouchers.startDate <= NOW()
            //    AND Vouchers.expDate >= NOW()
            //    AND Vouchers.deletedAt IS NULL`,
            //   { type: QueryTypes.SELECT },
            // );
            const [results, _] = yield database_1.sequelize.query(`SELECT Vouchers.*, Redemptions.id as redemptionId FROM Vouchers
        LEFT JOIN Redemptions ON Redemptions.campaignId = Vouchers.campaignId
        AND Redemptions.userId = '${req.headers.user_id}' 
        AND Redemptions.status = 'SUKSES' WHERE Vouchers.code = '${code}'
        ${withCategoryFilter}
        AND Vouchers.isActive = '1'
        AND Vouchers.startDate <= NOW()
        AND Vouchers.expDate >= NOW()
        AND Vouchers.deletedAt IS NULL`, { type: sequelize_1.QueryTypes.SELECT });
            if (!results) {
                throw new Error('Kode Voucher tidak aktif atau telah kadaluarsa!');
            }
            return VoucherRepository.mapToCheckVoucherDto(results);
        });
    }
    static isUserEligibleForVoucher(isVoucherForNewCustomer, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!isVoucherForNewCustomer) {
                return true;
            }
            const { isVoucherNewUserUsed } = yield this.getStatusCustomer(userId);
            if (!isVoucherNewUserUsed) {
                return true;
            }
            return false;
        });
    }
    static setCustomerStatus(userId, isNewUser, isVoucherNewUserUsed) {
        return __awaiter(this, void 0, void 0, function* () {
            const currentCustomer = yield customer_model_1.Customer.findByPk(userId);
            if (!currentCustomer) {
                return yield customer_model_1.Customer.create({
                    id: userId,
                    isNewUser,
                    isVoucherNewUserUsed,
                });
            }
            currentCustomer.isNewUser = isNewUser;
            currentCustomer.isVoucherNewUserUsed = isVoucherNewUserUsed;
            currentCustomer.save();
            return currentCustomer;
        });
    }
    static addNewVoucher(req) {
        return __awaiter(this, void 0, void 0, function* () {
            const { code = '', category = 'SEMUA', description = '', campaignId = 0, startDate = null, expDate = null, total = 0, discountType = 'AMOUNT', discountOff = 0, isActive = '1', isNewUser = false, } = req.body;
            if (code.length <= 0) {
                throw new Error('Voucher Code should not empty');
            }
            if (code.length > 6) {
                throw new Error('Max Code Length is 6 char.');
            }
            const acceptedDiscountType = ['AMOUNT', 'PERCENT'];
            if (!acceptedDiscountType.includes(discountType.toUpperCase())) {
                throw new Error('Discount Type is required, valid value is AMOUNT or PERCENT');
            }
            const checkCode = yield VoucherRepository.findByCode(code);
            if (checkCode) {
                throw new Error(`Kode voucher ${code} sudah digunakan, silahkan masukkan kode yang lain`);
            }
            const dataVoucher = {
                code,
                description,
                category,
                discountType,
                discountOff,
                qty: total,
                total,
                isActive,
                isNewUser,
                campaignId,
                startDate,
                expDate,
            };
            return yield VoucherRepository.create(dataVoucher);
        });
    }
    static switchStatusVoucher(req) {
        return __awaiter(this, void 0, void 0, function* () {
            const voucherId = req.params.voucherId;
            const voucher = yield voucher_model_1.Voucher.findByPk(voucherId);
            if (!voucher) {
                throw new Error('Voucher not found');
            }
            voucher.isActive = voucher.isActive === '1' ? '0' : '1';
            yield voucher.save();
            return voucher;
        });
    }
    static switchUserTypeVoucher(req) {
        return __awaiter(this, void 0, void 0, function* () {
            const voucherId = req.params.voucherId;
            const voucher = yield voucher_model_1.Voucher.findByPk(voucherId);
            if (!voucher) {
                throw new Error('Voucher not found');
            }
            voucher.isNewUser = !voucher.isNewUser;
            yield voucher.save();
            return voucher;
        });
    }
    static redeemVoucher(req) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = null;
            const code = String(req.body.code);
            const userId = String(req.headers.user_id);
            const price = Number(req.body.price);
            try {
                yield database_1.sequelize.transaction((t) => __awaiter(this, void 0, void 0, function* () {
                    // Cek jika voucher tersedia
                    const [voucher, _] = yield database_1.sequelize.query(`SELECT Vouchers.*, Redemptions.id as redemptionId FROM Vouchers
            LEFT JOIN Redemptions ON Redemptions.campaignId = Vouchers.campaignId
            AND Redemptions.userId = '${userId}' 
            AND Redemptions.status = 'SUKSES' WHERE Vouchers.code = '${code}'
            AND Vouchers.isActive = '1'
            AND Vouchers.startDate <= NOW()
            AND Vouchers.expDate >= NOW()
            AND Vouchers.deletedAt IS NULL
            LIMIT 1`, { type: sequelize_1.QueryTypes.SELECT });
                    if (!voucher) {
                        throw new Error('Kode Voucher tidak aktif atau telah kadaluarsa!');
                    }
                    if (voucher.redemptionId) {
                        throw new Error('Kode voucher telah digunakan!');
                    }
                    if (voucher.qty <= 0) {
                        throw new Error('Kuota voucher telah habis!');
                    }
                    // Cek jika user berhak menggunakan kode voucher
                    let isNewUser = false;
                    let isVoucherNewUserUsed = false;
                    let currentCustomer = yield customer_model_1.Customer.findByPk(userId);
                    if (!currentCustomer) {
                        const [customerTransaction, meta] = yield database_1.cekTokoHistoryTransaction.query(`SELECT userId FROM HistoryTransaction WHERE userId = '${userId}' LIMIT 1`, { type: sequelize_1.QueryTypes.RAW });
                        if (customerTransaction.length === 0) {
                            isNewUser = true;
                        }
                        else {
                            isVoucherNewUserUsed = true;
                        }
                        currentCustomer = yield customer_model_1.Customer.create({
                            id: userId,
                            isNewUser,
                            isVoucherNewUserUsed,
                        }, { transaction: t });
                    }
                    let isUserEligible = false;
                    if (!voucher.isNewUser) {
                        isUserEligible = true;
                    }
                    else {
                        if (!isVoucherNewUserUsed) {
                            isUserEligible = true;
                        }
                    }
                    if (!isUserEligible) {
                        throw new Error('Kode voucher tidak bisa digunakan!');
                    }
                    // Dapatkan entitas voucher
                    const voucherToUpdate = yield voucher_model_1.Voucher.findByPk(voucher.id);
                    // kurangi kuantitas voucher
                    voucherToUpdate.qty = voucherToUpdate.qty - 1;
                    yield voucherToUpdate.save({ transaction: t });
                    // jika voucher untuk pengguna baru
                    if (voucherToUpdate.isNewUser) {
                        // ubah status customer menjadi bukan pengguna baru
                        currentCustomer.isNewUser = false;
                        currentCustomer.isVoucherNewUserUsed = true;
                        yield currentCustomer.save({ transaction: t });
                    }
                    // mulai perhitungan finalPrice dikurangi diskon dari voucher
                    const discountOff = Number(voucherToUpdate.discountOff);
                    let finalPrice = voucherToUpdate.discountType === 'AMOUNT'
                        ? price - discountOff
                        : price - price * (discountOff / 100);
                    finalPrice = finalPrice <= 0 ? 0 : Math.ceil(finalPrice);
                    const dataToDatabase = {
                        voucherId: voucherToUpdate.id,
                        code: voucherToUpdate.code,
                        date: moment_1.default().format('YYYY-MM-DD HH:mm:ss'),
                        userId,
                        status: 'SUKSES',
                        initialPrice: req.body.price,
                        finalPrice,
                        service: req.body.layanan,
                        discountType: voucherToUpdate.discountType,
                        discountOff: voucherToUpdate.discountOff,
                        merchantId: req.body.merchantId,
                        merchantName: req.body.merchantName,
                        userName: req.body.userName,
                        campaignId: voucherToUpdate.campaignId,
                    };
                    // simpan data redemptions
                    const redemption = yield redemption_model_1.Redemption.create(dataToDatabase, {
                        transaction: t,
                    });
                    let campaignName = 'Voucher Non-Campaign';
                    const campaign = yield campaign_model_1.Campaign.findOne({
                        attributes: {
                            exclude: ['deletedAt'],
                        },
                        where: {
                            id: redemption.campaignId,
                        },
                    });
                    if (campaign) {
                        campaignName = campaign.name;
                    }
                    result = redemption_repository_1.RedemptionRepository.mapToRedemptionResponse({ redemption, campaignName });
                }));
            }
            catch (error) {
                // Jika terjadi error, maka akan terjadi rollback secara otomatis
                throw error;
            }
            return result;
        });
    }
    static cancelRedeemVoucher(req) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = null;
            const code = String(req.body.code);
            const userId = String(req.headers.user_id);
            try {
                yield database_1.sequelize.transaction((t) => __awaiter(this, void 0, void 0, function* () {
                    // Cek jika voucher tersedia
                    const [voucher, _] = yield database_1.sequelize.query(`SELECT Vouchers.*, Redemptions.id as redemptionId FROM Vouchers
            LEFT JOIN Redemptions ON Redemptions.campaignId = Vouchers.campaignId
            AND Redemptions.code = Vouchers.code
            AND Redemptions.userId = '${userId}' 
            AND Redemptions.status = 'SUKSES' WHERE Vouchers.code = '${code}'
            AND Vouchers.isActive = '1'
            AND Vouchers.startDate <= NOW()
            AND Vouchers.expDate >= NOW()
            AND Vouchers.deletedAt IS NULL
            LIMIT 1`, { type: sequelize_1.QueryTypes.SELECT });
                    if (!voucher) {
                        throw new Error('Kode Voucher tidak aktif atau telah kadaluarsa!');
                    }
                    if (!voucher.redemptionId) {
                        throw new Error('Kode voucher belum digunakan!');
                    }
                    // temukan voucher
                    const voucherToUpdate = yield voucher_model_1.Voucher.findByPk(voucher.id);
                    // perbarui voucher quantity
                    voucherToUpdate.qty = voucherToUpdate.qty + 1;
                    yield voucherToUpdate.save({ transaction: t });
                    // jika ternyata voucher untuk customer baru
                    if (voucherToUpdate.isNewUser) {
                        // Temukan Customer
                        let isNewUser = false;
                        let isVoucherNewUserUsed = false;
                        let currentCustomer = yield customer_model_1.Customer.findByPk(userId);
                        if (!currentCustomer) {
                            const [customerTransaction, meta] = yield database_1.cekTokoHistoryTransaction.query(`SELECT userId FROM HistoryTransaction WHERE userId = '${userId}' LIMIT 1`, { type: sequelize_1.QueryTypes.RAW });
                            if (customerTransaction.length === 0) {
                                isNewUser = true;
                            }
                            else {
                                isVoucherNewUserUsed = true;
                            }
                            currentCustomer = yield customer_model_1.Customer.create({
                                id: userId,
                                isNewUser,
                                isVoucherNewUserUsed,
                            }, { transaction: t });
                        }
                        // update status customer
                        currentCustomer.isNewUser = false;
                        currentCustomer.isVoucherNewUserUsed = false;
                        yield currentCustomer.save({ transaction: t });
                    }
                    // Temukan Redemption yang ada kemudian update statusnya
                    const redemption = yield redemption_model_1.Redemption.findByPk(voucher.redemptionId);
                    redemption.status = 'DIBATALKAN';
                    result = yield redemption.save({ transaction: t });
                }));
            }
            catch (error) {
                // Jika terjadi error, maka akan terjadi rollback secara otomatis
                throw error;
            }
            return result;
        });
    }
    static mapToVoucherDTO(data) {
        const redeem = data.getDataValue('redeem');
        return {
            id: data.id,
            campaignId: data.campaignId,
            code: data.code,
            description: data.description,
            category: data.category,
            discountType: data.discountType,
            discountOff: data.discountOff,
            qty: (data.qty === null) ? 0 : data.qty,
            total: (data.total === null) ? 0 : data.total,
            redeem: (redeem === null) ? 0 : redeem,
            isActive: data.isActive,
            isNewUser: data.isNewUser,
            startDate: data.startDate,
            expDate: data.expDate,
            createdAt: data.createdAt,
            updatedAt: data.updatedAt,
            campaign: data.campaign,
        };
    }
}
exports.VoucherRepository = VoucherRepository;
VoucherRepository.mapToCheckVoucherDto = (voucher) => __awaiter(void 0, void 0, void 0, function* () {
    return {
        id: voucher.id,
        campaignId: voucher.campaignId,
        code: voucher.code,
        description: voucher.description,
        category: voucher.category,
        discountType: voucher.discountType,
        discountOff: voucher.discountOff,
        qty: voucher.qty,
        total: voucher.total,
        isActive: voucher.isActive,
        startDate: voucher.startDate,
        expDate: voucher.expDate,
        createdAt: voucher.createdAt,
        updatedAt: voucher.updatedAt,
        deletedAt: voucher.deletedAt,
        isNewUser: voucher.isNewUser ? true : false,
        redemptionId: voucher.redemptionId ? voucher.redemptionId : null,
    };
});
//# sourceMappingURL=voucher.repository.js.map