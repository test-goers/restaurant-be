"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const codeVoucherHelper_1 = require("../../../helpers/codeVoucherHelper");
const selectValueHelper_1 = require("../../../helpers/selectValueHelper");
const responseHandler_1 = require("../../core/helpers/responseHandler");
const campaign_repository_1 = require("../campaign/campaign.repository");
const voucher_repository_1 = require("./voucher.repository");
class VoucherMassiveController {
}
exports.default = VoucherMassiveController;
VoucherMassiveController.create = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const old = req.flash('val')[0];
    const campaign = yield campaign_repository_1.CampaignRepository.get(Number(req.params.id));
    const categories = selectValueHelper_1.categoriesValue;
    const typeDiscount = selectValueHelper_1.typeDiscountValue;
    return res.render('pages/master/voucher/massive/create.pug', { old, categories, typeDiscount, campaign });
});
VoucherMassiveController.store = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        console.log(req.body);
        const totalVoucher = req.body.totalVoucher || 0;
        req.body.startDate = req.body.startDate || null;
        req.body.expDate = req.body.expDate || null;
        req.body.total = req.body.qty;
        let error = false;
        let voucherAdded = 0;
        for (let iterasi = 0; iterasi < totalVoucher; iterasi++) {
            req.body.code = yield codeVoucherHelper_1.generateCodeVoucher(6);
            const create = yield voucher_repository_1.VoucherRepository.create(req.body);
            error = (create) ? false : true;
            voucherAdded = (!error) ? (voucherAdded + 1) : voucherAdded;
        }
        if (!error) {
            req.flash(`success`, `${voucherAdded} dari ${totalVoucher} Voucher berhasil ditambahkan`);
        }
        else {
            req.flash('errors', `Gagal menambahkan ${totalVoucher - voucherAdded} voucher`);
        }
        res.redirect(`/campaign/${req.body.campaignId}/show`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=voucherMassiveController.js.map