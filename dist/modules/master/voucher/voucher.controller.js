"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const selectValueHelper_1 = require("../../../helpers/selectValueHelper");
const responseHandler_1 = require("../../core/helpers/responseHandler");
const campaign_repository_1 = require("../campaign/campaign.repository");
const redemption_repository_1 = require("../redemption/redemption.repository");
const voucher_repository_1 = require("./voucher.repository");
class VoucherController {
}
exports.default = VoucherController;
VoucherController.index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const categories = selectValueHelper_1.categoriesFilter;
    return res.render('pages/master/voucher/index.pug', { categories });
});
VoucherController.create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const old = req.flash('val')[0];
    const categories = selectValueHelper_1.categoriesValue;
    const typeDiscount = selectValueHelper_1.typeDiscountValue;
    return res.render('pages/master/voucher/create.pug', { old, categories, typeDiscount });
});
VoucherController.edit = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield voucher_repository_1.VoucherRepository.getById(req.params.id);
    const old = req.flash('val')[0];
    const categories = selectValueHelper_1.categoriesValue;
    const typeDiscount = selectValueHelper_1.typeDiscountValue;
    return res.render('pages/master/voucher/edit.pug', { data, old, categories, typeDiscount });
});
VoucherController.show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield voucher_repository_1.VoucherRepository.getById(req.params.id);
    const totalRedeem = yield redemption_repository_1.RedemptionRepository.getTotalRedeem(data.id);
    return res.render('pages/master/voucher/show', { data, totalRedeem });
});
VoucherController.store = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        if (req.body.code.length > 6) {
            throw new Error('Maksimal kode voucher adalah 6 karakter');
        }
        const checkCode = yield voucher_repository_1.VoucherRepository.findByCode(req.body.code);
        if (checkCode) {
            throw new Error(`Kode voucher ${req.body.code} sudah digunakan, silahkan masukkan kode yang lain`);
        }
        req.body.startDate = (req.body.startDate) ? req.body.startDate : null;
        req.body.expDate = (req.body.expDate) ? req.body.expDate : null;
        req.body.total = req.body.qty;
        const create = yield voucher_repository_1.VoucherRepository.create(req.body);
        if (create) {
            req.flash(`success`, `Voucher berhasil ditambahkan`);
        }
        else {
            req.flash('errors', 'Gagal menambahkan voucher');
        }
        res.redirect(`/voucher`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
VoucherController.update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        if (req.body.code.length > 6) {
            throw new Error('Maksimal kode voucher adalah 6 karakter');
        }
        const id = (req.body.id) ? req.body.id : 0;
        const checkCode = yield voucher_repository_1.VoucherRepository.findByCode(req.body.code, Number(id));
        if (checkCode) {
            throw new Error(`Kode voucher ${req.body.code} sudah digunakan, silahkan masukkan kode yang lain`);
        }
        const oldData = yield voucher_repository_1.VoucherRepository.getById(Number(id));
        // Update total berdasarkan qty
        if (req.body.qty > 0 && Number(req.body.qty) !== Number(oldData.qty)) {
            const selisihQty = Math.abs(Number(req.body.qty) - Number(oldData.qty));
            req.body.total = (req.body.qty > oldData.qty) ?
                (Number(selisihQty) + Number(oldData.total)) :
                (Number(oldData.total) - Number(selisihQty));
        }
        req.body.startDate = (req.body.startDate) ? req.body.startDate : null;
        req.body.expDate = (req.body.expDate) ? req.body.expDate : null;
        const update = yield voucher_repository_1.VoucherRepository.update(req.body, Number(id));
        if (update) {
            req.flash(`success`, `Voucher berhasil diperbaharui`);
        }
        else {
            req.flash('errors', 'Gagal memperbaharui voucher');
        }
        res.redirect(`/voucher`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
VoucherController.json = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const search = req.query.search || '';
        const limit = req.query.limit || null;
        const offset = req.query.offset || 0;
        const order = req.query.order || 'DESC';
        const campaignId = req.query.campaignId || null;
        let layanan = req.query.layanan || null;
        layanan = (campaignId !== null) ? (yield campaign_repository_1.CampaignRepository.get(Number(campaignId))).category : layanan;
        const data = yield voucher_repository_1.VoucherRepository.dataTable(String(search), String(layanan), String(order), Number(offset), Number(limit), Number(campaignId));
        res.json({
            rows: data.rows,
            total: data.count,
        });
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
VoucherController.delete = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield voucher_repository_1.VoucherRepository.delete(Number(req.body.id));
        req.flash(`success`, `Voucher berhasil dihapus`);
        return res.redirect('/voucher');
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
VoucherController.switchActive = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = {
            isActive: req.body.isActive,
        };
        const keterangan = (Number(req.body.isActive) === 1) ? 'diaktifkan' : 'dinonaktifkan';
        yield voucher_repository_1.VoucherRepository.update(data, Number(req.body.id));
        req.flash(`success`, `Voucher berhasil ${keterangan}`);
        return res.redirect(`/voucher/${req.body.id}/show`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
//# sourceMappingURL=voucher.controller.js.map