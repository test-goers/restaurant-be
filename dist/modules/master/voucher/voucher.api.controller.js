"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseHandler_1 = require("../../core/helpers/responseHandler");
const voucher_repository_1 = require("./voucher.repository");
class VoucherAPIController {
}
exports.default = VoucherAPIController;
VoucherAPIController.getAll = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield voucher_repository_1.VoucherRepository.getAll(req);
        return data
            ? responseHandler_1.ResponseHandler.jsonSuccess(res, data)
            : responseHandler_1.ResponseHandler.jsonError(res, 'Tidak ada voucher untuk saat ini');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
VoucherAPIController.detail = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { code } = req.query;
        if (!code) {
            throw new Error('Kode voucher tidak ditemukan');
        }
        const voucher = yield voucher_repository_1.VoucherRepository.get({ code: code.toString() });
        return voucher
            ? responseHandler_1.ResponseHandler.jsonSuccess(res, voucher)
            : responseHandler_1.ResponseHandler.jsonError(res, 'Tidak ada voucher untuk saat ini');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
VoucherAPIController.detailVoucher = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { voucherId } = req.params;
        const voucher = yield voucher_repository_1.VoucherRepository.getById(voucherId);
        return voucher
            ? responseHandler_1.ResponseHandler.jsonSuccess(res, voucher)
            : responseHandler_1.ResponseHandler.jsonError(res, 'Tidak ada voucher untuk saat ini');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
VoucherAPIController.redeem = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield voucher_repository_1.VoucherRepository.redeemVoucher(req);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
VoucherAPIController.check = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const voucher = yield voucher_repository_1.VoucherRepository.getVoucherAndCheckRedeemption(String(req.query.code), String(req.query.layanan), req);
        if (voucher.redemptionId) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Kode voucher telah digunakan!');
        }
        if (voucher.qty <= 0) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Kuota voucher telah habis!');
        }
        const isUserEligible = yield voucher_repository_1.VoucherRepository.isUserEligibleForVoucher(voucher.isNewUser, String(req.headers.user_id));
        if (!isUserEligible) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Kode voucher tidak bisa digunakan!');
        }
        return responseHandler_1.ResponseHandler.jsonSuccess(res, voucher);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
VoucherAPIController.cancelRedeem = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const cancelRedeem = yield voucher_repository_1.VoucherRepository.cancelRedeemVoucher(req);
        return cancelRedeem
            ? responseHandler_1.ResponseHandler.jsonSuccess(res, cancelRedeem)
            : responseHandler_1.ResponseHandler.jsonError(res, 'Gagal cancel redeem voucher');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
VoucherAPIController.adminGetAllVoucher = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield voucher_repository_1.VoucherRepository.adminGetAllVoucher(req);
        return data
            ? responseHandler_1.ResponseHandler.jsonSuccess(res, data)
            : responseHandler_1.ResponseHandler.jsonError(res, 'Tidak ada voucher untuk saat ini');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
VoucherAPIController.addNewVoucher = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield voucher_repository_1.VoucherRepository.addNewVoucher(req);
        if (!data) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Gagal menambahkan voucher');
        }
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
VoucherAPIController.updateDetailVoucher = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (req.body.code.length > 6) {
            throw new Error('Maksimal kode voucher adalah 6 karakter');
        }
        const id = (req.params.voucherId) ? req.params.voucherId : 0;
        const checkCode = yield voucher_repository_1.VoucherRepository.findByCode(req.body.code, Number(id));
        if (checkCode) {
            throw new Error(`Kode voucher ${req.body.code} sudah digunakan, silahkan masukkan kode yang lain`);
        }
        const oldData = yield voucher_repository_1.VoucherRepository.getById(Number(id));
        // Update total berdasarkan qty
        if (req.body.qty > 0 && Number(req.body.qty) !== Number(oldData.qty)) {
            const selisihQty = Math.abs(Number(req.body.qty) - Number(oldData.qty));
            req.body.total = (req.body.qty > oldData.qty) ?
                (Number(selisihQty) + Number(oldData.total)) :
                (Number(oldData.total) - Number(selisihQty));
        }
        req.body.startDate = (req.body.startDate) ? req.body.startDate : null;
        req.body.expDate = (req.body.expDate) ? req.body.expDate : null;
        const update = yield voucher_repository_1.VoucherRepository.update(req.body, Number(id));
        if (!update) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Gagal mengupdate voucher');
        }
        return responseHandler_1.ResponseHandler.jsonSuccess(res, update);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
VoucherAPIController.removeVoucher = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield voucher_repository_1.VoucherRepository.delete(Number(req.params.voucherId));
        return responseHandler_1.ResponseHandler.jsonSuccess(res, null, 'Voucher berhasil dihapus');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
VoucherAPIController.switchStatusVoucher = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const voucher = yield voucher_repository_1.VoucherRepository.switchStatusVoucher(req);
        if (!voucher) {
            throw new Error('Gagal mengubah status voucher');
        }
        return responseHandler_1.ResponseHandler.jsonSuccess(res, voucher, 'Status Voucher Diperbarui');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
VoucherAPIController.switchUserTypeVoucher = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const voucher = yield voucher_repository_1.VoucherRepository.switchUserTypeVoucher(req);
        if (!voucher) {
            throw new Error('Gagal mengubah user type voucher');
        }
        return responseHandler_1.ResponseHandler.jsonSuccess(res, voucher, 'Status Voucher Diperbarui');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
VoucherAPIController.getAllRedeemsInVoucher = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield voucher_repository_1.VoucherRepository.getAllRedeemsInVoucher(req);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, result);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
//# sourceMappingURL=voucher.api.controller.js.map