"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VoucherExportRepository = void 0;
const exceljs_1 = require("exceljs");
class VoucherExportRepository {
    static generateExcel(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const workbook = new exceljs_1.Workbook();
            const sheet = workbook.addWorksheet('Laporan Riwayat Penggunaan Voucher');
            sheet.mergeCells('A1:K1');
            sheet.getCell('A1:K1').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('A1:K1').value = {
                richText: [
                    {
                        font: { bold: true },
                        text: 'LAPORAN RIWAYAT PENGGUNAAN VOUCHER',
                    },
                ],
            };
            sheet.getCell('A3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('B3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('C3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('D3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('E3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('F3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('G3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('H3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('I3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('J3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('K3').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('A3').value = { richText: [{ font: { bold: true }, text: 'No' }] };
            sheet.getCell('B3').value = { richText: [{ font: { bold: true }, text: 'Tanggal Redeem' }] };
            sheet.getCell('C3').value = { richText: [{ font: { bold: true }, text: 'Kode Voucher' }] };
            sheet.getCell('D3').value = { richText: [{ font: { bold: true }, text: 'Merchant' }] };
            sheet.getCell('E3').value = { richText: [{ font: { bold: true }, text: 'Pengguna' }] };
            sheet.getCell('F3').value = { richText: [{ font: { bold: true }, text: 'Tipe Diskon' }] };
            sheet.getCell('G3').value = { richText: [{ font: { bold: true }, text: 'Diskon' }] };
            sheet.getCell('H3').value = { richText: [{ font: { bold: true }, text: 'Harga Awal' }] };
            sheet.getCell('I3').value = { richText: [{ font: { bold: true }, text: 'Harga Akhir' }] };
            sheet.getCell('J3').value = { richText: [{ font: { bold: true }, text: 'Layanan' }] };
            sheet.getCell('K3').value = { richText: [{ font: { bold: true }, text: 'Status' }] };
            sheet.columns = [
                { key: 'no', width: 5 },
                { key: 'date', width: 20, alignment: { horizontal: 'center' } },
                { key: 'code', width: 15, alignment: { horizontal: 'center' } },
                { key: 'merchantName', width: 30 },
                { key: 'userName', width: 20 },
                { key: 'discountType', width: 20, alignment: { horizontal: 'center' } },
                { key: 'discountOff', width: 20 },
                { key: 'initialPrice', width: 20 },
                { key: 'finalPrice', width: 20 },
                { key: 'service', width: 20, alignment: { horizontal: 'center' } },
                { key: 'status', width: 15, alignment: { horizontal: 'center' } },
            ];
            sheet.addRows(data);
            return workbook;
        });
    }
}
exports.VoucherExportRepository = VoucherExportRepository;
//# sourceMappingURL=voucherExport.repository.js.map