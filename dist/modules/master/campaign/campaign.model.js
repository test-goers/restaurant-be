"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Campaign = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const redemption_model_1 = require("../redemption/redemption.model");
const voucher_model_1 = require("../voucher/voucher.model");
let Campaign = class Campaign extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.HasMany(() => voucher_model_1.Voucher),
    __metadata("design:type", Array)
], Campaign.prototype, "vouchers", void 0);
__decorate([
    sequelize_typescript_1.HasMany(() => redemption_model_1.Redemption),
    __metadata("design:type", Array)
], Campaign.prototype, "redemptions", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Campaign.prototype, "name", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Campaign.prototype, "description", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Campaign.prototype, "category", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Campaign.prototype, "isActive", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Campaign.prototype, "startDate", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Campaign.prototype, "expDate", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Campaign.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Campaign.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Campaign.prototype, "deletedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Boolean)
], Campaign.prototype, "isNewUser", void 0);
Campaign = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'Campaigns',
        timestamps: true,
        paranoid: true,
    })
], Campaign);
exports.Campaign = Campaign;
//# sourceMappingURL=campaign.model.js.map