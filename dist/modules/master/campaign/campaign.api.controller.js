"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const formatting_1 = require("../../core/helpers/formatting");
const responseHandler_1 = require("../../core/helpers/responseHandler");
const voucher_repository_1 = require("../voucher/voucher.repository");
const campaign_repository_1 = require("./campaign.repository");
const campaignExport_repository_1 = require("./campaignExport.repository");
class CampaignAPIController {
}
exports.default = CampaignAPIController;
CampaignAPIController.getAll = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield campaign_repository_1.CampaignRepository.getAll(req);
        return data
            ? responseHandler_1.ResponseHandler.jsonSuccess(res, data)
            : responseHandler_1.ResponseHandler.jsonError(res, 'Tidak ada campaign untuk saat ini');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
CampaignAPIController.addNewCampaign = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield campaign_repository_1.CampaignRepository.addNewCampaign(req);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
CampaignAPIController.getDetailCampaign = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield campaign_repository_1.CampaignRepository.get(Number(req.params.campaignId));
        if (!data) {
            throw new Error('Campaign tidak ditemukan');
        }
        const result = yield campaign_repository_1.CampaignRepository.mapToDetailCampaign(data);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, result);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
CampaignAPIController.updateDetailCampaign = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield campaign_repository_1.CampaignRepository.updateCampaign(req);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, result);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
CampaignAPIController.removeCampaign = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield campaign_repository_1.CampaignRepository.deleteCampaign(req);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, '', 'Campaign berhasil dihapus');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
CampaignAPIController.switchStatusCampaign = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const campaign = yield campaign_repository_1.CampaignRepository.switchStatusCampaign(req);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, campaign);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
CampaignAPIController.switchUserTypeCampaign = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const campaign = yield campaign_repository_1.CampaignRepository.switchUserTypeCampaign(req);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, campaign);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
CampaignAPIController.getAllVoucherInCampaign = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield campaign_repository_1.CampaignRepository.getAllVoucherInCampaign(req);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
CampaignAPIController.getAllRedeemsInCampaign = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield campaign_repository_1.CampaignRepository.getAllRedeemsInCampaign(req);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
CampaignAPIController.bulkGenerateVoucherCampaign = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield campaign_repository_1.CampaignRepository.bulkGenerateVoucherCampaign(req);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error.message);
    }
});
CampaignAPIController.exportExcel = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const search = req.query.search || '';
        const limit = req.query.limit || (yield voucher_repository_1.VoucherRepository.countAll());
        const offset = req.query.offset || 0;
        const order = req.query.order || 'asc';
        const campaignId = req.params.campaignId || null;
        const campaign = yield campaign_repository_1.CampaignRepository.get(Number(campaignId));
        const data = yield voucher_repository_1.VoucherRepository.dataTable(String(search), campaign.category, String(order), Number(offset), Number(limit), Number(campaignId));
        const result = data.rows.map((value, index) => ({
            no: index + 1,
            code: value.code,
            potongan: String(value.discountType === 'AMOUNT' ? `Rp${formatting_1.numberFormat(value.discountOff)}` : `${formatting_1.numberFormat(value.discountOff)}%`),
            layanan: value.category,
            qty: Number(value.qty),
            periode: String(`${moment_1.default(value.startDate).format('DD-MM-Y')} s.d. ${moment_1.default(value.expDate).format('DD-MM-Y')}`),
        }));
        const detail = {
            namaCampaign: campaign.name,
            layananCampaign: campaign.category,
            periodeCampaign: String(`${moment_1.default(campaign.startDate).format('DD-MM-Y')} s.d. ${moment_1.default(campaign.expDate).format('DD-MM-Y')}`),
        };
        const workbook = yield campaignExport_repository_1.CampaignExportRepository.generateExcel(result, detail);
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', `attachment; filename=Data_Rekapitulasi_Voucher_Campaign.xlsx`);
        workbook.xlsx.write(res).then(() => {
            res.status(200).end();
        });
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=campaign.api.controller.js.map