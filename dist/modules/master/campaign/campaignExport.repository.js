"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CampaignExportRepository = void 0;
const exceljs_1 = require("exceljs");
class CampaignExportRepository {
    static generateExcel(data, detail) {
        return __awaiter(this, void 0, void 0, function* () {
            const workbook = new exceljs_1.Workbook();
            const sheet = workbook.addWorksheet('Daftar Voucher');
            sheet.mergeCells('A1:K1');
            sheet.getCell('A1:K1').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('A1:K1').value = {
                richText: [
                    {
                        font: { bold: true },
                        text: 'DAFTAR VOUCHER',
                    },
                ],
            };
            sheet.mergeCells('A2:B2');
            sheet.mergeCells('A3:B3');
            sheet.mergeCells('A4:B4');
            sheet.getCell('A2:B2').alignment = { vertical: 'middle', horizontal: 'left' };
            sheet.getCell('A3:B3').alignment = { vertical: 'middle', horizontal: 'left' };
            sheet.getCell('A4:B4').alignment = { vertical: 'middle', horizontal: 'left' };
            sheet.getCell('A2:B2').value = { richText: [{ font: { bold: true }, text: 'Nama Campaign' }] };
            sheet.getCell('A3:B3').value = { richText: [{ font: { bold: true }, text: 'Layanan' }] };
            sheet.getCell('A4:B4').value = { richText: [{ font: { bold: true }, text: 'Periode' }] };
            sheet.getCell('C2').alignment = { vertical: 'middle', horizontal: 'left' };
            sheet.getCell('C2').value = { richText: [{ font: { bold: false }, text: `${detail.namaCampaign}` }] };
            sheet.getCell('C3').alignment = { vertical: 'middle', horizontal: 'left' };
            sheet.getCell('C3').value = { richText: [{ font: { bold: false }, text: `${detail.layananCampaign}` }] };
            sheet.getCell('C4').alignment = { horizontal: 'left' };
            sheet.getCell('C4').value = { richText: [{ font: { bold: false }, text: `${detail.periodeCampaign}` }] };
            sheet.getCell('A6').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('B6').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('C6').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('D6').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('E6').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('F6').alignment = { vertical: 'middle', horizontal: 'center' };
            sheet.getCell('A6').value = { richText: [{ font: { bold: true }, text: 'No' }] };
            sheet.getCell('B6').value = { richText: [{ font: { bold: true }, text: 'Kode Voucher' }] };
            sheet.getCell('C6').value = { richText: [{ font: { bold: true }, text: 'Potongan' }] };
            sheet.getCell('D6').value = { richText: [{ font: { bold: true }, text: 'Layanan' }] };
            sheet.getCell('E6').value = { richText: [{ font: { bold: true }, text: 'Qty' }] };
            sheet.getCell('F6').value = { richText: [{ font: { bold: true }, text: 'Periode' }] };
            sheet.columns = [
                { key: 'no', width: 5 },
                { key: 'code', width: 15, alignment: { horizontal: 'center' } },
                { key: 'potongan', width: 15, alignment: { horizontal: 'center' } },
                { key: 'layanan', width: 15, alignment: { horizontal: 'center' } },
                { key: 'qty', width: 15, alignment: { horizontal: 'center' } },
                { key: 'periode', width: 15, alignment: { horizontal: 'center' } },
            ];
            sheet.addRows(data);
            return workbook;
        });
    }
}
exports.CampaignExportRepository = CampaignExportRepository;
//# sourceMappingURL=campaignExport.repository.js.map