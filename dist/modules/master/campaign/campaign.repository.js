"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CampaignRepository = void 0;
const moment_1 = __importDefault(require("moment"));
const sequelize_1 = require("sequelize");
const codeVoucherHelper_1 = require("../../../helpers/codeVoucherHelper");
const redemption_model_1 = require("../redemption/redemption.model");
const voucher_model_1 = require("../voucher/voucher.model");
const voucher_repository_1 = require("../voucher/voucher.repository");
const campaign_model_1 = require("./campaign.model");
class CampaignRepository {
    static get(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield campaign_model_1.Campaign.findByPk(id);
            return data;
        });
    }
    static getAll(req) {
        return __awaiter(this, void 0, void 0, function* () {
            const layanan = req.query.layanan || 'SEMUA';
            const status = req.query.status === 'active' ? '1' : '0';
            const perPage = req.query.limit || 10;
            const page = req.query.page || 1;
            const order = req.query.order || 'asc';
            let isNewUser = req.query.isNewUser || null;
            let showExpired = req.query.showExpired || null;
            if (typeof isNewUser === 'string') {
                isNewUser = (isNewUser === 'true') ? true : false;
            }
            if (typeof showExpired === 'string') {
                showExpired = (showExpired === 'true') ? true : false;
            }
            const limit = perPage ? Number(perPage) : 10;
            const offset = page ? (Number(page) - 1) * Number(limit) : 0;
            const whereQuery = {
                category: layanan,
                isActive: status,
                isNewUser,
                expDate: {
                    [sequelize_1.Op.gte]: moment_1.default().format('YYYY-MM-DD HH:mm:ss'),
                },
            };
            if (isNewUser === null) {
                delete whereQuery.isNewUser;
            }
            if (showExpired) {
                delete whereQuery.expDate;
            }
            const data = yield campaign_model_1.Campaign.findAndCountAll({
                attributes: {
                    include: [
                        [sequelize_1.Sequelize.literal(`( SELECT IFNULL(SUM(qty), 0) FROM Vouchers WHERE Vouchers.campaignId = Campaign.id AND Vouchers.deletedAt IS null)`), 'totalAvailable'],
                        [sequelize_1.Sequelize.literal(`( SELECT COUNT(id) FROM Redemptions WHERE Redemptions.campaignId = Campaign.id AND Redemptions.status = 'SUKSES' AND Redemptions.deletedAt IS null)`), 'totalRedeem'],
                        [sequelize_1.Sequelize.literal(`( SELECT IFNULL(SUM(total), 0) FROM Vouchers WHERE Vouchers.campaignId = Campaign.id AND Vouchers.deletedAt IS null)`), 'totalVoucher'],
                    ],
                    exclude: ['deletedAt'],
                },
                where: whereQuery,
                order: [
                    ['id', order],
                ],
                offset,
                limit,
            });
            let listData = [];
            if (data.rows.length > 0) {
                listData = data.rows.map(CampaignRepository.mapCampaignToResponse);
            }
            return {
                campaigns: listData,
                total: data.count,
                page: page ? Number(page) : 1,
                limit,
            };
        });
    }
    static castNullableStringToInt(data) {
        const { input } = data;
        return (input === null) ? 0 : (typeof input === 'string') ? parseInt(input, 10) : input;
    }
    static mapCampaignToResponse(campaign) {
        const totalAvailable = CampaignRepository.castNullableStringToInt({
            input: campaign.getDataValue('totalAvailable'),
        });
        const totalRedeem = CampaignRepository.castNullableStringToInt({
            input: campaign.getDataValue('totalRedeem'),
        });
        const totalVoucher = CampaignRepository.castNullableStringToInt({
            input: campaign.getDataValue('totalVoucher'),
        });
        return {
            id: campaign.getDataValue('id'),
            name: campaign.getDataValue('name'),
            description: campaign.getDataValue('description'),
            category: campaign.getDataValue('category'),
            isActive: campaign.getDataValue('isActive'),
            startDate: campaign.getDataValue('startDate'),
            expDate: campaign.getDataValue('expDate'),
            createdAt: campaign.getDataValue('createdAt'),
            updatedAt: campaign.getDataValue('updatedAt'),
            isNewUser: campaign.getDataValue('isNewUser'),
            voucher: {
                total: totalVoucher,
                redeemed: totalRedeem,
                available: totalAvailable,
            },
        };
    }
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const create = yield campaign_model_1.Campaign.create(data);
            return create;
        });
    }
    static update(data, id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield campaign_model_1.Campaign.update(data, {
                where: { id },
            });
            const updatedData = yield campaign_model_1.Campaign.findByPk(id);
            return updatedData;
        });
    }
    static delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield campaign_model_1.Campaign.destroy({ where: { id } });
        });
    }
    static dataTable(search = '', category = 'ALL', order = 'asc', offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const categories = (category === 'ALL') ? null : category;
            const data = yield campaign_model_1.Campaign.findAndCountAll({
                attributes: {
                    include: [
                        'id',
                        'name',
                        'description',
                        'category',
                        'isActive',
                        'startDate',
                        'expDate',
                        [sequelize_1.Sequelize.literal(`( SELECT SUM(qty) FROM Vouchers WHERE Vouchers.campaignId = Campaign.id AND Vouchers.deletedAt IS null)`), 'tersedia'],
                        [sequelize_1.Sequelize.literal(`( SELECT SUM(total) FROM Vouchers WHERE Vouchers.campaignId = Campaign.id AND Vouchers.deletedAt IS null)`), 'total'],
                        [sequelize_1.Sequelize.literal(`( SELECT COUNT(id) FROM Redemptions WHERE Redemptions.campaignId = Campaign.id AND Redemptions.status = 'SUKSES' AND Redemptions.deletedAt IS null)`), 'redeem'],
                    ],
                    exclude: ['createdAt', 'updatedAt', 'deletedAt'],
                },
                where: {
                    [sequelize_1.Op.and]: {
                        category: categories || { [sequelize_1.Op.ne]: null },
                        [sequelize_1.Op.or]: {
                            name: { [sequelize_1.Op.substring]: `%${search}%` },
                            category: { [sequelize_1.Op.substring]: `%${search}%` },
                            startDate: { [sequelize_1.Op.substring]: `%${search}%` },
                            expDate: { [sequelize_1.Op.substring]: `%${search}%` },
                        },
                    },
                },
                order: [
                    ['id', order],
                ],
                offset,
                limit,
                logging: console.log,
            });
            console.log(data);
            return data;
        });
    }
    static findByName(name, id = 0) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield campaign_model_1.Campaign.findOne({
                where: {
                    [sequelize_1.Op.and]: {
                        name,
                        id: (id !== null) ? { [sequelize_1.Op.notIn]: [id] } : { [sequelize_1.Op.notIn]: [] },
                    },
                },
            });
            return data;
        });
    }
    static getTotal() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield campaign_model_1.Campaign.count();
            return data;
        });
    }
    static getTotalRedeem(campaignId) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield redemption_model_1.Redemption.count({
                where: {
                    campaignId,
                    status: 'SUKSES',
                },
            });
            return data;
        });
    }
    static getTotalTersedia(campaignId) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield voucher_model_1.Voucher.findAll({
                where: {
                    campaignId,
                },
                include: [
                    { model: redemption_model_1.Redemption },
                ],
            });
            let totalTersedia = 0;
            data.map((value, index) => ([
                totalTersedia += value.qty,
            ]));
            return totalTersedia;
        });
    }
    static getTotalVoucher(campaignId) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield voucher_model_1.Voucher.findOne({
                attributes: [
                    [sequelize_1.Sequelize.fn('SUM', sequelize_1.Sequelize.col('total')), 'total'],
                ],
                where: {
                    campaignId,
                },
                group: ['campaignId'],
            });
            return data ? data.total : 0;
        });
    }
}
exports.CampaignRepository = CampaignRepository;
CampaignRepository.mapToDetailCampaign = (data) => __awaiter(void 0, void 0, void 0, function* () {
    let countVoucher = yield CampaignRepository.getTotalVoucher(data.id);
    const countRedeem = yield CampaignRepository.getTotalRedeem(data.id);
    const countTersedia = yield CampaignRepository.getTotalTersedia(data.id);
    if (countVoucher === null) {
        countVoucher = countRedeem + countTersedia;
    }
    return {
        id: data.id,
        name: data.name,
        description: data.description,
        category: data.category,
        isActive: data.isActive,
        startDate: data.startDate,
        expDate: data.expDate,
        isNewUser: data.isNewUser,
        voucher: {
            total: (typeof countVoucher === 'string') ? Number(countVoucher) : countVoucher,
            redeemed: countRedeem,
            available: countTersedia,
        },
    };
});
CampaignRepository.updateCampaign = (req) => __awaiter(void 0, void 0, void 0, function* () {
    const campaignId = Number(req.params.campaignId);
    if (!campaignId) {
        throw new Error('Campaign ID is required');
    }
    const { startDate = null, expDate = null, isActive, isNewUser = false, description, category, name } = req.body;
    const isCampaignExist = yield campaign_model_1.Campaign.findByPk(campaignId);
    if (!isCampaignExist) {
        throw new Error('Campaign tidak ditemukan');
    }
    const checkCampaign = yield CampaignRepository.findByName(name, campaignId);
    if (checkCampaign) {
        throw new Error(`Campaign dengan nama ${name} sudah digunakan, silahkan masukkan nama yang lain`);
    }
    isCampaignExist.startDate = startDate;
    isCampaignExist.expDate = expDate;
    isCampaignExist.name = name;
    isCampaignExist.description = description;
    isCampaignExist.category = category;
    isCampaignExist.isActive = isActive;
    isCampaignExist.isNewUser = isNewUser;
    yield isCampaignExist.save();
    const updateVoucherData = {
        description,
        category,
        startDate,
        expDate,
    };
    yield voucher_repository_1.VoucherRepository.updateFromCampaign(updateVoucherData, campaignId);
    return yield CampaignRepository.mapToDetailCampaign(isCampaignExist);
});
CampaignRepository.deleteCampaign = (req) => __awaiter(void 0, void 0, void 0, function* () {
    const campaignId = Number(req.params.campaignId);
    const campaignDeleted = yield campaign_model_1.Campaign.destroy({ where: { id: campaignId } });
    if (!campaignDeleted) {
        throw new Error('Campaign tidak ditemukan');
    }
    const deletedVouchers = yield voucher_model_1.Voucher.destroy({ where: { campaignId } });
    console.log(deletedVouchers);
    return true;
});
CampaignRepository.addNewCampaign = (req) => __awaiter(void 0, void 0, void 0, function* () {
    const { name = '', description, category, isActive = '1', startDate, expDate, isNewUser = false } = req.body;
    if (!name) {
        throw new Error('Nama campaign harus diisi');
    }
    const checkName = yield CampaignRepository.findByName(name);
    if (checkName) {
        throw new Error(`Campaign dengan nama ${name} sudah digunakan, silahkan masukkan nama yang lain`);
    }
    const result = yield campaign_model_1.Campaign.create({
        name,
        description,
        category,
        isActive,
        startDate,
        expDate,
        isNewUser,
    });
    return result;
});
CampaignRepository.switchStatusCampaign = (req) => __awaiter(void 0, void 0, void 0, function* () {
    const campaignId = Number(req.params.campaignId);
    const campaignToUpdate = yield campaign_model_1.Campaign.findByPk(campaignId);
    if (!campaignToUpdate) {
        throw new Error('Campaign tidak ditemukan');
    }
    campaignToUpdate.isActive = (campaignToUpdate.isActive === '0') ? '1' : '0';
    yield campaignToUpdate.save();
    const updateVoucherData = {
        isActive: campaignToUpdate.isActive,
    };
    yield voucher_repository_1.VoucherRepository.updateFromCampaign(updateVoucherData, campaignId);
    return campaignToUpdate;
});
CampaignRepository.switchUserTypeCampaign = (req) => __awaiter(void 0, void 0, void 0, function* () {
    const campaignId = Number(req.params.campaignId);
    const campaignToUpdate = yield campaign_model_1.Campaign.findByPk(campaignId);
    if (!campaignToUpdate) {
        throw new Error('Campaign tidak ditemukan');
    }
    campaignToUpdate.isNewUser = !campaignToUpdate.isNewUser;
    yield campaignToUpdate.save();
    const updateVoucherData = {
        isNewUser: campaignToUpdate.isNewUser,
    };
    yield voucher_repository_1.VoucherRepository.updateFromCampaign(updateVoucherData, campaignId);
    return campaignToUpdate;
});
CampaignRepository.getAllVoucherInCampaign = (req) => __awaiter(void 0, void 0, void 0, function* () {
    const campaignId = Number(req.params.campaignId);
    const campaignToSearch = yield campaign_model_1.Campaign.findByPk(campaignId);
    if (!campaignToSearch) {
        throw new Error('Campaign tidak ditemukan');
    }
    const perPage = req.query.limit || 10;
    const page = req.query.page || 1;
    const limit = perPage ? Number(perPage) : 10;
    const offset = page ? (Number(page) - 1) * Number(limit) : 0;
    const data = yield voucher_model_1.Voucher.findAndCountAll({
        attributes: {
            include: [
                [sequelize_1.Sequelize.literal(`( SELECT COUNT(id) FROM Redemptions WHERE Redemptions.voucherId = Voucher.id AND Redemptions.status = 'SUKSES' AND Redemptions.deletedAt IS null)`), 'redeem'],
            ],
            exclude: ['deletedAt'],
        },
        where: {
            campaignId,
        },
        order: [['id', 'ASC']],
        limit,
        offset,
    });
    let listData = [];
    if (data.rows.length > 0) {
        listData = data.rows.map(voucher_repository_1.VoucherRepository.mapToVoucherDTO);
    }
    return {
        vouchers: listData,
        total: data.count,
        page: Number(page),
        limit: Number(limit),
    };
});
CampaignRepository.getAllRedeemsInCampaign = (req) => __awaiter(void 0, void 0, void 0, function* () {
    const campaignId = Number(req.params.campaignId);
    const campaignToSearch = yield campaign_model_1.Campaign.findByPk(campaignId);
    if (!campaignToSearch) {
        throw new Error('Campaign tidak ditemukan');
    }
    const perPage = req.query.limit || 10;
    const page = req.query.page || 1;
    const limit = perPage ? Number(perPage) : 10;
    const offset = page ? (Number(page) - 1) * Number(limit) : 0;
    const data = yield redemption_model_1.Redemption.findAndCountAll({
        where: {
            campaignId,
        },
        order: [['createdAt', 'DESC']],
        limit,
        offset,
    });
    return {
        redeems: data.rows,
        total: data.count,
        page: Number(page),
        limit: Number(limit),
    };
});
CampaignRepository.bulkGenerateVoucherCampaign = (req) => __awaiter(void 0, void 0, void 0, function* () {
    const campaignId = Number(req.params.campaignId);
    const campaignToGenerate = yield campaign_model_1.Campaign.findByPk(campaignId);
    if (!campaignToGenerate) {
        throw new Error('Campaign tidak ditemukan');
    }
    if (!req.body) {
        throw new Error('Request Body is required');
    }
    const { totalCode = 0, discountType = 'AMOUNT', discountOff = 0, totalVoucherPerCode = 0 } = req.body;
    const acceptedDiscountType = ['AMOUNT', 'PERCENT'];
    if (!acceptedDiscountType.includes(discountType.toUpperCase())) {
        throw new Error('Discount Type is required, valid value is AMOUNT or PERCENT');
    }
    const result = [];
    for (let i = 0; i < totalCode;) {
        const code = yield codeVoucherHelper_1.generateCodeVoucher(6);
        const voucher = yield voucher_model_1.Voucher.create({
            code,
            campaignId: campaignToGenerate.id,
            description: campaignToGenerate.description,
            category: campaignToGenerate.category,
            discountType: discountType.toUpperCase(),
            discountOff,
            qty: totalVoucherPerCode,
            total: totalVoucherPerCode,
            isActive: campaignToGenerate.isActive,
            isNewUser: campaignToGenerate.isNewUser,
            startDate: campaignToGenerate.startDate,
            expDate: campaignToGenerate.expDate,
            createdAt: new Date(),
            updatedAt: new Date(),
        });
        if (voucher) {
            i++;
            result.push(voucher);
        }
    }
    return result;
});
//# sourceMappingURL=campaign.repository.js.map