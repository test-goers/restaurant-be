"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const selectValueHelper_1 = require("../../../helpers/selectValueHelper");
const responseHandler_1 = require("../../core/helpers/responseHandler");
const redemption_repository_1 = require("../redemption/redemption.repository");
const voucher_repository_1 = require("../voucher/voucher.repository");
const campaign_repository_1 = require("./campaign.repository");
class CampaignController {
}
exports.default = CampaignController;
CampaignController.index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const categories = selectValueHelper_1.categoriesFilter;
    return res.render('pages/master/campaign/index.pug', { categories });
});
CampaignController.create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const old = req.flash('val')[0];
    const categories = selectValueHelper_1.categoriesValue;
    const typeDiscount = selectValueHelper_1.typeDiscountValue;
    return res.render('pages/master/campaign/create.pug', { old, categories, typeDiscount });
});
CampaignController.edit = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield campaign_repository_1.CampaignRepository.get(Number(req.params.id));
    const old = req.flash('val')[0];
    const categories = selectValueHelper_1.categoriesValue;
    const typeDiscount = selectValueHelper_1.typeDiscountValue;
    return res.render('pages/master/campaign/edit.pug', { data, old, categories, typeDiscount });
});
CampaignController.show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield campaign_repository_1.CampaignRepository.get(Number(req.params.id));
    let countVoucher = yield campaign_repository_1.CampaignRepository.getTotalVoucher(Number(req.params.id));
    const countRedeem = yield campaign_repository_1.CampaignRepository.getTotalRedeem(Number(req.params.id));
    const countTersedia = yield campaign_repository_1.CampaignRepository.getTotalTersedia(Number(req.params.id));
    countVoucher = (countVoucher == null) ? countRedeem + countTersedia : countVoucher;
    return res.render('pages/master/campaign/show.pug', { data, countVoucher, countRedeem, countTersedia });
});
CampaignController.store = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const checkCode = yield campaign_repository_1.CampaignRepository.findByName(req.body.name);
        if (checkCode) {
            throw new Error(`Campaign dengan nama ${req.body.name} sudah digunakan, silahkan masukkan nama yang lain`);
        }
        req.body.startDate = req.body.startDate || null;
        req.body.expDate = req.body.expDate || null;
        req.body.total = req.body.qty;
        const create = yield campaign_repository_1.CampaignRepository.create(req.body);
        if (create) {
            req.flash(`success`, `Campaign berhasil ditambahkan`);
        }
        else {
            req.flash('errors', 'Gagal menambahkan campaign');
        }
        res.redirect(`/campaign`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
CampaignController.update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const id = req.body.id || 0;
        const checkCampaign = yield campaign_repository_1.CampaignRepository.findByName(req.body.name, Number(id));
        if (checkCampaign) {
            throw new Error(`Campaign dengan nama ${req.body.name} sudah digunakan, silahkan masukkan nama yang lain`);
        }
        req.body.startDate = (req.body.startDate) ? req.body.startDate : null;
        req.body.expDate = (req.body.expDate) ? req.body.expDate : null;
        const update = yield campaign_repository_1.CampaignRepository.update(req.body, Number(id));
        const updateVoucherData = {
            description: req.body.description,
            category: req.body.category,
            startDate: req.body.startDate,
            expDate: req.body.expDate,
        };
        yield voucher_repository_1.VoucherRepository.updateFromCampaign(updateVoucherData, Number(id));
        if (update) {
            req.flash(`success`, `Campaign berhasil diperbaharui dan informasi voucher berhasil diupdate`);
        }
        else {
            req.flash('errors', 'Gagal memperbaharui campaign');
        }
        res.redirect(`/campaign`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
CampaignController.json = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const search = req.query.search || '';
        const limit = req.query.limit || null;
        const offset = req.query.offset || 0;
        const order = req.query.order || 'asc';
        const layanan = req.query.layanan || '';
        const data = yield campaign_repository_1.CampaignRepository.dataTable(String(search), String(layanan), String(order), Number(offset), Number(limit));
        console.log(data.rows);
        res.json({
            rows: data.rows,
            total: data.count,
        });
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
CampaignController.delete = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield campaign_repository_1.CampaignRepository.delete(Number(req.body.id));
        yield voucher_repository_1.VoucherRepository.deleteByCampaignId(Number(req.body.id));
        req.flash(`success`, `Campaign berhasil dihapus`);
        return res.redirect('/campaign');
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
CampaignController.switchActive = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = {
            isActive: req.body.isActive,
        };
        const keterangan = (Number(req.body.isActive) === 1) ? 'diaktifkan' : 'dinonaktifkan';
        yield campaign_repository_1.CampaignRepository.update(data, Number(req.body.id));
        yield voucher_repository_1.VoucherRepository.updateStatusFromCampaign(data, Number(req.body.id));
        req.flash(`success`, `Campaign berhasil ${keterangan}`);
        return res.redirect(`/campaign/${req.body.id}/show`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
CampaignController.calculation = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const search = req.query.search || '';
        const limit = req.query.limit || null;
        const offset = req.query.offset || 0;
        const order = req.query.order || 'asc';
        const campaignId = req.query.campaignId || '';
        const data = yield redemption_repository_1.RedemptionRepository.campaignAcumulation(String(search), String(campaignId), String(order), Number(offset), Number(limit));
        res.json({
            rows: data.rows,
            total: data.count.length,
        });
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=campaign.controller.js.map