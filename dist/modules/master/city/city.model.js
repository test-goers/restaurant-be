"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.City = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const district_model_1 = require("../district/district.model");
const province_model_1 = require("../province/province.model");
let City = class City extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.HasMany(() => district_model_1.District),
    __metadata("design:type", Array)
], City.prototype, "district", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], City.prototype, "name", void 0);
__decorate([
    sequelize_typescript_1.ForeignKey(() => province_model_1.Province),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], City.prototype, "provinceId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => province_model_1.Province),
    __metadata("design:type", Array)
], City.prototype, "province", void 0);
City = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'Cities',
        timestamps: true,
        paranoid: true,
    })
], City);
exports.City = City;
//# sourceMappingURL=city.model.js.map