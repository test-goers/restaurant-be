"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const moment_1 = __importDefault(require("moment"));
const uuid_1 = require("uuid");
const cekPayMethod_1 = require("../../../../helpers/cekPayMethod");
const pascaBayarHelper_1 = require("../../../../helpers/pascaBayarHelper");
const paymentHelper_1 = require("../../../../helpers/paymentHelper");
const credential_1 = require("../../../core/config/credential");
const responseHandler_1 = require("../../../core/helpers/responseHandler");
const commision_repository_1 = require("../../commision/commision.repository");
const cekPay_repository_1 = __importDefault(require("../cekPay.repository"));
const cekPayDetail_repository_1 = require("../cekPayDetail.repository");
const provider_repository_1 = require("../provider/provider.repository");
const pulsaTransaction_schema_1 = require("../pulsa/pulsaTransaction.schema");
const pascaBayarSelulerTransaction_repository_1 = require("./pascaBayarSelulerTransaction.repository");
class PascaBayarSelulerTransactionController {
}
exports.default = PascaBayarSelulerTransactionController;
PascaBayarSelulerTransactionController.checkout = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const identification = {
            nomorHP: req.body.idpel1,
        };
        const { value, error } = pulsaTransaction_schema_1.MobileNumberRequestSchema.validate(identification);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, `Validation error ${error}`);
        }
        const providerName = yield provider_repository_1.ProviderRepository.getProvider(value.nomorHP);
        if (!providerName) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Provider tidak ditemukan');
        }
        const kodeProduk = pascaBayarHelper_1.kodeProdukPascaBayarSeluler(providerName.provider);
        if (!kodeProduk) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Kode produk dari provide tidak ditemukan');
        }
        const uid = uuid_1.v4().toUpperCase();
        const invoiceNumber = yield cekPay_repository_1.default.invoiceNumber();
        const cekTokoRequest = Object.assign(Object.assign({}, credential_1.rajaBillerCredential), { method: cekPayMethod_1.cekPayMethod.pulsaPostPaid.inqueryTransaction, idpel1: req.body.idpel1, idpel2: req.body.idpel2, idpel3: req.body.idpel3, kode_produk: kodeProduk, ref1: invoiceNumber });
        const rajaBillerResponse = yield axios_1.default.post(process.env.RAJABILLER_ENDPOINT, cekTokoRequest)
            .then((response) => response.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, `Network error ${err}`));
        if (rajaBillerResponse.STATUS === '14') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor tidak terdaftar dan tidak ditemukan');
        }
        if (rajaBillerResponse.STATUS === '88') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah dibayarkan');
        }
        const billAmount = Number(rajaBillerResponse.NOMINAL) + Number(rajaBillerResponse.ADMIN) - Number(req.body.point);
        const generatedPayment = (req.body.metodePembayaran === 'EWALLET') ? yield paymentHelper_1.generatePayment(paymentHelper_1.generateCallback(req), invoiceNumber, billAmount, req.body.merchant) : yield paymentHelper_1.generateInvoice(invoiceNumber, billAmount, invoiceNumber, req.body.merchant);
        if (!generatedPayment) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Gagal membuat payment');
        }
        let paymentExpiredAt = moment_1.default(generatedPayment.expiry_date).format('YYYY-MM-DD HH:mm:ss');
        if (req.body.metodePembayaran === 'EWALLET') {
            paymentExpiredAt = req.body.merchant === 'ID_OVO' ? moment_1.default().add(55, 's').format('YYYY-MM-DD HH:mm:ss') : moment_1.default().add(30, 'm').format('YYYY-MM-DD HH:mm:ss');
        }
        const commision = yield commision_repository_1.CommisionRepository.getCommision('RAJABILLER', 'HP PASCABAYAR', kodeProduk);
        const resultToDatabase = {
            uid,
            userId: req.headers.id,
            date: moment_1.default().format('YYYY-MM-DD'),
            invoiceNumber,
            accNumber1: req.body.idpel1,
            category: req.body.kategori,
            product: 'SELULER PASCABAYAR',
            providerName: providerName.provider,
            productCode: kodeProduk,
            nominal: Number(rajaBillerResponse.NOMINAL),
            basicPrice: Number(rajaBillerResponse.NOMINAL),
            margin: 0,
            adminPayment: Number(rajaBillerResponse.ADMIN),
            commision: (commision) ? commision.commision : 0,
            billAmount,
            poin: req.body.point,
            paymentId: generatedPayment.id,
            paymentMethod: req.body.metodePembayaran,
            merchant: req.body.merchant,
            merchantNumber: req.body.nomorAkun,
            voucherCode: req.body.kodeVoucher,
            paymentExpiredAt,
        };
        const addDetailCekPay = {
            invoiceNumber,
            customerName: rajaBillerResponse.NAMA_PELANGGAN,
            periode: rajaBillerResponse.PERIODE,
        };
        const detailCekpay = yield cekPayDetail_repository_1.CekPayDetailRepository.create(addDetailCekPay);
        const dataDatabase = yield cekPay_repository_1.default.create(resultToDatabase);
        const responseDatabase = Object.assign(Object.assign({}, dataDatabase.get()), { payment: Object.assign({}, generatedPayment), detail: Object.assign({}, detailCekpay.get()) });
        return responseHandler_1.ResponseHandler.jsonSuccess(res, responseDatabase);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
PascaBayarSelulerTransactionController.purchase = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const invoiceNumber = String(req.query.invoiceNumber);
        const transaction = yield cekPay_repository_1.default.getInvoiceNumber(invoiceNumber);
        if (!transaction) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor invoice tidak ditemukan');
        }
        if ((transaction.paymentStatus === 'PAID' || transaction.paymentStatus === 'SUCCEEDED') && transaction.transactionStatus === 'SUKSES') {
            return responseHandler_1.ResponseHandler.jsonError(res, `Transaksi telah berhasil dilakukan pada ${moment_1.default(transaction.updatedAt).format('YYYY-MM-DD HH:mm:ss')}`);
        }
        if (transaction.paymentStatus === 'EXPIRED') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah melebihi batas pembayaran');
        }
        if (moment_1.default(transaction.paymentExpiredAt).isBefore(moment_1.default()) && transaction.paymentStatus === 'PENDING') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah melebihi batas pembayaran');
        }
        let paymentStatus = 'PENDING';
        if (transaction.paymentMethod === 'EWALLET') {
            paymentStatus = yield paymentHelper_1.checkPaymentStatus(transaction.paymentId, 'EWALLET');
            if (paymentStatus === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, `Selesaikan pembayaran sebelum ${moment_1.default(transaction.paymentExpiredAt).format('YYYY-MM-DD HH:mm:ss')}`);
            }
        }
        else {
            paymentStatus = yield paymentHelper_1.checkPaymentStatus(transaction.paymentId, 'BANK');
            console.log({ paymentStatus });
            if (paymentStatus === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, `Selesaikan pembayaran sebelum ${moment_1.default(transaction.paymentExpiredAt).format('YYYY-MM-DD HH:mm:ss')}`);
            }
        }
        const transactionNumber = yield cekPay_repository_1.default.invoiceNumber('transaction');
        const cekTokoRequest = Object.assign(Object.assign({}, credential_1.rajaBillerCredential), { method: cekPayMethod_1.cekPayMethod.pulsaPostPaid.payTransaction, idpel1: transaction.accNumber1, idpel2: transaction.accNumber2, idpel3: transaction.accNumber3, kode_produk: transaction.productCode, ref1: transactionNumber, ref2: transaction.ref2, ref3: transaction.ref3, nominal: transaction.nominal });
        const rajaBillerResponse = yield axios_1.default.post(process.env.RAJABILLER_ENDPOINT, cekTokoRequest)
            .then((response) => response.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, `Network error ${err}`));
        if (rajaBillerResponse.STATUS === '14') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor tidak terdaftar dan tidak ditemukan');
        }
        if (rajaBillerResponse.STATUS === '88') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah dibayarkan');
        }
        const updateToDatabase = {
            transactionNumber,
            deductedBalance: rajaBillerResponse.SALDO_TERPOTONG,
            endingBalance: rajaBillerResponse.SISA_SALDO,
            paymentStatus,
            transactionStatus: (rajaBillerResponse.KET === 'APPROVE') ? 'SUKSES' : 'GAGAL',
        };
        const updateDetail = {
            periode: rajaBillerResponse.PERIODE,
            tarif: rajaBillerResponse.NOMINAL,
            urlStruk: rajaBillerResponse.URL_STRUK,
        };
        yield cekPayDetail_repository_1.CekPayDetailRepository.update(updateDetail, invoiceNumber);
        yield cekPay_repository_1.default.update(updateToDatabase, invoiceNumber);
        const dataReponse = {
            waktu: moment_1.default(rajaBillerResponse.WAKTU, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss'),
            kodeProduk: rajaBillerResponse.KODE_PRODUK,
            idPel1: rajaBillerResponse.IDPEL1,
            namaPelanggan: rajaBillerResponse.NAMA_PELANGGAN,
            periode: rajaBillerResponse.PERIODE,
            harga: Number(rajaBillerResponse.NOMINAL),
            admin: Number(rajaBillerResponse.ADMIN),
            totalHarga: Number(rajaBillerResponse.NOMINAL) + Number(rajaBillerResponse.ADMIN),
            ref1: rajaBillerResponse.REF1,
            ref2: rajaBillerResponse.REF2,
            status: rajaBillerResponse.STATUS,
            ket: rajaBillerResponse.KET,
            saldoTerpotong: rajaBillerResponse.SALDO_TERPOTONG,
            sisaSaldo: rajaBillerResponse.SISA_SALDO,
            urlStruk: rajaBillerResponse.URL_STRUK,
        };
        return responseHandler_1.ResponseHandler.jsonSuccess(res, dataReponse);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
PascaBayarSelulerTransactionController.inquiry = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const identification = {
            nomorHP: req.params.nomorHp,
        };
        const { value, error } = pulsaTransaction_schema_1.MobileNumberRequestSchema.validate(identification);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, `Validation error ${error}`);
        }
        const providerName = yield provider_repository_1.ProviderRepository.getProvider(value.nomorHP);
        if (!providerName) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Provider tidak ditemukan');
        }
        const kodeProduk = pascaBayarHelper_1.kodeProdukPascaBayarSeluler(providerName.provider);
        if (!kodeProduk) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Kode produk dari provide tidak ditemukan');
        }
        const cekTokoRequest = Object.assign(Object.assign({}, credential_1.rajaBillerCredential), { method: cekPayMethod_1.cekPayMethod.pulsaPostPaid.inqueryTransaction, idpel1: req.params.nomorHp, idpel2: '', idpel3: '', kode_produk: kodeProduk, ref1: '' });
        const rajaBillerResponse = yield axios_1.default.post(process.env.RAJABILLER_ENDPOINT, cekTokoRequest)
            .then((response) => response.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, `Network error ${err}`));
        if (rajaBillerResponse.STATUS === '14') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor tidak terdaftar dan tidak ditemukan');
        }
        if (rajaBillerResponse.STATUS === '88') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah dibayarkan');
        }
        const data = {
            waktu: moment_1.default(rajaBillerResponse.WAKTU, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss'),
            kodeProduk: rajaBillerResponse.KODE_PRODUK,
            idPel1: rajaBillerResponse.IDPEL1,
            namaPelanggan: rajaBillerResponse.NAMA_PELANGGAN,
            periode: rajaBillerResponse.PERIODE,
            harga: Number(rajaBillerResponse.NOMINAL),
            admin: Number(rajaBillerResponse.ADMIN),
            totalHarga: Number(rajaBillerResponse.NOMINAL) + Number(rajaBillerResponse.ADMIN),
            ref1: rajaBillerResponse.REF1,
            ref2: rajaBillerResponse.REF2,
            status: rajaBillerResponse.STATUS,
            ket: rajaBillerResponse.KET,
            saldoTerpotong: rajaBillerResponse.SALDO_TERPOTONG,
            sisaSaldo: rajaBillerResponse.SISA_SALDO,
            urlStruk: rajaBillerResponse.URL_STRUK,
        };
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
PascaBayarSelulerTransactionController.history = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const status = (req.body.status) ? req.body.status : '';
        const minDate = (req.body.minDate) ? req.body.minDate : null;
        const maxDate = (req.body.maxDate) ? req.body.maxDate : moment_1.default();
        const transaksi = yield pascaBayarSelulerTransaction_repository_1.PascaBayarSelulerRepository.getByUserId(req.headers.user_id, status, minDate, maxDate);
        return (transaksi) ? responseHandler_1.ResponseHandler.jsonSuccess(res, transaksi) : responseHandler_1.ResponseHandler.jsonError(res, 'Data tidak ditemukan');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=pascaBayarSelulerTransaction.controller.js.map