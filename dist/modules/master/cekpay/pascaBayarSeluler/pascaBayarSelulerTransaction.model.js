"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PascaBayarSeluler = void 0;
const moment_1 = __importDefault(require("moment"));
const sequelize_typescript_1 = require("sequelize-typescript");
let PascaBayarSeluler = class PascaBayarSeluler extends sequelize_typescript_1.Model {
    get batasPembayaran() {
        const theDate = this.getDataValue('batasPembayaran');
        return moment_1.default(theDate).format('YYYY-MM-DD HH:mm:ss');
    }
};
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "uid", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PascaBayarSeluler.prototype, "userId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "paymentId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "kodeProduk", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PascaBayarSeluler.prototype, "waktu", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "pelangganId1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "pelangganId2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "pelangganId3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "namaPelanggan", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "periode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PascaBayarSeluler.prototype, "nominal", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PascaBayarSeluler.prototype, "admin", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PascaBayarSeluler.prototype, "adminPayment", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PascaBayarSeluler.prototype, "point", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "metodePembayaran", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "merchant", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "nomorAkun", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "kodeVoucher", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "urlStruk", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "ref1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "ref2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "ref3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PascaBayarSeluler.prototype, "saldoTerpotong", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PascaBayarSeluler.prototype, "sisaSaldo", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "statusTrx", void 0);
__decorate([
    sequelize_typescript_1.Column(sequelize_typescript_1.DataType.DATE),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [])
], PascaBayarSeluler.prototype, "batasPembayaran", null);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PascaBayarSeluler.prototype, "statusPembayaran", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PascaBayarSeluler.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PascaBayarSeluler.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PascaBayarSeluler.prototype, "deletedAt", void 0);
PascaBayarSeluler = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'TransaksiSelulerPasca',
        timestamps: true,
        paranoid: true,
    })
], PascaBayarSeluler);
exports.PascaBayarSeluler = PascaBayarSeluler;
//# sourceMappingURL=pascaBayarSelulerTransaction.model.js.map