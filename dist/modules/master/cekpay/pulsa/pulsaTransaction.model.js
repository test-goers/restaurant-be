"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PulseTransaction = void 0;
const moment_1 = __importDefault(require("moment"));
const sequelize_typescript_1 = require("sequelize-typescript");
let PulseTransaction = class PulseTransaction extends sequelize_typescript_1.Model {
    get paymentExpiredAt() {
        const theDate = this.getDataValue('paymentExpiredAt');
        return moment_1.default(theDate).format('YYYY-MM-DD HH:mm:ss');
    }
    get transactionExpiredAt() {
        const theDate = this.getDataValue('transactionExpiredAt');
        return moment_1.default(theDate).format('YYYY-MM-DD HH:mm:ss');
    }
};
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "uid", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "userId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "partnerProduct", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "partnerPayment", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "transactionNumber", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "invoiceNumber", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "providerName", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "product", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "productCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "accNumber1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "nominal", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "basicPrice", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "margin", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "adminPayment", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "adminTransaction", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "commision", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "poin", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "ref1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "ref2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "ref3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "paymentId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "paymentMethod", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "merchant", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "merchantNumber", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "voucherCode", void 0);
__decorate([
    sequelize_typescript_1.Column(sequelize_typescript_1.DataType.DATE),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [])
], PulseTransaction.prototype, "paymentExpiredAt", null);
__decorate([
    sequelize_typescript_1.Column(sequelize_typescript_1.DataType.DATE),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [])
], PulseTransaction.prototype, "transactionExpiredAt", null);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "deductedBalance", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PulseTransaction.prototype, "endingBalance", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "paymentStatus", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PulseTransaction.prototype, "transactionStatus", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PulseTransaction.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PulseTransaction.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PulseTransaction.prototype, "deletedAt", void 0);
PulseTransaction = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'PulseTransaction',
        timestamps: true,
        paranoid: true,
    })
], PulseTransaction);
exports.PulseTransaction = PulseTransaction;
//# sourceMappingURL=pulsaTransaction.model.js.map