"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PulsaTransactionRepository = void 0;
const moment_1 = __importDefault(require("moment"));
const sequelize_1 = require("sequelize");
const pulsaTransaction_model_1 = require("./pulsaTransaction.model");
class PulsaTransactionRepository {
    static get() {
        return __awaiter(this, void 0, void 0, function* () {
            const offset = this.offset;
            const limit = this.limit;
            const where = this.where;
            const order = this.order;
            const data = yield pulsaTransaction_model_1.PulseTransaction.findAll({ where, order, limit, offset });
            return data;
        });
    }
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const creating = yield pulsaTransaction_model_1.PulseTransaction.create(Object.assign({}, data));
            return creating;
        });
    }
    static updateStatus(nomorTransaksi, status) {
        return __awaiter(this, void 0, void 0, function* () {
            yield pulsaTransaction_model_1.PulseTransaction.update({ statusTrx: status }, { where: { nomorTransaksi } });
            const transaction = yield pulsaTransaction_model_1.PulseTransaction.findOne({
                where: { nomorTransaksi },
            });
            return transaction;
        });
    }
    static getDetailByUid(uid) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield pulsaTransaction_model_1.PulseTransaction.findOne({
                where: { uid },
            });
            return data;
        });
    }
    static getDetailById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield pulsaTransaction_model_1.PulseTransaction.findByPk(id);
            return data;
        });
    }
    static getDetailByTransaction(nomorTransaksi) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield pulsaTransaction_model_1.PulseTransaction.findOne({
                where: { nomorTransaksi },
            });
            return data;
        });
    }
    static checkTransactionStatus(nomorTransaksi) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield pulsaTransaction_model_1.PulseTransaction.findOne({
                where: { nomorTransaksi },
            });
            return data;
        });
    }
    static update(data, nomorTransaksi) {
        return __awaiter(this, void 0, void 0, function* () {
            yield pulsaTransaction_model_1.PulseTransaction.update(data, {
                where: { nomorTransaksi },
            });
            const transaksi = pulsaTransaction_model_1.PulseTransaction.findOne({
                where: { nomorTransaksi },
            });
            return transaksi;
        });
    }
    static getByUserId(userId, statusTrx, minDate = null, maxDate = null, offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            if (statusTrx === '') {
                return this.getAllByUserId(userId, minDate, maxDate, offset, limit);
            }
            const transaksi = yield pulsaTransaction_model_1.PulseTransaction.findAndCountAll({
                where: {
                    [sequelize_1.Op.and]: {
                        userId,
                        statusTrx,
                        createdAt: {
                            [sequelize_1.Op.gte]: moment_1.default(minDate).toDate(),
                            [sequelize_1.Op.lte]: moment_1.default(maxDate).toDate(),
                        },
                    },
                },
                order: [
                    ['createdAt', 'DESC'],
                ],
                offset,
                limit,
            });
            return transaksi;
        });
    }
    static getAllByUserId(userId, minDate = null, maxDate = null, offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const transaksi = yield pulsaTransaction_model_1.PulseTransaction.findAndCountAll({
                where: {
                    [sequelize_1.Op.and]: {
                        userId,
                        createdAt: {
                            [sequelize_1.Op.gte]: moment_1.default(minDate).toDate(),
                            [sequelize_1.Op.lte]: moment_1.default(maxDate).toDate(),
                        },
                    },
                },
                order: [
                    ['createdAt', 'DESC'],
                ],
                offset,
                limit,
            });
            return transaksi;
        });
    }
}
exports.PulsaTransactionRepository = PulsaTransactionRepository;
PulsaTransactionRepository.limit = 10;
PulsaTransactionRepository.offset = 0;
PulsaTransactionRepository.include = [];
//# sourceMappingURL=pulsaTransaction.repository.js.map