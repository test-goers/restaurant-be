"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const moment_1 = __importDefault(require("moment"));
const uuid_1 = require("uuid");
const cekPayMethod_1 = require("../../../../helpers/cekPayMethod");
const paymentHelper_1 = require("../../../../helpers/paymentHelper");
const credential_1 = require("../../../core/config/credential");
const responseHandler_1 = require("../../../core/helpers/responseHandler");
const cekPay_repository_1 = __importDefault(require("../cekPay.repository"));
const provider_repository_1 = require("../provider/provider.repository");
const pulsaTransaction_repository_1 = require("./pulsaTransaction.repository");
const pulsaTransaction_schema_1 = require("./pulsaTransaction.schema");
class PulsaTransactionController {
}
exports.default = PulsaTransactionController;
PulsaTransactionController.priceList = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { value, error } = pulsaTransaction_schema_1.MobileNumberRequestSchema.validate(req.params);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, `${error}`);
        }
        const providerName = yield provider_repository_1.ProviderRepository.getProvider(value.nomorHP);
        if (!providerName) {
            return responseHandler_1.ResponseHandler.jsonSuccess(res, 'Provider tidak ditemukan');
        }
        const userRequest = {
            method: cekPayMethod_1.cekPayMethod.pulsa.checkGroupPrices,
            produk: '',
            group: providerName.provider,
        };
        const rajaBillerRequest = Object.assign(Object.assign({}, credential_1.rajaBillerCredential), userRequest);
        const rajabillerResponse = yield axios_1.default.post(process.env.RAJABILLER_ENDPOINT, rajaBillerRequest)
            .then((response) => response.data.DATA)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, `Network error ${err}`));
        let data = [];
        if (rajabillerResponse.length > 0) {
            const filter = rajabillerResponse.filter((item) => item.idproduk.substr(1, 1).match('[0-9]'));
            const addNominal = filter.map((v, k) => {
                const obj = {};
                obj.nominal = Number(v.idproduk.replace(/[^0-9]+/g, '') + '000');
                return Object.assign(Object.assign({}, v), obj);
            });
            data = addNominal.sort((a, b) => a.nominal - b.nominal);
        }
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
PulsaTransactionController.checkout = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const uid = uuid_1.v4().toUpperCase();
        const invoiceNumber = yield cekPay_repository_1.default.invoiceNumber();
        const billAmount = Number(req.body.harga) + Number(req.body.margin) + Number(req.body.admin) - Number(req.body.point);
        const generatedPayment = (req.body.metodePembayaran === 'EWALLET') ? yield paymentHelper_1.generatePayment(paymentHelper_1.generateCallback(req), invoiceNumber, billAmount, req.body.merchant) : yield paymentHelper_1.generateInvoice(invoiceNumber, billAmount, invoiceNumber, req.body.merchant);
        if (!generatedPayment) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Gagal membuat payment');
        }
        let paymentExpiredAt = moment_1.default(generatedPayment.expiry_date).format('YYYY-MM-DD HH:mm:ss');
        if (req.body.metodePembayaran === 'EWALLET') {
            paymentExpiredAt = req.body.merchant === 'ID_OVO' ? moment_1.default().add(55, 's').format('YYYY-MM-DD HH:mm:ss') : moment_1.default().add(30, 'm').format('YYYY-MM-DD HH:mm:ss');
        }
        const resultToDatabase = {
            uid,
            userId: req.headers.id,
            date: moment_1.default().format('YYYY-MM-DD'),
            invoiceNumber,
            accNumber1: req.body.nomorHP,
            category: req.body.kategori,
            providerName: req.body.provider,
            productCode: req.body.kodeProduk,
            product: `${req.body.provider} Rp. ${req.body.nominal.toLocaleString('ID')}`,
            nominal: req.body.nominal,
            basicPrice: req.body.harga,
            margin: req.body.margin,
            adminPayment: req.body.admin,
            commision: 0,
            billAmount,
            poin: req.body.point,
            paymentId: generatedPayment.id,
            paymentMethod: req.body.metodePembayaran,
            merchant: req.body.merchant,
            merchantNumber: req.body.nomorAkun,
            voucherCode: req.body.kodeVoucher,
            paymentExpiredAt,
        };
        const { value, error } = pulsaTransaction_schema_1.PulsaPrepaidTransactionRequestSchema.validate(resultToDatabase);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, `${error}`);
        }
        const dataDatabase = yield cekPay_repository_1.default.create(value);
        const responseDatabase = Object.assign(Object.assign({}, dataDatabase.get()), { payment: Object.assign({}, generatedPayment) });
        return responseHandler_1.ResponseHandler.jsonSuccess(res, responseDatabase);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res, error);
    }
});
PulsaTransactionController.purchase = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const invoiceNumber = String(req.query.invoiceNumber);
        const transaction = yield cekPay_repository_1.default.getInvoiceNumber(invoiceNumber);
        if (!transaction) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor invoice tidak ditemukan');
        }
        if ((transaction.paymentStatus === 'PAID' || transaction.paymentStatus === 'SUCCEEDED') && transaction.transactionStatus === 'SUKSES') {
            return responseHandler_1.ResponseHandler.jsonError(res, `Transaksi telah berhasil dilakukan pada ${moment_1.default(transaction.updatedAt).format('YYYY-MM-DD HH:mm:ss')}`);
        }
        if (transaction.paymentStatus === 'EXPIRED') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah melebihi batas pembayaran');
        }
        if (moment_1.default(transaction.paymentExpiredAt).isBefore(moment_1.default())) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah melebihi batas pembayaran');
        }
        let paymentStatus = 'PENDING';
        if (transaction.paymentMethod === 'EWALLET') {
            paymentStatus = yield paymentHelper_1.checkPaymentStatus(transaction.paymentId, 'EWALLET');
            if (paymentStatus === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, `Selesaikan pembayaran sebelum ${moment_1.default(transaction.paymentExpiredAt).format('YYYY-MM-DD HH:mm:ss')}`);
            }
        }
        else {
            paymentStatus = yield paymentHelper_1.checkPaymentStatus(transaction.paymentId, 'BANK');
            console.log({ paymentStatus });
            if (paymentStatus === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, `Selesaikan pembayaran sebelum ${moment_1.default(transaction.paymentExpiredAt).format('YYYY-MM-DD HH:mm:ss')}`);
            }
        }
        const transactionNumber = yield cekPay_repository_1.default.invoiceNumber('transaction');
        const data = Object.assign(Object.assign({}, credential_1.rajaBillerCredential), { 'method': cekPayMethod_1.cekPayMethod.pulsa.transactionBuy, 'no_hp': transaction.accNumber1, 'kode_produk': transaction.productCode, 'ref1': transactionNumber });
        const response = yield axios_1.default.post(process.env.RAJABILLER_ENDPOINT, data)
            .then((item) => item.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, `Network error ${err}`));
        const updateToDatabase = {
            transactionNumber,
            deductedBalance: response.SALDO_TERPOTONG,
            endingBalance: response.SISA_SALDO,
            paymentStatus,
            transactionStatus: response.STATUS_TRX,
        };
        yield cekPay_repository_1.default.update(updateToDatabase, invoiceNumber);
        const dataReponse = {
            waktu: moment_1.default(response.WAKTU, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss'),
            kodeProduk: response.KODE_PRODUK,
            nomorHP: response.NO_HP,
            periode: response.PERIODE,
            harga: Number(response.NOMINAL),
            ref1: response.REF1,
            ref2: response.REF2,
            status: response.STATUS,
            ket: response.KET,
            saldoTerpotong: response.SALDO_TERPOTONG,
            sisaSaldo: response.SISA_SALDO,
            serialNumber: response.SN,
        };
        return responseHandler_1.ResponseHandler.jsonSuccess(res, Object.assign({}, dataReponse));
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
PulsaTransactionController.history = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const status = (req.body.status) ? req.body.status : '';
        const minDate = (req.body.minDate) ? req.body.minDate : null;
        const maxDate = (req.body.maxDate) ? req.body.maxDate : moment_1.default();
        const transaksi = yield pulsaTransaction_repository_1.PulsaTransactionRepository.getByUserId(req.headers.id, status, minDate, maxDate);
        return (transaksi) ? responseHandler_1.ResponseHandler.jsonSuccess(res, transaksi) : responseHandler_1.ResponseHandler.jsonError(res, 'Data tidak ditemukan');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=pulsaTransaction.controller.js.map