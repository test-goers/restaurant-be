"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Telkom = void 0;
const moment_1 = __importDefault(require("moment"));
const sequelize_typescript_1 = require("sequelize-typescript");
let Telkom = class Telkom extends sequelize_typescript_1.Model {
    get batasPembayaran() {
        const theDate = this.getDataValue('batasPembayaran');
        return moment_1.default(theDate).format('YYYY-MM-DD HH:mm:ss');
    }
};
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "uid", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Telkom.prototype, "userId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "paymentId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "kodeProduk", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Telkom.prototype, "waktu", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "pelangganId1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "pelangganId2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "pelangganId3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "namaPelanggan", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "periode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Telkom.prototype, "nominal", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Telkom.prototype, "admin", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Telkom.prototype, "adminPayment", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Telkom.prototype, "point", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "metodePembayaran", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "merchant", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "nomorAkun", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "kodeVoucher", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "urlStruk", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "ref1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "ref2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "ref3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Telkom.prototype, "saldoTerpotong", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Telkom.prototype, "sisaSaldo", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "detail", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "statusTrx", void 0);
__decorate([
    sequelize_typescript_1.Column(sequelize_typescript_1.DataType.DATE),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [])
], Telkom.prototype, "batasPembayaran", null);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Telkom.prototype, "statusPembayaran", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Telkom.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Telkom.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Telkom.prototype, "deletedAt", void 0);
Telkom = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'TransaksiTelkom',
        timestamps: true,
        paranoid: true,
    })
], Telkom);
exports.Telkom = Telkom;
//# sourceMappingURL=telkomTransaction.model.js.map