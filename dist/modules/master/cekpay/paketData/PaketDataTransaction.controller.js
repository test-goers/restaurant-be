"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const uuid_1 = require("uuid");
const paymentHelper_1 = require("../../../../helpers/paymentHelper");
const responseHandler_1 = require("../../../core/helpers/responseHandler");
const cekPay_repository_1 = __importDefault(require("../cekPay.repository"));
const pulsaTransaction_schema_1 = require("./../pulsa/pulsaTransaction.schema");
const paketData_repository_1 = require("./paketData.repository");
class PaketDataTransactionController {
}
exports.default = PaketDataTransactionController;
PaketDataTransactionController.checkout = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const uid = uuid_1.v4().toUpperCase();
        const invoiceNumber = yield cekPay_repository_1.default.invoiceNumber();
        const billAmount = Number(req.body.harga) + Number(req.body.margin) + Number(req.body.admin) - Number(req.body.point);
        const generatedPayment = (req.body.metodePembayaran === 'EWALLET') ? yield paymentHelper_1.generatePayment(paymentHelper_1.generateCallback(req), invoiceNumber, billAmount, req.body.merchant) : yield paymentHelper_1.generateInvoice(invoiceNumber, billAmount, invoiceNumber, req.body.merchant);
        if (!generatedPayment) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Gagal membuat payment');
        }
        let paymentExpiredAt = moment_1.default(generatedPayment.expiry_date).format('YYYY-MM-DD HH:mm:ss');
        if (req.body.metodePembayaran === 'EWALLET') {
            paymentExpiredAt = req.body.merchant === 'ID_OVO' ? moment_1.default().add(55, 's').format('YYYY-MM-DD HH:mm:ss') : moment_1.default().add(30, 'm').format('YYYY-MM-DD HH:mm:ss');
        }
        const resultToDatabase = {
            uid,
            userId: req.headers.id,
            date: moment_1.default().format('YYYY-MM-DD'),
            invoiceNumber,
            accNumber1: req.body.nomorHP,
            category: req.body.kategori,
            product: req.body.produk,
            providerName: req.body.provider,
            productCode: req.body.kodeProduk,
            nominal: req.body.nominal,
            basicPrice: req.body.harga,
            margin: req.body.margin,
            adminPayment: req.body.admin,
            commision: 0,
            billAmount,
            poin: req.body.point,
            paymentId: generatedPayment.id,
            paymentMethod: req.body.metodePembayaran,
            merchant: req.body.merchant,
            merchantNumber: req.body.nomorAkun,
            voucherCode: req.body.kodeVoucher,
            paymentExpiredAt,
        };
        const { value, error } = pulsaTransaction_schema_1.PulsaPrepaidTransactionRequestSchema.validate(resultToDatabase);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, `${error}`);
        }
        const dataDatabase = yield cekPay_repository_1.default.create(value);
        const responseDatabase = Object.assign(Object.assign({}, dataDatabase.get()), { payment: Object.assign({}, generatedPayment) });
        return responseHandler_1.ResponseHandler.jsonSuccess(res, responseDatabase);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res, error);
    }
});
PaketDataTransactionController.history = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const status = (req.body.status) ? req.body.status : '';
        const minDate = (req.body.minDate) ? req.body.minDate : null;
        const maxDate = (req.body.maxDate) ? req.body.maxDate : moment_1.default();
        const transaksi = yield paketData_repository_1.PaketDataRepository.getByUserId(req.headers.user_id, status, minDate, maxDate);
        return (transaksi) ? responseHandler_1.ResponseHandler.jsonSuccess(res, transaksi) : responseHandler_1.ResponseHandler.jsonError(res, 'Data tidak ditemukan');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=PaketDataTransaction.controller.js.map