"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaketData = void 0;
const moment_1 = __importDefault(require("moment"));
const sequelize_typescript_1 = require("sequelize-typescript");
let PaketData = class PaketData extends sequelize_typescript_1.Model {
    get batasPembayaran() {
        const theDate = this.getDataValue('batasPembayaran');
        return moment_1.default(theDate).format('YYYY-MM-DD HH:mm:ss');
    }
};
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "uid", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PaketData.prototype, "userId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "nomorTransaksi", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "paymentId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "nomorHP", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "provider", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "produk", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "kodeProduk", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PaketData.prototype, "nominal", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PaketData.prototype, "harga", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PaketData.prototype, "margin", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PaketData.prototype, "admin", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PaketData.prototype, "adminPayment", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PaketData.prototype, "komisi", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PaketData.prototype, "point", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "metodePembayaran", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "merchant", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "nomorAkun", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "kodeVoucher", void 0);
__decorate([
    sequelize_typescript_1.Column(sequelize_typescript_1.DataType.DATE),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [])
], PaketData.prototype, "batasPembayaran", null);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PaketData.prototype, "saldoTerpotong", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PaketData.prototype, "sisaSaldo", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "statusTrx", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PaketData.prototype, "statusPembayaran", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PaketData.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PaketData.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PaketData.prototype, "deletedAt", void 0);
PaketData = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'TransaksiPaketData',
        timestamps: true,
        paranoid: true,
    })
], PaketData);
exports.PaketData = PaketData;
//# sourceMappingURL=paketData.model.js.map