"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaketDataRepository = void 0;
const moment_1 = __importDefault(require("moment"));
const sequelize_1 = require("sequelize");
const paketData_model_1 = require("./paketData.model");
class PaketDataRepository {
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const creating = yield paketData_model_1.PaketData.create(Object.assign({}, data));
            return creating;
        });
    }
    static getDetailByTransaction(nomorTransaksi) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield paketData_model_1.PaketData.findOne({
                where: { nomorTransaksi },
            });
            return data;
        });
    }
    static checkTransactionStatus(nomorTransaksi) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield paketData_model_1.PaketData.findOne({
                where: { nomorTransaksi },
            });
            return data.statusTrx;
        });
    }
    static update(data, nomorTransaksi) {
        return __awaiter(this, void 0, void 0, function* () {
            yield paketData_model_1.PaketData.update(Object.assign({}, data), {
                where: { nomorTransaksi },
            });
            const transaksi = paketData_model_1.PaketData.findOne({
                where: { nomorTransaksi },
            });
            return transaksi;
        });
    }
    static getByUserId(userId, statusTrx, minDate = null, maxDate = null, offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            if (statusTrx === '') {
                return this.getAllByUserId(userId, minDate, maxDate, offset, limit);
            }
            const transaksi = yield paketData_model_1.PaketData.findAndCountAll({
                where: {
                    [sequelize_1.Op.and]: {
                        userId,
                        statusTrx,
                        createdAt: {
                            [sequelize_1.Op.gte]: moment_1.default(minDate).toDate(),
                            [sequelize_1.Op.lte]: moment_1.default(maxDate).toDate(),
                        },
                    },
                },
                order: [
                    ['createdAt', 'DESC'],
                ],
                offset,
                limit,
            });
            return transaksi;
        });
    }
    static getAllByUserId(userId, minDate = null, maxDate = null, offset = 0, limit = 0) {
        return __awaiter(this, void 0, void 0, function* () {
            const transaksi = yield paketData_model_1.PaketData.findAndCountAll({
                where: {
                    [sequelize_1.Op.and]: {
                        userId,
                        createdAt: {
                            [sequelize_1.Op.gte]: moment_1.default(minDate).toDate(),
                            [sequelize_1.Op.lte]: moment_1.default(maxDate).toDate(),
                        },
                    },
                },
                order: [
                    ['createdAt', 'DESC'],
                ],
                offset,
                limit,
            });
            return transaksi;
        });
    }
}
exports.PaketDataRepository = PaketDataRepository;
//# sourceMappingURL=paketData.repository.js.map