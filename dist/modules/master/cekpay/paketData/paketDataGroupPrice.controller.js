"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const cekPayMethod_1 = require("../../../../helpers/cekPayMethod");
const credential_1 = require("../../../core/config/credential");
const responseHandler_1 = require("../../../core/helpers/responseHandler");
const provider_repository_1 = require("../provider/provider.repository");
class PaketDataGroupPriceController {
}
exports.default = PaketDataGroupPriceController;
PaketDataGroupPriceController.priceList = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const providerName = yield provider_repository_1.ProviderRepository.getProvider(req.params.nomorHP);
        const userRequest = {
            method: cekPayMethod_1.cekPayMethod.pulsa.checkGroupPrices,
            produk: '',
            group: providerName.provider,
        };
        const rajaBillerRequest = Object.assign(Object.assign({}, credential_1.rajaBillerCredential), userRequest);
        const rajabillerResponse = yield axios_1.default.post(process.env.RAJABILLER_ENDPOINT, rajaBillerRequest)
            .then((response) => response.data.DATA);
        const dataPattern = ['SD', 'ID', 'IG', 'FI', 'XD', 'AX', 'TD', 'SM', 'CM'];
        const filter = rajabillerResponse.filter((item) => dataPattern.includes(item.idproduk.substr(0, 2)) === true);
        const addNominal = filter.map((value, key) => {
            const obj = {};
            obj.nominal = Number(value.idproduk.replace(/[^0-9]+/g, '') + '000');
            return Object.assign(Object.assign({}, value), obj);
        });
        const data = addNominal.sort((a, b) => a.nominal - b.nominal);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res, error);
    }
});
//# sourceMappingURL=paketDataGroupPrice.controller.js.map