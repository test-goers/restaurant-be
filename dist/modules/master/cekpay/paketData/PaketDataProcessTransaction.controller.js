"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const moment_1 = __importDefault(require("moment"));
const cekPayMethod_1 = require("../../../../helpers/cekPayMethod");
const paymentHelper_1 = require("../../../../helpers/paymentHelper");
const credential_1 = require("../../../core/config/credential");
const responseHandler_1 = require("../../../core/helpers/responseHandler");
const cekPay_repository_1 = __importDefault(require("../cekPay.repository"));
class PaketDataProcessTransactionController {
}
exports.default = PaketDataProcessTransactionController;
PaketDataProcessTransactionController.purchase = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const invoiceNumber = String(req.query.invoiceNumber);
        const transaction = yield cekPay_repository_1.default.getInvoiceNumber(invoiceNumber);
        if (!transaction) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor invoice tidak ditemukan');
        }
        if ((transaction.paymentStatus === 'PAID' || transaction.paymentStatus === 'SUCCEEDED') && transaction.transactionStatus === 'SUKSES') {
            return responseHandler_1.ResponseHandler.jsonError(res, `Transaksi telah berhasil dilakukan pada ${moment_1.default(transaction.updatedAt).format('YYYY-MM-DD HH:mm:ss')}`);
        }
        if (transaction.paymentStatus === 'EXPIRED') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah melebihi batas pembayaran');
        }
        if (moment_1.default(transaction.paymentExpiredAt).isBefore(moment_1.default()) && transaction.paymentStatus === 'PENDING') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah melebihi batas pembayaran');
        }
        let paymentStatus = 'PENDING';
        if (transaction.paymentMethod === 'EWALLET') {
            paymentStatus = yield paymentHelper_1.checkPaymentStatus(transaction.paymentId, 'EWALLET');
            if (paymentStatus === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, `Selesaikan pembayaran sebelum ${moment_1.default(transaction.paymentExpiredAt).format('YYYY-MM-DD HH:mm:ss')}`);
            }
        }
        else {
            paymentStatus = yield paymentHelper_1.checkPaymentStatus(transaction.paymentId, 'BANK');
            console.log({ paymentStatus });
            if (paymentStatus === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, `Selesaikan pembayaran sebelum ${moment_1.default(transaction.paymentExpiredAt).format('YYYY-MM-DD HH:mm:ss')}`);
            }
        }
        const transactionNumber = yield cekPay_repository_1.default.invoiceNumber('transaction');
        const data = Object.assign(Object.assign({}, credential_1.rajaBillerCredential), { 'method': cekPayMethod_1.cekPayMethod.pulsa.transactionBuy, 'no_hp': transaction.accNumber1, 'kode_produk': transaction.productCode, 'ref1': transactionNumber });
        const response = yield axios_1.default.post(process.env.RAJABILLER_ENDPOINT, data)
            .then((item) => item.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, `Network error ${err}`));
        const updateToDatabase = {
            endingBalance: response.SISA_SALDO,
            deductedBalance: response.SALDO_TERPOTONG,
            transactionStatus: response.STATUS_TRX,
            paymentStatus,
            transactionNumber,
        };
        yield cekPay_repository_1.default.update(updateToDatabase, invoiceNumber);
        const dataReponse = {
            waktu: moment_1.default(response.WAKTU, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss'),
            kodeProduk: response.KODE_PRODUK,
            nomorHP: response.NO_HP,
            periode: response.PERIODE,
            harga: Number(response.NOMINAL),
            ref1: response.REF1,
            ref2: response.REF2,
            status: response.STATUS,
            ket: response.KET,
            saldoTerpotong: response.SALDO_TERPOTONG,
            sisaSaldo: response.SISA_SALDO,
            serialNumber: response.SN,
        };
        return responseHandler_1.ResponseHandler.jsonSuccess(res, Object.assign({}, dataReponse));
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=PaketDataProcessTransaction.controller.js.map