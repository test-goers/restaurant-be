"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const sequelize_typescript_1 = require("sequelize-typescript");
let CekPay = class CekPay extends sequelize_typescript_1.Model {
    get date() {
        const theDate = moment_1.default(this.getDataValue('date')).format('YYYY-MM-DD');
        return this.getDataValue('date') ? theDate : null;
    }
    get paymentExpiredAt() {
        const theDate = moment_1.default(this.getDataValue('paymentExpiredAt')).format('YYYY-MM-DD HH:mm:ss');
        return this.getDataValue('paymentExpiredAt') ? theDate : null;
    }
    get transactionExpiredAt() {
        const theDate = moment_1.default(this.getDataValue('transactionExpiredAt')).format('YYYY-MM-DD HH:mm:ss');
        return this.getDataValue('transactionExpiredAt') ? theDate : null;
    }
    get paidAt() {
        const theDate = moment_1.default(this.getDataValue('paidAt')).format('YYYY-MM-DD HH:mm:ss');
        return this.getDataValue('paidAt') ? theDate : null;
    }
};
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "uid", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "userId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "partnerProduct", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "partnerPayment", void 0);
__decorate([
    sequelize_typescript_1.Column(sequelize_typescript_1.DataType.DATEONLY),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [])
], CekPay.prototype, "date", null);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "transactionNumber", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "invoiceNumber", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "accNumber1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "accNumber2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "accNumber3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "category", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "providerName", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "productCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "product", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "nominal", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "basicPrice", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "sellPrice", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "margin", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "adminPayment", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "adminTransaction", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "commision", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "poin", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "voucherCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "billAmount", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "paymentId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "paymentMethod", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "merchant", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "merchantNumber", void 0);
__decorate([
    sequelize_typescript_1.Column(sequelize_typescript_1.DataType.DATE),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [])
], CekPay.prototype, "paymentExpiredAt", null);
__decorate([
    sequelize_typescript_1.Column(sequelize_typescript_1.DataType.DATE),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [])
], CekPay.prototype, "transactionExpiredAt", null);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "paymentStatus", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "transactionStatus", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "ref1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "ref2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "ref3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "deductedBalance", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "endingBalance", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "paymentMethod2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPay.prototype, "paymentChannel", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "amount", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "paidAmount", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "feesPaidAmount", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], CekPay.prototype, "adjustedReceivedAmount", void 0);
__decorate([
    sequelize_typescript_1.Column(sequelize_typescript_1.DataType.DATE),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [])
], CekPay.prototype, "paidAt", null);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], CekPay.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], CekPay.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], CekPay.prototype, "deletedAt", void 0);
CekPay = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'CekPay',
        timestamps: true,
        paranoid: true,
    })
], CekPay);
exports.default = CekPay;
//# sourceMappingURL=cekPay.model.js.map