"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PLNPostpaidTransactionRepository = void 0;
const moment_1 = __importDefault(require("moment"));
const sequelize_1 = require("sequelize");
const plnPostpaidTransaction_model_1 = require("./plnPostpaidTransaction.model");
class PLNPostpaidTransactionRepository {
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const creating = yield plnPostpaidTransaction_model_1.PLNPostpaidTransaction.create(Object.assign({}, data));
            return creating;
        });
    }
    static getDetailByTransaction(referenceCode) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield plnPostpaidTransaction_model_1.PLNPostpaidTransaction.findOne({
                where: { referenceCode },
            });
            return data;
        });
    }
    static update(data, referenceCode) {
        return __awaiter(this, void 0, void 0, function* () {
            yield plnPostpaidTransaction_model_1.PLNPostpaidTransaction.update(data, {
                where: { referenceCode },
            });
            const transaksi = plnPostpaidTransaction_model_1.PLNPostpaidTransaction.findOne({
                where: { referenceCode },
            });
            return transaksi;
        });
    }
    static getByUserId(userId, minDate = null, maxDate = null, offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const transaksi = yield plnPostpaidTransaction_model_1.PLNPostpaidTransaction.findAndCountAll({
                where: {
                    [sequelize_1.Op.and]: {
                        userId,
                        createdAt: {
                            [sequelize_1.Op.gte]: moment_1.default(minDate).toDate(),
                            [sequelize_1.Op.lte]: moment_1.default(maxDate).toDate(),
                        },
                    },
                },
                order: [
                    ['createdAt', 'DESC'],
                ],
                offset,
                limit,
            });
            return transaksi;
        });
    }
    static getAllByUserId(userId, minDate = null, maxDate = null) {
        return __awaiter(this, void 0, void 0, function* () {
            const transaksi = yield plnPostpaidTransaction_model_1.PLNPostpaidTransaction.findAll({
                where: {
                    [sequelize_1.Op.and]: {
                        userId,
                        createdAt: {
                            [sequelize_1.Op.gte]: moment_1.default(minDate).toDate(),
                            [sequelize_1.Op.lte]: moment_1.default(maxDate).toDate(),
                        },
                    },
                },
                order: [
                    ['createdAt', 'DESC'],
                ],
            });
            return transaksi;
        });
    }
}
exports.PLNPostpaidTransactionRepository = PLNPostpaidTransactionRepository;
//# sourceMappingURL=plnPostpaidTransaction.repository.js.map