"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const md5_1 = __importDefault(require("md5"));
const moment_1 = __importDefault(require("moment"));
const uuid_1 = require("uuid");
const paymentHelper_1 = require("../../../../../helpers/paymentHelper");
const responseHandler_1 = require("../../../../core/helpers/responseHandler");
const plnPostpaidTransaction_repository_1 = require("./plnPostpaidTransaction.repository");
const plnPostpaidTransaction_schema_1 = require("./plnPostpaidTransaction.schema");
class PLNPostpaidTransactionController {
}
exports.default = PLNPostpaidTransactionController;
PLNPostpaidTransactionController.inquiry = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const uid = uuid_1.v4().toUpperCase();
        const referencecode = 'PLNPOSTPAID-' + uid;
        const signature = md5_1.default(process.env.PASTI_USERID + process.env.PASTI_PIN + referencecode);
        const cekTokoRequest = {
            transactioncode: 'pasti-inquiry',
            userid: process.env.PASTI_USERID,
            userpin: process.env.PASTI_PIN,
            signature,
            referencecode,
            productcode: 'HPLNPASCA',
            accnumber1: req.params.accNumber,
            accnumber2: '',
            accnumber3: '',
            ref1: '',
        };
        const { value, error } = plnPostpaidTransaction_schema_1.InquiryListrikPascaRequestSchema.validate(cekTokoRequest);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error ' + error);
        }
        const pastiRequest = yield axios_1.default.post(process.env.PASTI_ENDPOINT, value)
            .then((response) => response.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, 'Network error ' + error));
        const pastiResponse = pastiRequest;
        const tempResponse = {
            uid,
            traceCode: pastiResponse.tracecode,
            inquiryCode: pastiResponse.transactionstatus,
            inquiryExpired: moment_1.default(pastiResponse.inquiryexpired).format('YYYY-MM-DD HH:mm:ss'),
            referenceCode: referencecode,
            accNumber1: pastiResponse.accnumber1 ? pastiResponse.accnumber1 : null,
            accNumber2: pastiResponse.accnumber2 ? pastiResponse.accnumber2 : null,
            accNumber3: pastiResponse.accnumber3 ? pastiResponse.accnumber3 : null,
            contactNumber: pastiResponse.contactnumber ? pastiResponse.contactnumber : null,
            nominal: pastiResponse.nominal ? pastiResponse.nominal : 0,
            adminFee: pastiResponse.adminfee ? pastiResponse.adminfee : 0,
            ref1: pastiResponse.ref1 ? pastiResponse.ref1 : null,
            ref2: pastiResponse.ref2 ? pastiResponse.ref2 : null,
            ref3: pastiResponse.ref3 ? pastiResponse.ref3 : null,
        };
        const additionalData = pastiResponse.additionaldata;
        const data = Object.assign(Object.assign({}, tempResponse), additionalData);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res, error);
    }
});
PLNPostpaidTransactionController.checkout = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const uid = uuid_1.v4().toUpperCase();
        const referencecode = 'PLNPOSTPAID-' + uid;
        const signature = md5_1.default(process.env.PASTI_USERID + process.env.PASTI_PIN + referencecode);
        const cekTokoRequest = {
            transactioncode: 'pasti-inquiry',
            userid: process.env.PASTI_USERID,
            userpin: process.env.PASTI_PIN,
            signature,
            referencecode,
            productcode: 'HPLNPASCA',
            accnumber1: req.body.accNumber,
            accnumber2: '',
            accnumber3: '',
            ref1: '',
        };
        const { value, error } = plnPostpaidTransaction_schema_1.InquiryListrikPascaRequestSchema.validate(cekTokoRequest);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error ' + error);
        }
        const pastiRequest = yield axios_1.default.post(process.env.PASTI_ENDPOINT, value)
            .then((response) => response);
        // .catch((err) => ResponseHandler.jsonError(res, 'Network error ' + err))
        const pastiResponse = pastiRequest.data;
        const harga = Number(pastiResponse.nominal) + Number(pastiResponse.adminfee) - req.body.point;
        const generatedPayment = (req.body.paymentMethod === 'EWALLET') ? yield paymentHelper_1.generatePayment(paymentHelper_1.generateCallback(req), referencecode, harga, req.body.merchant) : null; // payment bank
        console.log(generatedPayment);
        if (!generatedPayment) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Gagal membuat payment');
        }
        const data = {
            uid,
            userId: req.headers.id,
            traceCode: pastiResponse.tracecode,
            inquiryCode: pastiResponse.transactionstatus,
            inquiryExpired: moment_1.default(pastiResponse.inquiryexpired, 'YYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss'),
            transactionStatus: Number(pastiResponse.transactionstatus),
            referenceCode: referencecode,
            transactionStatusText: pastiRequest.statusText,
            transactionTime: moment_1.default(pastiResponse.responsetime, 'YYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss'),
            accName: pastiResponse.accname === '' ? null : pastiResponse.accname,
            accNumber1: pastiResponse.accnumber1 === '' ? null : pastiResponse.accnumber1,
            accNumber2: pastiResponse.accnumber2 === '' ? null : pastiResponse.accnumber2,
            accNumber3: pastiResponse.accnumber3 === '' ? null : pastiResponse.accnumber3,
            nominal: Number(pastiResponse.nominal),
            adminFee: Number(pastiResponse.adminfee),
            ref1: pastiResponse.ref1 === '' ? null : pastiResponse.ref1,
            ref2: pastiResponse.ref2 === '' ? null : pastiResponse.ref2,
            ref3: pastiResponse.ref3 === '' ? null : pastiResponse.ref3,
            contactNumber: req.body.contactNumber,
            billingPeriod: pastiResponse.billingperiod === '' ? null : pastiResponse.billingperiod,
            additionalData: pastiResponse.additionaldata,
            point: req.body.point,
            paymentMethod: req.body.paymentMethod,
            merchant: req.body.merchant,
            merchantNumber: req.body.merchantNumber,
            voucherCode: req.body.voucherCode,
            statusPembayaran: generatedPayment.status,
        };
        const dataDatabase = yield plnPostpaidTransaction_repository_1.PLNPostpaidTransactionRepository.create(data);
        const responseDatabase = Object.assign(Object.assign({}, dataDatabase.get()), { payment: Object.assign({}, generatedPayment) });
        return responseHandler_1.ResponseHandler.jsonSuccess(res, responseDatabase);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res, error);
    }
});
PLNPostpaidTransactionController.purchase = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const transaction = yield plnPostpaidTransaction_repository_1.PLNPostpaidTransactionRepository.getDetailByTransaction(req.params.referenceCode);
        if (!transaction) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor transaksi tidak ditemukan');
        }
        let statusPembayaran = 'PENDING';
        if (transaction.paymentMethod === 'EWALLET') {
            statusPembayaran = yield paymentHelper_1.checkPaymentStatus(transaction.paymentId, 'EWALLET');
            if (statusPembayaran === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, 'Silahkan selesaikan pembayaran terlebih dahulu');
            }
        }
        else {
            statusPembayaran = yield paymentHelper_1.checkPaymentStatus(transaction.paymentId, 'BANK');
            if (statusPembayaran === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, 'Silahkan selesaikan pembayaran terlebih dahulu');
            }
        }
        const signature = md5_1.default(process.env.PASTI_USERID + process.env.PASTI_PIN + req.params.referenceCode);
        const cekTokoPurchaseRequest = {
            transactioncode: 'pasti-pembayaran',
            userid: process.env.PASTI_USERID,
            userpin: process.env.PASTI_PIN,
            signature,
            referencecode: transaction.referenceCode,
            productcode: 'HPLNPASCA',
            accnumber1: transaction.accNumber1 ? transaction.accNumber1 : '',
            accnumber2: transaction.accNumber2 ? transaction.accNumber2 : '',
            accnumber3: transaction.accNumber3 ? transaction.accNumber3 : '',
            contactnumber: transaction.contactNumber ? transaction.contactNumber : '',
            nominal: transaction.nominal,
            adminfee: transaction.adminFee,
            ref1: transaction.ref1 ? transaction.ref1 : '',
            ref2: transaction.ref2 ? transaction.ref2 : '',
            ref3: transaction.ref3 ? transaction.ref3 : '',
            inquirycode: transaction.inquiryCode,
        };
        const { value, error } = plnPostpaidTransaction_schema_1.PurchaseListrikPascaRequestSchema.validate(cekTokoPurchaseRequest);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error ' + error);
        }
        const data = yield axios_1.default.post(process.env.PASTI_ENDPOINT, value)
            .then((response) => response.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, 'Network error ' + err));
        const pastiTransactionStatus = {
            billingPeriod: data.billingperiod,
            info: data.info,
            additionalData: data.additionalData,
            transactionStatus: data.transactionstatus,
            transactionStatusText: '',
            statusPembayaran,
        };
        yield plnPostpaidTransaction_repository_1.PLNPostpaidTransactionRepository.update(pastiTransactionStatus, req.params.referenceCode);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res, error);
    }
});
PLNPostpaidTransactionController.status = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const transaction = yield plnPostpaidTransaction_repository_1.PLNPostpaidTransactionRepository.getDetailByTransaction(req.params.referencecode);
        if (!transaction) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor transaksi tidak ditemukan');
        }
        const cekTokoStatusRequest = {
            transactioncode: 'pasti-cek-status',
            tracecode: transaction.traceCode,
            userid: process.env.PASTI_USERID,
            userpin: process.env.PASTI_PIN,
            signature: md5_1.default(process.env.PASTI_USERID + process.env.PASTI_PIN + transaction.referenceCode),
            referencecode: transaction.referenceCode,
        };
        const { value, error } = plnPostpaidTransaction_schema_1.StatusListrikPascaRequestSchema.validate(cekTokoStatusRequest);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error ' + error);
        }
        const data = yield axios_1.default.post(process.env.PASTI_ENDPOINT, value)
            .then((response) => response.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, 'Network error ' + err));
        const pastiTransactionStatus = {
            transactionStatus: data.transactionstatus,
        };
        yield plnPostpaidTransaction_repository_1.PLNPostpaidTransactionRepository.update(pastiTransactionStatus, req.body.referenceCode);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
PLNPostpaidTransactionController.history = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const minDate = (req.body.minDate) ? req.body.minDate : null;
        const maxDate = (req.body.maxDate) ? req.body.maxDate : moment_1.default();
        const transaksi = yield plnPostpaidTransaction_repository_1.PLNPostpaidTransactionRepository.getByUserId(req.headers.user_id, minDate, maxDate);
        return (transaksi) ? responseHandler_1.ResponseHandler.jsonSuccess(res, transaksi) : responseHandler_1.ResponseHandler.jsonError(res, 'Data tidak ditemukan');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=plnPostpaidTransaction.controller.js.map