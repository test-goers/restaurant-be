"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatusListrikPascaRequestSchema = exports.PurchaseListrikPascaRequestSchema = exports.InquiryListrikPascaRequestSchema = void 0;
const typesafe_joi_1 = __importDefault(require("typesafe-joi"));
exports.InquiryListrikPascaRequestSchema = typesafe_joi_1.default.object({
    transactioncode: typesafe_joi_1.default.string().required(),
    userid: typesafe_joi_1.default.string().required(),
    userpin: typesafe_joi_1.default.string().required(),
    referencecode: typesafe_joi_1.default.string().required(),
    signature: typesafe_joi_1.default.string().required(),
    productcode: typesafe_joi_1.default.string().required(),
    accnumber1: typesafe_joi_1.default.string().required(),
    accnumber2: typesafe_joi_1.default.string().optional().allow(null, ''),
    accnumber3: typesafe_joi_1.default.string().optional().allow(null, ''),
    ref1: typesafe_joi_1.default.string().optional().allow(null, ''),
});
exports.PurchaseListrikPascaRequestSchema = typesafe_joi_1.default.object({
    transactioncode: typesafe_joi_1.default.string().required(),
    userid: typesafe_joi_1.default.string().required(),
    userpin: typesafe_joi_1.default.string().required(),
    signature: typesafe_joi_1.default.string().required(),
    referencecode: typesafe_joi_1.default.string().required(),
    productcode: typesafe_joi_1.default.string().required(),
    accnumber1: typesafe_joi_1.default.string().required(),
    accnumber2: typesafe_joi_1.default.string().required(),
    accnumber3: typesafe_joi_1.default.string().required(),
    contactnumber: typesafe_joi_1.default.string().required(),
    nominal: typesafe_joi_1.default.string().required(),
    adminfee: typesafe_joi_1.default.string().required(),
    ref1: typesafe_joi_1.default.string().required(),
    ref2: typesafe_joi_1.default.string().required(),
    ref3: typesafe_joi_1.default.string().required(),
    inquirycode: typesafe_joi_1.default.string().required(),
});
exports.StatusListrikPascaRequestSchema = typesafe_joi_1.default.object({
    transactioncode: typesafe_joi_1.default.string().required(),
    tracecode: typesafe_joi_1.default.string().required(),
    userid: typesafe_joi_1.default.string().required(),
    userpin: typesafe_joi_1.default.string().required(),
    signature: typesafe_joi_1.default.string().required(),
    referencecode: typesafe_joi_1.default.string().required(),
});
//# sourceMappingURL=plnPostpaidTransaction.schema.js.map