"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PLNPostpaidTransaction = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
let PLNPostpaidTransaction = class PLNPostpaidTransaction extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "uid", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNPostpaidTransaction.prototype, "userId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "paymentId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "traceCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "inquiryCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNPostpaidTransaction.prototype, "inquiryExpired", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNPostpaidTransaction.prototype, "transactionStatus", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "referenceCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "transactionStatusText", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNPostpaidTransaction.prototype, "transactionTime", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "accName", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "accNumber1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "accNumber2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "accNumber3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNPostpaidTransaction.prototype, "nominal", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNPostpaidTransaction.prototype, "adminFee", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "ref1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "ref2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "ref3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "contactNumber", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "info", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "billingPeriode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "additionalData", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNPostpaidTransaction.prototype, "point", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "paymentMethod", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "merchant", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "merchantNumber", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "voucherCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPostpaidTransaction.prototype, "statusPembayaran", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNPostpaidTransaction.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNPostpaidTransaction.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNPostpaidTransaction.prototype, "deletedAt", void 0);
PLNPostpaidTransaction = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'TransaksiListrikPasca',
        timestamps: true,
        paranoid: true,
    })
], PLNPostpaidTransaction);
exports.PLNPostpaidTransaction = PLNPostpaidTransaction;
//# sourceMappingURL=plnPostpaidTransaction.model.js.map