"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PLNTransactionRepository = void 0;
const plnTransaction_model_1 = require("./plnTransaction.model");
class PLNTransactionRepository {
    static get() {
        return __awaiter(this, void 0, void 0, function* () {
            const offset = this.offset;
            const limit = this.limit;
            const where = this.where;
            const order = this.order;
            const data = yield plnTransaction_model_1.PLNTransaction.findAll({ where, order, limit, offset });
            return data;
        });
    }
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const creating = yield plnTransaction_model_1.PLNTransaction.create(Object.assign({}, data));
            return creating;
        });
    }
    static updateStatus(nomorTransaksi, status) {
        return __awaiter(this, void 0, void 0, function* () {
            yield plnTransaction_model_1.PLNTransaction.update({ statusTrx: status }, { where: { nomorTransaksi } });
            const transaction = yield plnTransaction_model_1.PLNTransaction.findOne({
                where: { nomorTransaksi },
            });
            return transaction;
        });
    }
    static update(data, nomorTransaksi) {
        return __awaiter(this, void 0, void 0, function* () {
            yield plnTransaction_model_1.PLNTransaction.update(data, {
                where: { nomorTransaksi },
            });
            const transaksi = plnTransaction_model_1.PLNTransaction.findOne({
                where: { nomorTransaksi },
            });
            return transaksi;
        });
    }
    static getDetailByTransaction(nomorTransaksi) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield plnTransaction_model_1.PLNTransaction.findOne({
                where: { nomorTransaksi },
            });
            return data;
        });
    }
}
exports.PLNTransactionRepository = PLNTransactionRepository;
PLNTransactionRepository.limit = 10;
PLNTransactionRepository.offset = 0;
PLNTransactionRepository.include = [];
//# sourceMappingURL=plnTransaction.repository.js.map