"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PLNTransaction = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
let PLNTransaction = class PLNTransaction extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "uid", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNTransaction.prototype, "userId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "nomorTransaksi", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "paymentId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "idPel1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "idPel2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "idPel3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "namaPelanggan", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "periode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "kodeProduk", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNTransaction.prototype, "nominal", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNTransaction.prototype, "harga", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNTransaction.prototype, "hargaJual", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNTransaction.prototype, "margin", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNTransaction.prototype, "admin", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNTransaction.prototype, "komisi", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNTransaction.prototype, "point", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "metodePembayaran", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "merchant", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "nomorAkun", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "adminPayment", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "kodeVoucher", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNTransaction.prototype, "batasPembayaran", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNTransaction.prototype, "saldoTerpotong", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNTransaction.prototype, "sisaSaldo", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "ref1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "ref2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "ref3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "statusPembayaran", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "statusTrx", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "urlStruk", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNTransaction.prototype, "detail", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNTransaction.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNTransaction.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNTransaction.prototype, "deletedAt", void 0);
PLNTransaction = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'TransaksiPLN',
        timestamps: true,
        paranoid: true,
    })
], PLNTransaction);
exports.PLNTransaction = PLNTransaction;
//# sourceMappingURL=plnTransaction.model.js.map