"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const moment_1 = __importDefault(require("moment"));
const uuid_1 = require("uuid");
const cekPayMethod_1 = require("../../../../../helpers/cekPayMethod");
const paymentHelper_1 = require("../../../../../helpers/paymentHelper");
const credential_1 = require("../../../../core/config/credential");
const responseHandler_1 = require("../../../../core/helpers/responseHandler");
const commision_repository_1 = require("../../../commision/commision.repository");
const cekPay_repository_1 = __importDefault(require("../../cekPay.repository"));
const cekPay_schema_1 = require("../../cekPay.schema");
const cekPayDetail_repository_1 = require("../../cekPayDetail.repository");
class PLNTransactionController {
}
exports.default = PLNTransactionController;
PLNTransactionController.inquiry = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (!req.query.produk) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error, produk required');
        }
        if (!req.query.idPel) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error, idpel1 required');
        }
        if (req.query.idPel.length < 11) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor pelanggan minimal 11 digit');
        }
        if (req.query.produk.length === 0) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error, produk required');
        }
        let produk = '';
        if (req.query.produk === 'TOKENLISTRIK') {
            produk = 'PLNPRAH';
        }
        else if (req.query.produk === 'LISTRIKPASCABAYAR') {
            produk = 'PLNPASCH';
        }
        else {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error, produk tidak tersedia');
        }
        const uid = uuid_1.v4().toUpperCase();
        const nomorTransaksi = `${req.query.produk}-${uid}`;
        const userRequest = Object.assign(Object.assign({}, credential_1.rajaBillerCredential), { method: cekPayMethod_1.cekPayMethod.pln.inquiryTransaction, idpel1: req.query.idPel, idpel2: '', idpel3: '', kode_produk: produk, ref1: nomorTransaksi });
        const rajaBillerResponse = yield axios_1.default.post(process.env.RAJABILLER_ENDPOINT, userRequest)
            .then((response) => response.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, `Network error ${err}`));
        if (rajaBillerResponse.STATUS === '14') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor tidak terdaftar dan tidak ditemukan');
        }
        if (rajaBillerResponse.STATUS === '88') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah dibayarkan');
        }
        const data = {
            waktu: moment_1.default(rajaBillerResponse.WAKTU, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss'),
            kodeProduk: rajaBillerResponse.KODE_PRODUK,
            idPel1: rajaBillerResponse.IDPEL1,
            namaPelanggan: rajaBillerResponse.NAMA_PELANGGAN,
            periode: rajaBillerResponse.PERIODE,
            harga: Number(rajaBillerResponse.NOMINAL),
            admin: Number(rajaBillerResponse.ADMIN),
            totalHarga: Number(rajaBillerResponse.NOMINAL) + Number(rajaBillerResponse.ADMIN),
            ref1: rajaBillerResponse.REF1,
            ref2: rajaBillerResponse.REF2,
            status: rajaBillerResponse.STATUS,
            ket: rajaBillerResponse.KET,
            saldoTerpotong: rajaBillerResponse.SALDO_TERPOTONG,
            sisaSaldo: rajaBillerResponse.SISA_SALDO,
            urlStruk: rajaBillerResponse.URL_STRUK,
        };
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res, error);
    }
});
PLNTransactionController.checkout = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (!req.body.produk) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error, produk required');
        }
        if (!req.body.idPel) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error, idpel1 required');
        }
        if (req.body.idPel.length < 11) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor pelanggan minimal 11 digit');
        }
        if (req.body.produk.length === 0) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error, produk required');
        }
        const uid = uuid_1.v4().toUpperCase();
        const invoiceNumber = yield cekPay_repository_1.default.invoiceNumber();
        let produk = '';
        if (req.body.produk === 'TOKENLISTRIK') {
            produk = 'PLNPRAH';
        }
        else if (req.body.produk === 'LISTRIKPASCABAYAR') {
            produk = 'PLNPASCH';
        }
        else {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error, produk tidak tersedia');
        }
        const userRequest = Object.assign(Object.assign({}, credential_1.rajaBillerCredential), { method: cekPayMethod_1.cekPayMethod.pln.inquiryTransaction, idpel1: req.body.idPel, idpel2: '', idpel3: '', kode_produk: produk, ref1: invoiceNumber });
        const rajaBillerResponse = yield axios_1.default.post(process.env.RAJABILLER_ENDPOINT, userRequest)
            .then((response) => response.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, `Network error ${err}`));
        if (rajaBillerResponse.STATUS === '14') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor tidak terdaftar dan tidak ditemukan');
        }
        if (rajaBillerResponse.STATUS === '88') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah dibayarkan');
        }
        const billAmount = Number(req.body.hargaJual) + Number(req.body.margin) + Number(req.body.admin) - Number(req.body.point);
        const generatedPayment = (req.body.metodePembayaran === 'EWALLET') ? yield paymentHelper_1.generatePayment(paymentHelper_1.generateCallback(req), invoiceNumber, billAmount, req.body.merchant) : yield paymentHelper_1.generateInvoice(invoiceNumber, billAmount, invoiceNumber, req.body.merchant);
        if (!generatedPayment) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Gagal membuat payment');
        }
        let paymentExpiredAt = moment_1.default(generatedPayment.expiry_date).format('YYYY-MM-DD HH:mm:ss');
        if (req.body.metodePembayaran === 'EWALLET') {
            paymentExpiredAt = req.body.merchant === 'ID_OVO' ? moment_1.default().add(55, 's').format('YYYY-MM-DD HH:mm:ss') : moment_1.default().add(30, 'm').format('YYYY-MM-DD HH:mm:ss');
        }
        const commision = yield commision_repository_1.CommisionRepository.getCommision('RAJABILLER', 'PLN', produk);
        const resultToDatabase = {
            uid,
            userId: req.headers.id,
            date: moment_1.default().format('YYYY-MM-DD'),
            invoiceNumber,
            accNumber1: req.body.idPel,
            category: req.body.kategori,
            product: req.body.produk,
            providerName: 'PLN',
            productCode: produk,
            nominal: req.body.nominal,
            basicPrice: req.body.hargaJual,
            margin: req.body.margin,
            adminPayment: req.body.admin,
            commision: (commision) ? commision.commision : 0,
            billAmount,
            poin: req.body.point,
            paymentId: generatedPayment.id,
            paymentMethod: req.body.metodePembayaran,
            merchant: req.body.merchant,
            merchantNumber: req.body.nomorAkun,
            voucherCode: req.body.kodeVoucher,
            paymentExpiredAt,
        };
        const { value, error } = cekPay_schema_1.PLNTransactionRequestSchema.validate(resultToDatabase);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, `${error}`);
        }
        const addDetailCekPay = {
            invoiceNumber,
            customerName: rajaBillerResponse.NAMA_PELANGGAN,
            periode: rajaBillerResponse.PERIODE,
        };
        const detailCekPay = yield cekPayDetail_repository_1.CekPayDetailRepository.create(addDetailCekPay);
        const dataDatabase = yield cekPay_repository_1.default.create(value);
        const responseDatabase = Object.assign(Object.assign({}, dataDatabase.get()), { payment: Object.assign({}, generatedPayment), detail: Object.assign({}, detailCekPay.get()) });
        return responseHandler_1.ResponseHandler.jsonSuccess(res, responseDatabase);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res, error);
    }
});
PLNTransactionController.purchase = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const invoiceNumber = String(req.query.invoiceNumber);
        const transaction = yield cekPay_repository_1.default.getInvoiceNumber(invoiceNumber);
        if (!transaction) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor invoice tidak ditemukan');
        }
        if ((transaction.paymentStatus === 'PAID' || transaction.paymentStatus === 'SUCCEEDED') && transaction.transactionStatus === 'SUKSES') {
            return responseHandler_1.ResponseHandler.jsonError(res, `Transaksi telah berhasil dilakukan pada ${moment_1.default(transaction.updatedAt).format('YYYY-MM-DD HH:mm:ss')}`);
        }
        if (transaction.paymentStatus === 'EXPIRED') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah melebihi batas pembayaran');
        }
        if (moment_1.default(transaction.paymentExpiredAt).isBefore(moment_1.default()) && transaction.paymentStatus === 'PENDING') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi telah melebihi batas pembayaran');
        }
        let paymentStatus = 'PENDING';
        if (transaction.paymentMethod === 'EWALLET') {
            paymentStatus = yield paymentHelper_1.checkPaymentStatus(transaction.paymentId, 'EWALLET');
            if (paymentStatus === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, `Selesaikan pembayaran sebelum ${moment_1.default(transaction.paymentExpiredAt).format('YYYY-MM-DD HH:mm:ss')}`);
            }
        }
        else {
            paymentStatus = yield paymentHelper_1.checkPaymentStatus(transaction.paymentId, 'BANK');
            console.log({ paymentStatus });
            if (paymentStatus === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, `Selesaikan pembayaran sebelum ${moment_1.default(transaction.paymentExpiredAt).format('YYYY-MM-DD HH:mm:ss')}`);
            }
        }
        const transactionNumber = yield cekPay_repository_1.default.invoiceNumber('transaction');
        const data = Object.assign(Object.assign({}, credential_1.rajaBillerCredential), { method: cekPayMethod_1.cekPayMethod.pln.payTransaction, idpel1: transaction.accNumber1, idpel2: '', idpel3: '', kode_produk: transaction.productCode, ref1: (transaction.ref1 == null) ? '' : transaction.ref1, ref2: transactionNumber, ref3: (transaction.ref3 == null) ? '' : transaction.ref3, nominal: transaction.nominal });
        const response = yield axios_1.default.post(process.env.RAJABILLER_ENDPOINT, data)
            .then((item) => item.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, `Network error ${err}`));
        if (response.NAMA_PELANGGAN === 'undefined' || response.NAMA_PELANGGAN === '') {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Pelanggan tidak ditemukan');
        }
        const updateToDatabase = {
            transactionNumber,
            deductedBalance: response.SALDO_TERPOTONG,
            endingBalance: response.SISA_SALDO,
            paymentStatus,
            transactionStatus: (response.KET === 'TRANSAKSI SUKSES') ? 'SUKSES' : 'GAGAL',
        };
        const updateDetail = {
            periode: response.PERIODE,
            tarif: response.DETAIL.SUBSCRIBERSEGMENTATION,
            urlStruk: response.URL_STRUK,
            token: (response.DETAIL.TOKEN) ? response.DETAIL.TOKEN : null,
            daya: Number(response.DETAIL.POWERCONSUMINGCATEGORY),
            kwh: (response.DETAIL.PURCHASEDKWHUNIT) ? Number(response.DETAIL.PURCHASEDKWHUNIT) : null,
        };
        yield cekPayDetail_repository_1.CekPayDetailRepository.update(updateDetail, invoiceNumber);
        yield cekPay_repository_1.default.update(updateToDatabase, invoiceNumber);
        const dataReponse = {
            waktu: moment_1.default(response.WAKTU, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss'),
            kodeProduk: response.KODE_PRODUK,
            idPel1: response.IDPEL1,
            namaPelanggan: response.NAMA_PELANGGAN,
            periode: response.PERIODE,
            harga: Number(response.NOMINAL),
            admin: Number(response.ADMIN),
            totalHarga: Number(response.NOMINAL) + Number(response.ADMIN),
            ref1: response.REF1,
            ref2: response.REF2,
            status: response.STATUS,
            ket: response.KET,
            saldoTerpotong: response.SALDO_TERPOTONG,
            sisaSaldo: response.SISA_SALDO,
            urlStruk: response.URL_STRUK,
        };
        return responseHandler_1.ResponseHandler.jsonSuccess(res, dataReponse);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=plnTransaction.controller.js.map