"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PLNTransactionRequestSchema = void 0;
const typesafe_joi_1 = __importDefault(require("typesafe-joi"));
exports.PLNTransactionRequestSchema = typesafe_joi_1.default.object({
    uid: typesafe_joi_1.default.string().required(),
    userId: typesafe_joi_1.default.string().required(),
    nomorTransaksi: typesafe_joi_1.default.string().required(),
    paymentId: typesafe_joi_1.default.string().required(),
    idPel1: typesafe_joi_1.default.string().required(),
    namaPelanggan: typesafe_joi_1.default.string().required(),
    periode: typesafe_joi_1.default.string().allow(null, ''),
    kodeProduk: typesafe_joi_1.default.string().required(),
    nominal: typesafe_joi_1.default.number().required(),
    harga: typesafe_joi_1.default.number().required(),
    hargaJual: typesafe_joi_1.default.number().required(),
    margin: typesafe_joi_1.default.number().required(),
    admin: typesafe_joi_1.default.number().required(),
    komisi: typesafe_joi_1.default.number().required(),
    point: typesafe_joi_1.default.number().required(),
    metodePembayaran: typesafe_joi_1.default.string().required(),
    merchant: typesafe_joi_1.default.string().required(),
    nomorAkun: typesafe_joi_1.default.string().required(),
    adminPayment: typesafe_joi_1.default.number().required(),
    kodeVoucher: typesafe_joi_1.default.string().required(),
    batasPembayaran: typesafe_joi_1.default.date().optional().allow(null),
    saldoTerpotong: typesafe_joi_1.default.number().optional().allow(0),
    sisaSaldo: typesafe_joi_1.default.string().optional().allow(0),
    ref1: typesafe_joi_1.default.string().optional().allow(null),
    ref2: typesafe_joi_1.default.string().optional().allow(null),
    ref3: typesafe_joi_1.default.string().optional().allow(null),
    statusTrx: typesafe_joi_1.default.string().optional(),
});
//# sourceMappingURL=plnTransaction.schema.js.map