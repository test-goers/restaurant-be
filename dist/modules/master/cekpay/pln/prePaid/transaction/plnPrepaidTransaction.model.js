"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PLNPrepaidTransaction = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
let PLNPrepaidTransaction = class PLNPrepaidTransaction extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "uid", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNPrepaidTransaction.prototype, "userId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "paymentId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "traceCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "transactionStatus", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "transactionTime", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "productCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "accNumber1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "accNumber2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "accNumber3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "inquiryCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNPrepaidTransaction.prototype, "inquiryExpired", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnPelangganId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnNomorMeter", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnPelangganNama", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnTarifDaya", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnTarif", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnKategoriDaya", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnNoRefPLN", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnNoRefVendor", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnKodeDistribusi", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnUPJ", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnUpjPhone", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnKwhLimit", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnTokenUnSolid1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "plnTokenUnSolid2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "ref1", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "ref2", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "ref3", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "serialNumber", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "referenceCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNPrepaidTransaction.prototype, "nominal", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNPrepaidTransaction.prototype, "price", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNPrepaidTransaction.prototype, "margin", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], PLNPrepaidTransaction.prototype, "point", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "paymentMethod", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "merchant", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "merchantNumber", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "voucherCode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], PLNPrepaidTransaction.prototype, "statusPembayaran", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNPrepaidTransaction.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNPrepaidTransaction.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], PLNPrepaidTransaction.prototype, "deletedAt", void 0);
PLNPrepaidTransaction = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'TransaksiTokenListrik',
        timestamps: true,
        paranoid: true,
    })
], PLNPrepaidTransaction);
exports.PLNPrepaidTransaction = PLNPrepaidTransaction;
//# sourceMappingURL=plnPrepaidTransaction.model.js.map