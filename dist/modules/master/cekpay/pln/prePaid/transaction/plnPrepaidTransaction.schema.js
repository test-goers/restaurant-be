"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatusTokenListrikRequestSchema = exports.PurchaseTokenListrikRequestSchema = exports.InquiryTokenListrikRequestSchema = void 0;
const typesafe_joi_1 = __importDefault(require("typesafe-joi"));
exports.InquiryTokenListrikRequestSchema = typesafe_joi_1.default.object({
    transactioncode: typesafe_joi_1.default.string().required(),
    querycode: typesafe_joi_1.default.string().required(),
    productcode: typesafe_joi_1.default.string().required(),
    accnumber: typesafe_joi_1.default.string().required(),
    userid: typesafe_joi_1.default.string().required(),
    userpin: typesafe_joi_1.default.string().required(),
    referencecode: typesafe_joi_1.default.string().required(),
    signature: typesafe_joi_1.default.string().required(),
});
exports.PurchaseTokenListrikRequestSchema = typesafe_joi_1.default.object({
    transactioncode: typesafe_joi_1.default.string().required(),
    productcode: typesafe_joi_1.default.string().required(),
    accnumber: typesafe_joi_1.default.string().required(),
    userid: typesafe_joi_1.default.string().required(),
    userpin: typesafe_joi_1.default.string().required(),
    referencecode: typesafe_joi_1.default.string().required(),
    signature: typesafe_joi_1.default.string().required(),
    ref1: typesafe_joi_1.default.string().required(),
    ref2: typesafe_joi_1.default.string().required(),
    ref3: typesafe_joi_1.default.string().required(),
});
exports.StatusTokenListrikRequestSchema = typesafe_joi_1.default.object({
    transactioncode: typesafe_joi_1.default.string().required(),
    tracecode: typesafe_joi_1.default.string().required(),
    userid: typesafe_joi_1.default.string().required(),
    userpin: typesafe_joi_1.default.string().required(),
    signature: typesafe_joi_1.default.string().required(),
    referencecode: typesafe_joi_1.default.string().required(),
});
//# sourceMappingURL=plnPrepaidTransaction.schema.js.map