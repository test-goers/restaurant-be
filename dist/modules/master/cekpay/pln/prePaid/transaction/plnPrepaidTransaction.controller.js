"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const md5_1 = __importDefault(require("md5"));
const moment_1 = __importDefault(require("moment"));
const uuid_1 = require("uuid");
const paymentHelper_1 = require("../../../../../../helpers/paymentHelper");
const responseHandler_1 = require("../../../../../core/helpers/responseHandler");
const plnPrepaidTransaction_repository_1 = require("./plnPrepaidTransaction.repository");
const plnPrepaidTransaction_schema_1 = require("./plnPrepaidTransaction.schema");
class PLNPrepaidTransactionController {
}
exports.default = PLNPrepaidTransactionController;
PLNPrepaidTransactionController.inquiry = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const uid = uuid_1.v4().toUpperCase();
        const referencecode = 'PLNPREPAID-' + uid;
        const signature = md5_1.default(process.env.PASTI_USERID + process.env.PASTI_PIN + referencecode);
        const cekTokoRequest = {
            transactioncode: 'pasti-prabayar',
            querycode: 'inquiry',
            productcode: req.params.productCode,
            accnumber: req.params.accNumber,
            userid: process.env.PASTI_USERID,
            userpin: process.env.PASTI_PIN,
            referencecode,
            signature,
        };
        const { value, error } = plnPrepaidTransaction_schema_1.InquiryTokenListrikRequestSchema.validate(cekTokoRequest);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error ' + error);
        }
        const pastiRequest = yield axios_1.default.post(process.env.PASTI_ENDPOINT, value)
            .then((response) => response.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, 'Network error ' + err));
        const pastiResponse = pastiRequest;
        const data = {
            uid,
            traceCode: pastiResponse.tracecode,
            transactionStatus: pastiResponse.transactionstatus,
            transactionTime: pastiResponse.transactiontime,
            productCode: pastiResponse.productcode ? pastiResponse.productcode : null,
            accNumber1: pastiResponse.accnumber1 ? pastiResponse.accnumber1 : null,
            accNumber2: pastiResponse.accnumber2 ? pastiResponse.accnumber2 : null,
            accNumber3: pastiResponse.accnumber3 ? pastiResponse.accnumber3 : null,
            inquiryCode: pastiResponse.inquirycode ? pastiResponse.inquirycode : null,
            inquiryExpired: moment_1.default(pastiResponse.inquiryexpired).format('YYYY-MM-DD HH:mm:ss'),
            plnPelangganId: pastiResponse.PLN_IDPELANGGAN ? pastiResponse.PLN_IDPELANGGAN : null,
            plnNomorMeter: pastiResponse.PLN_NOMETER ? pastiResponse.PLN_NOMETER : null,
            plnPelangganNama: pastiResponse.PLN_NAMAPELANGGAN ? pastiResponse.PLN_NAMAPELANGGAN : null,
            plnTarifDaya: pastiResponse.PLN_TARIFDAYA ? pastiResponse.PLN_TARIFDAYA : null,
            plnTarif: pastiResponse.PLN_TARIF ? pastiResponse.PLN_TARIF : null,
            plnKategoriDaya: pastiResponse.PLN_KATEGORIDAYA ? pastiResponse.PLN_KATEGORIDAYA : null,
            plnNoRefPLN: pastiResponse.PLN_NOREFPLN ? pastiResponse.PLN_NOREFPLN : null,
            plnNoRefVendor: pastiResponse.PLN_NOREFVENDOR ? pastiResponse.PLN_NOREFVENDOR : null,
            plnKodeDistribusi: pastiResponse.PLN_KODEDISTRIBUSI ? pastiResponse.PLN_KODEDISTRIBUSI : null,
            plnUPJ: pastiResponse.PLN_UPJ ? pastiResponse.PLN_UPJ : null,
            plnUpjPhone: pastiResponse.PLN_UPJPHONE ? pastiResponse.PLN_UPJPHONE : null,
            plnKwhLimit: pastiResponse.PLN_KWHLIMIT ? pastiResponse.PLN_KWHLIMIT : null,
            plnTokenUnSolid1: pastiResponse.PLN_TOKENUNSOLD1 ? pastiResponse.PLN_TOKENUNSOLD1 : null,
            plnTokenUnSolid2: pastiResponse.PLN_TOKENUNSOLD2 ? pastiResponse.PLN_TOKENUNSOLD2 : null,
            ref1: pastiResponse.ref1 ? pastiResponse.ref1 : null,
            ref2: pastiResponse.ref2 ? pastiResponse.ref2 : null,
            ref3: pastiResponse.ref3 ? pastiResponse.ref3 : null,
            referenceCode: referencecode,
        };
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res, error);
    }
});
PLNPrepaidTransactionController.checkout = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const uid = uuid_1.v4().toUpperCase();
        const referencecode = 'PLNPREPAID-' + uid;
        const signature = md5_1.default(process.env.PASTI_USERID + process.env.PASTI_PIN + referencecode);
        const cekTokoRequest = {
            transactioncode: 'pasti-prabayar',
            querycode: 'inquiry',
            productcode: req.body.productCode,
            accnumber: req.body.accNumber,
            userid: process.env.PASTI_USERID,
            userpin: process.env.PASTI_PIN,
            referencecode,
            signature,
        };
        const { value, error } = plnPrepaidTransaction_schema_1.InquiryTokenListrikRequestSchema.validate(cekTokoRequest);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error ' + error);
        }
        const pastiRequest = yield axios_1.default.post(process.env.PASTI_ENDPOINT, value)
            .then((response) => response.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, 'Network error ' + err));
        const pastiResponse = pastiRequest;
        const harga = req.body.price + req.body.margin - req.body.point;
        const generatedPayment = (req.body.paymentMethod === 'EWALLET') ? yield paymentHelper_1.generatePayment(paymentHelper_1.generateCallback(req), referencecode, harga, req.body.merchant) : null; // payment bank
        if (!generatedPayment) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Gagal membuat payment');
        }
        const data = {
            uid,
            userId: req.headers.id,
            paymentId: generatedPayment.id,
            traceCode: pastiResponse.tracecode,
            transactionStatus: pastiResponse.transactionstatus,
            transactionTime: pastiResponse.transactiontime,
            productCode: pastiResponse.productcode ? pastiResponse.productcode : null,
            accNumber1: pastiResponse.accnumber1 ? pastiResponse.accnumber1 : null,
            accNumber2: pastiResponse.accnumber2 ? pastiResponse.accnumber2 : null,
            accNumber3: pastiResponse.accnumber3 ? pastiResponse.accnumber3 : null,
            inquiryCode: pastiResponse.inquirycode ? pastiResponse.inquirycode : null,
            inquiryExpired: moment_1.default(pastiResponse.inquiryexpired).format('YYYY-MM-DD HH:mm:ss'),
            plnPelangganId: pastiResponse.PLN_IDPELANGGAN ? pastiResponse.PLN_IDPELANGGAN : null,
            plnNomorMeter: pastiResponse.PLN_NOMETER ? pastiResponse.PLN_NOMETER : null,
            plnPelangganNama: pastiResponse.PLN_NAMAPELANGGAN ? pastiResponse.PLN_NAMAPELANGGAN : null,
            plnTarifDaya: pastiResponse.PLN_TARIFDAYA ? pastiResponse.PLN_TARIFDAYA : null,
            plnTarif: pastiResponse.PLN_TARIF ? pastiResponse.PLN_TARIF : null,
            plnKategoriDaya: pastiResponse.PLN_KATEGORIDAYA ? pastiResponse.PLN_KATEGORIDAYA : null,
            plnNoRefPLN: pastiResponse.PLN_NOREFPLN ? pastiResponse.PLN_NOREFPLN : null,
            plnNoRefVendor: pastiResponse.PLN_NOREFVENDOR ? pastiResponse.PLN_NOREFVENDOR : null,
            plnKodeDistribusi: pastiResponse.PLN_KODEDISTRIBUSI ? pastiResponse.PLN_KODEDISTRIBUSI : null,
            plnUPJ: pastiResponse.PLN_UPJ ? pastiResponse.PLN_UPJ : null,
            plnUpjPhone: pastiResponse.PLN_UPJPHONE ? pastiResponse.PLN_UPJPHONE : null,
            plnKwhLimit: pastiResponse.PLN_KWHLIMIT ? pastiResponse.PLN_KWHLIMIT : null,
            plnTokenUnSolid1: pastiResponse.PLN_TOKENUNSOLD1 ? pastiResponse.PLN_TOKENUNSOLD1 : null,
            plnTokenUnSolid2: pastiResponse.PLN_TOKENUNSOLD2 ? pastiResponse.PLN_TOKENUNSOLD2 : null,
            ref1: pastiResponse.ref1 ? pastiResponse.ref1 : null,
            ref2: pastiResponse.ref2 ? pastiResponse.ref2 : null,
            ref3: pastiResponse.ref3 ? pastiResponse.ref3 : null,
            referenceCode: referencecode,
            nominal: req.body.nominal,
            price: req.body.price,
            margin: req.body.margin,
            point: req.body.point,
            paymentMethod: req.body.paymentMethod,
            merchant: req.body.merchant,
            merchantNumber: req.body.merchantNumber,
            voucherCode: req.body.voucherCode,
            statusPembayaran: generatedPayment.status,
        };
        const dataDatabase = yield plnPrepaidTransaction_repository_1.PLNPrepaidTransactionRepository.create(data);
        const responseDatabase = Object.assign(Object.assign({}, dataDatabase.get()), { payment: Object.assign({}, generatedPayment) });
        return responseHandler_1.ResponseHandler.jsonSuccess(res, responseDatabase);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res, error);
    }
});
PLNPrepaidTransactionController.purchase = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const transaction = yield plnPrepaidTransaction_repository_1.PLNPrepaidTransactionRepository.getDetailByTransaction(req.params.referenceCode);
        if (!transaction) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor transaksi tidak ditemukan');
        }
        let statusPembayaran = 'PENDING';
        if (transaction.paymentMethod === 'EWALLET') {
            statusPembayaran = yield paymentHelper_1.checkPaymentStatus(transaction.paymentId, 'EWALLET');
            if (statusPembayaran === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, 'Silahkan selesaikan pembayaran terlebih dahulu');
            }
        }
        else {
            statusPembayaran = yield paymentHelper_1.checkPaymentStatus(transaction.paymentId, 'BANK');
            if (statusPembayaran === 'PENDING') {
                return responseHandler_1.ResponseHandler.jsonError(res, 'Silahkan selesaikan pembayaran terlebih dahulu');
            }
        }
        const signature = md5_1.default(process.env.PASTI_USERID + process.env.PASTI_PIN + req.params.referenceCode);
        const cekTokoPurchaseRequest = {
            transactioncode: 'pasti-prabayar',
            productcode: transaction.productCode,
            accnumber: transaction.accNumber1 ? transaction.accNumber1 : '',
            userid: process.env.PASTI_USERID,
            userpin: process.env.PASTI_PIN,
            referencecode: transaction.referenceCode,
            signature,
            ref1: transaction.ref1 ? transaction.ref1 : '',
            ref2: transaction.ref2 ? transaction.ref2 : '',
            ref3: transaction.ref3 ? transaction.ref3 : '',
        };
        const { value, error } = plnPrepaidTransaction_schema_1.PurchaseTokenListrikRequestSchema.validate(cekTokoPurchaseRequest);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error ' + error);
        }
        const pastiPurchaseRequest = yield axios_1.default.post(process.env.PASTI_ENDPOINT, value)
            .then((response) => response.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, 'Network error ' + err));
        const pastiPurchaseResponse = pastiPurchaseRequest;
        const data = pastiPurchaseResponse;
        const pastiTransactionStatus = {
            transactionStatus: data.transactionstatus,
            statusPembayaran,
        };
        yield plnPrepaidTransaction_repository_1.PLNPrepaidTransactionRepository.update(pastiTransactionStatus, req.params.referenceCode);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.serverError(res, error);
    }
});
PLNPrepaidTransactionController.status = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const transaction = yield plnPrepaidTransaction_repository_1.PLNPrepaidTransactionRepository.getDetailByTransaction(req.params.referencecode);
        const cekTokoStatusRequest = {
            transactioncode: 'pasti-cek-status',
            tracecode: transaction.traceCode,
            userid: process.env.PASTI_USERID,
            userpin: process.env.PASTI_PIN,
            signature: md5_1.default(process.env.PASTI_USERID + process.env.PASTI_PIN + transaction.referenceCode),
            referencecode: transaction.referenceCode,
        };
        const { value, error } = plnPrepaidTransaction_schema_1.StatusTokenListrikRequestSchema.validate(cekTokoStatusRequest);
        if (error) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error ' + error);
        }
        const pastiStatusRequest = yield axios_1.default.post(process.env.PASTI_ENDPOINT, value)
            .then((response) => response.data)
            .catch((err) => responseHandler_1.ResponseHandler.jsonError(res, 'Network error ' + err));
        const pastiStatusResponse = pastiStatusRequest;
        const data = pastiStatusResponse;
        const pastiTransactionStatus = {
            transactionStatus: data.transactionstatus,
        };
        yield plnPrepaidTransaction_repository_1.PLNPrepaidTransactionRepository.update(pastiTransactionStatus, req.body.referenceCode);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
PLNPrepaidTransactionController.history = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const minDate = (req.body.minDate) ? req.body.minDate : null;
        const maxDate = (req.body.maxDate) ? req.body.maxDate : moment_1.default();
        const transaksi = yield plnPrepaidTransaction_repository_1.PLNPrepaidTransactionRepository.getByUserId(req.headers.user_id, minDate, maxDate);
        return (transaksi) ? responseHandler_1.ResponseHandler.jsonSuccess(res, transaksi) : responseHandler_1.ResponseHandler.jsonError(res, 'Data tidak ditemukan');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=plnPrepaidTransaction.controller.js.map