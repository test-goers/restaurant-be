"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CekPayDetailRepository = void 0;
const cekPayDetail_model_1 = __importDefault(require("./cekPayDetail.model"));
class CekPayDetailRepository {
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const create = yield cekPayDetail_model_1.default.create(Object.assign({}, data));
            return create;
        });
    }
    static update(data, invoiceNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            yield cekPayDetail_model_1.default.update(data, {
                where: { invoiceNumber },
            });
            const transaksi = cekPayDetail_model_1.default.findOne({
                where: { invoiceNumber },
            });
            return transaksi;
        });
    }
}
exports.CekPayDetailRepository = CekPayDetailRepository;
//# sourceMappingURL=cekPayDetail.repository.js.map