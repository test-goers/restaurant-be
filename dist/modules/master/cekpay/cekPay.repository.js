"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const sequelize_1 = require("sequelize");
const sequelize_typescript_1 = require("sequelize-typescript");
const cekPay_model_1 = __importDefault(require("./cekPay.model"));
class CekPayRepository {
    static get() {
        return __awaiter(this, void 0, void 0, function* () {
            const offset = this.offset;
            const limit = this.limit;
            const where = this.where;
            const order = this.order;
            const data = yield cekPay_model_1.default.findAll({ where, order, limit, offset });
            return data;
        });
    }
    static invoiceNumber(trx = 'payment') {
        return __awaiter(this, void 0, void 0, function* () {
            const selectedField = trx === 'transaction' ? 'transactionNumber' : 'invoiceNumber';
            const lastNumber = yield cekPay_model_1.default.findOne({
                attributes: [
                    [sequelize_typescript_1.Sequelize.fn('MAX', sequelize_typescript_1.Sequelize.col(`${selectedField}`)), 'maks'],
                ],
            });
            const year = moment_1.default().format('YYYY');
            const month = moment_1.default().format('MM');
            const label = trx === 'transaction' ? 'TRANS' : 'INV';
            let invoiceNumber = `${label}-CP/${month}/${year}/0000001`;
            if (lastNumber.get('maks')) {
                const maks = String(lastNumber.get('maks'));
                const splitTransactionNumber = maks.split('/');
                const next = Number(splitTransactionNumber[3]) + 1;
                const len = 7;
                const diff = len - String(next).length;
                const step = '0';
                let postfix = '';
                for (let i = 1; i <= diff; i++) {
                    postfix += step;
                }
                postfix += next;
                invoiceNumber = `${label}-CP/${month}/${year}/${postfix}`;
            }
            return invoiceNumber;
        });
    }
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const create = yield cekPay_model_1.default.create(Object.assign({}, data));
            return create;
        });
    }
    static getInvoiceNumber(invoiceNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            const invoice = yield cekPay_model_1.default.findOne({
                where: { invoiceNumber },
            });
            return invoice;
        });
    }
    static update(data, invoiceNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            yield cekPay_model_1.default.update(data, {
                where: { invoiceNumber },
            });
            const transaksi = cekPay_model_1.default.findOne({
                where: { invoiceNumber },
            });
            return transaksi;
        });
    }
    static history(userId = '', transactionStatus = '', category = '', start = '', end = '', product = '', keyword = '', limit = '', offset = '') {
        return __awaiter(this, void 0, void 0, function* () {
            const date = (start !== '') ? { [sequelize_1.Op.between]: [start, end] } : { [sequelize_1.Op.lte]: moment_1.default(end).toDate() };
            const data = yield cekPay_model_1.default.findAll({
                attributes: [
                    ['invoiceNumber', 'no_order'],
                    ['category', 'kategori'],
                    ['billAmount', 'total'],
                    [sequelize_typescript_1.Sequelize.literal(`(CASE transactionStatus WHEN 'PENDING' THEN 'Menunggu Pembayaran' WHEN 'SUKSES' THEN 'Pesanan Selesai' WHEN 'GAGAL' THEN 'Pesanan Gagal' ELSE '' END)`), 'status_transaksi'],
                    [sequelize_typescript_1.Sequelize.fn('DATE_FORMAT', sequelize_typescript_1.Sequelize.col('createdAt'), '%Y-%m-%d %H:%i:%s'), 'tgl_insert']
                ],
                where: {
                    userId,
                    date,
                    [sequelize_1.Op.or]: [
                        { transactionStatus: (transactionStatus === '' ? { [sequelize_1.Op.not]: null } : transactionStatus) },
                        { category: (category === '' ? { [sequelize_1.Op.not]: null } : category) },
                        { product: (product === '' ? { [sequelize_1.Op.not]: null } : { [sequelize_1.Op.like]: `%${product}%` }) },
                    ],
                    [sequelize_1.Op.or]: [
                        { product: (keyword === '' ? { [sequelize_1.Op.not]: null } : { [sequelize_1.Op.like]: `%${keyword}%` }) },
                        { invoiceNumber: (keyword === '' ? { [sequelize_1.Op.not]: null } : { [sequelize_1.Op.like]: `%${keyword}%` }) },
                        { billAmount: (keyword === '' ? { [sequelize_1.Op.not]: null } : { [sequelize_1.Op.like]: `%${keyword}%` }) },
                        { category: (keyword === '' ? { [sequelize_1.Op.not]: null } : { [sequelize_1.Op.like]: `%${keyword}%` }) },
                    ],
                },
                order: this.order,
                limit: limit === '' ? null : Number(limit),
                offset: offset === '' ? null : Number(offset),
            });
            return data;
        });
    }
    static detail(userId, invoiceNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield cekPay_model_1.default.findOne({
                where: { userId, invoiceNumber },
            });
            return data;
        });
    }
}
CekPayRepository.limit = 10;
CekPayRepository.offset = 0;
CekPayRepository.include = [];
exports.default = CekPayRepository;
//# sourceMappingURL=cekPay.repository.js.map