"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const paymentHelper_1 = require("../../../helpers/paymentHelper");
const responseHandler_1 = require("../../core/helpers/responseHandler");
const cekPay_repository_1 = __importDefault(require("./cekPay.repository"));
class CekPayController {
}
exports.default = CekPayController;
CekPayController.closeInvoice = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const invoiceNumber = String(req.body.invoiceNumber);
        const data = yield cekPay_repository_1.default.getInvoiceNumber(invoiceNumber);
        if (!data) {
            return responseHandler_1.ResponseHandler.jsonError(res, 'Nomor invoice tidak ditemukan');
        }
        const closePayment = yield paymentHelper_1.closeInvoice(data.paymentId);
        const result = {
            invoiceNumber: data.invoiceNumber,
            paymentStatus: closePayment.status,
            paymentExpiredAt: closePayment.expiry_date,
        };
        yield cekPay_repository_1.default.update(result, invoiceNumber);
        return responseHandler_1.ResponseHandler.jsonSuccess(res, result);
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
CekPayController.transactionStatus = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const invoiceNumber = String(req.query.invoiceNumber);
        const data = yield cekPay_repository_1.default.getInvoiceNumber(invoiceNumber);
        const result = {
            invoiceNumber: data.invoiceNumber,
            transactionNumber: data.transactionNumber,
            paymentStatus: data.paymentStatus,
            transactionStatus: data.transactionStatus,
            paymentExpiredAt: data.paymentExpiredAt,
            transactionExpiredAt: data.transactionExpiredAt,
        };
        return data ? responseHandler_1.ResponseHandler.jsonSuccess(res, result) : responseHandler_1.ResponseHandler.jsonError(res, 'Nomor invoice tidak ditemukan');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
CekPayController.history = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const status = req.query.status ? req.query.status : '';
        const category = req.query.category ? req.query.category : '';
        const start = req.query.start ? req.query.start : '';
        const end = req.query.end ? req.query.end : moment_1.default().format('YYYY-MM-DD');
        const product = req.query.product ? req.query.product : '';
        const keyword = req.query.keyword ? req.query.keyword : '';
        const limit = req.query.limit ? req.query.limit : '10';
        const offset = req.query.offset ? req.query.offset : '0';
        const history = yield cekPay_repository_1.default.history(String(req.headers.id), String(status), String(category), String(start), String(end), String(product), String(keyword), String(limit), String(offset));
        return history ? responseHandler_1.ResponseHandler.jsonSuccess(res, history) : responseHandler_1.ResponseHandler.jsonError(res, 'Data tidak ditemukan');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
CekPayController.detail = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield cekPay_repository_1.default.detail(req.headers.id, req.query.invoiceNumber);
        return (data) ? responseHandler_1.ResponseHandler.jsonSuccess(res, data) : responseHandler_1.ResponseHandler.jsonError(res, 'Transaksi tidak ditemukan di pengguna yang mengakses');
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=cekPay.controller.js.map