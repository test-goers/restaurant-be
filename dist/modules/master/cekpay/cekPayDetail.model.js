"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_typescript_1 = require("sequelize-typescript");
let CekPayDetail = class CekPayDetail extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPayDetail.prototype, "invoiceNumber", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPayDetail.prototype, "customerName", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPayDetail.prototype, "periode", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPayDetail.prototype, "kwh", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPayDetail.prototype, "tarif", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPayDetail.prototype, "daya", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPayDetail.prototype, "token", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], CekPayDetail.prototype, "urlStruk", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], CekPayDetail.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], CekPayDetail.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], CekPayDetail.prototype, "deletedAt", void 0);
CekPayDetail = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'CekPayDetail',
        timestamps: true,
        paranoid: true,
    })
], CekPayDetail);
exports.default = CekPayDetail;
//# sourceMappingURL=cekPayDetail.model.js.map