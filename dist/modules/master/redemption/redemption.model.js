"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Redemption = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const campaign_model_1 = require("../campaign/campaign.model");
const voucher_model_1 = require("../voucher/voucher.model");
let Redemption = class Redemption extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.ForeignKey(() => voucher_model_1.Voucher),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Redemption.prototype, "voucherId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => voucher_model_1.Voucher),
    __metadata("design:type", Array)
], Redemption.prototype, "voucher", void 0);
__decorate([
    sequelize_typescript_1.ForeignKey(() => campaign_model_1.Campaign),
    __metadata("design:type", Number)
], Redemption.prototype, "campaignId", void 0);
__decorate([
    sequelize_typescript_1.BelongsTo(() => campaign_model_1.Campaign),
    __metadata("design:type", campaign_model_1.Campaign)
], Redemption.prototype, "campaign", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Redemption.prototype, "code", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Redemption.prototype, "date", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Redemption.prototype, "merchantId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Redemption.prototype, "merchantName", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Redemption.prototype, "userId", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Redemption.prototype, "userName", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Redemption.prototype, "status", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Redemption.prototype, "initialPrice", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Redemption.prototype, "finalPrice", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Redemption.prototype, "discountType", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Redemption.prototype, "discountOff", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Redemption.prototype, "service", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Redemption.prototype, "createdAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Redemption.prototype, "updatedAt", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", Date)
], Redemption.prototype, "deletedAt", void 0);
Redemption = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'Redemptions',
        timestamps: true,
        paranoid: true,
    })
], Redemption);
exports.Redemption = Redemption;
//# sourceMappingURL=redemption.model.js.map