"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseHandler_1 = require("../../core/helpers/responseHandler");
const redemption_repository_1 = require("./redemption.repository");
class RedemptionController {
}
exports.default = RedemptionController;
RedemptionController.json = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const search = (req.query.search) ? req.query.search : '';
        const limit = (req.query.limit) ? Number(req.query.limit) : yield redemption_repository_1.RedemptionRepository.countAll();
        const offset = (req.query.offset) ? Number(req.query.offset) : 0;
        const order = (req.query.order) ? req.query.order : 'asc';
        const voucherId = (req.query.voucherId) ? req.query.voucherId : null;
        const data = yield redemption_repository_1.RedemptionRepository.dataTable(String(search), Number(voucherId), String(order), Number(offset), Number(limit));
        res.json({
            rows: data.rows,
            total: data.count,
        });
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
RedemptionController.calculation = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const search = req.query.search || '';
        const limit = req.query.limit || null;
        const offset = req.query.offset || 0;
        const order = req.query.order || 'asc';
        const voucherId = req.query.voucherId || '';
        const data = yield redemption_repository_1.RedemptionRepository.merchantAcumulation(String(search), String(voucherId), String(order), Number(offset), Number(limit));
        res.json({
            rows: data.rows,
            total: data.rows.length,
        });
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
//# sourceMappingURL=redemption.controller.js.map