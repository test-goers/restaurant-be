"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedemptionRepository = void 0;
const sequelize_1 = require("sequelize");
const sequelize_2 = require("sequelize");
const voucher_model_1 = require("../voucher/voucher.model");
const redemption_model_1 = require("./redemption.model");
class RedemptionRepository {
    static getAll(code) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield redemption_model_1.Redemption.findAll({
                where: { code },
            });
            return data;
        });
    }
    static get(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield redemption_model_1.Redemption.findByPk(id);
            return data;
        });
    }
    static getTotal() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield redemption_model_1.Redemption.count();
            return data;
        });
    }
    static dataTable(search = '', voucherId, order = 'asc', offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield redemption_model_1.Redemption.findAndCountAll({
                attributes: {
                    exclude: ['createdAt', 'deletedAt', 'updatedAt'],
                },
                where: {
                    [sequelize_2.Op.and]: {
                        voucherId: voucherId || { [sequelize_2.Op.ne]: null },
                        [sequelize_2.Op.or]: {
                            code: { [sequelize_2.Op.substring]: `%${search}%` },
                            initialPrice: { [sequelize_2.Op.substring]: `%${search}%` },
                            finalPrice: { [sequelize_2.Op.substring]: `%${search}%` },
                            merchantName: { [sequelize_2.Op.substring]: `%${search}%` },
                            userName: { [sequelize_2.Op.substring]: `%${search}%` },
                        },
                    },
                },
                order: [
                    ['createdAt', order],
                ],
                offset,
                limit,
            });
            return data;
        });
    }
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const create = yield redemption_model_1.Redemption.create(data);
            return create;
        });
    }
    static checkRedeem(userId, code) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield redemption_model_1.Redemption.findOne({
                where: {
                    [sequelize_2.Op.and]: {
                        userId,
                        code,
                        status: 'SUKSES',
                    },
                },
            });
            return data;
        });
    }
    static cancel(code, userId, lastQty) {
        return __awaiter(this, void 0, void 0, function* () {
            yield redemption_model_1.Redemption.update({ status: 'DIBATALKAN' }, {
                where: {
                    [sequelize_2.Op.and]: {
                        code,
                        userId,
                    },
                },
            });
            const avaliableQty = lastQty + 1;
            yield voucher_model_1.Voucher.update({ qty: avaliableQty }, {
                where: { code },
            });
            const result = yield redemption_model_1.Redemption.findOne({
                where: {
                    [sequelize_2.Op.and]: {
                        code,
                        userId,
                    },
                },
            });
            return result;
        });
    }
    static merchantAcumulation(search = '', voucherId, order = 'asc', offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const redemption = yield redemption_model_1.Redemption.findAndCountAll({
                attributes: [
                    'merchantName', 'discountOff', 'discountType',
                    [sequelize_1.Sequelize.fn('COUNT', sequelize_1.Sequelize.col('merchantName')), 'totalRedeem'],
                ],
                where: {
                    [sequelize_2.Op.and]: {
                        voucherId: voucherId || { [sequelize_2.Op.ne]: null },
                        status: 'SUKSES',
                        [sequelize_2.Op.or]: {
                            code: { [sequelize_2.Op.substring]: `%${search}%` },
                            initialPrice: { [sequelize_2.Op.substring]: `%${search}%` },
                            finalPrice: { [sequelize_2.Op.substring]: `%${search}%` },
                            merchantName: { [sequelize_2.Op.substring]: `%${search}%` },
                            userName: { [sequelize_2.Op.substring]: `%${search}%` },
                        },
                    },
                },
                group: ['merchantName', 'discountOff', 'discountType'],
                order: [
                    ['merchantName', order],
                ],
                offset,
                limit,
            });
            return redemption;
        });
    }
    static countAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const create = yield redemption_model_1.Redemption.count();
            return create;
        });
    }
    static checkRedeemWithCampaign(userId, campaignId) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield redemption_model_1.Redemption.findOne({
                where: {
                    [sequelize_2.Op.and]: {
                        userId,
                        campaignId,
                        status: 'SUKSES',
                    },
                },
            });
            return data;
        });
    }
    static getTotalRedeem(voucherId) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield redemption_model_1.Redemption.count({
                where: {
                    voucherId,
                    status: 'SUKSES',
                },
            });
            return data;
        });
    }
    static campaignAcumulation(search = '', campaignId, order = 'asc', offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const redemption = yield redemption_model_1.Redemption.findAndCountAll({
                attributes: [
                    'merchantName', 'code', 'discountOff', 'discountType',
                    [sequelize_1.Sequelize.fn('COUNT', sequelize_1.Sequelize.col('merchantName')), 'totalRedeem'],
                ],
                where: {
                    [sequelize_2.Op.and]: {
                        campaignId: campaignId || { [sequelize_2.Op.ne]: null },
                        status: 'SUKSES',
                        [sequelize_2.Op.or]: {
                            code: { [sequelize_2.Op.substring]: `%${search}%` },
                            initialPrice: { [sequelize_2.Op.substring]: `%${search}%` },
                            finalPrice: { [sequelize_2.Op.substring]: `%${search}%` },
                            merchantName: { [sequelize_2.Op.substring]: `%${search}%` },
                            userName: { [sequelize_2.Op.substring]: `%${search}%` },
                        },
                    },
                },
                group: ['merchantName', 'code', 'discountOff', 'discountType'],
                order: [
                    ['merchantName', order],
                ],
                offset,
                limit,
                logging: console.log,
            });
            console.log(redemption);
            return redemption;
        });
    }
    static mapToRedemptionResponse(data) {
        const { redemption, campaignName } = data;
        return {
            id: redemption.id,
            voucherId: redemption.voucherId,
            code: redemption.code,
            date: redemption.date,
            userId: redemption.userId,
            status: redemption.status,
            initialPrice: redemption.initialPrice,
            finalPrice: redemption.finalPrice,
            service: redemption.service,
            discountType: redemption.discountType,
            discountOff: redemption.discountOff,
            merchantId: redemption.merchantId,
            merchantName: redemption.merchantName,
            userName: redemption.userName,
            campaignId: redemption.campaignId,
            campaignName,
            createdAt: redemption.createdAt,
            updatedAt: redemption.updatedAt,
        };
    }
}
exports.RedemptionRepository = RedemptionRepository;
//# sourceMappingURL=redemption.repository.js.map