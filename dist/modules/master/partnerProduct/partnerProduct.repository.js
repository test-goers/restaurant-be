"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PartnerProductRepository = void 0;
const sequelize_1 = require("sequelize");
const partnerProduct_model_1 = require("./partnerProduct.model");
class PartnerProductRepository {
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const partnerProduct = yield partnerProduct_model_1.PartnerProduct.create(data);
            return partnerProduct;
        });
    }
    static get(search = '', limit = 10, offset = 0) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield partnerProduct_model_1.PartnerProduct.findAndCountAll({
                where: search.length > 0 ?
                    {
                        name: {
                            [sequelize_1.Op.like]: `%${String(search)}%`,
                        },
                    } : {},
                offset: Number(offset),
                limit: Number(limit),
            });
            return data;
        });
    }
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const partnerProduct = yield partnerProduct_model_1.PartnerProduct.findAll();
            return partnerProduct;
        });
    }
    static find(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const partnerProduct = yield partnerProduct_model_1.PartnerProduct.findByPk(id);
            return partnerProduct;
        });
    }
    static findOne(where) {
        return __awaiter(this, void 0, void 0, function* () {
            const partnerProduct = yield partnerProduct_model_1.PartnerProduct.findOne({ where });
            return partnerProduct;
        });
    }
    static update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            yield partnerProduct_model_1.PartnerProduct.update(data, { where: { id } });
            const partnerProduct = yield partnerProduct_model_1.PartnerProduct.findByPk(id);
            return partnerProduct;
        });
    }
    static delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield partnerProduct_model_1.PartnerProduct.destroy({
                where: {
                    id: Number(id),
                },
            });
        });
    }
}
exports.PartnerProductRepository = PartnerProductRepository;
//# sourceMappingURL=partnerProduct.repository.js.map