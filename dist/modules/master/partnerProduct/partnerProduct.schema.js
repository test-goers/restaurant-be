"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PartnerProductRequestSchema = void 0;
const typesafe_joi_1 = __importDefault(require("typesafe-joi"));
exports.PartnerProductRequestSchema = typesafe_joi_1.default.object({
    name: typesafe_joi_1.default.string().required().error(new Error('Partner Product Name is not allowed to be empty')),
});
//# sourceMappingURL=partnerProduct.schema.js.map