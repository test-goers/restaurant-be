"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const redirectBack_1 = require("../../core/helpers/redirectBack");
const partnerProduct_repository_1 = require("./partnerProduct.repository");
const partnerProduct_schema_1 = require("./partnerProduct.schema");
class PartnerProductController {
}
exports.default = PartnerProductController;
PartnerProductController.index = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    return res.render('pages/master/partnerProduct/index.pug');
});
PartnerProductController.data = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield partnerProduct_repository_1.PartnerProductRepository.get(req.query.search, req.query.limit, req.query.offset);
    return res.json({ rows: data.rows, total: data.count });
});
PartnerProductController.create = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const old = req.flash('val')[0];
    return res.render('pages/master/partnerProduct/create.pug', { old });
});
PartnerProductController.store = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const isExist = yield partnerProduct_repository_1.PartnerProductRepository.findOne({ name: req.body.name });
        req.body.name = req.body.name.toUpperCase();
        if (isExist) {
            throw new Error('Partner Name is already exist');
        }
        const { error, value } = partnerProduct_schema_1.PartnerProductRequestSchema.validate(req.body);
        if (redirectBack_1.redirectBackIfError(error, req, res, value)) {
            return;
        }
        const partnerProduct = yield partnerProduct_repository_1.PartnerProductRepository.create(value);
        req.flash(`success`, `Insert Partner Product successfully`);
        res.redirect(`./edit/${partnerProduct.id}`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
PartnerProductController.edit = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const partnerProduct = yield partnerProduct_repository_1.PartnerProductRepository.find(Number(req.params.partnerProductId));
        if (!partnerProduct) {
            throw new Error('Data not found');
        }
        const old = req.flash('val')[0];
        return res.render('pages/master/partnerProduct/edit.pug', { old, partnerProduct });
    }
    catch (error) {
        return res.render('pages/errors/error.pug', { error: 404 });
    }
});
PartnerProductController.update = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const id = req.body.partnerProductId;
        delete req.body.partnerProductId;
        req.body.name = req.body.name.toUpperCase();
        const { error, value } = partnerProduct_schema_1.PartnerProductRequestSchema.validate(req.body);
        if (redirectBack_1.redirectBackIfError(error, req, res, value)) {
            return;
        }
        yield partnerProduct_repository_1.PartnerProductRepository.update(id, value);
        req.flash(`success`, `Update Partner Product successfully`);
        res.redirect(`back`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
PartnerProductController.delete = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    yield partnerProduct_repository_1.PartnerProductRepository.delete(req.query.id);
    return res.redirect('/partner-product');
});
//# sourceMappingURL=partnerProduct.controller.js.map