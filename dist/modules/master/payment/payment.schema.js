"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentRequestSchema = void 0;
const typesafe_joi_1 = __importDefault(require("typesafe-joi"));
exports.PaymentRequestSchema = typesafe_joi_1.default.object({
    paymentType: typesafe_joi_1.default.string().required(),
    merchantCode: typesafe_joi_1.default.string().optional().allow(null),
    channel: typesafe_joi_1.default.string().required().error(new Error('Channel is not allowed to be empty')),
    label: typesafe_joi_1.default.string().required(),
    icon: typesafe_joi_1.default.string().optional().allow(null),
    costType: typesafe_joi_1.default.string().required(),
    cost: typesafe_joi_1.default.number().required(),
});
//# sourceMappingURL=payment.schema.js.map