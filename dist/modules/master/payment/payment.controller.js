"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const redirectBack_1 = require("../../core/helpers/redirectBack");
const payment_repository_1 = require("./payment.repository");
const payment_schema_1 = require("./payment.schema");
class PaymentController {
}
exports.default = PaymentController;
PaymentController.index = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    return res.render('pages/master/payment/index.pug');
});
PaymentController.data = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield payment_repository_1.PaymentRepository.get(req.query.search, req.query.limit, req.query.offset);
    return res.json({ rows: data.rows, total: data.count });
});
PaymentController.create = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const old = req.flash('val')[0];
    return res.render('pages/master/payment/create.pug', { old });
});
PaymentController.store = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const isExist = yield payment_repository_1.PaymentRepository.findOne({ channel: req.body.channel });
        if (isExist) {
            throw new Error('Channel is already exist');
        }
        if (req.icon) {
            req.body.icon = req.icon.filename;
        }
        req.body.merchantCode = req.body.merchantCode.length > 0 ? req.body.merchantCode : null;
        const { error, value } = payment_schema_1.PaymentRequestSchema.validate(req.body);
        if (redirectBack_1.redirectBackIfError(error, req, res, value)) {
            return;
        }
        const payment = yield payment_repository_1.PaymentRepository.create(value);
        req.flash(`success`, `Insert Payment successfully`);
        res.redirect(`./edit/${payment.id}`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
PaymentController.edit = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const payment = yield payment_repository_1.PaymentRepository.find(Number(req.params.paymentId));
        if (!payment) {
            throw new Error('Data not found');
        }
        const old = req.flash('val')[0];
        return res.render('pages/master/payment/edit.pug', { old, payment });
    }
    catch (error) {
        return res.render('pages/errors/error.pug', { error: 404 });
    }
});
PaymentController.update = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        const id = req.body.paymentId;
        delete req.body.paymentId;
        if (req.icon) {
            req.body.icon = req.icon.filename;
        }
        req.body.merchantCode = req.body.merchantCode.length > 0 ? req.body.merchantCode : null;
        const { error, value } = payment_schema_1.PaymentRequestSchema.validate(req.body);
        if (redirectBack_1.redirectBackIfError(error, req, res, value)) {
            return;
        }
        yield payment_repository_1.PaymentRepository.update(id, value);
        req.flash(`success`, `Update Payment successfully`);
        res.redirect(`back`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
PaymentController.delete = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    yield payment_repository_1.PaymentRepository.delete(req.query.id);
    return res.redirect('/payment');
});
//# sourceMappingURL=payment.controller.js.map