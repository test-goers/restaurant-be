"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentRepository = void 0;
const sequelize_1 = require("sequelize");
const payment_model_1 = require("./payment.model");
class PaymentRepository {
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const payment = yield payment_model_1.Payment.create(data);
            return payment;
        });
    }
    static get(search = '', limit = 10, offset = 0) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield payment_model_1.Payment.findAndCountAll({
                where: search.length > 0 ?
                    {
                        [sequelize_1.Op.or]: [
                            {
                                paymentType: {
                                    [sequelize_1.Op.like]: `%${String(search)}%`,
                                },
                            },
                            {
                                merchantCode: {
                                    [sequelize_1.Op.like]: `%${String(search)}%`,
                                },
                            },
                            {
                                label: {
                                    [sequelize_1.Op.like]: `%${String(search)}%`,
                                },
                            },
                        ],
                    } : {},
                offset: Number(offset),
                limit: Number(limit),
            });
            return data;
        });
    }
    static find(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const payment = yield payment_model_1.Payment.findByPk(id);
            return payment;
        });
    }
    static findOne(where) {
        return __awaiter(this, void 0, void 0, function* () {
            const payment = yield payment_model_1.Payment.findOne({ where });
            return payment;
        });
    }
    static update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            yield payment_model_1.Payment.update(data, { where: { id } });
            const payment = yield payment_model_1.Payment.findByPk(id);
            return payment;
        });
    }
    static delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield payment_model_1.Payment.destroy({
                where: {
                    id: Number(id),
                },
            });
        });
    }
    static getPaymentMethod(paymentType = '') {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield payment_model_1.Payment.findAll({
                attributes: [
                    'merchantCode',
                    'channel',
                    'label',
                    'costType',
                    'cost',
                    [sequelize_1.Sequelize.fn('CONCAT', process.env.BASE_URL, '/assets/images/', sequelize_1.Sequelize.col('icon')), 'icon']
                ],
                where: {
                    paymentType,
                },
            });
            return data;
        });
    }
}
exports.PaymentRepository = PaymentRepository;
//# sourceMappingURL=payment.repository.js.map