"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.accountRequestSchema = void 0;
const typesafe_joi_1 = __importDefault(require("typesafe-joi"));
exports.accountRequestSchema = typesafe_joi_1.default.object({
    email: typesafe_joi_1.default.string(),
    password: typesafe_joi_1.default.string(),
    phone: typesafe_joi_1.default.string(),
    nameOfBank: typesafe_joi_1.default.string(),
    noRekening: typesafe_joi_1.default.string(),
    namaRekening: typesafe_joi_1.default.string(),
});
//# sourceMappingURL=account.shcema.js.map