"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountRepository = void 0;
const user_model_1 = require("../user/user.model");
class AccountRepository {
    static getDetail(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield user_model_1.User.findByPk(id);
            return user;
        });
    }
    static updateAccount(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            yield user_model_1.User.update(Object.assign({}, data), { where: { id } });
            const user = yield user_model_1.User.findByPk(id);
            return user;
        });
    }
}
exports.AccountRepository = AccountRepository;
//# sourceMappingURL=account.repository.js.map