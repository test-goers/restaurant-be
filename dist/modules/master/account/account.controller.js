"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountController = void 0;
const express_1 = require("express");
const asyncHandler_1 = require("../../core/helpers/asyncHandler");
const responseHandler_1 = require("../../core/helpers/responseHandler");
const account_repository_1 = require("./account.repository");
const accountRouter = express_1.Router();
accountRouter.get('/', asyncHandler_1.asyncHandler((req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // console.log('jajaj');
})));
accountRouter.get(':/id', asyncHandler_1.asyncHandler((req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const account = yield account_repository_1.AccountRepository.getDetail(parseInt(req.params.id, 10));
    // const response: IResponse = {
    //   responseType: ResponseType.SUCCESS,
    //   message: 'Berhasil mengambil data expertise',
    //   data: account,
    // };
    return responseHandler_1.ResponseHandler.jsonSuccess(res, { data: account }, 'Berhasil mengambil data user');
})));
accountRouter.put('/:id/update', asyncHandler_1.asyncHandler((req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const account = yield account_repository_1.AccountRepository.updateAccount(parseInt(req.params.id, 10), req.body);
    return responseHandler_1.ResponseHandler.jsonSuccess(res, { data: account });
})));
const AccountController = express_1.Router();
exports.AccountController = AccountController;
AccountController.use('/account', accountRouter);
//# sourceMappingURL=account.controller.js.map