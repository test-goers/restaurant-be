"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Province = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const city_model_1 = require("../city/city.model");
let Province = class Province extends sequelize_typescript_1.Model {
};
__decorate([
    sequelize_typescript_1.HasMany(() => city_model_1.City),
    __metadata("design:type", Array)
], Province.prototype, "city", void 0);
__decorate([
    sequelize_typescript_1.Column,
    __metadata("design:type", String)
], Province.prototype, "name", void 0);
Province = __decorate([
    sequelize_typescript_1.Table({
        tableName: 'Provinces',
        timestamps: true,
        paranoid: true,
    })
], Province);
exports.Province = Province;
//# sourceMappingURL=province.model.js.map