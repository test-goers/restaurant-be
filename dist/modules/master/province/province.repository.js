"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProvinceRepository = void 0;
const sequelize_1 = require("sequelize");
const province_model_1 = require("./province.model");
class ProvinceRepository {
    static getAll(search = '') {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield province_model_1.Province.findAll({
                where: {
                    name: {
                        [sequelize_1.Op.like]: `%${search}%`,
                    },
                },
            });
            return data;
        });
    }
}
exports.ProvinceRepository = ProvinceRepository;
//# sourceMappingURL=province.repository.js.map