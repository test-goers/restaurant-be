"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const responseHandler_1 = require("../../core/helpers/responseHandler");
const user_repository_1 = require("./user.repository");
const user_schema_1 = require("./user.schema");
class UserController {
    static profile(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = yield user_repository_1.UserRepository.getDetail(req.user.id);
                return responseHandler_1.ResponseHandler.jsonSuccess(res, data);
            }
            catch (error) {
                return responseHandler_1.ResponseHandler.serverError(res, 'Gagal mengambil data');
            }
        });
    }
    static updateProfile(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const file = req.files;
                if (file.photo) {
                    req.body.photo = file.photo[0].filename;
                }
                const { value, error } = user_schema_1.UserUpdateRequestSchema.validate(req.body);
                if (error) {
                    return responseHandler_1.ResponseHandler.jsonError(res, 'Validasi error ' + error);
                }
                yield user_repository_1.UserRepository.updateUser(req.user.id, value);
                const data = yield user_repository_1.UserRepository.getDetail(req.user.id);
                return responseHandler_1.ResponseHandler.jsonSuccess(res, data, 'Berhasil melakukan update data');
            }
            catch (error) {
                return responseHandler_1.ResponseHandler.serverError(res, 'Gagal mengupdate data');
            }
        });
    }
    static settingProfile(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield user_repository_1.UserRepository.getDetail(Number(req.user.id));
            const old = req.flash('val')[0];
            return res.render('pages/admin/setting/index.pug', { user, old });
        });
    }
}
exports.default = UserController;
UserController.index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    return res.render('pages/master/user/index.pug');
});
UserController.store = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        if (req.body.username.length < 6) {
            throw new Error('Username harus lebih dari 6 karakter');
        }
        if (req.body.fullname.length < 6) {
            throw new Error('Fullname harus lebih dari 6 karakter');
        }
        if (req.body.password.length < 6) {
            throw new Error('Password harus lebih dari 6 karakter');
        }
        const checkUsername = yield user_repository_1.UserRepository.checkUsernameExists(req.body.username);
        if (checkUsername) {
            throw new Error(`Username ${req.body.username} sudah digunakan, silahkan masukkan username yang lain`);
        }
        req.body.password = (req.body.password) ? yield bcrypt_1.default.hash(req.body.password, 256) : null;
        const create = yield user_repository_1.UserRepository.create(req.body);
        if (create) {
            req.flash(`success`, `User berhasil ditambahkan`);
        }
        else {
            req.flash('errors', 'Gagal menambahkan user');
        }
        res.redirect(`/user`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
UserController.updatePassword = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        if (req.body.password === req.body.oldpassword) {
            throw new Error('Kata sandi lama dan baru tidak boleh sama');
        }
        if (req.body.password !== req.body.confirmpassword) {
            throw new Error('Konfirmasi kata sandi tidak sesuai');
        }
        const user = yield user_repository_1.UserRepository.getDetail(Number(req.user.id));
        const passwordIsValid = bcrypt_1.default.compareSync(req.body.oldpassword, user.password);
        if (!passwordIsValid) {
            throw new Error('Kata sandi lama tidak sesuai');
        }
        delete req.body.oldpassword;
        delete req.body.confirmpassword;
        req.body.password = bcrypt_1.default.hashSync(req.body.password, 10);
        console.table(req.body);
        const create = yield user_repository_1.UserRepository.updatePassword(req.user.id, req.body);
        if (create) {
            req.flash(`success`, `Kata sandi berhasil disimpan`);
        }
        else {
            req.flash('errors', 'Kata sandi gagal disimpan');
        }
        res.redirect(`/setting`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
UserController.create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const old = req.flash('val')[0];
    return res.render('pages/master/user/create.pug', { old });
});
UserController.edit = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield user_repository_1.UserRepository.getById(req.params.id);
    const old = req.flash('val')[0];
    return res.render('pages/master/user/edit.pug', { data, old });
});
UserController.show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield user_repository_1.UserRepository.getById(req.params.id);
    return res.render('pages/master/user/show', { data });
});
UserController.delete = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield user_repository_1.UserRepository.delete(Number(req.body.id));
        req.flash(`success`, `User berhasil dihapus`);
        return res.redirect('/user');
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
UserController.update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        delete req.body._csrf;
        if (req.body.username.length < 6) {
            throw new Error('Username harus lebih dari 6 karakter');
        }
        if (req.body.fullname.length < 6) {
            throw new Error('Fullname harus lebih dari 6 karakter');
        }
        const id = (req.body.id) ? req.body.id : 0;
        let dataToDatabase = {};
        if (req.body.password === '' && req.body.confirmPassword === '') {
            dataToDatabase = {
                username: req.body.username,
                fullname: req.body.fullname,
            };
        }
        else {
            if (req.body.password !== req.body.confirmPassword) {
                throw new Error('Konfirmasi password tidak sesuai');
            }
            const password = bcrypt_1.default.hashSync(req.body.password, 10);
            dataToDatabase = {
                username: req.body.username,
                fullname: req.body.fullname,
                password,
            };
        }
        const update = yield user_repository_1.UserRepository.update(dataToDatabase, Number(id));
        if (update) {
            req.flash(`success`, `User berhasil diperbaharui`);
        }
        else {
            req.flash('errors', 'Gagal memperbaharui user');
        }
        res.redirect(`/user`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
UserController.json = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const search = req.query.search || '';
        const limit = req.query.limit || 10;
        const offset = req.query.offset || 0;
        const order = req.query.order || 'DESC';
        const data = yield user_repository_1.UserRepository.dataTable(String(search), String(order), Number(offset), Number(limit));
        res.json({
            rows: data.rows,
            total: data.count,
        });
    }
    catch (error) {
        return responseHandler_1.ResponseHandler.jsonError(res, error);
    }
});
UserController.switchActive = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = {
            isActive: req.body.isActive,
        };
        const keterangan = (Number(req.body.isActive) === 1) ? 'diaktifkan' : 'dinonaktifkan';
        yield user_repository_1.UserRepository.update(data, Number(req.body.id));
        req.flash(`success`, `Admin berhasil ${keterangan}`);
        return res.redirect(`/user`);
    }
    catch (error) {
        req.flash('val', req.body);
        req.flash(`errors`, error.message);
        res.redirect(`back`);
    }
});
//# sourceMappingURL=user.controller.js.map