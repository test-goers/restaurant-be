"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const sequelize_1 = require("sequelize");
const user_model_1 = require("./user.model");
class UserRepository {
    static getAll(username, fullname, isActive = '1', limit = 10, offset = 0) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield user_model_1.User.findAndCountAll({
                attributes: {
                    exclude: ['password', 'createdAt', 'updatedAt', 'deletedAt'],
                },
                where: {
                    [sequelize_1.Op.and]: {
                        isActive,
                    },
                },
                order: [['expDate', 'ASC']],
                limit,
                offset,
            });
            return data;
        });
    }
    static get() {
        return __awaiter(this, void 0, void 0, function* () {
            const offset = this.offset;
            const limit = this.limit;
            const where = this.where;
            const order = this.order;
            const data = yield user_model_1.User.findAll({ where, order, limit, offset });
            return data;
        });
    }
    static getDetail(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield user_model_1.User.findOne({
                where: { id },
                attributes: {
                    exclude: ['password', 'deletedAt'],
                },
            });
            return data;
        });
    }
    static getUsername(username) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield user_model_1.User.findOne({
                where: { username },
                attributes: {
                    exclude: ['password'],
                },
            });
            return data;
        });
    }
    static checkUsernameExists(username) {
        return __awaiter(this, void 0, void 0, function* () {
            const isExists = yield user_model_1.User.findOne({
                where: {
                    username,
                },
            });
            return isExists ? true : false;
        });
    }
    static create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield user_model_1.User.create(Object.assign({}, data));
            return user;
        });
    }
    static updateUser(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { fullname, photo, username } = data;
            yield user_model_1.User.update(Object.assign({ fullname, photo, username }), { where: { id } });
            const user = yield user_model_1.User.findByPk(id);
            return user;
        });
    }
    static updateAccount(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { username, password } = data;
            if (password) {
                const encryptedPassword = bcrypt_1.default.hashSync(password, 256);
                yield user_model_1.User.update(Object.assign({ username, password: encryptedPassword }), { where: { id } });
            }
            else {
                yield user_model_1.User.update(Object.assign({ username }), { where: { id } });
            }
            const user = yield user_model_1.User.findByPk(id);
            return user;
        });
    }
    static findByUsername(username, iduser) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield user_model_1.User.findOne({
                where: {
                    username,
                    id: { [sequelize_1.Op.ne]: iduser },
                },
            });
            return data;
        });
    }
    static updatePassword(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            yield user_model_1.User.update({ password: data.password }, { where: { id } });
            const user = yield user_model_1.User.findByPk(id);
            return user;
        });
    }
    static dataTable(search = '', order = 'DESC', offset = 0, limit = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield user_model_1.User.findAndCountAll({
                attributes: {
                    exclude: ['password', 'createdAt', 'deletedAt', 'updatedAt'],
                },
                where: {
                    [sequelize_1.Op.or]: {
                        username: { [sequelize_1.Op.substring]: `%${search}%` },
                        fullname: { [sequelize_1.Op.substring]: `%${search}%` },
                    },
                },
                order: [['id', order]],
                offset,
                limit,
            });
            return data;
        });
    }
    static getById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield user_model_1.User.findOne({
                where: { id },
            });
            return data;
        });
    }
    static delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield user_model_1.User.destroy({ where: { id } });
        });
    }
    static update(data, id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield user_model_1.User.update(data, { where: { id } });
            const updatedData = yield user_model_1.User.findByPk(id);
            return updatedData;
        });
    }
}
exports.UserRepository = UserRepository;
UserRepository.limit = 10;
UserRepository.offset = 0;
UserRepository.include = [];
//# sourceMappingURL=user.repository.js.map