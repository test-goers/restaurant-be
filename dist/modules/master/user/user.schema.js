"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpdateRequestSchema = exports.UserRequestSchema = void 0;
const typesafe_joi_1 = __importDefault(require("typesafe-joi"));
exports.UserRequestSchema = typesafe_joi_1.default.object({
    username: typesafe_joi_1.default.string().email().required(),
    fullname: typesafe_joi_1.default.string().required(),
    password: typesafe_joi_1.default.string().min(8).required(),
});
exports.UserUpdateRequestSchema = typesafe_joi_1.default.object({
    username: typesafe_joi_1.default.string(),
    fullname: typesafe_joi_1.default.date(),
});
//# sourceMappingURL=user.schema.js.map