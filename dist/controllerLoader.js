"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
require("reflect-metadata");
const apiControllerLoader_1 = require("./apiControllerLoader");
const auth_controller_1 = require("./modules/core/controllers/auth.controller");
const dashboard_controller_1 = __importDefault(require("./modules/core/controllers/dashboard.controller"));
const controllerLoader = (app) => {
    app.use(apiControllerLoader_1.apiController);
    app.use(auth_controller_1.authController);
    app.use(dashboard_controller_1.default);
};
module.exports = controllerLoader;
//# sourceMappingURL=controllerLoader.js.map