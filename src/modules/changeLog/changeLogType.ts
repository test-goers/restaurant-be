export class ChangeLogType {
  public static CREATE = 'create'
  public static UPDATE = 'update'
  public static DELETE = 'delete'
}
