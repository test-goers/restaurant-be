import { Column, HasOne, Model, Table } from 'sequelize-typescript';
import { CekTokoAuth } from './cekTokoAuth.model';

@Table({
  tableName: 'tb_user',
  timestamps: false,
})
class CekTokoUser extends Model {
  @Column
  public uid: string;

  @HasOne(() => CekTokoAuth, 'user_id')
  public CekTokoAuth: CekTokoAuth;
}

export { CekTokoUser };
