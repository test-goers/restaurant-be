import { BelongsTo, Column, ForeignKey, Model, Table } from 'sequelize-typescript'
import { CekTokoUser } from './cekTokoUser.model'

@Table({
  tableName: 'users_authentication',
  timestamps: false,
})
class CekTokoAuth extends Model<CekTokoAuth> {
  @ForeignKey(() => CekTokoUser)
  @Column
  public userId: string

  @BelongsTo(() => CekTokoUser)
  public cekTokoUser: CekTokoUser

  @Column
  public token: string

  @Column
  public expiredAt: Date
}

export { CekTokoAuth }

