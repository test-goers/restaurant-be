import { NextFunction, Request, Response } from 'express';

export const apiMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  res.locals.isApi = true;
  res.locals.api = true;
  next();
};
