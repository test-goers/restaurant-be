import aws from 'aws-sdk';
import dotenv from 'dotenv';
import { Router } from 'express';
import multer from 'multer';
import multerS3 from 'multer-s3';
import slugify from 'slugify';
import { FileUpload } from '../../master/files/file/fileUpload.model';

dotenv.config();

const s3 = new aws.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  region: 'ap-southeast-1',
})

const storageBucketProduct = multer({
  storage: multerS3({
    s3,
    bucket: process.env.AWS_BUCKET_NAME,
    acl: 'public-read',
    metadata(req: any, file: { fieldname: any; }, cb: (arg0: any, arg1: { fieldname: any; }) => void) {
      cb(null, { fieldname: file.fieldname });
    },
    key(req: any, file: { fieldname: any; originalname: string; }, cb: (arg0: any, arg1: string) => void) {
      cb(null, `products/${file.fieldname}_${Date.now()}_${slugify(file.originalname)}`);
    },
  }),
})

const storageBucketMerchant = multer({
  storage: multerS3({
    s3,
    bucket: process.env.AWS_BUCKET_NAME,
    acl: 'public-read',
    metadata(req: any, file: { fieldname: any; }, cb: (arg0: any, arg1: { fieldname: any; }) => void) {
      cb(null, { fieldname: file.fieldname });
    },
    key(req: any, file: { fieldname: any; originalname: string; }, cb: (arg0: any, arg1: string) => void) {
      cb(null, `merchants/${file.fieldname}_${Date.now()}_${slugify(file.originalname)}`);
    },
  }),
})

const storageBucketCategory = multer({
  storage: multerS3({
    s3,
    bucket: process.env.AWS_BUCKET_NAME,
    acl: 'public-read',
    metadata(req: any, file: { fieldname: any; }, cb: (arg0: any, arg1: { fieldname: any; }) => void) {
      cb(null, { fieldname: file.fieldname });
    },
    key(req: any, file: { fieldname: any; originalname: string; }, cb: (arg0: any, arg1: string) => void) {
      cb(null, `categories/${file.fieldname}_${Date.now()}_${slugify(file.originalname)}`);
    },
  }),
})

const productImageMiddleware = ({
  fields,
  name = null,
  tag = 'file',
  access = 'public',
}: {
  fields: any;
  name?: string;
  tag?: string;
  access?: string;
}) => {
  const router = Router();
  router.use(storageBucketProduct.fields(fields), async (req: any, res, next) => {
    await Promise.all(
      fields.map(async (element: { name: string; maxCount: number }) => {
        if (req.files[element.name] !== undefined) {
          const file = req.files[element.name][0];
          const saveFile: object = {
            key: file.key,
            location: file.location,
            mimetype: file.mimetype,
          }

          const dataFromUpload = await FileUpload.create(saveFile)
          req[element.name] = dataFromUpload;
        } else {
          req[element.name] = {};
        }
      }),
    );
    next();
  });
  return router;
};

const merchantImageMiddleware = ({
  fields,
  name = null,
  tag = 'file',
  access = 'public',
}: {
  fields: any;
  name?: string;
  tag?: string;
  access?: string;
}) => {
  const router = Router();
  router.use(storageBucketMerchant.fields(fields), async (req: any, res, next) => {
    await Promise.all(
      fields.map(async (element: { name: string; maxCount: number }) => {
        if (req.files[element.name] !== undefined) {
          const file = req.files[element.name][0];
          const saveFile: object = {
            key: file.key,
            location: file.location,
            mimetype: file.mimetype,
          }

          const dataFromUpload = await FileUpload.create(saveFile)
          req[element.name] = dataFromUpload;
        } else {
          req[element.name] = {};
        }
      }),
    );
    next();
  });
  return router;
};

const categoryImageMiddleware = ({
  fields,
  name = null,
  tag = 'file',
  access = 'public',
}: {
  fields: any;
  name?: string;
  tag?: string;
  access?: string;
}) => {
  const router = Router();
  router.use(storageBucketCategory.fields(fields), async (req: any, res, next) => {
    await Promise.all(
      fields.map(async (element: { name: string; maxCount: number }) => {
        if (req.files[element.name] !== undefined) {
          const file = req.files[element.name][0];
          const saveFile: object = {
            key: file.key,
            location: file.location,
            mimetype: file.mimetype,
          }

          const dataFromUpload = await FileUpload.create(saveFile)
          req[element.name] = dataFromUpload;
        } else {
          req[element.name] = {};
        }
      }),
    );
    next();
  });
  return router;
};


export { productImageMiddleware, merchantImageMiddleware, categoryImageMiddleware };
