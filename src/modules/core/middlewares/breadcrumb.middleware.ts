import { NextFunction, Request, Response } from 'express';
import { pathToRegexp } from 'path-to-regexp';
import * as url from 'url';
import { breadcrumbConfig } from '../config/breadcrumb.config';

export interface IBreadcrumbItem {
  name: string;
  url: string;
  last: boolean;
}

export const breadcrumbMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const urlParts = url.parse(req.url).pathname.split('/');
  const breadcrumbs: IBreadcrumbItem[] = [];
  for (let i = 0; i < urlParts.length; i++) {
    const constructedUrl = urlParts.slice(0, i + 1).join('/');
    const breadcrumb = breadcrumbConfig.find(breadcrumbItem =>
      pathToRegexp(breadcrumbItem.url).test(constructedUrl),
    );
    if (breadcrumb) {
      breadcrumbs.push({
        name: breadcrumb.name,
        url: constructedUrl,
        last: i === urlParts.length - 1,
      });
    }
  }

  res.locals.breadcrumbs = breadcrumbs;

  next();
};
