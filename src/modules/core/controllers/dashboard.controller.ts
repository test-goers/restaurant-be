import { Router } from 'express';
import DashboardRestaurantController from '../../master/restaurant/dashboard/dashboard.controller';
import RestaurantController from '../../master/restaurant/restaurant.controller';
import { isAuthenticated } from '../config/passport';

const dashboardRouter = Router();

dashboardRouter.get('/', isAuthenticated, DashboardRestaurantController.index);
dashboardRouter.get('/dashboard', isAuthenticated, DashboardRestaurantController.index);

dashboardRouter.get('/restaurant', isAuthenticated, RestaurantController.index)
dashboardRouter.get('/restaurant/json', isAuthenticated, RestaurantController.json)
dashboardRouter.get('/restaurant/create', isAuthenticated, RestaurantController.create)
dashboardRouter.post('/restaurant/store', isAuthenticated, RestaurantController.store)
dashboardRouter.get('/restaurant/:id/edit', isAuthenticated, RestaurantController.edit)
dashboardRouter.get('/restaurant/:id/show', isAuthenticated, RestaurantController.show)
dashboardRouter.post('/restaurant/delete', isAuthenticated, RestaurantController.delete)
dashboardRouter.post('/restaurant/update', isAuthenticated, RestaurantController.update)



const dashboardController = Router();
dashboardController.use('/', isAuthenticated, dashboardRouter);
export default dashboardController;

