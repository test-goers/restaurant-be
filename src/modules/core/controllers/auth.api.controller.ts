import bcrypt from 'bcrypt';
import { Response } from 'express';
import jwt from 'jsonwebtoken';
import { Request } from '../../../typings/request';
import { User } from '../../master/user/user.model';
import { UserRepository } from '../../master/user/user.repository';
import { ResponseHandler } from '../helpers/responseHandler';

export default class AuthApiController {
  public static login = async (req: Request, res: Response) => {
    try {
      const { username } = req.body;

      const user = await User.findOne({
        where: { username },
        raw: true,
      })
      if (!user) {
        return ResponseHandler.jsonError(res, 'User tidak ditemukan');
      }

      if (user.deletedAt === null) { delete user.deletedAt; }

      const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
      if (!passwordIsValid) {
        return ResponseHandler.jsonError(res, 'password tidak sesuai');
      }
      const { password, ...result } = user;
      const token = jwt.sign(result, process.env.SESSION_SECRET);
      const data = {
        token,
        result,
      };
      return ResponseHandler.jsonSuccess(res, data, 'Login Berhasil');
    } catch (error) {
      console.log(error);
      return ResponseHandler.serverError(res, 'Gagal Login');
    }
  };

  public static me = async (req: Request, res: Response) => {
    const user = await UserRepository.getDetail(req.user.id);
    const data = {
      user,
    };
    return ResponseHandler.jsonSuccess(res, data);
  };

  public static resetPassword = async (req: Request, res: Response) => {
    try {
      const user = await UserRepository.getUsername(req.body.username);

      if (!user) {
        return ResponseHandler.jsonError(res, 'User Tidak ditemukan');
      }

      await User.update(
        {
          password: await bcrypt.hash(req.body.password, 10),
        },
        {
          where: { id: user.id },
        },
      );

      return ResponseHandler.jsonSuccess(res, { user }, 'update password berhasil');
    } catch (error) {
      return ResponseHandler.serverError(res);
    }
  };
}
