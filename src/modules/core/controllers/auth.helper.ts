import { User } from '../../master/user/user.model';
 
const getMjmlResetPassword = (user: User, linkResetPassword: string): string => console.log(linkResetPassword) + `
  <mjml>
    <mj-head>
      <mj-title>Temu Expert</mj-title>
      <mj-preview>Verifikasi Akun</mj-preview>
      <mj-attributes>
        <mj-all font-family="Muli SemiBold"></mj-all>
        <mj-text font-weight="400" font-size="16px" color="#000000" line-height="24px" font-family="'Helvetica Neue', Helvetica, Arial, sans-serif"></mj-text>
      </mj-attributes>
      <mj-style inline="inline">
        .body-section { -webkit-box-shadow: 1px 4px 11px 0px rgba(43, 184, 117, 0.15); -moz-box-shadow: 1px 4px 11px 0px rgba(43, 184, 117, 0.15); box-shadow: 1px 4px 11px 0px rgba(43, 184, 117, 0.15); }
      </mj-style>
      <mj-style inline="inline">
        .text-link { color: #5e6ebf }
      </mj-style>
      <mj-style inline="inline">
        .footer-link { color: #888888 }
      </mj-style>

    </mj-head>
    <mj-body background-color="white">
      <mj-section full-width="full-width" background-color="white">
        <mj-column width="100%"></mj-column>
      </mj-section>

      <mj-wrapper padding-top="0" padding-bottom="0" css-class="body-section" border="1px solid #2BB875">
        <mj-section background-color="white" padding-bottom="0">
          <mj-column width="100%">
            <mj-image src="" alt="" align="center" width="150px" />
          </mj-column>
        </mj-section>
        <mj-section background-color="#ffffff" padding-left="15px" padding-right="15px">
          <mj-column width="100%">
            <mj-text color="#212b35" font-weight="bold" font-size="20px" align="center">
              Permintaan Reset Password 
            </mj-text>
          </mj-column>
        </mj-section>

        <mj-section background-color="#ffffff" padding-left="15px" padding-right="15px" padding-bottom="0">
          <mj-column width="100%">
            <mj-text  align="center" font-weight="bold" color="#637381" font-size="16px">
              Hallo, ${ user.username }
            </mj-text>
            <mj-text color="#637381" font-size="16px">
              Anda Mendapatkan Email ini Karena anda Meminta mereset password
            </mj-text>
          </mj-column>
        </mj-section>
        
        <mj-section background-color="#ffffff" padding-top="0" padding-bottom="0" padding-left="15px" padding-right="15px">
          <mj-column width="100%">
            <mj-text  align="center" color="#637381" font-size="16px">
              Abaikan Email ini jika anda tidak merasa melakukan reset password
            </mj-text>
            <mj-button background-color="#2BB875" align="center" color="#ffffff" font-size="17px" font-weight="bold" href="${linkResetPassword}">
              Reset Password
            </mj-button>
          </mj-column>
        </mj-section>
        <mj-section background-color="#ffffff" padding-left="15px" padding-right="15px" padding-top="0">
          <mj-column width="100%">
            <mj-divider border-color="#DFE3E8" border-width="1px" />
          </mj-column>
        </mj-section>
        <mj-section background-color="#ffffff" padding="0 15px 15px 15px">
          <mj-column width="100%">
            <mj-text color="#637381" font-weight="bold" font-size="16px" padding-bottom="0">
              Perhatian!
            </mj-text>
            <mj-text color="#637381" font-size="16px">
              Link ini hanya satu kali pakai, jika sudah tidak berfungsi silahkan ulangi lagi.
            </mj-text>
            <mj-text color="#637381" font-size="16px">
              Terima kasih
            </mj-text>
          </mj-column>
        </mj-section>
      </mj-wrapper>
    </mj-body>
  </mjml>
`;
const getMjmlRegister = (user: User, verificationLink: string): string => console.log(verificationLink) + `
  <mjml>
    <mj-head>
      <mj-title>Temu Expert</mj-title>
      <mj-preview>verifikasi Akun</mj-preview>
      <mj-attributes>
        <mj-all font-family="Muli SemiBold"></mj-all>
        <mj-text font-weight="400" font-size="16px" color="#000000" line-height="24px" font-family="'Helvetica Neue', Helvetica, Arial, sans-serif"></mj-text>
      </mj-attributes>
      <mj-style inline="inline">
        .body-section { -webkit-box-shadow: 1px 4px 11px 0px rgba(43, 184, 117, 0.15); -moz-box-shadow: 1px 4px 11px 0px rgba(43, 184, 117, 0.15); box-shadow: 1px 4px 11px 0px rgba(43, 184, 117, 0.15); }
      </mj-style>
      <mj-style inline="inline">
        .text-link { color: #5e6ebf }
      </mj-style>
      <mj-style inline="inline">
        .footer-link { color: #888888 }
      </mj-style>

    </mj-head>
    <mj-body background-color="white">
      <mj-section full-width="full-width" background-color="white">
        <mj-column width="100%"></mj-column>
      </mj-section>

      <mj-wrapper padding-top="0" padding-bottom="0" css-class="body-section" border="1px solid #2BB875">
        <mj-section background-color="white" padding-bottom="0">
          <mj-column width="100%">
            <mj-image src="" alt="" align="center" width="150px" />
          </mj-column>
        </mj-section>
        <mj-section background-color="#ffffff" padding-left="15px" padding-right="15px">
          <mj-column width="100%">
            <mj-text color="#212b35" font-weight="bold" font-size="20px" align="center">
              Selamat!
            </mj-text>
          </mj-column>
        </mj-section>

        <mj-section background-color="#ffffff" padding-left="15px" padding-right="15px" padding-bottom="0">
          <mj-column width="100%">
            <mj-text  align="center" font-weight="bold" color="#637381" font-size="16px">
              Halo, ${ user.username }
            </mj-text>
            <mj-text color="#637381" font-size="16px">
              Anda telah melakukan registrasi untuk Official Website Temu Expert.
            </mj-text>
            <mj-text color="#637381" font-size="16px">
              Klik link di bawah untuk mengaktifkan akun Anda :
            </mj-text>
          </mj-column>
        </mj-section>
        
        <mj-section background-color="#ffffff" padding-top="0" padding-bottom="0" padding-left="15px" padding-right="15px">
          <mj-column width="100%">
            <mj-button background-color="#2BB875" align="center" color="#ffffff" font-size="17px" font-weight="bold" href="${verificationLink}">
              Link
            </mj-button>
          </mj-column>
        </mj-section>
        <mj-section background-color="#ffffff" padding-left="15px" padding-right="15px" padding-top="0">
          <mj-column width="100%">
            <mj-divider border-color="#DFE3E8" border-width="1px" />
          </mj-column>
        </mj-section>
        <mj-section background-color="#ffffff" padding="0 15px 15px 15px">
          <mj-column width="100%">
            <mj-text color="#637381" font-weight="bold" font-size="16px" padding-bottom="0">
              Perhatian!
            </mj-text>
            <mj-text color="#637381" font-size="16px">
              Link ini hanya satu kali pakai, jika sudah tidak berfungsi silahkan ulangi lagi.
            </mj-text>
            <mj-text color="#637381" font-size="16px">
              Terima kasih
            </mj-text>
          </mj-column>
        </mj-section>
      </mj-wrapper>
    </mj-body>
  </mjml>
`;

export { getMjmlResetPassword, getMjmlRegister };

