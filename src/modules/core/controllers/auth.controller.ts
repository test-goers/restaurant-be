import { Request, Response, Router } from 'express';
import { isAuthenticated } from '../config/passport';
import { asyncHandler } from './../helpers/asyncHandler';

const authRouter = Router();

authRouter.get('/login', async (req: Request, res: Response) => {
  res.render('login.pug', {
    errors: req.flash('error'),
  });
});

authRouter.get(
  '/logout',
  isAuthenticated,
  async (req: Request, res: Response) => {
    req.session.destroy((error: Error) => {
      if (error) {
        throw Error(error.message);
      }
      res.redirect('/auth/login');
    });
  },
);

authRouter.get(
  '/reset_password',
  asyncHandler(async (req: Request, res: Response) => {
    return res.render('reset_password_mobile.pug');
  }),
);

const authController = Router();
authController.use('/auth', authRouter);

export { authController };

