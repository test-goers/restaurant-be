import { CekTokoAuth } from '../models/cekTokoAuth.model';
import { CekTokoUser } from '../models/cekTokoUser.model';

export class CekTokoRepository {
  public static async getToken(userId: number): Promise<CekTokoUser>{
    // const token = await CekTokoUser.findOne({
    //   where: {
    //     uid: 'as9msadxpASlkdsaaB7',
    //   }
    // });
    // return token;
    const token = await CekTokoUser.findOne({
      // attributes: [['$users_authentication.token$', 'token']],
      where: {
        id: userId,
      },
      include: [
        {
          model: CekTokoAuth,
          where: {
            // expired_at: {
            //   [Op.gt]: Sequelize.fn('NOW'),
            // }
          },
        },
      ],
    })
    return token;
  }
}