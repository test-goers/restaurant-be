export const numberFormat = (numbers: number) => {
  return numbers.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.');
  // return new Intl.NumberFormat('IDR-ID', {
  //   style: 'currency',
  //   currency: 'IDR',
  // }).format(number);
};