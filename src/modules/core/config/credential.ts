import dotenv from 'dotenv';
dotenv.config();

const rajaBillerCredential: object = {
  uid: process.env.RAJABILLER_UID,
  pin: process.env.RAJABILLER_PIN,
}

export { rajaBillerCredential };

