import nodemailer from 'nodemailer';
import smtpTransport from 'nodemailer-smtp-transport';

interface ISendEmail {
  to: string,
  subject: string,
  html: string
}


function createTransporter() {
  return nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: process.env.GMAIL_EMAIL,
      pass: process.env.GMAIL_PASSWORD,
    },
  }));
}

export class Mailer {
  public static async sendEmail({ to, subject, html }: ISendEmail): Promise<void> {
    console.log('send email message');
    return new Promise((resolve, reject) => {
      createTransporter().sendMail({
        from: process.env.GMAIL_EMAIL,
        to,
        subject,
        html,
      }, (error, info) => {
        if (error) {
          reject(error);
        } else {
          resolve(info);
        }
      });
    });
  }
}
