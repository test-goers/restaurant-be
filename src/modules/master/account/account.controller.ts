import { Request, Response, Router } from 'express';
import { asyncHandler } from '../../core/helpers/asyncHandler';
import { ResponseHandler } from '../../core/helpers/responseHandler';
import { AccountRepository } from './account.repository';

const accountRouter = Router()
accountRouter.get(
  '/',
  asyncHandler( async (req: Request, res: Response)=>{
    // console.log('jajaj');
    
  }),
)
accountRouter.get(
  ':/id',
  asyncHandler(async(req: Request, res: Response)=>{
    const account = await AccountRepository.getDetail(parseInt(req.params.id,10));
    // const response: IResponse = {
    //   responseType: ResponseType.SUCCESS,
    //   message: 'Berhasil mengambil data expertise',
    //   data: account,
    // };
    return ResponseHandler.jsonSuccess(res, { data: account }, 'Berhasil mengambil data user')
  }),
)

accountRouter.put(
  '/:id/update',
  asyncHandler(async(req:Request, res: Response)=>{
    const account = await AccountRepository.updateAccount(parseInt(req.params.id,10), req.body);
    return ResponseHandler.jsonSuccess(res, { data: account })
  }),
)

const AccountController = Router();
AccountController.use('/account', accountRouter);

export { AccountController };

