import { User } from '../user/user.model';

export class AccountRepository {
  public static async getDetail(id: number): Promise<User>{
    const user = await User.findByPk(id)
    return user;
  }

  public static async updateAccount(id: number, data: any): Promise<User>{
    await User.update({...data}, {where: {id}});
    const user = await User.findByPk(id);
    return user;
  }
}
