import Joi from 'typesafe-joi';

export const accountRequestSchema = Joi.object({
  email: Joi.string(),
  password: Joi.string(),
  phone: Joi.string(),
  nameOfBank: Joi.string(),
  noRekening: Joi.string(),
  namaRekening: Joi.string(),
})

export type accountCreateRequest = Joi.Literal<typeof accountRequestSchema>