import bcrypt from 'bcrypt';
import { Response } from 'express';
import { Request } from '../../../typings/request';
import { ResponseHandler } from '../../core/helpers/responseHandler';
import { UserRepository } from './user.repository';
import { UserUpdateRequestSchema } from './user.schema';

export default class UserController {
  public static index = async (req: Request, res: Response) => {

    return res.render('pages/master/user/index.pug')
  }

  public static async profile(req: Request, res: Response): Promise<void> {
    try {
      const data = await UserRepository.getDetail(req.user.id)
      return ResponseHandler.jsonSuccess(res, data)
    } catch (error) {
      return ResponseHandler.serverError(res, 'Gagal mengambil data')
    }
  }

  public static async updateProfile(req: Request, res: Response): Promise<void> {
    try {
      const file: any = req.files
      if (file.photo) {
        req.body.photo = file.photo[0].filename
      }
      const { value, error } = UserUpdateRequestSchema.validate(req.body)
      if (error) { return ResponseHandler.jsonError(res, 'Validasi error ' + error) }
      await UserRepository.updateUser(req.user.id, value)
      const data = await UserRepository.getDetail(req.user.id);
      return ResponseHandler.jsonSuccess(res, data, 'Berhasil melakukan update data')
    } catch (error) {
      return ResponseHandler.serverError(res, 'Gagal mengupdate data')
    }
  }

  public static async settingProfile(req: Request, res: Response): Promise<void> {
    const user = await UserRepository.getDetail(Number(req.user.id))
    const old = req.flash('val')[0]
    return res.render('pages/admin/setting/index.pug', { user, old });
  }

  public static store = async (req: Request, res: Response) => {
    try {
      delete req.body._csrf
      if (req.body.username.length < 6) { throw new Error('Username harus lebih dari 6 karakter') }
      if (req.body.fullname.length < 6) { throw new Error('Fullname harus lebih dari 6 karakter') }
      if (req.body.password.length < 6) { throw new Error('Password harus lebih dari 6 karakter') }

      const checkUsername = await UserRepository.checkUsernameExists(req.body.username)
      if (checkUsername) { throw new Error(`Username ${req.body.username} sudah digunakan, silahkan masukkan username yang lain`) }

      req.body.password = (req.body.password) ? await bcrypt.hash(req.body.password, 256) : null

      const create = await UserRepository.create(req.body)

      if (create) {
        req.flash(`success`, `User berhasil ditambahkan`)
      } else {
        req.flash('errors', 'Gagal menambahkan user')
      }
      res.redirect(`/user`);
    } catch (error) {
      req.flash('val', req.body);
      req.flash(`errors`, error.message);
      res.redirect(`back`);
    }
  }

  public static updatePassword = async (req: Request, res: Response) => {
    try {
      delete req.body._csrf
      if (req.body.password === req.body.oldpassword) {
        throw new Error('Kata sandi lama dan baru tidak boleh sama')
      }
      if (req.body.password !== req.body.confirmpassword) {
        throw new Error('Konfirmasi kata sandi tidak sesuai')
      }
      const user = await UserRepository.getDetail(Number(req.user.id))
      const passwordIsValid = bcrypt.compareSync(req.body.oldpassword, user.password);
      if (!passwordIsValid) {
        throw new Error('Kata sandi lama tidak sesuai')
      }
      delete req.body.oldpassword
      delete req.body.confirmpassword
      req.body.password = bcrypt.hashSync(req.body.password, 10);
      console.table(req.body)

      const create = await UserRepository.updatePassword(req.user.id, req.body)

      if (create) {
        req.flash(`success`, `Kata sandi berhasil disimpan`)
      } else {
        req.flash('errors', 'Kata sandi gagal disimpan')
      }
      res.redirect(`/setting`);
    } catch (error) {
      req.flash('val', req.body);
      req.flash(`errors`, error.message);
      res.redirect(`back`);
    }
  }

  public static create = async (req: Request, res: Response) => {
    const old = req.flash('val')[0]

    return res.render('pages/master/user/create.pug', { old })
  }

  public static edit = async (req: Request, res: Response) => {
    const data = await UserRepository.getById(req.params.id)
    const old = req.flash('val')[0]

    return res.render('pages/master/user/edit.pug', { data, old })
  }

  public static show = async (req: Request, res: Response) => {
    const data = await UserRepository.getById(req.params.id)

    return res.render('pages/master/user/show', { data })
  }

  public static delete = async (req: Request, res: Response) => {
    try {
      await UserRepository.delete(Number(req.body.id))
      req.flash(`success`, `User berhasil dihapus`)
      return res.redirect('/user')
    } catch (error) {
      req.flash('val', req.body);
      req.flash(`errors`, error.message);
      res.redirect(`back`);
    }
  }

  public static update = async (req: Request, res: Response) => {
    try {
      delete req.body._csrf
      if (req.body.username.length < 6) { throw new Error('Username harus lebih dari 6 karakter') }
      if (req.body.fullname.length < 6) { throw new Error('Fullname harus lebih dari 6 karakter') }
      const id = (req.body.id) ? req.body.id : 0

      let dataToDatabase: object = {}

      if (req.body.password === '' && req.body.confirmPassword === '') {
        dataToDatabase = {
          username: req.body.username,
          fullname: req.body.fullname,
        }
      } else {
        if (req.body.password !== req.body.confirmPassword) {
          throw new Error('Konfirmasi password tidak sesuai')
        }
        const password = bcrypt.hashSync(req.body.password, 10)
        dataToDatabase = {
          username: req.body.username,
          fullname: req.body.fullname,
          password,
        }
      }

      const update = await UserRepository.update(dataToDatabase, Number(id))

      if (update) {
        req.flash(`success`, `User berhasil diperbaharui`)
      } else {
        req.flash('errors', 'Gagal memperbaharui user')
      }

      res.redirect(`/user`);
    } catch (error) {
      req.flash('val', req.body);
      req.flash(`errors`, error.message);
      res.redirect(`back`);
    }
  }

  public static json = async (req: Request, res: Response) => {
    try {
      const search = req.query.search || ''
      const limit = req.query.limit || 10
      const offset = req.query.offset || 0
      const order = req.query.order || 'DESC'

      const data = await UserRepository.dataTable(
        String(search),
        String(order),
        Number(offset),
        Number(limit))

      res.json({
        rows: data.rows,
        total: data.count,
      });
    } catch (error) {
      return ResponseHandler.jsonError(res, error)
    }
  }

  public static switchActive = async (req: Request, res: Response) => {
    try {
      const data: object = {
        isActive: req.body.isActive,
      }
      const keterangan = (Number(req.body.isActive) === 1) ? 'diaktifkan' : 'dinonaktifkan'

      await UserRepository.update(data, Number(req.body.id))

      req.flash(`success`, `Admin berhasil ${keterangan}`)
      return res.redirect(`/user`)
    } catch (error) {
      req.flash('val', req.body);
      req.flash(`errors`, error.message);
      res.redirect(`back`);
    }
  }

}