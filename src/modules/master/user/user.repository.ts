import bcrypt from 'bcrypt';
import { Op } from 'sequelize';
import { User } from './user.model';

export class UserRepository {
  public static async getAll(
    username: string,
    fullname: string,
    isActive: string = '1',
    limit: number = 10,
    offset: number = 0,
  ): Promise<{ count: number; rows: User[] }> {
    const data = await User.findAndCountAll({
      attributes: {
        exclude: ['password', 'createdAt', 'updatedAt', 'deletedAt'],
      },
      where: {
        [Op.and]: {
          isActive,
        },
      },
      order: [['expDate', 'ASC']],
      limit,
      offset,
    });

    return data;
  }

  public static async get(): Promise<User[]> {
    const offset: number = this.offset;
    const limit: number = this.limit;
    const where: {} = this.where;
    const order: [any] = this.order;
    const data = await User.findAll({ where, order, limit, offset });
    return data;
  }

  public static async getDetail(id: number): Promise<User> {
    const data = await User.findOne({
      where: { id },
      attributes: {
        exclude: ['password', 'deletedAt'],
      },
    });
    return data;
  }

  public static async getUsername(username: string): Promise<User> {
    const data = await User.findOne({
      where: { username },
      attributes: {
        exclude: ['password'],
      },
    });
    return data;
  }

  public static async checkUsernameExists(username: string): Promise<boolean> {
    const isExists = await User.findOne({
      where: {
        username,
      },
    });
    return isExists ? true : false;
  }

  public static async create(data: any): Promise<User> {
    const user = await User.create({ ...data });
    return user;
  }

  public static async updateUser(id: number, data: any): Promise<User> {
    const { fullname, photo, username } = data;
    await User.update({ ...{ fullname, photo, username } }, { where: { id } });
    const user = await User.findByPk(id);
    return user;
  }

  public static async updateAccount(id: number, data: any): Promise<User> {
    const { username, password } = data;
    if (password) {
      const encryptedPassword = bcrypt.hashSync(password, 256);
      await User.update({ ...{ username, password: encryptedPassword } }, { where: { id } });
    } else {
      await User.update({ ...{ username } }, { where: { id } });
    }
    const user = await User.findByPk(id);
    return user;
  }

  public static async findByUsername(username: string, iduser: number): Promise<User> {
    const data = await User.findOne({
      where: {
        username,
        id: { [Op.ne]: iduser },
      },
    });

    return data;
  }

  public static async updatePassword(id: number, data: any): Promise<User> {
    await User.update({ password: data.password }, { where: { id } });
    const user = await User.findByPk(id);
    return user;
  }

  public static async dataTable(
    search: string = '',
    order: string = 'DESC',
    offset: number = 0,
    limit: number = 10,
  ) {
    const data = await User.findAndCountAll({
      attributes: {
        exclude: ['password', 'createdAt', 'deletedAt', 'updatedAt'],
      },
      where: {
        [Op.or]: {
          username: { [Op.substring]: `%${search}%` },
          fullname: { [Op.substring]: `%${search}%` },
        },
      },
      order: [['id', order]],
      offset,
      limit,
    });

    return data;
  }

  public static async getById(id: any): Promise<User> {
    const data = await User.findOne({
      where: { id },
    });

    return data;
  }

  public static async delete(id: number): Promise<void> {
    await User.destroy({ where: { id } });
  }

  public static async update(data: any, id: number): Promise<User> {
    await User.update(data, { where: { id } });
    const updatedData = await User.findByPk(id);

    return updatedData;
  }

  protected static limit: number = 10;
  protected static offset: number = 0;
  protected static where: {};
  protected static include: object[] = [];
  protected static order: [['fullname', 'ASC']];
}
