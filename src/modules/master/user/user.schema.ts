import Joi from 'typesafe-joi'

export const UserRequestSchema = Joi.object({
  username: Joi.string().email().required(),
  fullname: Joi.string().required(),
  password: Joi.string().min(8).required(),
})

export const UserUpdateRequestSchema = Joi.object({
  username: Joi.string(),
  fullname: Joi.date(),
})

export type UserCreateSchema = Joi.Literal<typeof UserRequestSchema>
export type UserUpdateRequestSchema = Joi.Literal<typeof UserUpdateRequestSchema>