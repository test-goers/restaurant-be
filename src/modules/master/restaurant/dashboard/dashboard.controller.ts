import { NextFunction, Response } from 'express'
import moment from 'moment'
import { Request } from '../../../../typings/request'
import { RestaurantRepository } from '../restaurant.repository'

export default class DashboardRestaurantController {
  public static index = async (req: Request, res: Response, next: NextFunction) => {
    const lastUpdate = moment().format('DD-MM-YYYY HH:MM:SS')
    const data = await RestaurantRepository.getCount()
    return res.render('home.pug', { lastUpdate, data })
  }
}