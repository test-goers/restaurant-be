import { NextFunction, Response } from 'express';
import { Request } from '../../../typings/request';
import { ResponseHandler } from '../../core/helpers/responseHandler';
import { RestaurantRepository } from './restaurant.repository';

export default class RestaurantApiController {
  public static getAll = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = await RestaurantRepository.getAll(req);
      return data
        ? ResponseHandler.jsonSuccess(res, data)
        : ResponseHandler.jsonError(res, 'Tidak ada restoran untuk saat ini');
    } catch (error) {
      return ResponseHandler.jsonError(res, error.message);
    }
  }

  public static getDetail = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = await RestaurantRepository.get(Number(req.params.id));
      return data
        ? ResponseHandler.jsonSuccess(res, data)
        : ResponseHandler.jsonError(res, 'Tidak ada restoran untuk saat ini');
    } catch (error) {
      return ResponseHandler.jsonError(res, error.message);
    }
  };
}