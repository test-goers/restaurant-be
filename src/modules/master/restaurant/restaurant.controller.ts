import { Response } from 'express';
import { Request } from '../../../typings/request';
import { ResponseHandler } from '../../core/helpers/responseHandler';
import { RestaurantRepository } from './restaurant.repository';

export default class RestaurantController {
  public static index = async (req: Request, res: Response) => {
    return res.render('pages/master/restaurant/index.pug')
  }
  public static create = async (req: Request, res: Response) => {
    const old = req.flash('val')[0]

    return res.render('pages/master/restaurant/create.pug', { old })
  }

  public static json = async (req: Request, res: Response) => {
    try {
      const search = req.query.search || ''
      const limit = req.query.limit || null
      const order = req.query.order || 'asc'
      const offset = req.query.offset || 0

      const data = await RestaurantRepository.dataTable(
        String(search),
        String(order),
        Number(offset),
        Number(limit),
      )

      res.json({
        rows: data.rows,
        total: data.count,
      })
    } catch (error) {
      return ResponseHandler.jsonError(res, error)
    }
  }

  public static store = async (req: Request, res: Response) => {
    try {
      delete req.body._csrf
      const create = await RestaurantRepository.create(req.body)

      if (create) {
        req.flash(`success`, `Restaurant berhasil ditambahkan`)
      } else {
        req.flash('errors', 'Gagal menambahkan Restaurant')
      }
      res.redirect(`/restaurant`);
    } catch (error) {
      req.flash('val', req.body);
      req.flash(`errors`, error.message);
      res.redirect(`back`);
    }
  }

  public static delete = async (req: Request, res: Response) => {
    try {
      await RestaurantRepository.delete(Number(req.body.id))
      req.flash(`success`, `Restaurant berhasil dihapus`)
      return res.redirect('/restaurant')
    } catch (error) {
      req.flash('val', req.body);
      req.flash(`errors`, error.message);
      res.redirect(`back`);
    }
  }

  public static show = async (req: Request, res: Response) => {
    const data = await RestaurantRepository.get(Number(req.params.id))

    return res.render('pages/master/restaurant/show.pug', { data })
  }

  public static edit = async (req: Request, res: Response) => {
    const data = await RestaurantRepository.get(Number(req.params.id))
    const old = req.flash('val')[0]

    return res.render('pages/master/restaurant/edit.pug', { data, old })
  }

  public static update = async (req: Request, res: Response) => {
    try {
      delete req.body._csrf
      const id = req.body.id || 0

      const update = await RestaurantRepository.update(req.body, Number(id))

      if (update) {
        req.flash(`success`, `Restaurant berhasil diperbaharui`)
      } else {
        req.flash('errors', 'Gagal memperbaharui restaurant')
      }
      res.redirect(`/restaurant`);
    } catch (error) {
      req.flash('val', req.body);
      req.flash(`errors`, error.message);
      res.redirect(`back`);
    }
  }


}