import { Column, Model, Table } from 'sequelize-typescript';

@Table({
  tableName: 'Restaurants',
  timestamps: true,
  paranoid: true,
})

class Restaurant extends Model<Restaurant> {

  @Column
  public name: string

  @Column
  public description: string

  @Column
  public schedule: string

  @Column
  public createdAt: Date;

  @Column
  public updatedAt: Date;

  @Column
  public deletedAt: Date;
}

export { Restaurant };
