import { Op } from 'sequelize';
import { Restaurant } from './restaurant.model';

export class RestaurantRepository {
  public static async dataTable(
    search: string = '',
    order: string = 'asc',
    offset: number = 0,
    limit: number = 10): Promise<{ count: number, rows: Restaurant[] }> {
    const data = await Restaurant.findAndCountAll({
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt'],
      },
      where: {
        [Op.or]: {
          name: { [Op.substring]: `%${search}%` },
          schedule: { [Op.substring]: `%${search}%` },
        },
      },
      order: [
        ['id', order],
      ],
      offset,
      limit,
      logging: console.log,
    })
    return data
  }

  public static async create(data: any): Promise<Restaurant> {
    const create = await Restaurant.create(data)
    return create
  }

  public static async delete(id: number): Promise<void> {
    await Restaurant.destroy({ where: { id } })
  }

  public static async get(id: number): Promise<Restaurant> {
    const data = await Restaurant.findByPk(id)
    return data
  }

  public static async update(data: any, id: number): Promise<Restaurant> {
    await Restaurant.update(data, {
      where: { id },
    })
    const updatedData = await Restaurant.findByPk(id)
    return updatedData
  }

  public static async getCount(): Promise<number> {
    const data = await Restaurant.count()

    return data
  }

  public static async getAll(req: any): Promise<{ count: number; rows: Restaurant[] }> {
    const limit = req.query.limit || 10;
    const offset = req.query.offset || 0;
    const search = req.query.search || '';

    const data = await Restaurant.findAndCountAll({
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt'],
      },
      where: {
        [Op.or]: {
          name: { [Op.substring]: `%${search}%` },
          schedule: { [Op.substring]: `%${search}%` },
        },
      },
      offset,
      limit,
      logging: console.log,
    })
    return {
      rows: data.rows,
      count: data.count,
    };
  }
}