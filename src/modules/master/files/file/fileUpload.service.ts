import aws from 'aws-sdk';
import { AWSError } from 'aws-sdk';
import dotenv from 'dotenv';
import { FileUpload } from './fileUpload.model';

dotenv.config();

export class FileUploadService {
  public static async removeFile(id: number): Promise<void> {
    const oldData = await FileUpload.findByPk(id)
    await FileUpload.destroy({ where: { id } })

    const s3 = new aws.S3({
      accessKeyId: process.env.AWS_ACCESS_KEY,
      secretAccessKey: process.env.AWS_SECRET_KEY,
      region: 'ap-southeast-1',
    })

    s3.deleteObject({
      Key: oldData.key,
      Bucket: process.env.AWS_BUCKET_NAME,
    }, async (err: AWSError) => {
      if (err) { console.log(err) }
    });
  }
}
