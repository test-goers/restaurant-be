import { Column, Model, Table } from 'sequelize-typescript';

@Table({
  tableName: 'Files',
  timestamps: true,
  paranoid: true,
})

class FileUpload extends Model<FileUpload> {

  @Column
  public key: string;

  @Column
  public location: string;

  @Column
  public mimetype: string;

  @Column
  public createdAt: Date;

  @Column
  public updatedAt: Date;

  @Column
  public deletedAt: Date;
}

export { FileUpload };

