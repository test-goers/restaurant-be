import { Request as Requests } from 'express';
import { User } from '../modules/master/user/user.model';
export interface Request extends Requests {
  user: User;
  userRoles: object;
  swaggerDoc: any;
}
