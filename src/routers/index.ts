import { NextFunction, Router } from 'express';
import { apiAuthentication } from '../modules/core/config/passport';
import AuthApiController from '../modules/core/controllers/auth.api.controller';
import { productImageMiddleware } from '../modules/core/middlewares/file.middleware';
import HealthService from '../modules/health.service';
import RestaurantApiController from '../modules/master/restaurant/restaurant.api.controller';
import UserController from '../modules/master/user/user.controller';

function group(cb: any) {
  const route = Router()
  cb(route)
  return route
}

const appRouter = Router();

appRouter.use('/auth', group((router: Router, next: NextFunction) => {
  router.post('/login', AuthApiController.login);
  router.post('/reset-password', AuthApiController.resetPassword);
  router.get('/me', apiAuthentication, AuthApiController.me);
}))

appRouter.use('/health', group((router: Router, next: NextFunction) => {
  router.get('/', HealthService.index);
}))

appRouter.use('/user', apiAuthentication, group((router: Router, next: NextFunction) => {
  router.get('/', UserController.index)
  router.get('/profile', UserController.profile)
  router.patch('/profile', productImageMiddleware({ fields: [{ name: 'photo', maxCount: 1 }] }), UserController.updateProfile)
}))

appRouter.use('/restaurant', group((router: Router, next: NextFunction) => {
  router.get('/', RestaurantApiController.getAll)
  router.get('/:id', RestaurantApiController.getDetail)
}))


export default appRouter