import { Router } from 'express';
import { apiMiddleware } from './modules/core/middlewares/api.middleware';
import router from './routers';

const apiControllerList = Router();
apiControllerList.use(router)
const apiController = Router();
apiController.use('/api', apiMiddleware, apiControllerList);
// apiController.use(
//   '/api-docs',
//   isAuthenticated,
//   (req: Request, res: Response, next: any) => {
//     swaggerDocument.servers[0].url = `http://${req.get('host')}/api`;
//     req.swaggerDoc = swaggerDocument;
//     next();
//   },
//   swaggerUi.serve,
//   swaggerUi.setup(),
// );

export { apiController };

