import express from 'express';
import 'reflect-metadata';
import { apiController } from './apiControllerLoader';
import { authController } from './modules/core/controllers/auth.controller';
import dashboardController from './modules/core/controllers/dashboard.controller';


const controllerLoader = (app: express.Application) => {
  app.use(apiController);
  app.use(authController);
  app.use(dashboardController);
}

export = controllerLoader;

