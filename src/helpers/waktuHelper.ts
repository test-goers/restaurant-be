import moment from 'moment'

const reformatWaktu = (waktu: string) => moment(waktu, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss')

export { reformatWaktu }