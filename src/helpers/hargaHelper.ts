import { ceil } from 'lodash';

const pembulatanHarga = (harga: number) => (harga >= 10000) ? ceil(harga / 10000) * 10000 : ceil(harga / 1000) * 1000
const pembulatanHargaByKodeProduk = (kodeProduk: string) => parseInt(kodeProduk.match(/\d+/)[0] + '000', 0)
const pembulatanHargaAdminPayment = (costType: string, cost: any, harga: any) => (costType === 'fixed') ? cost : Math.ceil((cost / 100) * harga)
const generateHargaAwal = (harga: any = 0, admin: any = 0, margin: any = 0, point: any = 0) => harga + admin + margin - point

export { pembulatanHarga, pembulatanHargaByKodeProduk, pembulatanHargaAdminPayment, generateHargaAwal }