const categoriesValue: Array<{}> = [
    { 'value': 'SEMUA', 'text': 'Semua Layanan' },
    { 'value': 'CEKTOKO', 'text': 'Cektoko' },
    { 'value': 'CEKPAY', 'text': 'Cekpay' },
]

const typeDiscountValue: Array<{}> = [
    { 'value': 'AMOUNT', 'text': 'Amount' },
    { 'value': 'PERCENT', 'text': 'Percent' },
]
  
const categoriesFilter: Array<{}> = [
  { 'value': 'ALL', 'text': 'All' },
  { 'value': 'SEMUA', 'text': 'Semua Layanan' },
  { 'value': 'CEKTOKO', 'text': 'Cektoko' },
  { 'value': 'CEKPAY', 'text': 'Cekpay' },
]


export { categoriesValue, typeDiscountValue, categoriesFilter }