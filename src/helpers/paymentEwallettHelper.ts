import axios from 'axios'
import { authPayment } from './paymentHelper'

export const generatePaymentEwallet = async (callback: string, nomorTransaksi: string, harga: number, channelCode: string) => {
  const paymentData: object = {
    'reference_id': nomorTransaksi,
    'currency': 'IDR',
    'amount': harga,
    'checkout_method': 'ONE_TIME_PAYMENT',
    'channel_code': channelCode,
    'channel_properties': {
      'success_redirect_url': `${callback}?invoiceNumber=${nomorTransaksi}`,
    },
    'metadata': {
      'branch_area': 'PLUIT',
      'branch_city': 'JAKARTA',
    },
  }

  const paymentProcess = await axios.post(process.env.EWALLET_PAYMENT_ENDPOINT, paymentData, authPayment)
    .then((item) => item.data)
    .catch((err) => err.message)

  return paymentProcess
}