import axios from 'axios';
import { authPayment } from './paymentHelper';

export const generatePaymentBank = async (
  externalId: string = '',
  amount: number = 0,
  description: string = '',
  merchant: string = '',
) => {
  try {
    const payload = {
      'external_id': externalId,
      amount,
      payer_email: 'customer@cektoko.com',
      description,
    }

    const data = await axios.post(process.env.XENDIT_BANK_INVOICE_ENDPOINT, payload, authPayment)
    .then((response) => response.data)
    .catch((err) => err.message)

    let va: any = {}

    if (data.available_banks.length > 0) {
      [va] = data.available_banks.filter((v: any) => v.bank_code === merchant)
      va.id = data.id
      va.status = data.status
    }

    console.log(va);
    

    return { expiry_date: data.expiry_date, ...va };
  } catch (error) {
    return error;
  }
};
