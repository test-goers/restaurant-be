import axios from 'axios'
import { generatePaymentBank } from './paymentBankHelper'
import { generatePaymentEwallet } from './paymentEwallettHelper'

const authPayment = {
  auth: {
    username: process.env.PAYMENT_TOKEN,
    password: '',
  },
}

const generatePayment = async (callback: string, nomorTransaksi: string, harga: number, channelCode: string) => {
  const eWallet = await generatePaymentEwallet(callback, nomorTransaksi, harga, channelCode)
  return {
    id: eWallet.id,
    actions: eWallet.actions,
    status: eWallet.status,
  }
}

const generateInvoice = async (externalId: string, nominal: number, description: string, merchant: string) => {
  const bank = await generatePaymentBank(externalId, nominal, description, merchant)
  return bank
}

const checkPaymentStatus = (paymentId: string, metodePembayaran: string) => {
  if (metodePembayaran === 'EWALLET') {
    const paymentCheck = axios.get(`${process.env.EWALLET_PAYMENT_ENDPOINT}/${paymentId}`, authPayment)
      .then((item) => item.data.status)
      .catch((err) => err.message)
  
    return paymentCheck
  } else {
    const paymentCheck = axios.get(`${process.env.XENDIT_BANK_INVOICE_ENDPOINT}/${paymentId}`, authPayment)
      .then((item) => item.data.status)
      .catch((err) => err.message)
  
    return paymentCheck
  }
}

const closeInvoice = (paymentId: string) => {
  const paymentCheck = axios.post(`https://api.xendit.co/invoices/${paymentId}/expire!`, {}, authPayment)
    .then((item) => item.data)
    .catch((err) => `${err.message}`)

  return paymentCheck
}

const generateCallback = (req: any) => `http://${req.headers.host}${req.baseUrl}\/proses`

export { authPayment, generatePayment, generateInvoice, checkPaymentStatus, closeInvoice, generateCallback }
