const kodeProdukPascaBayarSeluler = (provider: string) => {

  const kodeProduk: { [key: string]: string } = {
    'TELKOMSEL': 'HPTSELH',
    'AXIS / XL': 'HPXL',
    'KARTU3': 'HPTHREE',
    'ISAT': 'HPMTRIX',
    'SMART': 'HPSMART',
    'FREN': 'HPFREN',
  }

  return (kodeProduk[provider]) ? kodeProduk[provider] : false
}

export { kodeProdukPascaBayarSeluler }