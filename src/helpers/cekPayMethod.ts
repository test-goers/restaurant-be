const cekPayMethod = {
  saldo: {
    balance: 'rajabiller.balance',
  },
  pulsa: {
    checkGroupPrices: 'rajabiller.cekharga_gp',
    transactionBuy: 'rajabiller.pulsa',
  },
  pulsaPostPaid: {
    inqueryTransaction: 'rajabiller.inq',
    payTransaction: 'rajabiller.pay',
  },
  telkom: {
    inqueryTransaction: 'rajabiller.inq',
    payTransaction: 'rajabiller.pay',
  },
  pln: {
    inquiryTransaction: 'rajabiller.inq',
    payTransaction: 'rajabiller.paydetail',
  },
}

const pulsaResponseParse = (provider: string, response: string) => response.split('.')[0] === `Operator pulsa ${provider} tidak ditemukan` ? 'data tidak ditemukan' : response

const cekPayProduk = {
  telkom: {
    telepon: 'TELEPON',
    speedy: 'SPEEDY',
  },
}

const kodeProdukSelulerPascabayar = (provider: string) => {
  switch (provider) {
    case 'TELKOMSEL': {
      return 'HPTSELH'
    }
    case 'AXIS / XL': {
      return 'HPXL'
    }
    case 'KARTU3': {
      return 'HPTHREE'
    }
    case 'ISAT': {
      return 'HPMTRIX'
    }
    case 'FREN': {
      return 'HPFREN'
    }
    case 'SMART': {
      return 'HPSMART'
    }
    default: {
      return false
    }
  }
}

const cekInputTelkom = (pelangganId: string) => (pelangganId.charAt(0) === '0') ? 'TELEPON' : 'SPEEDY'

export { cekPayMethod, pulsaResponseParse, cekPayProduk, cekInputTelkom, kodeProdukSelulerPascabayar }

