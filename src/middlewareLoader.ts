import compression from 'compression';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import flash from 'express-flash';
import session from 'express-session';
import expressStatusMonitor from 'express-status-monitor';
import lusca from 'lusca';
import moment from 'moment';
import 'moment/locale/id';
import morgan from 'morgan';
import numeral from 'numeral';
import passport from 'passport';
import path from 'path';
import 'reflect-metadata';
import responseTime from 'response-time';
import { logger } from './helpers/logger';
import { breadcrumbMiddleware } from './modules/core/middlewares/breadcrumb.middleware';
import { pathMiddleware } from './modules/core/middlewares/path.middleware';
moment().locale('id');

const middlewareLoader = (app: express.Application) => {
  const MySQLStore = require('connect-mysql')(session);
  const options = {
    config: {
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
    },
  };
  app.use(expressStatusMonitor());
  app.use(compression());
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(cors())
  app.options('*', cors())

  const tx2 = require('tx2')
  const meter = tx2.meter({
    name: 'req/sec',
    samples: 1,
    timeframe: 60,
  })

  app.use(responseTime((req, res, time) => {
      meter.mark()
  }))

  app.use(
    morgan('tiny', {
      stream: {
        write: message => {
          logger.info(message);
        },
      },
    }),
  );
  app.use(cookieParser());
  app.use(
    session({
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 3,
      },
      secret: process.env.SESSION_SECRET,
      resave: true,
      saveUninitialized: true,
      store: new MySQLStore(options),
    }),
  );
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(flash());
  app.use((req, res, next) => {
    // if (req.is('multipart/form-data')) {
    //   next();
    // }
    if (req.path.split('/')[1] === 'api' || req.is('multipart/form-data')) {
      next();
    } else {
      lusca.csrf()(req, res, next);
    }
  });
  app.use(lusca.xframe('SAMEORIGIN'));
  app.use(lusca.xssProtection(true));
  app.disable('x-powered-by');
  app.use(async (req, res, next) => {
    res.locals.user = req.user;
    moment.locale('id');
    res.locals.moment = moment;
    res.locals.numeral = numeral();
    next();
  });
  // Authentication
  app.post(
    '/auth/login',
    passport.authenticate('local', {
      failureFlash: true,
      failureRedirect: '/auth/login',
      successRedirect: '/',
    }),
  );
  app.use('/file', express.static(path.join(__dirname, '../file')));
  app.use(pathMiddleware);
  app.use(breadcrumbMiddleware);
};

export = middlewareLoader;
