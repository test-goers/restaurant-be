import 'reflect-metadata';
import { Sequelize } from 'sequelize-typescript';
import { Restaurant } from './modules/master/restaurant/restaurant.model';
import { User } from './modules/master/user/user.model';

const modelLoader = (sequelize: Sequelize) => {
  sequelize.addModels([
    User,
    Restaurant,
  ])
}


export { modelLoader };

